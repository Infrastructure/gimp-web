# Mirror administration

## Architecture

Mirrors are synced from `download.gimp.org` (hosted on
`master.gnome.org`). Current mirroring infrastructure is using
[Mirrorbits](https://github.com/etix/mirrorbits) which handles
geoIP-based redirection, health check (and removing down mirrors from
rotation automatically), security checks and more for us.

Note that to we still have some redirection from `/pub/` and
`/mirror/pub/` to `/` in order for old URLs to stay alive (cf. the old
basic round-robin mirroring (based on [Apache rewrite
rules](https://gitlab.gnome.org/Infrastructure/puppet/-/blob/0df77787596314f41de55b270e016cfd99ce2a53/modules/httpd/files/sites.d/download.gimp.org.conf#L65-82)).
Now the download root is just the old `/pub/`, yet it is important to
keep the redirections forever because the web is full of legacy GIMP
download URLs.

The list of mirrors in rotation is found at: `/etc/httpd/download.gimp.org.map`

Each file from this list should have associated rsync credentials
(though some may not have any yet, if they were created from older
process time; when this is the case, the mirror administrators should be
contacted for proper re-configuration of their servers):

* rsync credentials are set in: `/etc/rsyncd/secrets`
* the login part of the credential must also be copied in: `/etc/rsyncd.conf`

The login must be in both files, otherwise syncing will not work.

## New mirror procedure

If someone requests to be an official mirror, the procedure they must
follow is:

* Create a mirror request on the [Infrastructure tracker](https://gitlab.gnome.org/Infrastructure/Infrastructure/issues/new?issuable_template=new-mirror)
* Write in title and introduction that you want to be a mirror for GIMP in particular
* Add `/cc @Jehan` in the request description or in a comment
* Fill the relevant fields, and add in particular: the final `https`
  mirror URL, as well as either `rsync` or `ftp` public access to your
  mirror.
* If you want a specific name and URL to be shown in the mirror sponsor
  list, please write it explicitly in the report (otherwise we will
  choose whatever organization name and URL seems the most relevant from
  what you wrote down)
* Once we are done verifying your organization, we will send you rsync
  credentials for syncing your mirror
* There is nothing to do in particular to appear on the Sponsors page
  which will be updated regularly through scripts. Yet it may take a few
  days or even weeks at times. So don’t worry if your organization name
  does not appear immediately!

Note: a web URL will soon be available giving this procedure.

Then a member of GNOME's Infrastructure team will take care of this
request. Note that some members of GIMP may also have some rights, for
instance @Jehan can take care of part of the actions:

1. Verify the organization behind the request matches our expectations
   in terms of being trustworthy. Some of the expectations are:
   * they mirror other FOSS projects already (especially as official
     mirrors)
   * they are a well known organization which would gain from being a good
     actor of the Free Software movement (unlikely to do anything shaddy
     which would give a very bad reputation issue)
   * if the mirror and claimed organization are different domain names,
     verify they are the really linked (through `whois` or other means)
2. Verify that the `https` URL has no major issue (our redirect happens
   in https-only so a working https URL is mandatory)
3. Test the given `rsync` server with `rsync -avz rsync://example.org`
   or the FTP server with a FTP client.
4. If 1. to 3. are fine, generate an user and a password (e.g. with
   `pwgen 10 --symbols`) and add them to `/etc/rsyncd/secrets` in
   `master.gnome.org`. This is done by directly editing the file.
   Do not forget to leave a comment as well for reference (possibly a
   Gitlab report link).
   [*requires to be in proper admin group*]
5. Once this is done, edit
   [/etc/rsyncd.conf](https://gitlab.gnome.org/Infrastructure/ansible/-/blob/master/roles/ftpadmin/files/rsyncd.conf)
   by creating a Merge Request to
   [Infrastructure/ansible](https://gitlab.gnome.org/Infrastructure/ansible/)
   (not editing it directly on the server, unlike 3.).
6. Then once the MR is merged, ask the mirror admin for their public GPG
   key.
7. When they return their key, send the rsync credentials, encrypted
   with this key, then signed by yours (which should be on a public key
   server for non-tampering verification) by email, and ask them to
   notify when the mirror is properly set-up and synced. Wait for their
   answer.
   To encrypt the credentials with their key:

   - Import the received key: `gpg --import mirror.gpg` and check the
     associated email with `gpg --list-keys` (e.g. `mirror@example.com`
     in our example below).
   - store the credentials in format "login:password" in a file:
     ```sh
     echo "login:password" > mirror-secrets.txt
     ```
   - Encrypt the file:
     ```sh
     gpg --output mirror-secrets.txt.gpg --encrypt --recipient mirror@example.com mirror-secrets.txt
     ```
   - Sign the encrypted file with your own key:
     ```sh
     gpg --output mirror-secrets.txt.gpg.sig --detach-sign mirror-secrets.txt.gpg
     ```
   - Send both files `mirror-secrets.txt.gpg` and
     `mirror-secrets.txt.gpg.sig`, telling them where they can import
     your key (ideally from a public key server, so that they can more
     easily verify the source). Being able to check the signature will
     ensure that the encrypted file was not tampered with (i.e. no
     MitM attach).

   Note: other methods may be done to exchange the credentials, for
   instance if the mirror administrator has a safe cloud-type server
   (provided by their organization) in https. What should be avoided is
   exchanging credentials in plain text over the internet.

8. Once they notify you that the sync is complete, land the mirror this
   way:

    ```sh
    # From your computer.
    ssh <user>@master.gimp.org
    # From remote master.gimp.org
    sudo -u podman -i
    # Inside podman
    podman ps
    podman exec -it mirrorbits /bin/bash
    ```

   * Now you are communicating with mirrorbits (and have a new prompt).
     The command `mb help` gives you a list of commands. For instance
     `mb list` returns the mirrors and their status.
   * To add a mirror, look at `mb add -help`. For instance with ftp
     scanning:
    ```sh
    mb add -http https://example.org/mirrors/gimp/ -ftp ftp://example.org/mirrors/gimp/ example.org
    mb enable cc.uoc.gr
    ```
     Or with rsync:
    ```sh
    mb add -http https://example.org/mirrors/gimp/ -rsync rsync://example.org/gimp example.org
    mb enable lysator.liu.se
    ```
9. If all went well (i.e. that `mb list` should eventually tell you that
   the mirror is "up"), close the report.
10. on `gimp-web` repository, run:

   ```sh
   tools/downloads/update-mirrors.py
   ```
   [*requires to have access to the OpenShift console*]
11. The script will update `tools/downloads/downloads.http.txt`
   automatically and should tell you that
   `content/downloads/mirrors.json` has to be updated too. Do so by
   writing the public name of the mirror organization, link, location
   and other data. In "more", also add the report link for reference.
   If other data need to be updated, do so as well (for instance if
   other mirrors changed).
12. Run again:

   ```sh
   tools/downloads/update-mirrors.py
   ```
   This time, it should tell you everything is fine.

13. Verify all mirrors (especially new ones) are well synced at least
   for the last release:

   ```sh
   tools/downloads/gimp-check-mirrors.py
   ```
14. Commit all the changes and push them to `gimp-web`'s `testing`
   branch.
15. After a short time, make sure that testing's [sponsor page](https://testing.gimp.org/donating/sponsors.html)
   is properly updated.

Of course, the public website will be updated when you merge `testing`
into `master` branch which does not have to happen immediately.

## Mirror maintenance
### Debugging mirror issues

Sometimes the `mb logs` command might not give very detailed
information, but OpenShift contains much more detailed logs:

* Go to https://console-openshift-console.apps.openshift4.gnome.org/
* Go to "Administrator" view.
* Click Workloads → Pods
* Filter for mirrorbits
* Select one of the running pods (not the `redis-*` or the `*-deploy`
  pods).
* Click the `Logs` tab.

### Testing FTP, curl…

Note that pods are very bare and don't contain most basic software you would
expect on a common Linux box. For instance, the other day, I needed to test FTP
to one of the mirror, which had IP-restriction, so I needed to FTP from the same
IPs as mirrorbits. Unfortunately there are no FTP clients in mirrorbits pods.

Here are commands allowing to create dedicated pods with `lftp` software in it,
in the same IP as the rest:

```sh
oc run $USER-lftp-test-2 --image=nixery.dev/shell/lftp --restart=Never -- sleep 1d
oc rsh --shell='/bin/bash' -t pod/$USER-lftp-test-2
```

Note that this works with a few other common software. For instance, I could
test `curl` in a pod, with the same command, replacing `lftp` with `curl`. I
don't know the technical details behind this, except that apparently nixery.dev
is what is used for "adhoc docker images with some packages", and it uses "nix"
packaging.

### Conf files must match

Make sure that `auth users` in `[gimp]` section of `/etc/rsyncd.conf`
are in `/etc/rsyncd/secrets`. If they are not, then they would not be
able to sync anyway because they have no valid credentials, so you can
remove them.

Oppositely, users in the `secrets` file which are not in
`/etc/rsyncd.conf` would not be able to fetch data. So if they never
complained, it means they don't sync with us anymore.

### Security checks

From time to time, you could run:

```
tools/downloads/gimp-check-mirrors.py
```

By default, it would only check the last download tarball/installer/DMG.
If you want to check a specific file, add it (or them) to the command
line. If you set `--verify-checksum` option, then it will also check
data integrity.

This check is also run automatically and regularly by Gitlab CI.

## Future

Some work is being done to move to a MirrorBits infrastructure which
will bring even better download capability as it is able to choose the
closer mirror to a downloader.

It will also bring minimal download statistics.
