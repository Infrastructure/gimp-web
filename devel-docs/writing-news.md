# Writing news

## Path

The build system with catch any markdown file (`*.md`) under
`contents/news/`. We usually order the articles per-year folders, and
each article in their own folder.

If you add images for the article only (for instance screnshot specific
to a release), add them in the article's folder. For more generic
images, add them under `content/images/`.

## Format

### Markdown and HTML

The base format is markdown (per Pelican formatting rules).

HTML tags are also recognized, which allows to mix some HTML when some
rendering a bit more complicated is needed.

Internal link IDs are also generated from each title in lowercase, with
spaces replaced by an hyphen (`-`).

### Interesting tags

For complicated pages, `[ TOC ]` (remove the space, I am just preventing
Gitlab from rendering a table of contents too) tag is recognized and
will add a table of contents.

## Header

The header is important, in particular:

* `Title:` and `Summary:` are self-explanatory.
* `Author:` can be set to `Wilber` when we want to make a "team" post.
  If we want to have specific names and if there are several authors, it
  is better to use the `Authors:` tag with comma-separated names.
* `Slug:` is what the last part of the URI will look like.
* `Category:` should be set to `News` for articles.
* `Date:` is in ISO 8601 format. If ever you want to post several
  articles the same day, you must be careful to add the time too,
  otherwise the publication order will be random.
* `Status:` should be set to `draft` until finished, in which case the
  article will only be listed in https://www.gimp.org/drafts/ (or
  https://testing.gimp.org/drafts/ for the testing website, or in your
  local `/drafts/`), preventing publication by mistake. Note that if the
  `Date:` tag is in the future, the article will be also considered a
  draft even without a `Status:` set (yet it's better to have an
  explicit status).
* `Image:` must be set to an image used as metadata for the image
  representing the article. It will not be showed in the render, so you
  should also set your image with normal tags within the article
  contents. Note that for this tag, an absolute URL, scheme included, is
  required. As a consequence the algorithm is as follow: if you start
  your image with `/`, then the build system will consider it to be an
  absolute URI starting at the website root; otherwise the path is
  assumed to be relative to the article folder.

## OpenGraph

OpenGraph are metadata which allow various websites to generate previews
of an article. This heavily relies on the various header tags so you
should fill them well, in particular the `Title`, `Summary`, `Date` and
`Image`.

Note that without an image, the big Wilber will be used as default
article image which often does not render so nice. We recommend to
always set an image.
