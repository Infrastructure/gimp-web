from pelican import signals, contents

import collections
import datetime
import docutils.nodes
import html
import json
import os.path
import urllib.parse

# This is the first version for which we consider a book to be "new". When we
# will want to edit this page, just change this variable (e.g. to "3.0.0").
DELIMITER_VERSION="2.10.0"

class UnexpectedException(Exception): pass

def html_output(books):
    # Ideally each book should be tagged as being about a specific version of
    # GIMP. Unfortunately they are not, and I don't feel like searching info on
    # hundreds of books. Instead, we use the release date of the
    # DELIMITER_VERSION as a date delimiter instead.
    with open('content/gimp_versions.json') as data:
      GIMP = json.load(data, object_pairs_hook=collections.OrderedDict)
      for release_info in GIMP['STABLE']:
        if release_info['version'] == DELIMITER_VERSION:
          delimiter_date = release_info['date']
          delimiter_date = datetime.datetime.strptime(delimiter_date, '%Y-%m-%d')
          break

    # Languages are ordered by ISO-639-1 (or alike, for instance Chinese
    # variants use W3C codes), except for English which is always first (for
    # now, as the website is English only).
    codes = ['en'] + [c for c in sorted(books) if c != 'en']
    n_new_books = 0
    n_old_books = 0

    # Index
    new_output = u"<ul class='books-langs-index'>\n"
    old_output = u"<ul class='books-langs-index'>\n"
    has_old_books = []
    has_new_books = []
    for code in codes:
      for book in books[code]['data']:
        date = book['date']
        try:
          d = datetime.datetime.strptime(date, '%Y')
        except ValueError:
          try:
            d = datetime.datetime.strptime(date, '%Y-%m')
          except ValueError:
            d = datetime.datetime.strptime(date, '%Y-%m-%d')
        if d >= delimiter_date and code not in has_new_books:
          new_output += u"<li><a href='#{}'>Books in {}</a></li>".format(docutils.nodes.make_id(code), books[code]['language'])
          has_new_books += [code]
        if d < delimiter_date and code not in has_old_books:
          old_output += u"<li><a href='#{}'>Books in {}</a></li>".format(docutils.nodes.make_id(code), books[code]['language'])
          has_old_books += [code]
        if code in has_old_books and code in has_new_books:
          break
    new_output += u"</ul>"
    old_output += u"</ul>"

    # Sections per langs
    for code in codes:
      if code in has_new_books:
        new_output += u'<hr/>'
      if code in has_old_books:
        old_output += u'<hr/>'
      langid = docutils.nodes.make_id(code)
      lang   = books[code]['language']
      if code in has_new_books:
        new_output += u"<h2 id='{langid}'>{lang}<a class='headerlink' href='#{langid}' title='Permanent link'>&para;</a></h2>".format(langid=langid, lang=lang)
      if code in has_old_books:
        old_output += u"<h2 id='{langid}'>{lang}<a class='headerlink' href='#{langid}' title='Permanent link'>&para;</a></h2>".format(langid=langid, lang=lang)
      for book in books[code]['data']:
        bookid  = docutils.nodes.make_id(book['title'])
        image   = book['image']
        authors = book['authors'] if 'authors' in book else ['<em>n/a</em>']
        isbn13  = book['ISBN13'] if 'ISBN13' in book else None
        isbn10  = book['ISBN10'] if 'ISBN10' in book else None
        formats = book['format'] if 'format' in book else None
        pub     = book['publisher'] if 'publisher' in book else '<em>Self-published</em>'
        date    = book['date']
        try:
          d = datetime.datetime.strptime(date, '%Y')
          date_format = '%Y'
        except ValueError:
          try:
            d = datetime.datetime.strptime(date, '%Y-%m')
            date_format = '%B, %Y'
          except ValueError:
            d = datetime.datetime.strptime(date, '%Y-%m-%d')
            # strftime() format has '%c' for Locale’s appropriate date and time
            # representation with proper words, or '%x' without the time, but
            # not using words.
            # 'dateutil' module seems nice to parse, but doesn't seem to have
            # functions for locale-appropriate date printing.
            # There is also a 'babel' module which looks interesting but it
            # doesn't give me the format I'd want either. So for now, I
            # hard-code my format this way, though it would not work fine with
            # localization, once we decide to have a multi-lang website.
            date_format = '%B %-d, %Y'
        website = book['website'] if 'website' in book else None
        archive = book['archive'] if 'archive' in book else None
        desc    = book['description'] if 'description' in book else '<em>n/a</em>'
        book_html  = u""
        book_html += u"<h3 id='{bookid}'>{book}<a class='headerlink' href='#{bookid}' title='Permanent link'>&para;</a></h3>".format(book=book['title'], bookid=bookid)
        book_html += u"<p><img alt='{book}' src='{url}'></p>".format(book=html.escape(book['title'], quote=True), url=image)
        book_html += u"<dl>"
        if len(authors) > 1:
          book_html += u"<dt>{}</dt>".format("Authors")
        else:
          book_html += u"<dt>{}</dt>".format("Author")
        book_html += u"<dd>{}</dd>".format(", ".join(authors))
        if isbn13 is not None:
          book_html += u"<dt>{}</dt>".format("ISBN13")
          book_html += u"<dd>{}</dd>".format(isbn13)
        if isbn10 is not None:
          book_html += u"<dt>{}</dt>".format("ISBN10")
          book_html += u"<dd>{}</dd>".format(isbn10)
        if isbn13 is None and isbn10 is None:
          book_html += u"<dt>{}</dt>".format("ISBN")
          book_html += u"<dd><em>n/a</em></dd>"
        if formats is None or len(formats) > 1:
          book_html += u"<dt>{}</dt>".format("Formats")
        else:
          book_html += u"<dt>{}</dt>".format("Format")
        if formats is None:
          book_html += u"<dd><em>n/a</em></dd>"
        else:
          book_html += u"<dd>{}</dd>".format(", ".join(formats))
        book_html += u"<dt>{}</dt>".format("Publisher/Date")
        book_html += u"<dd>{} / {}</dd>".format(pub, d.strftime(date_format))
        if website is not None:
          parse  = urllib.parse.urlparse(website)
          url    = archive if archive is not None else website
          book_html += u"<dt>{}</dt>".format("Website")
          if archive is None:
            book_html += u"<dd><a href='{url}'>{domain}</a></dd>".format(url=url, domain=parse.netloc)
          else:
            book_html += u"<dd><a title='Historical URL captured by archive.org: {old_url}' href='{url}'>(archived) {domain}</a></dd>".format(url=url, domain=parse.netloc, old_url=website)
        book_html += u"<dt>{}</dt>".format("Description")
        book_html += u"<dd>{}</dd>".format(desc)
        book_html += u"</dl>"
        if d >= delimiter_date:
          new_output += book_html
          n_new_books += 1
        else:
          old_output += book_html
          n_old_books += 1

    return new_output, n_new_books, old_output, n_old_books, delimiter_date

def do_books(path, context):
    # normalize OS path separators to fwd slash
    path = path.replace(os.path.sep, '/')

    if '/books/' in path:
        # load json file from content
        try:
            with open('content/books/index_data.json') as data_file:
                books = json.load(data_file)

                try:
                    with open(path, 'rb') as f:
                        s = f.read()
                        #print(s.decode('utf-8'))
                        new_html, n_new_books, old_html, n_old_books, delimiter_date = html_output(books)
                        s = s.decode('utf-8').replace(u"<!-- NEW_BOOKS -->", new_html)
                        s = s.replace(u"<!-- OLD_BOOKS -->", old_html)
                        # Embed the day of generating the book list in ISO format.
                        day = datetime.date.today().isoformat()
                        s = s.replace(u"<!-- DATE -->", day)
                        # More metadata info to replace.
                        s = s.replace(u"<!-- DELIMITER_VERSION -->", DELIMITER_VERSION)
                        s = s.replace(u"<!-- DELIMITER_DATE -->", delimiter_date.strftime('%B %-d, %Y'))
                        s = s.replace(u"<!-- N_NEW_BOOKS -->", str(n_new_books))
                        s = s.replace(u"<!-- N_OLD_BOOKS -->", str(n_old_books))

                    with open( path, 'wb') as f:
                        f.write(s.encode('utf-8'))
                except Exception as error:
                    print('ERROR: trouble with reading/writing "{}": {}'.format(path, error))

        except IOError:
            print("Cannot open content/books/index_data.json!")
            try:
                with open(path, 'r') as f:
                    s = f.read()
                    #print s.decode('utf-8')
                    s = s.decode('utf-8').replace(u"<!-- MIRRORS -->", u"<p><small>Cannot open content/books/index_data.json</small></p>")
                with open( path, 'w') as f:
                    f.write( s.encode('utf-8') )
            except:
                print("Trouble with reading/writing path.")


def register():
    # Trying something to read the html after written...
    signals.content_written.connect(do_books)
