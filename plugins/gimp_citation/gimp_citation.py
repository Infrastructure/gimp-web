from pelican import signals, contents

import collections
import datetime
import json
import os.path

class UnexpectedException(Exception): pass

def html_output():
  year = datetime.datetime.now().year

  with open('content/gimp_versions.json') as data:
    GIMP = json.load(data, object_pairs_hook=collections.OrderedDict)
    for info in GIMP['STABLE']:
      version = info['version']
      break

  code = f'''
<pre>
  <code>
@software{{GIMP,
  author = {{{{The GIMP Development Team}}}},
  title = {{GNU Image Manipulation Program (GIMP), Version {version}. Community, Free Software (license GPLv3)}},
  year = {{{year}}},
  url = {{https://gimp.org/}},
  note = {{Version {version}, Free Software}}
}}
  </code>
</pre>
'''
  return code

def do_citation(path, context):
  # normalize OS path separators to fwd slash
  path = path.replace(os.path.sep, '/')

  if '/about/linking' in path:
    try:
      with open(path, 'rb') as f:
        s = f.read()
        code = html_output()
        s = s.decode('utf-8').replace(u"<!-- RESEARCH_CITATION -->", code)
        s = s.replace(u"<!-- RESEARCH_CITATION -->", code)
      with open(path, 'wb') as f:
        f.write(s.encode('utf-8'))
    except Exception as error:
        print('ERROR: trouble with reading/writing "{}": {}'.format(path, error))

def register():
  # Trying something to read the html after written...
  signals.content_written.connect(do_citation)
