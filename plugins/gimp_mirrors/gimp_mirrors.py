from pelican import signals, contents
import datetime
import os.path
from os.path import basename
import socket
import json
from urllib.parse import urlparse


'''
This is a skeleton for testing the parsing of a MIRRORS file
to show a list on /downloads/
'''

'''
It's been created by me, Pat David, to do what I need for wgo.
This is based heavily on stuff the LoTR was doing already...
Don't try to make any sense of it.  I don't know what I'm doing.
I am not kidding.
'''

'''
Update 2016-07-20
    Had to change the signal to
    `signals.content_written.connect(do_mirrors)`
    so we actualy catch the rendered .html files
    after they've been through the template.
    This way we can look for <!-- MIRRORS --> to
    replace it with the correct parsed list.
'''

class UnexpectedException(Exception): pass

def html_output(data):
    html = u"<dl class='download-mirrors'>\n"

    for country, values in sorted(data.items()):
        html += u"<dt>{}</dt>".format(country)

        for name, info in sorted(values.items()):
            about     = info['about']
            locations = info['location']
            urls      = info['url']
            n_mirrors = info['n-mirrors']
            if isinstance(about, str):
                # In Python3 str are unicode strings.
                tmp = about
            else:
                tmp = about.decode('utf-8')
            if locations is not None:
                if n_mirrors > 1:
                  loc_dict = {}
                  for i, location in enumerate(locations):
                    if location in loc_dict:
                      loc_dict[location] += [ urls[i] ]
                    else:
                      loc_dict[location] = [ urls[i] ]
                  loclist = []
                  for loc in loc_dict:
                    if len(loc_dict[loc]) == 1:
                      loclist += [ loc + u" <a href='{}'>⬏</a>".format(loc_dict[loc][0]) ]
                    else:
                      url_links = []
                      for link in loc_dict[loc]:
                        url_links += [ u"<a href='{}'>⬏</a>".format(link) ]
                      loclist += [ loc + ' '.join(url_links) ]
                  html += u"<dd><a href='{}'>{}</a> ({})</dd>\n".format(tmp, name, '; '.join(loclist))
                else:
                  html += u"<dd><a href='{}'>{}</a> ({} <a href='{}'>⬏</a>)</dd>\n".format(tmp, name, locations[0], urls[0])
            else:
                if n_mirrors > 1:
                  html += u"<dd><a href='{}'>{}</a> (<em>{} mirrors</em>)</dd>\n".format(tmp, name, n_mirrors)
                else:
                  html += u"<dd><a href='{}'>{}</a></dd>\n".format(tmp, name)

    html += u"</dl>"

    return html


def do_mirrors(path, context):


    # normalize OS path separators to fwd slash
    path = path.replace( os.path.sep, '/' )

    # if we are on a downloads page
    #if '/downloads/index.html' in path:
    if '/downloads/' in path or '/sponsors' in path:

        # create mirrors dict
        mirrors = {}

        # load mirrors.json file from content
        try:
            with open('./content/downloads/mirrors.json') as data_file:
                data = json.load(data_file)

                for record in data:
                    domain = urlparse(data[record]['mirrors'][0]).netloc
                    n_mirrors = len(data[record]['mirrors'])
                    countries = data[record]['country']

                    if 'location' in data[record]:
                        location = data[record]['location']
                        if n_mirrors != len(location):
                          print('WARNING: mismatch between the number of mirrors and locations for {}.'.format(domain))
                          print('         Skipping this mirror sponsor.')
                          continue
                        if isinstance(countries, list):
                          # "mirror" can be either a single string or a list. In
                          # the later case, it must be the size of mirrors and
                          # locations.
                          if n_mirrors != len(countries):
                            print('WARNING: mismatch between the number of mirrors and countries for {}.'.format(domain))
                            print('         Skipping this mirror sponsor.')
                            continue
                        elif not isinstance(countries, str):
                          print('WARNING: "country" field must be a string of a list for {}.'.format(domain))
                          print('         Skipping this mirror sponsor.')
                          continue
                    else:
                        location = None

                    mirror_urls = data[record]['mirrors']

                    if isinstance(countries, str):
                      countries = [ countries ] * n_mirrors

                    for country in set(countries):
                      locations = []
                      urls      = []
                      for i, c in enumerate(countries):
                        if c == country:
                          locations += [ location[i] ]
                          urls      += [ mirror_urls[i] ]
                      if country in mirrors:
                        mirrors[country][record] = {
                                                     'about': data[record]['about'],
                                                     'url': urls,
                                                     'location': locations,
                                                     'n-mirrors': len(locations)
                                                   }
                      else:
                        mirrors[country] = {
                                             record: {
                                                       'about': data[record]['about'],
                                                       'url': urls,
                                                       'location': locations,
                                                       'n-mirrors': len(locations)
                                                     }
                                           }
                try:
                    with open( path, 'rb') as f:
                        s = f.read()
                        #print(s.decode('utf-8'))
                        s = s.decode('utf-8').replace(u"<!-- MIRRORS -->", html_output(mirrors))
                        day = datetime.date.today().isoformat()
                        # Embed the day of generating the mirror list in ISO format.
                        s = s.replace(u"<!-- DATE -->", day)

                    with open( path, 'wb') as f:
                        f.write(s.encode('utf-8'))

                except:
                    print('ERROR: trouble with reading/writing "{}".'.format(path))

        except IOError:
            print("Cannot open /content/downloads/mirrors.json!")
            try:
                with open( path, 'r') as f:
                    s = f.read()
                    #print s.decode('utf-8')
                    s = s.decode('utf-8').replace(u"<!-- MIRRORS -->", u"<p><small>Cannot open mirrors.json</small></p>")

                with open( path, 'w') as f:
                    f.write( s.encode('utf-8') )

            except:
                print("Trouble with reading/writing path.")



def register():
    # Trying something to read the html after written...
    signals.content_written.connect(do_mirrors)

