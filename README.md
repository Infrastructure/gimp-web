# Installing gimp-web
These instructions are for building the GIMP website from the git repository.
It is assumed you are already acquainted with git.
If not, please consult https://wiki.gnome.org/Git

To download a fresh copy of the site (anonymous, read-only) use the following command:

`git clone https://gitlab.gnome.org/Infrastructure/gimp-web.git`



## Meta
There is a series of pages on the site that attempt to address many questions about building and using the new system (Pelican).
Refer to these pages for extended information in addition to what is below.
(`/about/meta/index.html`)



## Short
The new site is built using the Pelican static site generator, written in Python.
The tools to build and test the site are Python, Pelican, and a couple of modules (Markdown, typogrify).


### Getting the build environment
1. Install Python 3.x.
   For sanity, it is recommended to use a [virtual environment](https://docs.python.org/3/library/venv.html).
    ```
    python3 -m venv py3-venv
    source py3-venv/bin/activate
    ```
2. Install build requirements located in `py3-requirements.txt`.  
   This can be done easily by running `pip install -r py3-requirements.txt`.

Further information in greater detail can be found in the Pelican documentation (https://docs.getpelican.com/en/stable/).


#### Working versions

The site should build locally with a recent version of Python 3. It has
been reported to build with Python 3.5.7 and more recent versions such
as Python 3.9.2 or 3.9.7, though ideally citing accurate Python version
should not be needed. We expect this to work with any sufficiently
modern Python 3.

As for dependencies, below is a snapshot at a point in time. Yet, same
as the Python version, the website has been known to build fine with
newer dependency versions too.

```
beautifulsoup4==4.7.1
blinker==1.4
bs4==0.0.1
docutils==0.14
feedgenerator==1.9
Jinja2==2.10.1
Markdown==3.1.1
MarkupSafe==1.1.1
pelican==4.1.3
Pygments==2.4.2
python-dateutil==2.8.0
pytz==2019.1
six==1.12.0
smartypants==2.0.1
soupsieve==1.9.2
typogrify==2.0.7
Unidecode==1.1.1
```


#### Windows Environment Variables

Due to Windows character encoding, you may see an error when trying to run pelican.
We were seeing this error specifically:

```
CRITICAL: 'charmap' codec can't decode byte 0x8f in position 32396: character maps to <undefined>
```

You'll need to set Python to use UTF-8 character encodings by setting an Environment Variable `PYTHONUTF8` to the value `1`.


###  Pelican configuration files (pelicanconf.*)
The main settings file to build the site is pelicanconf.*.

There are four versions in our directory for various needs.

1. pelicanconf.py - this is the main settings file that coincides to WGO (www.gimp.org).
2. pelicanconf.testing.py - this is the settings file for building testing.gimp.org.
3. pelicanconf.static.py - this is the settings file for building static.gimp.org.
4. pelicanconf.local.py - this is a settings file tailored for a local build and test environment.

Unless you are monkeying with site-wide build settings, you shouldn't normally have to edit these files.
If you do, please consult Jehan, schumaml or patdavid before pushing to be sure.


### Building the site
Once the few prerequisites are installed, building the site is relatively straightforward.
From the project directory, you can invoke pelican:

`pelican -s pelicanconf.py`

This will build the site into a directory called `output`. Use one of the pelicanconfig files mentioned above depending on what version of the site you want to build.
For instance, if you are modifying the website and want to test locally,
you will probably want to build instead with:

```
pelican -s pelicanconf.local.py
```

This will allow in particular to view the local website with proper
relative links and non-broken images.

You can also use `make` to build the site: `make html`, `make testing`, `make static` or `make local` invoke the build for the corresponding version.

If you are writing content or developing the site, there is an option to have pelican watch the directories for file changes and to automatically recompile the site when a change is detected:

`pelican -r -s pelicanconf.py`


### Viewing the site
Python has a built-in simple web server that can be used to serve the site.
From the `output/` directory:

`python3 -m http.server`

The site can then be accessed locally at `localhost:8000`.

You can also run any other webserver that you want, of course.
The site files will be available in the `output/` directory.

*The below note does not seem true anymore with http.server replacing
SimpleHTTPServer but I left the note here in case I missed something.*

Note: the python SimpleHTTPServer doesn't serve svg images with the correct mimetype.
To do this, you should add your own mimetype for svg files:

    #!/usr/bin/python
    import SimpleHTTPServer
    import SocketServer
    import mimetypes

    PORT = 8000

    Handler = SimpleHTTPServer.SimpleHTTPRequestHandler

    Handler.extensions_map['.svg']='image/svg+xml'
    httpd = SocketServer.TCPServer(("", PORT), Handler)

    print "serving at port", PORT
    httpd.serve_forever()

(via: http://gotmetoo.blogspot.com/2013/07/python-simple-http-server-with-svg.html)



## Content
The site content source files are located in the folder `content/`.
There is a Pelican plugin that will mimic the hierarchy of the folders as urls.
This allows us to simply add directories to create new url hierarchy structures easily.


### Directories
So, `content/about/index.md` will be created as `output/about/index.html`.

If a new directory needs to be added, like `artists/`:

1. Create the directory, and an index.md file directly under it.
2. Add the new directory to the `pelicanconf.py` file in the variable `PAGE_PATHS`
    This is to make sure that the new directory gets parsed correctly.


### File Formats
#### Text files
The files can be written using ReStructuredText, Markdown, or HTML.
The latest information for using these formats can be found in the documentation:
https://docs.getpelican.com/en/latest/content.html

The majority of the files here are likely Markdown, as it's what I (Pat David) used.
I wrote a brief cheatsheet of the Markdown format that can be found on the site here:
`about/meta/markdown.html`

#### SVG files
The website is served with a strict Content Security Policy, and as a
consequence, inline scripts and styles are ignored by compliant browsers.
Normally according to the specification, SVG included via `<img>` should not apply
the policy (https://www.w3.org/TR/CSP11/#which-policy-applies), but at least
Firefox does not follow the spec properly right now and all SVG with inlined CSS
end up dark.

Here is the current process to add SVG images with Inkscape and keep the
styling:

1. keep the original as `some-file-name.src.svg`;
2. from Inkscape, export it as "Optimized SVG" as `some-file-name.svg`; make
sure that "Convert CSS attributes to XML attributes" option is checked;
3. edit `some-file-name.svg` with a text editor and run a search and replace to
clean the remaining style attributes: %s/style="[^"]*"//g
4. use only the optimized SVG in the website.

Further edits on the images should happen on the source file only, which should
then be re-exported with the above process.

Note: for complicated SVG images, it is possible that some features can only
work with CSS rules though this won't be the likely case for most simple images.
For such cases, there is no perfect solution at the time of writing (except
moving the CSS of every SVG images out as an external file, or setting our web
server to serve SVG images with `style-src 'unsafe-inline'`).

### Writing a News post
To write a News post, simply include your new post in the `news/` directory.
I have been using `YYYY-MM-DD Topic.md` as a file naming convention.

If you want to keep any post assets (images, etc) with the post, you can create a sub-folder + index.md file.
So, to create a new post about LGM:

1. Create a sub-folder under `news/`: `news/2016-01-12/`
2. Create an index.md file for the post contents `/news/2016-01-12/index.md`.

You can reference these images using the Pelican internal link format ( {filename}.. ).
So if the post has an image `gimp.png` in its directory that you want to reference in the post, you would use:

`<img src='{filename}gimp.png' alt='GIMP' />`

This way Pelican will resolve the actual image location no matter where it ends up.
This is particularly helpful when the post is `Status: draft`.


### Post metadata
There are a few pieces of metadata that should be included in the front-matter of a file.


#### Title
This should be self-explanatory.

#### Date
Use an ISO8601 formatted date, please (https://en.wikipedia.org/wiki/ISO_8601).
That means, YYYY-MM-DD(Thh:mm:ss-TMZ)

Thats, Year (4-digit), Month (2-digit), Day (2-digit), T, hour (2-digit), minute (2-digit), seconds (2-digit), "T", Timezone (T-05:00 for me offset from UTC by 6 hours.
2016-01-14T08:57:51-06:00

The date field can parse less information, so if you'd rather ignore the time, you can simply use:

`Date: 2016-01-14`


#### Category
We're not currently using the `Category` metadata field, but we might (probably will?) in the future.
So it doesn't hurt to fill it out now.
News posts have been filed as `Category: News`.
Other content should be tagged as appropriate (`Content: Tutorial` for instance).


#### Author(s)
The post authors are listed here.
If there is a single author, you can use the singular:

    `Author: Pat David`

If there are multiple authors, a comma-separated list will work with the plural:

    `Authors: Pat David, Alexandre Prokoudine, Michael Schumacher`

You can use the plural `Authors` with a single person without a problem.

    `Authors: Michael Schumacher`


#### Summary
If the item is a news item, this `Summary` metadata will be what appears on the news index page that lists all the posts.
Also possibly the front page if we decide to include snippets there as well.

If there is no `Summary` metadata tag, then the summayr will be auto-generated from the article contents (up to 255 characters in, auto truncate the last word, and end the contents with ellipses `...`.


#### Drafts
To mark a news item as a draft, include the `Status: draft` metadata in the head of the file.
It will not appear anywhere other than in the `drafts/` folder until the `Status` metadata is removed or changed to `Status: published`.
It will also not show up in the RSS/Atom feeds.
