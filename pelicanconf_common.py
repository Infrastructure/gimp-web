#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

# These are default values which may be modified by
# customize_environment().
SITEURL = 'https://www.gimp.org'
SITEMAP_SITEURL = 'https://www.gimp.org'

LOAD_CONTENT_CACHE = False
CHECK_MODIFIED_METHOD = 'mtime'
CACHE_CONTENT = False

FEED_DOMAIN = None
FEED_ATOM = None
FEED_RSS = None
FEED_ALL_ATOM = 'feeds/all.atom.xml'
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'
CATEGORY_FEED_ATOM = None
#TRANSLATION_FEED_ATOM = 'feeds/all-%s.atom.xml'
#TRANSLATION_FEED_RSS = 'feeds/all-%s.rss.xml'
TRANSLATION_FEED_ATOM = None
TRANSLATION_FEED_RSS = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# BEGIN: Pat testing feeds being generated
RELATIVE_URLS = False
FEED_DOMAIN = SITEURL
FEED_ATOM = 'feeds/atom.xml'
FEED_RSS = 'feeds/rss.xml'
# END:   Pat testing feeds being generated

DELETE_OUTPUT_DIRECTORY = False

# def customize_environment(environment):
#     global SITEURL, SITEMAP_SITEURL
#     global LOAD_CONTENT_CACHE, CHECK_MODIFIED_METHOD, CACHE_CONTENT
#     global FEED_DOMAIN, FEED_ATOM, FEED_RSS
#     global DELETE_OUTPUT_DIRECTORY
#     global RELATIVE_URLS

#     valid_environments = [ 'local', 'testing', 'static', 'production' ]
#     if environment not in valid_environments:
#         print("customize_environment() must be called with one of: {}".format(', '.join(valid_environments)))
#         exit(1)

#     if environment == 'testing':
#         SITEURL = 'https://testing.gimp.org'
#         SITEMAP_SITEURL = 'https://testing.gimp.org'
#     elif environment == 'static':
#         SITEURL = 'https://static.gimp.org'
#         SITEMAP_SITEURL = 'https://static.gimp.org'
#     else:
#         SITEURL = 'https://www.gimp.org'
#         SITEMAP_SITEURL = 'https://www.gimp.org'

#     if environment == 'local':
#         #
#         # Caching build for faster regeneration
#         #
#         LOAD_CONTENT_CACHE = True
#         CHECK_MODIFIED_METHOD = 'mtime'
#         CACHE_CONTENT = True
#     else:
#         # Feed generation is usually not desired when developing
#         FEED_DOMAIN = SITEURL
#         FEED_ATOM = 'feeds/atom.xml'
#         FEED_RSS = 'feeds/rss.xml'

#         DELETE_OUTPUT_DIRECTORY = True

#     # When developing, you probably want document relative URLs - so set this to True
#     # When publishing, set to False
#     RELATIVE_URLS = True if environment == 'local' else False

#def customize_environment(environment):
#    global SITEURL, SITEMAP_SITEURL
#    global LOAD_CONTENT_CACHE, CHECK_MODIFIED_METHOD, CACHE_CONTENT
#    global FEED_DOMAIN, FEED_ATOM, FEED_RSS
#    global DELETE_OUTPUT_DIRECTORY
#    global RELATIVE_URLS
#
#    valid_environments = [ 'local', 'testing', 'production' ]
#    if environment not in valid_environments:
#        print("customize_environment() must be called with one of: {}".format(', '.join(valid_environments)))
#        exit(1)
#
#    if environment == 'testing':
#        SITEURL = 'https://testing.gimp.org'
#        SITEMAP_SITEURL = 'https://testing.gimp.org'
#    else:
#        SITEURL = 'https://www.gimp.org'
#        SITEMAP_SITEURL = 'https://www.gimp.org'
#
#    if environment == 'local':
#        #
#        # Caching build for faster regeneration
#        #
#        LOAD_CONTENT_CACHE = True
#        CHECK_MODIFIED_METHOD = 'mtime'
#        CACHE_CONTENT = True
#    else:
#        # Feed generation is usually not desired when developing
#        FEED_DOMAIN = SITEURL
#        FEED_ATOM = 'feeds/atom.xml'
#        FEED_RSS = 'feeds/rss.xml'
#
#        DELETE_OUTPUT_DIRECTORY = True
#
#    # When developing, you probably want document relative URLs - so set this to True
#    # When publishing, set to False
#    RELATIVE_URLS = True if environment == 'local' else False

#######################################
## Values common to all environments ##
#######################################

# This will copy over these folders w/o modification
STATIC_PATHS = ['images', 'js', 'pages', 'tutorials', 'about', 'books',
                'develop', 'docs', 'donating', 'downloads', 'features', 'bugs',
                'links', 'man', 'release-notes', 'screenshots', 'source',
                'unix', 'robots.txt', 'COPYING', 'GNUGPLv2', 'GNUGPLv3', 'news',
                'contribute.json', 'gimp_versions.json', 'robots-testing.txt',
                'python', '.well-known']

#Plugins
PLUGIN_PATHS = ["plugins"]
#PLUGINS = ["page_hierarchy_gimp"]

# TEMP - remove plugins during py3 migration to test things first
# IF we build ok w/o them, then start adding them back one at a time
#PLUGINS = ["mimic_hierarchy", "i18n_subsites", "sitemap", "gimp_mirrors", "tipue_search", "random_header"]
PLUGINS = ["mimic_hierarchy", "sitemap",
           "gimp_mirrors", "gimp_books", "gimp_citation",
           "tipue_search", "random_header"]


# sitemap plugin settings
SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 1,
        'indexes': 0.5,
        'pages': 1
    },
    'changefreqs': {
        'articles': 'weekly',
        'indexes': 'weekly',
        'pages': 'weekly'
    }
}

# mapping: language_code -> settings_overrides_dict
#I18N_SUBSITES = {
#        'fr': {
#            'SITENAME': 'GIMP FR',
#            },
#        'ar': {
#            'SITENAME': 'GIMP AR',
#            },
#        }

AUTHOR = u'Pat David'
SITENAME = u'GIMP'

#GIMP_VERSION = u'2.8.20'

PATH = 'content'

TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = u'en'
DEFAULT_DATE_FORMAT = "%Y-%m-%d"

# Allow dating news posts in the future
# before then, they will automatically have
# Status: draft set
WITH_FUTURE_DATES = False

#  # Blogroll
#  LINKS = (('Pelican', 'http://getpelican.com/'),
#           ('Python.org', 'http://python.org/'),
#           ('Jinja2', 'http://jinja.pocoo.org/'),
#           ('You can modify those links in your config file', '#'),)
#
#  # Social widget
#  SOCIAL = (('You can add links in your config file', '#'),
#            ('Another social link', '#'),)

DEFAULT_PAGINATION = False



# Pat David changes while building/testing
READERS = {'html': None}

# This sets which directories will be parsed as pages (vs. news/articles)
# If a new directory is to be added under content/, make sure it gets added here.
PAGE_PATHS = ['about', 'frontpage', 'tutorials', 'books', 'develop', 'docs', 'donating', 'downloads', 'features', 'bugs', 'links', 'man', 'release-notes', 'screenshots', 'source', 'unix', 'search.md', 'registry']

ARTICLE_PATHS = ['news']

THEME = "./themes/newgimp"

PAGE_URL = "{slug}/"
PAGE_SAVE_AS = "{slug}/index.html"
#PAGE_SAVE_AS = "{slug}/{filename}"

ARTICLE_URL = "news/{date:%Y}/{date:%m}/{date:%d}/{slug}/"
ARTICLE_SAVE_AS = "news/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html"

# This redirects the old standard output of blog/news/articles post
# summaries on the front page.  It will now appear at the location
# below instead.
# The _actual_ index.html page is located at:
# content/pages/index.md -> which simply calls the home.html template
# See: http://docs.getpelican.com/en/3.6.3/faq.html#how-can-i-use-a-static-page-as-my-home-page
# This sets the "old" index.html page to be saved to /news/index.html.
INDEX_SAVE_AS = "/news/index.html"


TYPOGRIFY = True
TYPOGRIFY_IGNORE_TAGS = ['title']

#MD_EXTENSIONS = ['fenced_code', 'codehilite(css_class=codehilite)', 'extra', 'headerid', 'toc(permalink=True)']
# MD_EXTENSIOSN is deprecated, use MARKDOWN instead:
MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.fenced_code': {},
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.toc': {
            'permalink': True,
            },
    },
    'output_format': 'html5',
}

# Pagination testing stuff

DEFAULT_ORPHANS = 0
DEFAULT_PAGINATION = 10

# Debug output on pages
# Seting to 'True' will show child/parent pages at bottom of pages
PAGES_DEBUG = False

###########################################################
# Functions below for pushing data to the build system
###########################################################

#
# Parse the gimp_versions.json file for use around the site
# (mostly the /downloads/index.html page)
#

import json
from collections import OrderedDict
with open('content/gimp_versions.json') as data:
    GIMP = json.load(data, object_pairs_hook=OrderedDict)

if 'STABLE' in GIMP:
    # Set the current stable GIMP version from
    # the GIMP_VERSIONS json file.  The most
    # current version _should_ be the first key.
    GIMP_VERSION = GIMP['STABLE'][0]['version']
    GIMP_MAJOR_MINOR_VERSION = GIMP_VERSION[:GIMP_VERSION.index('.', GIMP_VERSION.index('.') + 1)]
    for info in GIMP['STABLE'] :
        version = info['version']
        if 'date' in info:
            try:
                RELEASE_DATE
            except NameError:
                RELEASE_DATE = info['date']
        if 'windows' in info:
            try:
                WINDOWS_FILE
            except NameError:
                WINDOWS_VER = version
                WINDOWS_MAJOR_MINOR_VER = version[:version.index('.', version.index('.') + 1)]
                WINDOWS_FILE = info['windows'][0]['filename']
                if 'sha256' in info['windows'][0]:
                    WINDOWS_HASH_FUN = 'sha256'
                elif 'sha512' in info['windows'][0]:
                    WINDOWS_HASH_FUN = 'sha512'
                elif 'md5' in info['windows'][0]:
                    WINDOWS_HASH_FUN = 'md5'
                WINDOWS_HASH = info['windows'][0][WINDOWS_HASH_FUN]
                if 'min-support' in info['windows'][0]:
                    WINDOWS_MIN_SUPPORT = info['windows'][0]["min-support"]
                if 'comment' in info['windows'][0]:
                    WINDOWS_COMMENT = info['windows'][0]["comment"]
                if 'date' in info['windows'][0]:
                    WINDOWS_DATE = info['windows'][0]["date"]
                if 'revision' in info['windows'][0]:
                    WINDOWS_REVISION = info['windows'][0]["revision"]
        if 'macos' in info:
            try:
                MACOS_X86_FILE
            except NameError:
              for package in info['macos']:
                if 'build-id' in package:
                  if package['build-id'] == 'org.gimp.GIMP_official.x86_64' or \
                     package['build-id'] == 'org.gimp.GIMP_official':
                    MACOS_X86_FILE = package['filename']
                    MACOS_X86_VER = version
                    MACOS_X86_MAJOR_MINOR_VER = version[:version.index('.', version.index('.') + 1)]
                    if 'sha256' in package:
                        MACOS_X86_HASH_FUN = 'sha256'
                    elif 'sha512' in package:
                        MACOS_X86_HASH_FUN = 'sha512'
                    elif 'md5' in package:
                        MACOS_X86_HASH_FUN = 'md5'
                    MACOS_X86_HASH = package[MACOS_X86_HASH_FUN]
                    if 'min-support' in package:
                        MACOS_X86_MIN_SUPPORT = package["min-support"]
                    if 'comment' in package:
                        MACOS_X86_COMMENT = package["comment"]
                    if 'date' in package:
                        MACOS_X86_DATE = package["date"]
                    if 'revision' in package:
                        MACOS_X86_REVISION = package["revision"]
                    break
            try:
                MACOS_ARM64_FILE
            except NameError:
              for package in info['macos']:
                if 'build-id' in package:
                  if package['build-id'] == 'org.gimp.GIMP_official.arm64':
                    MACOS_ARM64_FILE = package['filename']
                    MACOS_ARM64_VER = version
                    MACOS_ARM64_MAJOR_MINOR_VER = version[:version.index('.', version.index('.') + 1)]
                    if 'sha256' in package:
                        MACOS_ARM64_HASH_FUN = 'sha256'
                    elif 'sha512' in package:
                        MACOS_ARM64_HASH_FUN = 'sha512'
                    elif 'md5' in package:
                        MACOS_ARM64_HASH_FUN = 'md5'
                    MACOS_ARM64_HASH = package[MACOS_ARM64_HASH_FUN]
                    if 'min-support' in package:
                        MACOS_ARM64_MIN_SUPPORT = package["min-support"]
                    if 'comment' in package:
                        MACOS_ARM64_COMMENT = package["comment"]
                    if 'date' in package:
                        MACOS_ARM64_DATE = package["date"]
                    if 'revision' in package:
                        MACOS_ARM64_REVISION = package["revision"]
                    break
else:
    print('STABLE not defined')

if 'OLDSTABLE' in GIMP:
    # Set the current stable GIMP version from
    # the GIMP_VERSIONS json file.  The most
    # current version _should_ be the first key.
    GIMP_VERSION_OLDSTABLE = GIMP['OLDSTABLE'][0]['version']
    GIMP_MAJOR_MINOR_VERSION_OLDSTABLE= GIMP_VERSION_OLDSTABLE[:GIMP_VERSION_OLDSTABLE.index('.', GIMP_VERSION_OLDSTABLE.index('.') + 1)]
    for info in GIMP['OLDSTABLE'] :
        version = info['version']
        if 'date' in info:
            try:
                RELEASE_DATE_OLDSTABLE
            except NameError:
                RELEASE_DATE_OLDSTABLE = info['date']
        if 'windows' in info:
            try:
                WINDOWS_FILE_OLDSTABLE
            except NameError:
                WINDOWS_VER_OLDSTABLE = version
                WINDOWS_MAJOR_MINOR_VER_OLDSTABLE = version[:version.index('.', version.index('.') + 1)]
                WINDOWS_FILE_OLDSTABLE = info['windows'][0]['filename']
                WINDOWS_HASH_OLDSTABLE = info['windows'][0]['md5']
        if 'macos' in info:
            try:
                MACOS_FILE_OLDSTABLE
            except NameError:
                MACOS_VER_OLDSTABLE = version
                MACOS_MAJOR_MINOR_VER_OLDSTABLE= version[:version.index('.', version.index('.') + 1)]
                MACOS_FILE_OLDSTABLE = info['macos'][0]['filename']
                MACOS_HASH_OLDSTABLE = info['macos'][0]['md5']
else:
    print('OLDSTABLE not defined')

if 'DEVELOPMENT' in GIMP:
    # development version
    GIMP_VERSION_DEVELOPMENT = GIMP['DEVELOPMENT'][0]['version']
    GIMP_MAJOR_MINOR_VERSION_DEV = GIMP_VERSION_DEVELOPMENT[:GIMP_VERSION_DEVELOPMENT.rindex('.')]
    GIMP_MICRO_RC_VERSION_DEV = GIMP_VERSION_DEVELOPMENT[GIMP_VERSION_DEVELOPMENT.rindex('.') + 1:]
    try:
      GIMP_MICRO_VERSION_DEV = GIMP_MICRO_RC_VERSION_DEV[:GIMP_MICRO_RC_VERSION_DEV.rindex('-')]
      GIMP_RC_VERSION_DEV = GIMP_MICRO_RC_VERSION_DEV[GIMP_MICRO_RC_VERSION_DEV.rindex('-') + 1:]
    except ValueError:
      GIMP_MICRO_VERSION_DEV = GIMP_MICRO_RC_VERSION_DEV
      GIMP_RC_VERSION_DEV = None
    if GIMP_RC_VERSION_DEV is None:
      GIMP_VERSION_NIGHTLY = '{}.{}'.format(GIMP_MAJOR_MINOR_VERSION_DEV, int(GIMP_MICRO_VERSION_DEV) + 1)
    else:
      GIMP_VERSION_NIGHTLY = '{}+git'.format(GIMP_VERSION_DEVELOPMENT)
    for info in GIMP['DEVELOPMENT'] :
        version = info['version']
        if 'date' in info:
            try:
                RELEASE_DATE_DEVELOPMENT
            except NameError:
                RELEASE_DATE_DEVELOPMENT = info['date']
        if 'windows' in info:
            try:
                WINDOWS_FILE_DEVELOPMENT
            except NameError:
                WINDOWS_VER_DEVELOPMENT = version
                WINDOWS_FILE_DEVELOPMENT = info['windows'][0]['filename']
                if 'sha256' in info['windows'][0]:
                    WINDOWS_HASH_FUN_DEVELOPMENT = 'sha256'
                elif 'sha512' in info['windows'][0]:
                    WINDOWS_HASH_FUN_DEVELOPMENT = 'sha512'
                elif 'md5' in info['windows'][0]:
                    WINDOWS_HASH_FUN_DEVELOPMENT = 'md5'
                WINDOWS_HASH_DEVELOPMENT = info['windows'][0][WINDOWS_HASH_FUN_DEVELOPMENT]
                if 'min-support' in info['windows'][0]:
                    WINDOWS_MIN_SUPPORT_DEVELOPMENT = info['windows'][0]["min-support"]
                if 'comment' in info['windows'][0]:
                    WINDOWS_COMMENT_DEVELOPMENT = info['windows'][0]["comment"]
                if 'date' in info['windows'][0]:
                    WINDOWS_DATE_DEVELOPMENT = info['windows'][0]["date"]
                if 'revision' in info['windows'][0]:
                    WINDOWS_REVISION_DEVELOPMENT = info['windows'][0]["revision"]
        if 'macos' in info:
            try:
                MACOS_X86_FILE_DEVELOPMENT
            except NameError:
              for package in info['macos']:
                if 'build-id' in package:
                  if package['build-id'] == 'org.gimp.GIMP_official.x86_64' or \
                     package['build-id'] == 'org.gimp.GIMP_official':
                    MACOS_X86_FILE_DEVELOPMENT = package['filename']
                    MACOS_X86_VER_DEVELOPMENT = version
                    MACOS_X86_MAJOR_MINOR_VER_DEVEL = version[:version.index('.', version.index('.') + 1)]
                    if 'sha256' in package:
                        MACOS_X86_HASH_FUN_DEVELOPMENT = 'sha256'
                    elif 'sha512' in package:
                        MACOS_X86_HASH_FUN_DEVELOPMENT = 'sha512'
                    elif 'md5' in package:
                        MACOS_X86_HASH_FUN_DEVELOPMENT = 'md5'
                    MACOS_X86_HASH_DEVELOPMENT = package[MACOS_X86_HASH_FUN_DEVELOPMENT]
                    if 'min-support' in package:
                        MACOS_X86_MIN_SUPPORT_DEVELOPMENT = package["min-support"]
                    if 'comment' in package:
                        MACOS_X86_COMMENT_DEVELOPMENT = package["comment"]
                    if 'date' in package:
                        MACOS_X86_DATE_DEVELOPMENT = package["date"]
                    if 'revision' in package:
                        MACOS_X86_REVISION_DEVELOPMENT = package["revision"]
                    break
            try:
                MACOS_ARM64_FILE_DEVELOPMENT
            except NameError:
              for package in info['macos']:
                if 'build-id' in package:
                  if package['build-id'] == 'org.gimp.GIMP_official.arm64':
                    MACOS_ARM64_FILE_DEVELOPMENT = package['filename']
                    MACOS_ARM64_VER_DEVELOPMENT = version
                    MACOS_ARM64_MAJOR_MINOR_VER_DEVEL = version[:version.index('.', version.index('.') + 1)]
                    if 'sha256' in package:
                        MACOS_ARM64_HASH_FUN_DEVELOPMENT = 'sha256'
                    elif 'sha512' in package:
                        MACOS_ARM64_HASH_FUN_DEVELOPMENT = 'sha512'
                    elif 'md5' in package:
                        MACOS_ARM64_HASH_FUN_DEVELOPMENT = 'md5'
                    MACOS_ARM64_HASH_DEVELOPMENT = package[MACOS_ARM64_HASH_FUN_DEVELOPMENT]
                    if 'min-support' in package:
                        MACOS_ARM64_MIN_SUPPORT_DEVELOPMENT = package["min-support"]
                    if 'comment' in package:
                        MACOS_ARM64_COMMENT_DEVELOPMENT = package["comment"]
                    if 'date' in package:
                        MACOS_ARM64_DATE_DEVELOPMENT = package["date"]
                    if 'revision' in package:
                        MACOS_ARM64_REVISION_DEVELOPMENT = package["revision"]
                    break
        if 'appimage' in info:
            try:
                APPIMAGE_X86_FILE_DEVELOPMENT
            except NameError:
              for package in info['appimage']:
                if 'build-id' in package:
                  if package['build-id'] == 'org.gimp.GIMP_official.x86_64':
                    APPIMAGE_X86_FILE_DEVELOPMENT = package['filename']
                    APPIMAGE_X86_VER_DEVELOPMENT = version
                    APPIMAGE_X86_MAJOR_MINOR_VER_DEVEL = version[:version.index('.', version.index('.') + 1)]
                    if 'sha256' in package:
                        APPIMAGE_X86_HASH_FUN_DEVELOPMENT = 'sha256'
                    elif 'sha512' in package:
                        APPIMAGE_X86_HASH_FUN_DEVELOPMENT = 'sha512'
                    elif 'md5' in package:
                        APPIMAGE_X86_HASH_FUN_DEVELOPMENT = 'md5'
                    APPIMAGE_X86_HASH_DEVELOPMENT = package[APPIMAGE_X86_HASH_FUN_DEVELOPMENT]
                    if 'min-support' in package:
                        APPIMAGE_X86_MIN_SUPPORT_DEVELOPMENT = package["min-support"]
                    if 'comment' in package:
                        APPIMAGE_X86_COMMENT_DEVELOPMENT = package["comment"]
                    if 'date' in package:
                        APPIMAGE_X86_DATE_DEVELOPMENT = package["date"]
                    if 'revision' in package:
                        APPIMAGE_X86_REVISION_DEVELOPMENT = package["revision"]
                    break
            try:
                APPIMAGE_ARM64_FILE_DEVELOPMENT
            except NameError:
              for package in info['appimage']:
                if 'build-id' in package:
                  if package['build-id'] == 'org.gimp.GIMP_official.arm64':
                    APPIMAGE_ARM64_FILE_DEVELOPMENT = package['filename']
                    APPIMAGE_ARM64_VER_DEVELOPMENT = version
                    APPIMAGE_ARM64_MAJOR_MINOR_VER_DEVEL = version[:version.index('.', version.index('.') + 1)]
                    if 'sha256' in package:
                        APPIMAGE_ARM64_HASH_FUN_DEVELOPMENT = 'sha256'
                    elif 'sha512' in package:
                        APPIMAGE_ARM64_HASH_FUN_DEVELOPMENT = 'sha512'
                    elif 'md5' in package:
                        APPIMAGE_ARM64_HASH_FUN_DEVELOPMENT = 'md5'
                    APPIMAGE_ARM64_HASH_DEVELOPMENT = package[APPIMAGE_ARM64_HASH_FUN_DEVELOPMENT]
                    if 'min-support' in package:
                        APPIMAGE_ARM64_MIN_SUPPORT_DEVELOPMENT = package["min-support"]
                    if 'comment' in package:
                        APPIMAGE_ARM64_COMMENT_DEVELOPMENT = package["comment"]
                    if 'date' in package:
                        APPIMAGE_ARM64_DATE_DEVELOPMENT = package["date"]
                    if 'revision' in package:
                        APPIMAGE_ARM64_REVISION_DEVELOPMENT = package["revision"]
                    break
        if 'flatpak' in info:
            try:
                FLATPAK_FILE_DEVELOPMENT
            except NameError:
                FLATPAK_VER_DEVELOPMENT = version
                FLATPAK_FILE_DEVELOPMENT = {}
                FLATPAK_HASH_DEVELOPMENT = {}
                for arch, package in info['flatpak'].iteritems() :
                    FLATPAK_FILE_DEVELOPMENT[arch] = package[0]['filename']
                    FLATPAK_HASH_DEVELOPMENT[arch] = package[0]['md5']
else:
    print('DEVELOPMENT not defined')

#
# Random Header Background Image
#
# This is to get the possible header images
# and choose one randomly to display.
#
# Templates will use HEADER_IMG data to parse image information.
# Refer to the random_header plugin for actually putting the image
# in the correct stylesheet.
#
from random import randint
with open('header-images.json') as data:
    IMG_LIST = json.load(data)

HEADER_IMG = IMG_LIST[ randint(0, len(IMG_LIST) - 1) ]
