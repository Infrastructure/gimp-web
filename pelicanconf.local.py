#!/usr/bin/env python
# -*- coding: utf-8 -*- #

# make sure the current path is searched for imports
import sys
import os

sys.path.append(os.path.abspath(os.path.dirname(__file__)))

# Import everything to the global scope.
from pelicanconf_common import *
#customize_environment('local')

SITEURL = 'https://www.gimp.org'
SITEMAP_SITEURL = 'https://www.gimp.org'

LOAD_CONTENT_CACHE = True
CHECK_MODIFIED_METHOD = 'mtime'
CACHE_CONTENT = True
RELATIVE_URLS = True
