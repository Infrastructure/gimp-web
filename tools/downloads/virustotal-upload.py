#!/usr/bin/env python3

import os
import argparse
import requests
from requests_toolbelt import MultipartEncoder, MultipartEncoderMonitor

parser = argparse.ArgumentParser(description='Upload a large file to VirusTotal')
parser.add_argument('apikey', metavar='<apikey>',
                    help='Your VirusTotal API key')
parser.add_argument(dest='file', metavar='<file>',
                    help='The file to be uploaded')

args = parser.parse_args()

# get the upload url
# see https://developers.virustotal.com/v3.0/reference#files-upload-url
headers = { 'x-apikey' : args.apikey }
url = 'https://www.virustotal.com/api/v3/files/upload_url'

response = requests.get(url=url, timeout=10, headers=headers)

# response JSON
# {
#  "data": "http://www.virustotal.com/_ah/upload/AMmfu6b-_DXUeFe36Sb3b0F4B8mH9Nb-CHbRoUNVOPwG/"
# }

if response.status_code == 200:
    json = response.json()
    upload_url = json['data']

    files = {'file': open(os.path.abspath(args.file), 'rb')}

    def create_callback(encoder):
        length = encoder.len

        def upload_callback(monitor):
            print("Uploaded " + str(monitor.bytes_read) + " of " + str(length) + " bytes - "
                  + str(round(100*monitor.bytes_read/length, 2)) + "%", end='\r')

        return upload_callback

    encoder = MultipartEncoder(files)
    callback = create_callback(encoder);
    monitor = MultipartEncoderMonitor(encoder, callback)

    # upload file
    # see https://developers.virustotal.com/v3.0/reference#files-scan
    try:
        response = requests.post(url=upload_url, headers=headers, data=monitor)
    except KeyboardInterrupt:
        print("");
        print("Upload interrupted by user.")
        exit(1);

    # response JSON
    # {
    #     "data": {
    #            "type": "analysis",
    #            "id": "NjY0MjRlOTFjMDIyYTkyNWM0NjU2NWQzYWNlMzFmZmI6MTQ3NTA0ODI3Nw=="
    #     }
    # }

    # https://developers.virustotal.com/v3.0/reference#analysis
    if response.status_code == 200:
       json = response.json()
       analysis_id = json['data']['id']

       print('File analysis: https://www.virustotal.com/gui/file-analysis/' +  analysis_id)
