#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# This file is part of gimp-web
# Copyright (C) 2024 Jehan
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# crossroad is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with crossroad.  If not, see <http://www.gnu.org/licenses/>.

import argparse
import datetime
import getpass
import hashlib
import json
import os
import re
import socket
import subprocess
import sys
import tempfile
import time
import urllib.request

# TODO: with completely new minor series, we should:
# mkdir v3.x/
# mkdir -p v3.x/windows/
# mkdir v3.x/macos/
# mkdir v3.x/linux/
# mkdir v3.x/api-docs/
# touch v3.x/0.0_LATEST-IS-
# touch v3.x/windows/0.0_LATEST-IS-
# touch v3.x/macos/0.0_LATEST-IS-
# touch v3.x/SHA256SUM
# touch v3.x/SHA512SUM
#
# For now these are to be run manually.

# We don't want the temporary files to be on /tmp/ because then
# httpd_sys_content_t (selinux label) will not be set. Downloading on
# this directory as entry point though is fine.
incoming_folder         ='/mnt/ftp/incoming/'
pub_folder_pattern      ='/mnt/ftp/pub/gimp/v{}.{}'
babl_pub_folder_pattern ='/mnt/ftp/pub/babl/{}.{}/api-docs/'
gegl_pub_folder_pattern ='/mnt/ftp/pub/gegl/{}.{}/api-docs/'
mirrors_list            = './tools/downloads/downloads.http.txt'

parser = argparse.ArgumentParser(description='Upload GIMP release')
parser.add_argument('gimp_version')

parser.add_argument('--dry-run', dest='dryrun', action='store_true',
                    help="Do not actually publish (files will be downloaded locally and to remote's incoming folder).")
parser.add_argument('--no-sources', dest='nosources', action='store_true',
                    help="Do not check if sources were already published.")
parser.add_argument('--no-api-docs', dest='nodocs', action='store_true',
                    help="Do not publish API documentation.")
parser.add_argument('--no-appimage', dest='noappimage', action='store_true',
                    help="Do not check if AppImage packages were already published.")

args = parser.parse_args()

print("Release of GIMP {} starting… 🚀".format(args.gimp_version))

def get_version(version_string):
  '''
  Our pattern for GIMP version is: major.minor.micro
  We can have optional '-RC{num}' appended.
  '''
  pattern = re.compile('^([0-9]+)\\.([0-9]+)\\.([0-9]+)(-RC([0-9])+)?$')
  version = pattern.match(version_string)
  if version is None:
    sys.stderr.write("Invalid version: {}\n".format(version_string))
    sys.stderr.write("\tExpected format: major.minor.micro with optional appended -RC number.\n")
    sys.stderr.write("\tExamples: 3.0.0 or 3.0.0-RC1\n")
    sys.stderr.write("\tAborting.\n")
    sys.exit(os.EX_DATAERR)

  major = version.group(1)
  minor = version.group(2)
  micro = version.group(3)
  rc    = version.group(5)

  return major, minor, micro, rc

def get_url_revision(binary_name):
  try:
    url = input("Paste {} URL (Enter or Ctrl-D for none):\n".format(binary_name))
    url = url.strip()
    if url == '':
      url = None
  except EOFError:
    url = None

  revision = None
  rev_comment = None
  if url is not None:
    try:
      revision = input("Which revision is it? (0, Enter or Ctrl-D for none)")
      if revision.strip() == '':
        revision = 0

      try:
        revision = int(revision)
      except ValueError:
        sys.stderr.write("Revision '{}' is not a valid integer. Aborting.\n")
        sys.exit(os.EX_DATAERR)
    except EOFError:
      revision = 0

    if revision != 0:
      rev_comment = input('Comment for revision {}:\n'.format(revision))
      rev_comment = rev_comment.strip()

  return url, revision, rev_comment

def check_sources(git_user, pub_folder, tarball_name):
  cmds = [ 'ls {}/{}'.format(pub_folder, tarball_name)]
  remote_cmd = " && ".join(cmds)
  server = '{}@master.gimp.org'.format(git_user)
  try:
    subprocess.check_call(['ssh', server, remote_cmd])
  except subprocess.CalledProcessError:
    print('   * Sources were not released')
    return True

  print('   * Sources were already released. Ignoring this step.')
  return False

def check_existing(git_user, pub_folder, allow_none=False):
  server = '{}@master.gimp.org'.format(git_user)
  cmds  = [ 'cd {}'.format(pub_folder) ]
  if args.dryrun:
    allow_none = True

  if allow_none:
    cmds += [ 'if [ -n "`cat SHA256SUMS`" ]; then sha256sum -c SHA256SUMS; fi'.format(pub_folder)]
    cmds += [ 'if [ -n "`cat SHA512SUMS`" ]; then sha512sum -c SHA512SUMS; fi'.format(pub_folder)]
  else:
    cmds += [ 'sha256sum -c SHA256SUMS'.format(pub_folder)]
    cmds += [ 'sha512sum -c SHA512SUMS'.format(pub_folder)]
  remote_cmd = " && ".join(cmds)
  try:
    subprocess.check_call(['ssh', server, remote_cmd])
  except subprocess.CalledProcessError:
    print('   * Files in publication folder are corrupted: {}'.format(pub_folder))
    print('   * Aborting')
    sys.exit(os.EX_DATAERR)
  print('   * Files in publication folder are fine: {}'.format(pub_folder))

def download_remotely(url, bin_name, git_user, sha256url=None, sha512url=None,
                      old_bin_name=None):
  '''
  Download from remote server. While we could just scp from local server to
  remote one, it's not a bad idea to download the file twice and compare
  checksums (for possible corrupted downloads or security reasons) and compare
  them.
  All SSH usage in this script assume that you configured SSH correctly and
  securely, with a key, and that you already unlocked it using a SSH agent
  before running this script.
  '''
  sys.stdout.write('   * Downloading {} on remote server from {}\n'.format(bin_name, url))
  cmds  = [ 'cd {}'.format(incoming_folder) ]
  cmds += [ 'curl -L "{}" -o "{}"'.format(url, bin_name) ]
  cmds += [ 'sha256sum "{}"'.format(bin_name) ]
  cmds += [ 'sha512sum "{}"'.format(bin_name) ]
  remote_cmd = " && ".join(cmds)
  server = '{}@master.gimp.org'.format(git_user)
  output = subprocess.check_output(['ssh', server, remote_cmd])
  output = output.strip().split(b'\n')
  sha256_full_line = output[-2].strip().decode('ASCII')
  sha512_full_line = output[-1].strip().decode('ASCII')
  # Format is "checksum binary"
  sha256sum = sha256_full_line.split()[0]
  sha512sum = sha512_full_line.split()[0]
  print('   * SHA256: {}'.format(sha256sum))
  print('   * SHA512: {}'.format(sha512sum))

  try:
    if sha256url is not None:
      print('   * Comparing SHA256 with: {}\n'.format(sha256url))
      sha256_basename = os.path.basename(sha256url)
      cmds  = [ 'cd {}'.format(incoming_folder) ]
      cmds += [ 'curl -L "{}" -o "{}"'.format(sha256url, sha256_basename) ]
      if old_bin_name:
        cmds += [ 'sed -i \'s@ \\+/[a-z/]*/{}$@ {}@\' "{}"'.format(old_bin_name, bin_name, sha256_basename) ]
      cmds += [ 'sha256sum -c "{}"'.format(sha256_basename) ]
      remote_cmd = " && ".join(cmds)
      server = '{}@master.gimp.org'.format(git_user)
      subprocess.check_call(['ssh', server, remote_cmd])
      delete_remote_files(git_user, 'Cleanup', [sha256_basename])

    if sha512url is not None:
      print('   * Comparing SHA512 with: {}\n'.format(sha512url))
      sha512_basename = os.path.basename(sha512url)
      cmds  = [ 'cd {}'.format(incoming_folder) ]
      cmds += [ 'curl -L "{}" -o "{}"'.format(sha512url, sha512_basename) ]
      if old_bin_name:
        cmds += [ 'sed -i \'s@ \\+/[a-z/]*/{}$@ {}@\' "{}"'.format(old_bin_name, bin_name, sha512_basename) ]
      cmds += [ 'sha512sum -c "{}"'.format(sha512_basename) ]
      remote_cmd = " && ".join(cmds)
      server = '{}@master.gimp.org'.format(git_user)
      subprocess.check_call(['ssh', server, remote_cmd])
      delete_remote_files(git_user, 'Cleanup', [sha512_basename])
  except subprocess.CalledProcessError:
    print('   * Checksum failed for {}!!!'.format(bin_name))
    print('   * Aborting!')
    sys.exit(os.EX_DATAERR)

  return sha256sum, sha512sum, sha256_full_line, sha512_full_line

def download(url, tempdir, bin_name):
  tmp_path = None
  try:
    with urllib.request.urlopen(url, timeout = 10.0) as remote_file:
      local_path = os.path.join(tempdir, bin_name)
      with open(local_path, 'wb') as local_file:
        print("   * Writing in {} …".format(local_file.name))
        local_file.write(remote_file.read())
        tmp_path = local_file.name
  except (urllib.error.URLError, socket.timeout) as e:
      if isinstance(err, urllib.error.URLError):
        sys.stderr.write('\tDownload failed with reason: {}\n'.format(e.reason))
      else:
        sys.stderr.write('\tDownload failed from timeout.\n')
      sys.exit(os.EX_NOINPUT)
  return tmp_path

def compare_digests(path, remote_sha256sum, remote_sha512sum):
  sha256sum = hashlib.sha256()
  with open(path, 'rb') as f:
    data = f.read(65536)
    while data:
      sha256sum.update(data)
      data = f.read(65536)
  print('\tSHA256: {}'.format(sha256sum.hexdigest()))

  if sha256sum.hexdigest() != remote_sha256sum:
    sys.stderr.write('\tRemote SHA256 sum was: {}\n'.format(remote_sha256sum))
    sys.stderr.write('\tSHA256 sum for same binary downloaded remotely or locally differ.\n')
    sys.stderr.write("\tIt may either be a corrupted download or a security issue.")
    sys.stderr.write("\tAborting.\n")
    sys.exit(os.EX_DATAERR)

  sha512sum = hashlib.sha512()
  with open(path, 'rb') as f:
    data = f.read(65536)
    while data:
      sha512sum.update(data)
      data = f.read(65536)
  print('\tSHA512: {}'.format(sha512sum.hexdigest()))

  if sha512sum.hexdigest() != remote_sha512sum:
    sys.stderr.write('\tRemote SHA512 sum was: {}\n'.format(remote_sha512sum))
    sys.stderr.write('\tSHA256 sum for same binary downloaded remotely or locally differ.\n')
    sys.stderr.write("\tIt may either be a corrupted download or a security issue.")
    sys.stderr.write("\tAborting.\n")
    sys.exit(os.EX_DATAERR)

def get_deps_versions(git_tag):
  browse_pattern = 'https://gitlab.gnome.org/GNOME/gimp/-/jobs/artifacts/{}/browse?job=dev-docs'
  browse_url = browse_pattern.format(git_tag)
  print('   * Parsing contents of: {}'.format(browse_url))
  babl_pattern = re.compile(b'babl-api-docs-([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.tar\\.xz')
  gegl_pattern = re.compile(b'gegl-api-docs-([0-9]+)\\.([0-9]+)\\.([0-9]+)\\.tar\\.xz')
  with urllib.request.urlopen(browse_url, timeout = 10.0) as remote_file:
    browse  = remote_file.read()

    version = babl_pattern.search(browse)
    if version is None:
      sys.stderr.write("babl pattern not found in: {}\n".format(browse_url))
      sys.stderr.write("\tAborting.\n")
      sys.exit(os.EX_DATAERR)
    babl_major = int(version.group(1))
    babl_minor = int(version.group(2))
    babl_micro = int(version.group(3))

    version = gegl_pattern.search(browse)
    if version is None:
      sys.stderr.write("GEGL pattern not found in: {}\n".format(browse_url))
      sys.stderr.write("\tAborting.\n")
      sys.exit(os.EX_DATAERR)
    gegl_major = int(version.group(1))
    gegl_minor = int(version.group(2))
    gegl_micro = int(version.group(3))
  print('   * Found documentation for babl {}.{}.{}.'.format(babl_major, babl_minor, babl_micro))
  print('   * Found documentation for GEGL {}.{}.{}.'.format(gegl_major, gegl_minor, gegl_micro))

  return babl_major, babl_minor, babl_micro, gegl_major, gegl_minor, gegl_micro


def create_torrent(path, target, version, revision, revision_comment, server_reldir):
  '''
    Runs externally:
    tools/downloads/mt -c ${revcomment} -p ${server_reldir} -m ${mirrors_list} ${path}
  '''
  exe_name = os.path.basename(path)
  torrent_path = exe_name + '.torrent'
  if os.path.exists(torrent_path):
    sys.stderr.write('Torrent file exists: {}\n'.format(torrent_path))
    sys.exit(os.EX_DATAERR)

  torrent_message = 'GIMP {} {}'.format(version, target)
  if revision != 0:
    torrent_message += ' - Revision {}'.format(revision)
    if revision_comment != '':
      torrent_message += ': {}'.format(revision_comment)

  cmd = ['tools/downloads/mt', '-c', torrent_message,
         '-p', server_reldir, '-m', mirrors_list, path]
  subprocess.check_call(cmd)

  if not os.path.exists(torrent_path):
    sys.stderr.write('Torrent file does not exists: {}\n'.format(torrent_path))
    sys.exit(os.EX_SOFTWARE)

  return os.path.abspath(torrent_path)

def upload_torrent(git_user, torrent_path):
  server = '{}@master.gimp.org'.format(git_user)
  basename = os.path.basename(torrent_path)
  remote_path = '{}:{}/{}'.format(server, incoming_folder, basename)
  subprocess.check_call(['scp', torrent_path, remote_path])

def update_remote_sha(git_user, pub_folder, sha256_line, sha512_line):
  sha256path = os.path.join(pub_folder, 'SHA256SUMS')
  sha512path = os.path.join(pub_folder, 'SHA512SUMS')
  print("   * Moving to incoming folder…")
  cmds  = [ 'cd {}'.format(incoming_folder) ]
  print("   * Copying SHA*SUMS files to incoming folder…")
  cmds += [ 'cp "{0}" .'.format(sha256path) ]
  cmds += [ 'cp "{0}" .'.format(sha512path) ]
  print("   * Updating SHA*SUMS files…")
  cmds += [ 'echo "{}" >> SHA256SUMS'.format(sha256_line) ]
  cmds += [ 'echo "{}" >> SHA512SUMS'.format(sha512_line) ]
  print("   * Comparing SHA*SUMS files…")
  # The diff should be only 2 lines long.
  shacount = 'if [ $(diff {0} {1} |wc -l) -ne 2 ]; then false; fi'
  cmds += [ shacount.format(sha256path, 'SHA256SUMS') ]
  cmds += [ shacount.format(sha512path, 'SHA512SUMS') ]
  # The second diff line should be exactly equal to the checksum output.
  shacmp = 'if [ "$(diff {0} {1} |tail -1)" != "> {2}" ]; then false; fi'
  cmds += [ shacmp.format(sha256path, 'SHA256SUMS', sha256_line) ]
  cmds += [ shacmp.format(sha512path, 'SHA512SUMS', sha512_line) ]
  remote_cmd = " && ".join(cmds)
  server = '{}@master.gimp.org'.format(git_user)
  subprocess.check_call(['ssh', server, remote_cmd])

def get_files_info(git_user, pub_folder, check_latest=True):
  cmds  = [ 'ls -ltr "{}/"'.format(pub_folder) ]
  if check_latest:
    cmds += [ 'ls {}/0.0_LATEST-IS-*'.format(pub_folder)]
  remote_cmd = " && ".join(cmds)
  server = '{}@master.gimp.org'.format(git_user)
  output = subprocess.check_output(['ssh', server, remote_cmd])
  output = output.strip().split(b'\n')
  if check_latest:
    num_files   = len(output) - 2
    prev_latest = output[-1].strip().decode('ASCII')
  else:
    num_files   = len(output) - 1
    prev_latest = None
  print('   * INFO: the publication folder {} has {} files'.format(pub_folder, num_files))
  print('   * INFO: previous release: {}'.format(prev_latest))

  yesno = input("   => Do you want to see the contents of {}?  (y/N) ".format(pub_folder))
  if yesno.strip().lower() == 'y':
    print()
    if check_latest:
      print("\n".join([l.decode('UTF-8') for l in output[:-1]]))
    else:
      print("\n".join([l.decode('UTF-8') for l in output]))
    print()
  yesno = input("   => Continue? (y/N) ")
  if yesno.strip().lower() != 'y':
    print("   * Release aborted.")
    sys.exit(os.EX_TEMPFAIL)
  return num_files, prev_latest

def print_red(text):
  print('\033[31m{}\033[0m'.format(text))

def delete_remote_files(git_user, header, files):
  print()
  print_red('   !!!! WARNING !!!')
  print('   * {}: removing downloaded files with the following commands:'.format(header))
  rm_files = [ '"{}"'.format(os.path.basename(f)) for f in files ]
  rm_files = " ".join(rm_files)
  cmds = [ 'cd {}'.format(incoming_folder) ]
  cmds += [ 'rm {}'.format(rm_files) ]
  print()
  for cmd in cmds:
    print("     {}".format(cmd))
  seconds = 5
  print()
  sys.stdout.write('   * Deleting in…')
  while seconds > 0:
    sys.stdout.write(' {}'.format(seconds))
    sys.stdout.flush()
    time.sleep(1)
    seconds -= 1
  print()
  server = '{}@master.gimp.org'.format(git_user)
  remote_cmd = " && ".join(cmds)
  subprocess.check_call(['ssh', server, remote_cmd])

def publish(git_user, pub_folder, gimp_version, new_files, updated_files, num_files, prev_latest):
  # Make sure we have only basenames.
  all_files = [ os.path.basename(f) for f in new_files + updated_files ]

  #print("\tMoving to incoming folder…")
  cmds = [ 'cd {}'.format(incoming_folder) ]

  mv_filelist = [ '"{}"'.format(os.path.basename(f)) for f in new_files ]
  mv_filelist = " ".join(mv_filelist)
  cmds += [ 'mv -n {} {}'.format(mv_filelist, pub_folder) ]
  mv_filelist = [ '"{}"'.format(os.path.basename(f)) for f in updated_files ]
  mv_filelist = " ".join(mv_filelist)
  cmds += [ 'mv {} {}'.format(mv_filelist, pub_folder) ]
  chgrp_filelist = [ '"{}"'.format(os.path.join(pub_folder, f)) for f in all_files ]
  if prev_latest is not None:
    new_latest = '{}/0.0_LATEST-IS-{}'.format(pub_folder, gimp_version)
    if os.path.realpath(prev_latest) != os.path.realpath(new_latest):
      chgrp_filelist += [ '"{}"'.format(new_latest) ]
      cmds += [ 'touch "{}"'.format(new_latest) ]
      cmds += [ 'rm -I "{}"'.format(prev_latest) ]
  chgrp_filelist = " ".join(chgrp_filelist)
  cmds += [ 'chgrp gimpftpadmin {}'.format(chgrp_filelist) ]

  print("   * I am going to run the following commands on the remote server:")
  print()
  for cmd in cmds:
    print("     {}".format(cmd))
  print()
  yesno = input("   => Continue? (y/N) ")
  if yesno.strip().lower() != 'y':
    print("   * Release aborted.")
    sys.exit(os.EX_TEMPFAIL)

  if args.dryrun:
    print('   * Dry run: nothing is actually published!')
    delete_remote_files(git_user, 'Dry run', all_files)
    print("   * Verifying we still have {} files published.".format(num_files))
  else:
    print("   * Publication in progress…")
    server     = '{}@master.gimp.org'.format(git_user)
    remote_cmd = " && ".join(cmds)
    subprocess.check_call(['ssh', server, remote_cmd])
    num_files += len(new_files)
    print("   * Verifying we now have {} files published.".format(num_files))

  cmds  = [ 'ls -ltr {}/'.format(pub_folder) ]
  remote_cmd = " && ".join(cmds)
  server = '{}@master.gimp.org'.format(git_user)
  output = subprocess.check_output(['ssh', server, remote_cmd])
  output = output.strip().split(b'\n')
  new_num_files = len(output) - 1
  if new_num_files != num_files:
    print('   * Oh no! We have {} files now! Something wrong happened!'.format(new_num_files))
    print()
    print("\n".join([l.decode('UTF-8') for l in output]))
    print()
    sys.exit(os.EX_SOFTWARE)

  print('   * The publication folder has the right number of lines.')
  yesno = input("   => Do you want to see the contents of {}?  (y/N) ".format(pub_folder))
  if yesno.strip().lower() == 'y':
    print()
    print("\n".join([l.decode('UTF-8') for l in output]))
    print()

# (1) Check version.
major, minor, micro, rc = get_version(args.gimp_version)

# (2) Check if we are running this from root of gimp-web.
if not os.path.exists(mirrors_list):
  sys.stderr.write("Please run me from the root of the gimp-web repository.\n")
  sys.stderr.write("Aborting.\n")
  sys.exit(os.EX_NOINPUT)

# (3) Ask for git user to use.
git_user = getpass.getpass("What is your git user on master.gimp.org? ")
git_user = git_user.strip()

# (4) Check which packages to update.
win_url, win_rev, win_comment                         = get_url_revision('signed Windows installer')
mac_intel_url, mac_intel_rev, mac_intel_comment       = get_url_revision('macOS DMG for Intel')
mac_silicon_url, mac_silicon_rev, mac_silicon_comment = get_url_revision('macOS DMG for Apple Silicon')

if git_user == '':
  sys.stderr.write("No git user. Aborting.\n")
  sys.exit(os.EX_DATAERR)

# (5) Actually do the work!
with tempfile.TemporaryDirectory() as tempdir:
  jsonfile = {}
  jsonfile['version'] = args.gimp_version
  jsonfile['date']    = datetime.date.today().isoformat()
  pub_folder = pub_folder_pattern.format(major, minor)
  if rc is None:
    rc_string = ''
    rc_tag = ''
  else:
    rc_string = '-RC{}'.format(rc)
    rc_tag = '_RC{}'.format(rc)

  print("## Starting source release")
  print()
  print('0. Checking if sources were already released.')
  tarball_name = 'gimp-{}.{}.{}{}.tar.{}'.format(major, minor, micro, rc_string, 'xz' if int(major) > 2 else 'bz2')
  if args.nosources:
    print('1. Skipping publishing sources per --no-sources option')
  elif check_sources(git_user, pub_folder, tarball_name):
    print('1. Checking already published sources')
    check_existing(git_user, pub_folder, True)
    print('2. Downloading sources on remote server')
    git_tag = 'GIMP_{}_{}_{}{}'.format(major, minor, micro, rc_tag)
    src_pattern = 'https://gitlab.gnome.org/GNOME/gimp/-/jobs/artifacts/{}/raw/{}{}?job=sources-debian'
    url = src_pattern.format(git_tag, tarball_name, '')
    sha256url = src_pattern.format(git_tag, tarball_name, '.SHA256SUMS')
    sha512url = src_pattern.format(git_tag, tarball_name, '.SHA512SUMS')
    sha256sum, sha512sum, sha256_line, sha512_line = download_remotely(url, tarball_name, git_user,
                                                                       sha256url, sha512url)
    print('3. Updating SHA files on remote server')
    update_remote_sha(git_user, pub_folder, sha256_line, sha512_line)
    num_files, prev_latest = get_files_info(git_user, pub_folder)
    print('4. Publishing sources')
    publish(git_user, pub_folder, args.gimp_version,
            [ tarball_name ], [ 'SHA256SUMS', 'SHA512SUMS' ],
            num_files, prev_latest)
    print('5. Checking published sources')
    check_existing(git_user, pub_folder)
    jsonfile['source'] = [
                           {
                             'filename': tarball_name,
                             'sha512': sha512sum,
                             'sha256': sha256sum
                           }
                         ]

  print()
  print("## Starting API documentation release")
  print()

  if args.nodocs:
    print('1. Skipping publishing API documentation per --no-api-docs option')
  else:
    tarball_name   = 'gimp-api-docs-{}.{}.{}{}.tar.xz'.format(major, minor, micro, rc_string)
    api_pub_folder = os.path.join(pub_folder, "api-docs")
    git_tag = 'GIMP_{}_{}_{}{}'.format(major, minor, micro, rc_tag)
    if check_sources(git_user, api_pub_folder, tarball_name):
      print("### libgimp documentation")
      # libgimp API
      print('1. Checking already published libgimp docs')
      check_existing(git_user, api_pub_folder, True)
      print('2. Downloading libgimp docs on remote server')
      src_pattern = 'https://gitlab.gnome.org/GNOME/gimp/-/jobs/artifacts/{}/raw/{}{}?job=dev-docs'
      url = src_pattern.format(git_tag, tarball_name, '')
      sha256url = src_pattern.format(git_tag, tarball_name, '.SHA256SUMS')
      sha512url = src_pattern.format(git_tag, tarball_name, '.SHA512SUMS')
      sha256sum, sha512sum, sha256_line, sha512_line = download_remotely(url, tarball_name, git_user,
                                                                         sha256url, sha512url)
      print('3. Updating SHA files on remote server')
      update_remote_sha(git_user, api_pub_folder, sha256_line, sha512_line)
      num_files, prev_latest = get_files_info(git_user, api_pub_folder, False)
      print('4. Publishing libgimp documentation')
      publish(git_user, api_pub_folder, args.gimp_version,
              [ tarball_name ], [ 'SHA256SUMS', 'SHA512SUMS' ],
              num_files, prev_latest)
      print('5. Checking published libgimp documentation')
      check_existing(git_user, api_pub_folder)

    print("### babl/GEGL documentation")
    print('1. Grabbing version of babl and GEGL documentation')
    babl_major, babl_minor, babl_micro, gegl_major, gegl_minor, gegl_micro = get_deps_versions(git_tag)

    # babl API
    babl_pub_folder = babl_pub_folder_pattern.format(babl_major, babl_minor)
    tarball_name    = 'babl-api-docs-{}.{}.{}.tar.xz'.format(babl_major, babl_minor, babl_micro)
    if check_sources(git_user, babl_pub_folder, tarball_name):
      print("#### babl documentation")
      print('1. Checking already published babl docs')
      check_existing(git_user, babl_pub_folder, True)
      print('2. Downloading babl docs on remote server')
      git_tag = 'GIMP_{}_{}_{}{}'.format(major, minor, micro, rc_tag)
      src_pattern = 'https://gitlab.gnome.org/GNOME/gimp/-/jobs/artifacts/{}/raw/{}{}?job=dev-docs'
      url = src_pattern.format(git_tag, tarball_name, '')
      sha256url = src_pattern.format(git_tag, tarball_name, '.SHA256SUMS')
      sha512url = src_pattern.format(git_tag, tarball_name, '.SHA512SUMS')
      sha256sum, sha512sum, sha256_line, sha512_line = download_remotely(url, tarball_name, git_user,
                                                                         sha256url, sha512url)
      print('3. Updating SHA files on remote server')
      update_remote_sha(git_user, babl_pub_folder, sha256_line, sha512_line)
      num_files, prev_latest = get_files_info(git_user, babl_pub_folder, False)
      print('4. Publishing babl documentation')
      publish(git_user, babl_pub_folder, args.gimp_version,
              [ tarball_name ], [ 'SHA256SUMS', 'SHA512SUMS' ],
              num_files, prev_latest)
      print('5. Checking published babl documentation')
      check_existing(git_user, babl_pub_folder)

    # GEGL API
    gegl_pub_folder = gegl_pub_folder_pattern.format(gegl_major, gegl_minor)
    tarball_name    = 'gegl-api-docs-{}.{}.{}.tar.xz'.format(gegl_major, gegl_minor, gegl_micro)
    if check_sources(git_user, gegl_pub_folder, tarball_name):
      print("#### GEGL documentation")
      print('1. Checking already published GEGL docs')
      check_existing(git_user, gegl_pub_folder, True)
      print('2. Downloading GEGL docs on remote server')
      git_tag = 'GIMP_{}_{}_{}{}'.format(major, minor, micro, rc_tag)
      src_pattern = 'https://gitlab.gnome.org/GNOME/gimp/-/jobs/artifacts/{}/raw/{}{}?job=dev-docs'
      url = src_pattern.format(git_tag, tarball_name, '')
      sha256url = src_pattern.format(git_tag, tarball_name, '.SHA256SUMS')
      sha512url = src_pattern.format(git_tag, tarball_name, '.SHA512SUMS')
      sha256sum, sha512sum, sha256_line, sha512_line = download_remotely(url, tarball_name, git_user,
                                                                         sha256url, sha512url)
      print('3. Updating SHA files on remote server')
      update_remote_sha(git_user, gegl_pub_folder, sha256_line, sha512_line)
      num_files, prev_latest = get_files_info(git_user, gegl_pub_folder, False)
      print('4. Publishing GEGL documentation')
      publish(git_user, gegl_pub_folder, args.gimp_version,
              [ tarball_name ], [ 'SHA256SUMS', 'SHA512SUMS' ],
              num_files, prev_latest)
      print('5. Checking published GEGL documentation')
      check_existing(git_user, gegl_pub_folder)

  print()
  print("## Starting AppImage publishing")
  print()

  if args.noappimage:
    print('1. Skipping publishing AppImage packages per --no-appimage option')
  else:
    aarch64_package_name = 'GIMP-{}.{}.{}{}-aarch64.AppImage'.format(major, minor, micro, rc_string)
    x86_64_package_name  = 'GIMP-{}.{}.{}{}-x86_64.AppImage'.format(major, minor, micro, rc_string)
    linux_pub_folder = os.path.join(pub_folder, "linux")
    git_tag = 'GIMP_{}_{}_{}{}'.format(major, minor, micro, rc_tag)

    aarch64_torrent_path = aarch64_package_name + '.torrent'
    if os.path.exists(aarch64_torrent_path):
      full_torrent_path = os.path.abspath(aarch64_torrent_path)
      sys.stderr.write('Torrent file already exists for this version: {}\n'.format(full_torrent_path))
      sys.stderr.write('If this is on purpose, please delete it manually.\n')
      sys.stderr.write('Aborting.\n')
      sys.exit(os.EX_USAGE)
    x86_64_torrent_path = aarch64_package_name + '.torrent'
    if os.path.exists(x86_64_torrent_path):
      full_torrent_path = os.path.abspath(x86_64_torrent_path)
      sys.stderr.write('Torrent file already exists for this version: {}\n'.format(full_torrent_path))
      sys.stderr.write('If this is on purpose, please delete it manually.\n')
      sys.stderr.write('Aborting.\n')
      sys.exit(os.EX_USAGE)

    jsonfile['appimage'] = [ ]
    linux_relpath = 'gimp/v{}.{}/linux'.format(major, minor)

    print('1. Checking already published AppImage packages')
    check_existing(git_user, linux_pub_folder, True)

    if check_sources(git_user, linux_pub_folder, x86_64_package_name):
      print("### x86_64 AppImage package")
      print('2. Downloading AppImage packages on remote server')
      src_pattern = 'https://gitlab.gnome.org/GNOME/gimp/-/jobs/artifacts/{}/raw/{}{}?job=dist-appimage-weekly'
      url = src_pattern.format(git_tag, 'build/linux/appimage/_Output/', x86_64_package_name)
      sha256url = src_pattern.format(git_tag, 'build/linux/appimage/_Output/', x86_64_package_name + '.SHA256SUMS')
      sha512url = src_pattern.format(git_tag, 'build/linux/appimage/_Output/', x86_64_package_name + '.SHA512SUMS')
      sha256sum, sha512sum, sha256_line, sha512_line = download_remotely(url, x86_64_package_name, git_user,
                                                                         # TODO: use the SHA files when they are fixed.
                                                                         #sha256url, sha512url)
                                                                         None, None)

      print('3. Downloading locally {} from {}\n'.format(x86_64_package_name, url))
      local_path = download(url, tempdir, x86_64_package_name)
      print("\tx86_64 AppImage temporarily stored at: {}".format(local_path))

      print('4. Computing binary checksums')
      compare_digests(local_path, sha256sum, sha512sum)

      print('5. Creating torrent file')
      torrent_path = create_torrent(local_path,
                                    'AppImage for Linux on x86_64',
                                    args.gimp_version, 0,
                                    '', linux_relpath)
      print("   * Torrent created: {}".format(torrent_path))
      print("   * This file won't be deleted. You may inspect it with the following command:")
      print("     transmission-show {}".format(torrent_path))

      print('6. Uploading torrent file')
      upload_torrent(git_user, torrent_path)

      print('7. Updating SHA files on remote server')
      update_remote_sha(git_user, linux_pub_folder, sha256_line, sha512_line)
      num_files, prev_latest = get_files_info(git_user, linux_pub_folder, True)

      print('8. Publishing x86_64 AppImage package')
      publish(git_user, linux_pub_folder, args.gimp_version,
              [x86_64_package_name, torrent_path], [ 'SHA256SUMS', 'SHA512SUMS' ],
              num_files, prev_latest)

      json_dict = {
                    'date': datetime.date.today().isoformat(),
                    'filename': x86_64_package_name,
                    'sha512': sha512sum,
                    'sha256': sha256sum,
                    'build-id': 'org.gimp.GIMP_official.x86_64',
                    'min-support': 'Debian 12'
                  }
      jsonfile['appimage'] += [ json_dict ]

    if check_sources(git_user, linux_pub_folder, aarch64_package_name):
      print("### AArch64 AppImage package")
      print('2. Downloading AppImage packages on remote server')
      src_pattern = 'https://gitlab.gnome.org/GNOME/gimp/-/jobs/artifacts/{}/raw/{}{}?job=dist-appimage-weekly'
      url = src_pattern.format(git_tag, 'build/linux/appimage/_Output/', aarch64_package_name)
      sha256url = src_pattern.format(git_tag, 'build/linux/appimage/_Output/', aarch64_package_name + '.SHA256SUMS')
      sha512url = src_pattern.format(git_tag, 'build/linux/appimage/_Output/', aarch64_package_name + '.SHA512SUMS')
      sha256sum, sha512sum, sha256_line, sha512_line = download_remotely(url, aarch64_package_name, git_user,
                                                                         # TODO: use the SHA files when they are fixed.
                                                                         #sha256url, sha512url)
                                                                         None, None)

      print('3. Downloading locally {} from {}\n'.format(aarch64_package_name, url))
      local_path = download(url, tempdir, aarch64_package_name)
      print("\tAArch64 AppImage temporarily stored at: {}".format(local_path))

      print('4. Computing binary checksums')
      compare_digests(local_path, sha256sum, sha512sum)

      print('5. Creating torrent file')
      torrent_path = create_torrent(local_path,
                                    'AppImage for Linux on AArch64',
                                    args.gimp_version, 0,
                                    '', linux_relpath)
      print("   * Torrent created: {}".format(torrent_path))
      print("   * This file won't be deleted. You may inspect it with the following command:")
      print("     transmission-show {}".format(torrent_path))

      print('6. Uploading torrent file')
      upload_torrent(git_user, torrent_path)

      print('7. Updating SHA files on remote server')
      update_remote_sha(git_user, linux_pub_folder, sha256_line, sha512_line)
      num_files, prev_latest = get_files_info(git_user, linux_pub_folder, True)

      print('8. Publishing AArch64 AppImage package')
      publish(git_user, linux_pub_folder, args.gimp_version,
              [aarch64_package_name, torrent_path], [ 'SHA256SUMS', 'SHA512SUMS' ],
              num_files, prev_latest)

      json_dict = {
                    'date': datetime.date.today().isoformat(),
                    'filename': x86_64_package_name,
                    'sha512': sha512sum,
                    'sha256': sha256sum,
                    'build-id': 'org.gimp.GIMP_official.arm64',
                    'min-support': 'Debian 12'
                  }
      jsonfile['appimage'] += [ json_dict ]

    print('9. Checking published Linux packages')
    check_existing(git_user, linux_pub_folder)

  if win_url is not None:
    print()
    print("## Starting Windows release ##")
    print()
    win_pub_folder = os.path.join(pub_folder, "windows")

    if win_rev == 0:
      bin_name = 'gimp-{}-setup.exe'.format(args.gimp_version)
    else:
      bin_name = 'gimp-{}-setup-{}.exe'.format(args.gimp_version, win_rev)

    print('0. Checking already published Windows installers')
    check_existing(git_user, win_pub_folder, True)

    torrent_path = bin_name + '.torrent'
    if os.path.exists(torrent_path):
      full_torrent_path = os.path.abspath(torrent_path)
      sys.stderr.write('Torrent file already exists for this version: {}\n'.format(full_torrent_path))
      sys.stderr.write('If this is on purpose, please delete it manually.\n')
      sys.stderr.write('Aborting.\n')
      sys.exit(os.EX_USAGE)

    print('1. Downloading on remote server')
    sha256sum, sha512sum, sha256_full_line, sha512_full_line = download_remotely(win_url, bin_name, git_user)

    sys.stdout.write('2. Downloading locally {} from {}\n'.format(bin_name, win_url))
    local_path = download(win_url, tempdir, bin_name)
    print("\tWindows installer temporarily stored at: {}".format(local_path))

    print('3. Computing binary checksums')
    compare_digests(local_path, sha256sum, sha512sum)

    print('4. Creating torrent file')
    win_relpath = 'gimp/v{}.{}/windows'.format(major, minor)
    torrent_path = create_torrent(local_path,
                                  'Installer for Microsoft Windows - x86 32 and 64-bit, ARM 64-bit',
                                  args.gimp_version, win_rev, win_comment, win_relpath)
    print("   * Torrent created: {}".format(torrent_path))
    print("   * This file won't be deleted. You may inspect it with the following command:")
    print("     transmission-show {}".format(torrent_path))

    print('5. Uploading torrent file')
    upload_torrent(git_user, torrent_path)

    print('6. Updating remote SHA files')
    update_remote_sha(git_user, win_pub_folder, sha256_full_line, sha512_full_line)
    num_files, prev_latest = get_files_info(git_user, win_pub_folder)
    #os.unlink(torrent_path)

    print('7. Publishing')
    pub_version = args.gimp_version + ('' if win_rev == 0 else '-{}'.format(win_rev))
    publish(git_user, win_pub_folder, pub_version,
            [bin_name, torrent_path ], ['SHA256SUMS', 'SHA512SUMS'],
            num_files, prev_latest)

    print('8. Checking published Windows installers')
    check_existing(git_user, win_pub_folder)

    json_dict = {
                  'date': datetime.date.today().isoformat(),
                  'filename': bin_name,
                  'sha512': sha512sum,
                  'sha256': sha256sum,
                  'build-id': 'org.gimp.GIMP_official',
                  'min-support': 'Windows 10'
                }
    if win_rev != 0:
      json_dict['revision'] = int(win_rev)
      json_dict['comment']  = win_comment

    jsonfile['windows'] = [ json_dict ]

  macos_pub_folder = os.path.join(pub_folder, "macos")
  macos_relpath    = 'gimp/v{}.{}/macos'.format(major, minor)
  jsonfile['macos'] = [ ]
  if mac_intel_url is not None or mac_silicon_url is not None:
    print()
    print("## Starting macOS releases ##")
    print()
    print('0. Checking already published macOS installers')
    check_existing(git_user, macos_pub_folder, True)

    if mac_intel_url is not None:
      print()
      print("### Starting macOS release for Intel ###")
      print()

      old_bin_name = os.path.basename(mac_intel_url)
      if mac_intel_rev == 0:
        bin_name = 'gimp-{}-x86_64.dmg'.format(args.gimp_version)
      else:
        bin_name = 'gimp-{}-x86_64-{}.dmg'.format(args.gimp_version, mac_silicon_rev)

      torrent_path = bin_name + '.torrent'
      if os.path.exists(torrent_path):
        full_torrent_path = os.path.abspath(torrent_path)
        sys.stderr.write('Torrent file already exists for this version: {}\n'.format(full_torrent_path))
        sys.stderr.write('If this is on purpose, please delete it manually.\n')
        sys.stderr.write('Aborting.\n')
        sys.exit(os.EX_USAGE)

      print('1. Downloading on remote server')
      sha256url = mac_intel_url + '.sha256'
      sha256sum, sha512sum, sha256_full_line, sha512_full_line = download_remotely(mac_intel_url, bin_name, git_user,
                                                                                   sha256url=sha256url,
                                                                                   old_bin_name=old_bin_name)

      sys.stdout.write('2. Downloading locally {} from {}\n'.format(bin_name, mac_intel_url))
      local_path = download(mac_intel_url, tempdir, bin_name)
      print("\tmacOS DMG for Intel temporarily stored at: {}".format(local_path))

      print('3. Computing binary checksums')
      compare_digests(local_path, sha256sum, sha512sum)

      print('4. Creating torrent file')
      torrent_path = create_torrent(local_path,
                                    'DMG for macOS on Intel',
                                    args.gimp_version, mac_intel_rev,
                                    mac_intel_comment, macos_relpath)
      print("   * Torrent created: {}".format(torrent_path))
      print("   * This file won't be deleted. You may inspect it with the following command:")
      print("     transmission-show {}".format(torrent_path))

      print('5. Uploading torrent file')
      upload_torrent(git_user, torrent_path)

      print('6. Updating remote SHA files')
      update_remote_sha(git_user, macos_pub_folder, sha256_full_line, sha512_full_line)
      num_files, prev_latest = get_files_info(git_user, macos_pub_folder)
      #os.unlink(torrent_path)

      print('7. Publishing')
      pub_version = args.gimp_version + ('' if mac_intel_rev == 0 else '-{}'.format(mac_intel_rev))
      publish(git_user, macos_pub_folder, pub_version,
              [bin_name, torrent_path ], ['SHA256SUMS', 'SHA512SUMS'],
              num_files, prev_latest)

      json_dict = {
                    'date': datetime.date.today().isoformat(),
                    'filename': bin_name,
                    'sha512': sha512sum,
                    'sha256': sha256sum,
                    'build-id': 'org.gimp.GIMP_official.x86_64',
                    'min-support': 'macOS 11 Big Sur'
                  }

      if mac_intel_rev != 0:
        json_dict['revision'] = int(mac_intel_rev)
        json_dict['comment']  = mac_intel_comment

      jsonfile['macos'] += [ json_dict ]

    if mac_silicon_url is not None:
      print()
      print("### Starting macOS release for Apple Silicon ###")
      print()

      if mac_intel_url is None:
        check_latest = True
      else:
        # Only do this once for the whole macos/ directory.
        check_latest = False

      old_bin_name = os.path.basename(mac_silicon_url)
      if mac_silicon_rev == 0:
        bin_name = 'gimp-{}-arm64.dmg'.format(args.gimp_version)
      else:
        bin_name = 'gimp-{}-arm64-{}.dmg'.format(args.gimp_version, mac_intel_rev)

      torrent_path = bin_name + '.torrent'
      if os.path.exists(torrent_path):
        full_torrent_path = os.path.abspath(torrent_path)
        sys.stderr.write('Torrent file already exists for this version: {}\n'.format(full_torrent_path))
        sys.stderr.write('If this is on purpose, please delete it manually.\n')
        sys.stderr.write('Aborting.\n')
        sys.exit(os.EX_USAGE)

      print('1. Downloading on remote server')
      sha256url = mac_silicon_url + '.sha256'
      sha256sum, sha512sum, sha256_full_line, sha512_full_line = download_remotely(mac_silicon_url, bin_name, git_user,
                                                                                   sha256url=sha256url,
                                                                                   old_bin_name=old_bin_name)

      sys.stdout.write('2. Downloading locally {} from {}\n'.format(bin_name, mac_silicon_url))
      local_path = download(mac_silicon_url, tempdir, bin_name)
      print("   * macOS DMG for Apple Silicon temporarily stored at: {}".format(local_path))

      print('3. Computing binary checksums')
      compare_digests(local_path, sha256sum, sha512sum)

      print('4. Creating torrent file')
      torrent_path = create_torrent(local_path,
                                    'DMG for macOS on Apple Silicon',
                                    args.gimp_version, mac_silicon_rev,
                                    mac_silicon_comment, macos_relpath)
      print("   * Torrent created: {}".format(torrent_path))
      print("   * This file won't be deleted. You may inspect it with the following command:")
      print("     transmission-show {}".format(torrent_path))

      print('5. Uploading torrent file')
      upload_torrent(git_user, torrent_path)

      print('6. Updating remote SHA files')
      update_remote_sha(git_user, macos_pub_folder, sha256_full_line, sha512_full_line)
      num_files, prev_latest = get_files_info(git_user, macos_pub_folder, check_latest=check_latest)
      #os.unlink(torrent_path)

      print('7. Publishing')
      pub_version = args.gimp_version + ('' if mac_silicon_rev == 0 else '-{}'.format(mac_silicon_rev))
      publish(git_user, macos_pub_folder, pub_version,
              [bin_name, torrent_path ], ['SHA256SUMS', 'SHA512SUMS'],
              num_files, prev_latest)

      json_dict = {
                    'date': datetime.date.today().isoformat(),
                    'filename': bin_name,
                    'sha512': sha512sum,
                    'sha256': sha256sum,
                    'build-id': 'org.gimp.GIMP_official.arm64',
                    'min-support': 'macOS 11 Big Sur'
                  }
      if mac_silicon_rev != 0:
        json_dict['revision'] = int(mac_silicon_rev)
        json_dict['comment']  = mac_silicon_comment

      jsonfile['macos'] += [ json_dict ]

    print('8. Checking published macOS installers')
    check_existing(git_user, macos_pub_folder)

print("All done! Launch was a success!🎉🥂")

print("The following json can be added to content/gimp_versions.json:")
print(json.dumps(jsonfile, indent=4))

sys.exit(os.EX_OK)
