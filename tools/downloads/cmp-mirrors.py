#!/usr/bin/env python3

import argparse
import json
import os
import sys

def cmp_mirror_files(mirrorsfile, mirrorsjson):
  '''
  mirrorsfile is a file containing each mirrors as a https URL, one per
  line. This file is usually updated automatically from remote webserver
  configuration files through tools/downloads/update-mirrors.py

  mirrorsjson is a manually curated json file listing all the mirrors
  with their official entity names, an info URL and the location
  information.

  This function simply compare the files to make sure that every mirror
  from one is in the other, no more, no less.
  '''
  with open(mirrorsjson, 'r') as f:
    mirrors = json.load(f)

  missing_in_json = []

  with open(mirrorsfile, 'r') as f:
    for line in f:
      line = line.strip()
      if line == 'https://download.gimp.org/gimp/':
        # Not a mirror, the source server.
        continue
      for name in mirrors:
        if line in mirrors[name]['mirrors']:
          if len(mirrors[name]['mirrors']) > 1:
            mirrors[name]['mirrors'].remove(line)
          else:
            del mirrors[name]
          break
      else:
        missing_in_json += [line]

  if len(missing_in_json) > 0:
    sys.stderr.write('The following {} mirrors are not listed in {}:\n'.format(len(missing_in_json),
                                                                               mirrorsjson))
    for missing in missing_in_json:
      sys.stderr.write('\t{}\n'.format(missing))
    sys.stderr.write('\n')

  if len(mirrors) > 0:
    sys.stderr.write('The following {} mirrors should be removed from {}:\n'.format(len(mirrors),
                                                                                    mirrorsjson))
    for name in mirrors:
      sys.stderr.write('\t{}\n'.format(name))

  if len(missing_in_json) > 0 or len(mirrors) > 0:
    return False
  else:
    return True

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Update the list of mirrors as per download.gimp.org configuration.')
  parser.add_argument('--mirrorsfile', metavar='<file>', default=os.path.dirname(__file__) + '/downloads.http.txt',
                      help='A file with one download mirror per line.')
  parser.add_argument('--mirrorsjson', metavar='<file>',
                      default=os.path.dirname(__file__) + '/../../content/downloads/mirrors.json',
                      help='Json file with additional data per mirror (manually curated).')

  args = parser.parse_args()

  if cmp_mirror_files(args.mirrorsfile, args.mirrorsjson):
    sys.exit(os.EX_OK)
  else:
    sys.exit(os.EX_DATAERR)
