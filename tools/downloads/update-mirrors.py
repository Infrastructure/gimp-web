#!/usr/bin/env python3

import argparse
import importlib
import getpass
import gimputils.mgo
import json
import os
import os.path
import sys

parser = argparse.ArgumentParser(description='Update the list of mirrors as per download.gimp.org configuration.')
parser.add_argument('--mirrorsfile', metavar='<file>', default=os.path.dirname(__file__) + '/downloads.http.txt',
                    help='A file with one download mirror per line, either https:// or http://. This script will override the existing file.')

# (0) Ask for git user to use.
git_user = getpass.getpass("What is your git user on master.gnome.org? ")
git_user = git_user.strip()

if git_user == '':
  sys.stderr.write("No git user. Aborting.\n")
  sys.exit(os.EX_DATAERR)

args = parser.parse_args()

mirrors = gimputils.mgo.get_mirrors(git_user)

with open(args.mirrorsfile, 'wb') as f:
  for http in mirrors:
    f.write(http.encode('utf-8') + b'\n')

print("Mirrors updated in {}.".format(args.mirrorsfile))

mirrorsjson = os.path.dirname(__file__) + '/../../content/downloads/mirrors.json'
mirrorsjson = os.path.realpath(mirrorsjson)
print("Now comparing to {} ...".format(mirrorsjson))

# TODO: mirrorbits also contains the GeoIP detected code in the CountryCodes
# field of `mirrorbits show <mirror-id>`. We should compare it with the declared
# country.
cmp = importlib.import_module("cmp-mirrors")
try:
  same = cmp.cmp_mirror_files(args.mirrorsfile, mirrorsjson)
except json.decoder.JSONDecodeError as error:
  sys.stderr.write("Invalid JSON file '{}': {}\n".format(mirrorsjson, error))
  sys.exit(os.EX_DATAERR)

if same:
  print("Both files look to match. Please double-check before committing the change.")
  sys.exit(os.EX_OK)
else:
  # cmp.cmp_mirror_files() will already have outputted the errors.
  sys.exit(os.EX_DATAERR)
