#!/usr/bin/env python3

"""
download-plot -- Script to generate a graph of GIMP downloads
Copyright (C) 2022 Jehan

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

Usage: tools/downloads/download-plot.py  --start=2022-08-29
"""

import argparse
import datetime
import gimputils.oc
import gimputils.version
import locale
import matplotlib.pyplot
import os
import sys

parser = argparse.ArgumentParser(description='Generate download graphs')
parser.add_argument('--oc-token', metavar='<oc-token>', default=None, required=False,
                    help='Your OpenShift connection token.')
parser.add_argument('--keep-oc', action=argparse.BooleanOptionalAction,
                    help='Keep or delete the downloaded oc client.')
parser.add_argument('--no-download-oc', dest='download_oc', action='store_false',
                    help='Use a previously downloaded oc client. It implies --keep-oc unless --no-keep-oc is set.')

parser.add_argument('--gimp-version', metavar='<gimp-version>', default=None, required=False,
                    help='The version of GIMP to generate stats from')
parser.add_argument('--start', metavar='<start-date>', default=None, required=False,
                    help='The first day of statistics')
parser.add_argument('--end', metavar='<end-date>', default=None, required=False,
                    help='The last day of statistics')

args = parser.parse_args()

# Process GIMP versions.
gimp_version = args.gimp_version
if gimp_version is None:
  (tarball, _, _), (win_installer, _, _), (macos_dmg, _, _) = gimputils.version.find_latest()
  stable = True
else:
  major, minor, micro, stable = gimputils.version.validate(gimp_version)
  base_path = 'gimp/v{}.{}/'.format(major, minor)
  tarball = '{}gimp-{}.{}.{}.tar.{}'.format(base_path,
                                            major, minor, micro,
                                            'xz' if not stable and micro >= 12 else 'bz2')
  # TODO: handle revisions!
  win_installer = '{}windows/gimp-{}.{}.{}-setup.exe'.format(base_path, major, minor, micro)
  if major >= 3 or (not stable and major == 2 and minor == 99):
    macos_dmg = '{}macos/gimp-{}.{}.{}-x86_64.dmg'.format(base_path, major, minor, micro)
  else:
    macos_dmg = '{}osx/gimp-{}.{}.{}-x86_64.dmg'.format(base_path, major, minor, micro)

# TODO: get flathub URL depending on 'stable" value in order to include
# flathub statistics in there.

# Validate start and end dates
end = args.end
if end is None:
  # Stats up to yesterday by default (today's stats will be incomplete
  # by nature so including these in a graph would be misleading).
  end = (datetime.date.today() - datetime.timedelta(days=1)).isoformat()
else:
  try:
    datetime.datetime.fromisoformat(end)
  except ValueError:
    sys.stderr.write("Invalid ISO format end date: {}\n".format(end))
    sys.exit(os.EX_DATAERR)

start = args.start
if start is None:
  iso_end = datetime.datetime.fromisoformat(end)
  iso_start = iso_end.replace(day=1, month=1)
  start = iso_start.date().isoformat()
else:
  try:
    s = datetime.datetime.fromisoformat(start)
  except ValueError:
    sys.stderr.write("Invalid ISO format start date: {}\n".format(start))
    sys.exit(os.EX_DATAERR)
  e = datetime.datetime.fromisoformat(end)

  if s > e:
    sys.stderr.write("End date cannot be older than start date: {}\n".format(start))
    sys.exit(os.EX_DATAERR)

# TODO: we should add "special" events in the graph, such as release
# dates, not only of the GIMP version, but also of specific revisions of
# an installer or DMG (taken from content/gimp_versions.json).
# And what about flatpak updates?

# Connect to OpenShift
keep_oc = args.keep_oc
try:
  keep_oc = gimputils.oc.setup(keep_oc, args.download_oc)
  podname = gimputils.oc.connect(args.oc_token)
except gimputils.oc.Error as err:
  sys.stderr.write(err.message)
  sys.exit(err.code)


# Obtaining statistics for Windows and macOS
print('Obtaining statistics…')

win_downloads   = []
macos_downloads = []
labels          = []

day = datetime.datetime.fromisoformat(start)
end_date = datetime.datetime.fromisoformat(end)
while day <= end_date:
  stats = gimputils.oc.get_stats(podname, win_installer, day.date().isoformat())
  win_downloads += [stats]
  stats = gimputils.oc.get_stats(podname, macos_dmg, day.date().isoformat())
  macos_downloads += [stats]
  labels += [day.date()]
  #if len(labels) == 0:
    #labels += ['{}/{}/{}'.format(day.year, day.month, day.day)]
  #else:
    #labels += ['{}/{}'.format(day.month, day.day) if day.day == 1 else '{}'.format(day.day)]
  day += datetime.timedelta(days=1)

gimputils.oc.cleanup(keep_oc)

# TODO: Obtaining data from flathub
# Do we also want to grab tarball download stats?

color = 'tab:red'
fig, ax = matplotlib.pyplot.subplots()

# Shared X axis.
ax.set_title('Daily downloads')
ax.set_xlabel('Dates')

locale.setlocale(locale.LC_ALL, 'en_US')

# Y axis for Windows
ax.set_ylabel('Windows downloads', color=color)
win_plots, = ax.plot(labels, win_downloads, label='Windows', color=color)
ax.ticklabel_format(axis='y', style='plain', useLocale=True)
ax.tick_params(axis='y', labelcolor=color, color=color)

# Y axis for macOS
color = 'tab:blue'
ax_macos = ax.twinx()
ax_macos.set_ylabel('macOS downloads', color=color)
mac_plots, = ax_macos.plot(labels, macos_downloads, label='macOS', color=color)
ax_macos.ticklabel_format(axis='y', style='plain', useLocale=True)
ax_macos.tick_params(axis='y', labelcolor=color, color=color)

ax_macos.legend(handles=[win_plots, mac_plots])

#fig.tight_layout()
matplotlib.pyplot.show()
