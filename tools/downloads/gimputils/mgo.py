#!/usr/bin/env python3

import os
import subprocess

"""
gimputils.oc -- Module to interact with master.gimp.org server.
Copyright (C) 2024 Jehan

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

server = 'master.gimp.org'

class Error(Exception):
  '''
  Exception raised by API in this module.

  Attributes:
      code: exit code which we advise to use with sys.exit() if you
            consider this a fatale error.
      message: message to output on stderr.
  '''

  def __init__(self, code, message):
      self.code = code
      self.message = message
      super().__init__(self.message)

def get_mirrors(git_user):
  print('Obtaining mirror list…')
  stdin = 'sudo -u podman sh -c "cd /home/podman && podman exec -it mirrorbits mb list  -http -rsync -ftp" && exit\n'
  destination = '{}@{}'.format(git_user, server)
  try:
    p = subprocess.run(['ssh', '-tt', destination], timeout=20,
                       input=stdin, capture_output=True, text=True,
                       check=True)
    lines = p.stdout.split('\n')

    for l in lines:
      if l.startswith('Identifier'):
        lines = lines[lines.index(l)+1:]
        break

    for l in lines:
      if l.startswith('logout'):
        lines = lines[:lines.index(l)]
        break

    # Alphabetically ordered mirror list.
    mirrors = [ line.split()[1] for line in lines if line.strip() != '' ]
    mirrors.sort()
    return mirrors
  except subprocess.CalledProcessError:
    raise Error(os.EX_UNAVAILABLE, 'Failed to obtain mirror list from mirrorbits.\n')
