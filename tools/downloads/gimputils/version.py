#!/usr/bin/env python3

"""
gimputils.version -- Module to process GIMP's versions
Copyright (C) 2022 Jehan

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import json
import os
import re
import requests

class Error(Exception):
  '''
  Exception raised by API in this module.

  Attributes:
      code: exit code which we advise to use with sys.exit() if you
            consider this a fatale error.
      message: message to output on stderr.
  '''

  def __init__(self, error, message):
      self.code = code
      self.message = message
      super().__init__(self.message)

def validate(version):
  match = re.match('^([0-9]+)\\.([0-9]+)\\.([0-9]+)$', version)
  if match is None:
    raise Error(os.EX_USAGE, 'Invalid version {}'.format(version))
  else:
    major = int(match.group(1))
    minor = int(match.group(2))
    micro = int(match.group(3))
    stable = (minor % 2 == 0)
    return major, minor, micro, stable

def find_latest():
  # Parse the json file https://www.gimp.org/gimp_versions.json
  # We used to parse the 0.0_LATEST-IS-x.y.z file but it is not safe enough
  # anymore as the download server now always redirects to a mirror. It meant
  # that the mirror may be outdated, or worse that it may lie (there are some
  # mitigations for these issues with rsync/ftp checks, but this is not
  # foolproof and targetted attack may happen).
  latest   = []
  url      = 'https://www.gimp.org/gimp_versions.json'
  versions = requests.get(url).text
  versions = json.loads(versions)
  stable   = versions['STABLE'][0]
  version  = stable['version']
  date     = stable['date']

  # Find latest series.
  major = 0
  minor = 0
  pattern = re.compile('([0-9]+)\\.([0-9]+)\\.([0-9]+)')
  m = pattern.match(version)
  major = m.group(1)
  minor = m.group(2)
  micro = m.group(3)

  base_path = 'gimp/v{}.{}/'.format(major, minor)

  # Find latest tarball.
  source_file = stable['source'][0]['filename']
  checksum = stable['source'][0]['sha256']
  latest = [ (base_path + source_file, checksum, date) ]

  # Find latest Windows installer
  if 'windows' in stable:
    win_file = stable['windows'][0]['filename']
    checksum = stable['windows'][0]['sha256']
    date     = stable['windows'][0]['date']
    latest  += [ (base_path + 'windows/' + win_file, checksum, date) ]
  else:
    sys.stderr.write("WARNING: no Windows installer.\n\n")

  # Find latest macOS DMG
  if 'macos' in stable:
    mac_file = stable['macos'][0]['filename']
    checksum = stable['macos'][0]['sha256']
    date     = stable['macos'][0]['date']
    if (major == '2' and minor == '99') or int(major) > 2:
      latest += [ (base_path + 'macos/' + mac_file, checksum, date) ]
    else:
      # Note that macos/ and osx/ are symlinks of each others, but the real path
      # is import for mirrorbits statistics. So I get the right non-symlink
      # path, which changed for GIMP 2.99.
      latest += [ (base_path + 'osx/' + mac_file, checksum, date) ]
  else:
    sys.stderr.write("WARNING: no macOS DMG package.\n\n")

  return latest
