#!/usr/bin/env python3

"""
gimputils.oc -- Module to interact with GIMP's OpenShift instance
Copyright (C) 2022 Jehan

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os
import requests
import subprocess
import sys
import tarfile

class Error(Exception):
  '''
  Exception raised by API in this module.

  Attributes:
      code: exit code which we advise to use with sys.exit() if you
            consider this a fatale error.
      message: message to output on stderr.
  '''

  def __init__(self, code, message):
      self.code = code
      self.message = message
      super().__init__(self.message)

oc = os.path.dirname(__file__) + '/oc'

def setup(keep_oc, download_oc):
  '''
  Setting up the local oc environment.

  Returns: an updated keep_oc value to be used with cleanup() function once you
  are done.
  '''
  oc_tar = oc + '.tar'

  if keep_oc is None:
    if download_oc:
      keep_oc = False
    else:
      keep_oc = True

  if download_oc:
    if os.path.exists(oc):
      raise Error(os.EX_CANTCREAT,
                  'The file "{}" already exists. Please delete it or run with: --no-download-oc\n'.format(oc))
    if os.path.exists(oc_tar):
      raise Error(os.EX_CANTCREAT,
                 'The file "{}" already exists. Please delete it.\n'.format(oc_tar))

    print('Downloading OpenShift client…')
    with requests.get("https://downloads-openshift-console.apps.openshift4.gnome.org/amd64/linux/oc.tar", stream=True) as r:
      with open(oc_tar, 'wb') as fd:
        for chunk in r.iter_content(chunk_size=65536):
          fd.write(chunk)

    print('Extracting OpenShift client…')
    with tarfile.open(oc_tar) as tar:
      tar.extract('oc', os.path.dirname(__file__))

    os.remove(oc_tar)

  if not os.path.exists(oc):
    if download_oc:
      err_msg = "The file '{}' didn't exists. Download failed.\n".format(oc)
    else:
      err_msg = "The file '{}' didn't exists. Do not use --no-download-oc option.\n".format(oc)
    raise Error(os.EX_UNAVAILABLE, err_msg)

  return keep_oc

def cleanup(keep_oc):
  if not keep_oc:
    os.remove(oc)

def connect(oc_token):
  '''
  Connecting to OpenShift.

  Returns: the podname to use in further commands.
  '''
  if oc_token is not None:
    print('Logging-in to OpenShift…\n')
    try:
      subprocess.check_call(oc + " login --token={} --server=https://api.openshift4.gnome.org:6443".format(oc_token),
                            shell=True)
    except subprocess.CalledProcessError:
      raise Error(os.EX_NOUSER, 'Failed connection. Check your token.\n')

  print('Switch to GIMP project…')
  try:
    subprocess.check_call(oc + " project gimp", shell=True)
  except subprocess.CalledProcessError:
    err_msg = '\nOpenShift command failed. Are you logged-in?\n'
    err_msg += '* Go to https://console-openshift-console.apps.openshift4.gnome.org/\n'
    err_msg += '* Log-in in your browser.\n'
    err_msg += '* Click your name in top-right of OpenShift page.\n'
    err_msg += '* Select "Copy login command".\n'
    err_msg += '* Click "Display Token".\n'
    err_msg += '* Call me again adding --oc-token <the-copied-token>.\n\n'
    err_msg += 'If you are logged-in already, failure to switch to "gimp" project may indicate an access issue.\n'
    raise Error(os.EX_NOUSER, err_msg)

  sys.stdout.write('Obtaining last pod name…')
  try:
    output = subprocess.check_output(oc + " get pods --selector app=mirrorbits", shell=True)
    lines = output.decode('utf-8').split('\n')
    podname = [ line.split() for line in lines if line.startswith('mirrorbits-') ][-1][0]
  except subprocess.CalledProcessError:
    err_msg = '\nOpenShift command failed. Are you logged-in?\n'
    err_msg += '* Go to https://console-openshift-console.apps.openshift4.gnome.org/\n'
    err_msg += '* Log-in in your browser.\n'
    err_msg += '* Click your name in top-right of OpenShift page.\n'
    err_msg += '* Select "Copy login command".\n'
    err_msg += '* Click "Display Token".\n'
    err_msg += '* Call me again adding --oc-token <the-copied-token>.\n'
    raise Error(os.EX_NOUSER, err_msg)

  print(' {}'.format(podname))
  return podname

def get_mirrors(podname):
  print('Obtaining mirror list…')
  try:
    output = subprocess.check_output(oc + " exec {}  -- mb list -http -rsync -ftp".format(podname), shell=True)
    lines = output.decode('utf-8').split('\n')

    for l in lines:
      if l.startswith('Identifier'):
        lines = lines[lines.index(l)+1:]
        break
    # Alphabetically ordered mirror list.
    mirrors = [ line.split()[1] for line in lines if line.strip() != '' ]
    mirrors.sort()
    return mirrors
  except subprocess.CalledProcessError:
    raise Error(os.EX_UNAVAILABLE, 'OpenShift command failed. Please check the code.\n')

def get_stats(podname, file, day):
  if file[0] != '/':
    # mirrorbits returns output with prepended slash.
    file = '/' + file
  try:
    # mb stats file /gimp/v2.10/windows/gimp-2.10.32-setup.exe
    output = subprocess.check_output(oc + " exec {}  -- mb stats -start-date {} -end-date {} file {}".format(podname, day, day, file), shell=True)
    lines = output.decode('utf-8').split('\n')
    stats = None
    for l in lines:
      if l.startswith('{}:'.format(file)):
        stats = int(l.split()[1])
        break
    if stats is None:
      raise Error(os.EX_UNAVAILABLE, 'No stats for "{}".\n'.format(file))
    return stats
  except subprocess.CalledProcessError:
    raise Error(os.EX_UNAVAILABLE, 'OpenShift command failed. Please check the code.\n')
