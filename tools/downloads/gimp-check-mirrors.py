#!/usr/bin/env python3

import gimputils.version
import os
import argparse
import concurrent.futures
import fileinput
import hashlib
import re
import requests
import sys
import urllib.parse
import urllib3.exceptions

# argparse for mirrorsfile and uri
parser = argparse.ArgumentParser(description='Check if GIMP download mirrors have a file from download.gimp.org.')
parser.add_argument('--mirrorsfile', metavar='<file>', default=os.path.dirname(__file__) + '/downloads.http.txt',
                    help='A file with one download mirror per line, either https:// or http://. Each line is expected to point to the equivalent of https://download.gimp.org/gimp/, as some mirrors only have that directory structure.')
parser.add_argument('--verify-checksum', dest='verify_checksum', action='store_true',
                    help='Whether to do a deep verification by validating identical checksums.')
parser.add_argument(dest='uris', metavar='<uri>', nargs='*',
                    help='One or more URIs pointing to the file on download.gimp.org, e.g. https://download.gimp.org/gimp/v2.10/gimp-2.10.20.tar.bz2')

args = parser.parse_args()

def load_url(url, timeout):
    with requests.head(url, timeout=timeout) as conn:
        return conn

def verify_remote(uri, checksum):
  success     = False
  status      = None
  checksum_ok = None
  headers = {'User-Agent': 'Mozilla/5.0'}
  try:
    if checksum is not None:
      with requests.get(uri, stream=True, headers=headers) as response:
        # I don't think the chunk_size is useful, since docs
        # says that "stream=True will read data as it arrives
        # in whatever size the chunks are received" which is
        # the ideal way. But if it doesn't, let's use 2**16 as
        # a reasonable chunk size to process.
        status = str(response.status_code)
        if response.status_code == 200:
          # Download the file.
          domain = urllib.parse.urlparse(uri).netloc
          dirname = 'mirrors/{}/'.format(domain)
          os.makedirs(dirname, exist_ok=True)
          filename = '{}/{}'.format(dirname, os.path.basename(uri))
          with open(filename, 'wb') as fd:
            for chunk in response.iter_content(chunk_size=65536):
              fd.write(chunk)

          # Compute the file checksum.
          m = hashlib.sha256()
          with open(filename, "rb") as fd:
            # Since Python 3.11, we can simply do:
            # m = hashlib.file_digest(fd, "sha256")
            # But it's a bit too recent Python version so let's be more generic.
            chunk = fd.read(65536)
            while chunk:
              m.update(chunk)
              chunk = fd.read(65536)
          checksum_ok = False
          if isinstance(checksum, str):
            if m.hexdigest() == checksum:
              checksum_ok = True
              success = True
          else:
            # The checksum here is actually a concurrent.futures.Future.
            checksum = checksum.result()
            if m.digest() == checksum:
              checksum_ok = True
              success = True

          # Delete file unless the checksum was different.
          if checksum_ok:
            os.remove(filename)
            try:
              os.rmdir(dirname)
            except OSError:
              # Some other thread may still be checking a file from the same
              # mirror, or simply left a file which was corrupted.
              pass
    else:
      response = requests.head(url=uri, timeout=20, allow_redirects=True, headers=headers)
      status = str(response.status_code)
      if response.status_code == 200:
        success = True
  except requests.exceptions.ConnectionError as error:
    status = 'Connection error'
  except requests.exceptions.ConnectTimeout as error:
    status = 'Connection timed out'
  except requests.exceptions.ReadTimeout as error:
    status = 'Read timed out'
  except requests.exceptions.TooManyRedirects as error:
    status = 'Too many redirects'
  except requests.exceptions.ChunkedEncodingError as error:
    # Somehow when a "Connection reset by peer" happens, a ConnectionResetError
    # exception happens, but the requests package apparently doesn't handle this
    # appropriately and it chain-reacts into a urllib3.exceptions.ProtocolError
    # then requests.exceptions.ChunkedEncodingError errors. Though I am still
    # unsure if this is really expected (since the error is not too accurate nor
    # explicit). Moreover using error.strerror just returns None. I couldn't
    # find a proper API to this exception to get the text error message, though
    # the first argument seems to be a proper-enough error message. So that's
    # what I use.
    status = str(error.args[0])
  except requests.exceptions.RequestException as error:
    # The generic parent exception for the requests package.
    status = str(error.args[0])
  except ConnectionResetError:
    status = "Connection reset by peer"
  except urllib3.exceptions.ProtocolError as error:
    status = str(error.args[0])
  except OSError as error:
    status = str(error.strerror)

  return uri, success, status, checksum_ok

local_uris = []
origin_checksums = None
if len(args.uris) == 0:
    print("No URIs given as argument. Trying to guess the last packages.")
    latest_versions = gimputils.version.find_latest()
    origin_checksums = {}
    for (u, c, d) in latest_versions:
      local_uris += [ (u, d) ]
      origin_checksums[u] = c
else:
    # get local path
    for uri in args.uris:
        dgo_uri = uri
        dgo_uri_local = dgo_uri.replace('https://download.gimp.org/', '')
        dgo_uri_local = dgo_uri_local.replace('https://download.gimp.org/mirror/pub/', '')
        local_uris += [ (dgo_uri_local, None) ]

failed_checks = 0
error_count   = 0
check_count   = 0

def get_checksum(local_uri):
  origin_sum = hashlib.sha256()
  with requests.get('https://download.gimp.org/' + local_uri, stream=True) as response:
    for line in response.iter_content(chunk_size=65536, decode_unicode=False):
      origin_sum.update(line)
  return origin_sum.digest()

origin_executor  = None
if origin_checksums is None:
  origin_checksums = {}
  if args.verify_checksum:
    origin_executor = concurrent.futures.ThreadPoolExecutor(max_workers=None)
    for local_uri, _ in local_uris:
      origin_checksums[local_uri] = origin_executor.submit(get_checksum, local_uri)

# Docs says: "If max_workers is None or not given, it will default
# to the number of processors on the machine, multiplied by 5",
# which is fine by us.
with concurrent.futures.ThreadPoolExecutor(max_workers=None) as executor:
  test_results = {}
  for local_uri, _ in local_uris:
    test_results[local_uri] = []

    origin_checksum = None
    if args.verify_checksum:
      origin_checksum = origin_checksums[local_uri]

    with fileinput.input(files=(args.mirrorsfile), mode='r') as f:
      for line in f:
        if args.verify_checksum and line.strip() == 'https://download.gimp.org/gimp/':
          # Passing the main server which is also in the list. It's
          # our checksum base, we don't need to check it again.
          continue
        mirror_uri = line.strip() + local_uri
        check_count += 1

        future = executor.submit(verify_remote, mirror_uri, origin_checksum)
        test_results[local_uri] += [future]

  # Print results!
  for local_uri, release_date in local_uris:
    if release_date is None:
      print("Checking: {}".format(local_uri))
    else:
      print("Checking: {} (released on {})".format(local_uri, release_date))
    checked = 0
    try:
      # Waiting 10 min which is already a lot!
      for future in concurrent.futures.as_completed(test_results[local_uri], timeout=1200):
        checked += 1
        uri, success, status, checksum_ok = future.result()

        checksum_text = ''
        if checksum_ok is not None:
          if checksum_ok:
            # Success in green.
            checksum_text = ' (\033[32mchecksum OK\033[0m)'
          else:
            # Failed checksum are red. We absolutely need to check these.
            checksum_text = ' (\033[31mchecksum KO\033[0m)'

        if status != '200':
          # Failed downloads are orange, kind of a warning.
          status = '\033[33m{}\033[0m'.format(status)
        print(status + ' : ' + uri + checksum_text)
        sys.stdout.flush()
        if not success:
          error_count += 1
    except concurrent.futures.TimeoutError:
      failed = check_count / 3 - checked
      print("Some downloads took too long. Dropping {} checks.".format(failed))
      failed_checks += failed
    print()

if origin_executor is not None:
  origin_executor.shutdown()

if error_count == 0:
    sys.exit(os.EX_OK)
else:
    sys.stderr.write("{} / {} errors reported.\n".format(error_count + failed_checks, check_count))
    # Return negative error count as information error code.
    sys.exit(- error_count - failed_checks)

sys.exit(os.EX_DATAERR)
