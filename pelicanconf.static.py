#!/usr/bin/env python
# -*- coding: utf-8 -*- #

# make sure the current path is searched for imports
import sys
import os

sys.path.append(os.path.abspath(os.path.dirname(__file__)))

# Import everything to the global scope.
from pelicanconf_common import *
#customize_environment('static')

SITEURL = 'https://static.gimp.org'
SITEMAP_SITEURL = 'https://static.gimp.org'

FEED_DOMAIN = SITEURL
FEED_ATOM = 'feeds/atom.xml'
FEED_RSS = 'feeds/rss.xml'
