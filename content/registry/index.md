Title: Registry
Date: 2019-06-11T22:01:27-06:00 
Author: Pat David
Status: Published


**The registry is dead!**  
**Long live the registry!**

Yes, for years users could download content to extend GIMP in various ways (plugins, scripts, brushes, etc.) from registry.gimp.org.

The problem is that the entire site was hosted on a _very_ old installation of [Drupal][] and there was nobody who was willing to step up and take ownership (including upgrading and maintaining the site long term).
This was not a trivial task to take over and the project is already stretched a little thin for volunteers.

[Drupal]: https://www.drupal.org/ "Drupal - the open-source CMS"

Couple this with the fact that the interface and method of reaching users this way was outdated/needlessly complicated for users, and you can understand why we were ok with retiring the site.

On the good side, this cloud is not without a silver lining!
The one and only Jehan is currently working on integrating the functionality of the old registry site with a new approach by bringing it into GIMP properly.
This means that in the future there will be an interface within GIMP to browse and manage the same types of assets you used to find on the registry (plugins, scripts, brushes, etc.).
Hopefully we'll also have similar functionality to [Firefox Add-ons][] to allow ratings, uploads, and more (both in a web-based interface and from within GIMP).

[Firefox Add-ons]: https://addons.mozilla.org/en-US/firefox/ "Firefox Add-ons"

If you _really_ need something from the old site, there's a static archive of the contents [available on Github](https://github.com/pixlsus/registry.gimp.org_static) that you can access.
