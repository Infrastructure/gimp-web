Title: GIMP From Source
Date: 2015-08-17T15:38:06-05:00
Modified: 2019-05-24T13:07:19-06:00
Authors: Pat David
Status: hidden


The official distribution of GIMP is the source code, distributed in tar files from the GIMP FTP site and its [mirrors](/downloads/#mirrors). The same source code can be compiled to create binaries for different platforms such as [GNU/Linux](/unix/), [Microsoft Windows](/windows/), [macOS](/macintosh/), [Solaris](/unix/) and many others.

*   [GIMP Source Code](#gimp-source-code)
*   [GIMP Requirements](#gimp-requirements)
*   [GIMP from Git](#gimp-from-git)
*   [Optional packages](#optional-packages)

### Stable releases

Is recommended for most users. Pre-compiled binaries of the stable GIMP are usually available for many platforms (see the platform-specific pages for more details), so you do not even have to compile the code yourself.

### Development releases

For those who want to check the progress towards the next stable release, and who want to help the developers by testing the code and reporting bugs. The development version contains more features than the stable version but it may crash from time to time so be sure to save your work often. If you are using this version, it is a good idea to subscribe to some of the GIMP [mailing lists](/discuss.html) (gimp-user or gimp-developer) so that you can follow the discussions about new features and known bugs.

### Git repository

This is for those who want to live on the bleeding edge. This will give you the latest version of the source code with the latest features, but also with the latest bugs (eeek!). The repository contains several versions of the code called "branches" so you can fetch the latest version ("trunk") or a stable version from a maintenance branch. If you intend to [contribute](/develop/) to the development of GIMP, then you should try using Git. As the code is constantly evolving and features are added (or removed) every day, you should have a look at the [developers' site](https://developer.gimp.org/) and subscribe to the gimp-developer mailing list if you compile the code from Git.

## GIMP Source Code

The GIMP source code is distributed as tarballs. It is available from the GIMP FTP site and its [mirrors](/downloads/#mirrors).

## GIMP Requirements

All requirements below must be met to be able to compile GIMP from source. This list might change depending on the releases being worked on during development of GIMP. Look at the files INSTALL and README in the tarballs for details.

### Stable version 2.10.x

<table markdown='span' class='gimpfromsrc'>
<tbody>
<tr>
<th>Package</th>
<th>Version</th>
<th>FTP</th>
<th>HTTP</th>
<th>Description</th>
</tr>
<tr>
<td>pkg-config</td>
<td>0.16.0 or newer</td>
<td>-</td>
<td><a href="https://www.freedesktop.org/software/pkgconfig/">Download</a></td>
<td>A system for managing library compile/link flags</td>
</tr>
<tr>
<td>GTK+</td>
<td>2.24.32 or newer</td>
<td><a href="ftp://ftp.gnome.org/pub/GNOME/sources/gtk+/">Download</a></td>
<td>-</td>
<td>The GIMP toolkit</td>
</tr>
<tr>
<td>GLib</td>
<td>2.54.2 or newer</td>
<td><a href="ftp://ftp.gnome.org/pub/GNOME/sources/glib/">Download</a></td>
<td>-</td>
<td>Glib Convenience Library</td>
</tr>
<tr>
<td>Pango</td>
<td>1.29.4 or newer</td>
<td><a href="ftp://ftp.gnome.org/pub/GNOME/sources/pango/">Download</a></td>
<td>-</td>
<td>Text layout engine, GIMP also requires PangoCairo — a Pango backend using Cairo</td>
</tr>
<tr>
<td>Fontconfig</td>
<td>2.12.4 or newer</td>
<td>-</td>
<td><a href="https://freedesktop.org/fontconfig/release/">Download</a></td>
<td>Font Configuration</td>
</tr>
<tr>
<td>babl</td>
<td>0.1.62 or newer</td>
<td><a href="https://download.gimp.org/babl/">Download</a></td>
<td>-</td>
<td>Pixel format translation library</td>
</tr>
<tr>
<td>GEGL</td>
<td>0.4.14 or newer</td>
<td><a href="https://download.gimp.org/gegl/">Download</a></td>
<td>-</td>
<td>Generic Graphics Library</td>
</tr>
</tbody>
</table>


## GIMP from Git

The source code of GIMP is maintained in the GNOME Git repository. Besides offering version tracking, branching, avanced diff support and else, this repository grants everyone access to the latest revision of the GIMP source code.
Follow this guide and you will have the most recent GIMP in no time:

*   [Best way to keep up with GIMP from git](/source/howtos/gimp-git-build.html)

To find out more about GIMP development, [developer.gimp.org/](https://developer.gimp.org/) should answer the questions you have.

## <a name="optional_packages">Optional packages</a>

To make it easy for you to understand how to get GIMP and what is required to run GIMP, the list of optional packages has been outlined below. The list outlines what the additional package adds, like support for fileformats, etc.

<table markdown="span" class='gimpfromsrc'>
<tbody>
<tr>
<th>Package</th>
<th>FTP</th>
<th>HTTP</th>
<th>Description</th>
<th>Dependency</th>
</tr>
<tr>
<td>aalib</td>
<td>-</td>
<td><a href="http://aa-project.sourceforge.net/aalib/">Download</a></td>
<td>ASCII art library</td>
<td>Optional</td>
</tr>
<tr>
<td>libexif</td>
<td>-</td>
<td><a href="https://sourceforge.net/projects/libexif">Download</a></td>
<td>EXIF tag support for JPEGs</td>
<td>Optional</td>
</tr>
<tr>
<td>libjpeg</td>
<td><a href="ftp://ftp.uu.net/graphics/jpeg/">Download</a></td>
<td>-</td>
<td>JPEG support</td>
<td>Optional (explicit disable)</td>
</tr>
<tr>
<td>libpng</td>
<td>-</td>
<td><a href="http://www.libpng.org/">Download</a></td>
<td>PNG support</td>
<td>Optional (explicit disable)</td>
</tr>
<tr>
<td>libtiff</td>
<td>-</td>
<td><a href="http://www.libtiff.org/">Download</a></td>
<td>TIFF support</td>
<td>Optional (explicit disable)</td>
</tr>
<tr>
<td>libmng</td>
<td>-</td>
<td><a href="http://www.libmng.com/">Download</a></td>
<td>MNG support</td>
<td>Optional (plugin won't be built)</td>
</tr>
<tr>
<td>libxpm</td>
<td><a href="ftp://ftp.x.org/contrib/libraries/">Download</a></td>
<td><a href="http://koala.ilog.fr/ftp/pub/xpm/">Download</a></td>
<td>XPM support</td>
<td>Optional</td>
</tr>
<tr>
<td>librsvg</td>
<td><a href="ftp://ftp.gnome.org/mirror/gnome.org/sources/librsvg/">Download</a></td>
<td><a href="https://wiki.gnome.org/Projects/LibRsvg">Download</a></td>
<td>Scalable Vector Graphics</td>
<td>Optional (plugin won't be built)</td>
</tr>
<tr>
<td>libwmf</td>
<td>-</td>
<td><a href="http://wvware.sourceforge.net/libwmf.html">Download</a></td>
<td>Library to convert wmf files</td>
<td>Optional (plugin won't be built)</td>
</tr>
<tr>
<td>webkit</td>
<td>-</td>
<td><a href="https://wiki.gnome.org/Projects/WebKitGtk">Download</a></td>
<td>HTML renderer and web content engine</td>
<td>Optional (Help Browser won't be built)</td>
</tr>
<tr>
<td>zlib</td>
<td>-</td>
<td><a href="https://www.zlib.net/">Download</a></td>
<td>Compression routines</td>
<td>Optional</td>
</tr>
<tr>
<td>Python</td>
<td><a href="ftp://ftp.python.org/pub/python/">Download</a></td>
<td><a href="https://www.python.org/">Download</a></td>
<td>Python support</td>
<td>Optional</td>
</tr>
</tbody>
</table>
