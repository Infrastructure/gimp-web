Title: Ancient Books About GIMP
Date: 2023-10-10
Author: Jehan
Summary: Non-exhaustive list of older published books about GIMP
Status: hidden

This page is a non-exhaustive list of **books published before GIMP <!-- DELIMITER_VERSION --> release** (<!-- DELIMITER_DATE -->).
We also have a [page on recent books](/books/).</br>
We welcome [missing books reports](https://gitlab.gnome.org/Infrastructure/gimp-web/-/issues/new).

ℹ️  These books are not endorsed by the GIMP project. We simply list what
we are notified of. Descriptions are publisher's and do not reflect developers
opinions.

<!-- OLD_BOOKS -->

<hr/>
<small>
🛈  *List generated on <!-- DATE --> from <!-- N_OLD_BOOKS --> [reported books](https://gitlab.gnome.org/Infrastructure/gimp-web/-/blob/testing/content/books/index_data.json)
published before <!-- DELIMITER_DATE -->.*
<br/>
🖝  *[Reports of missing books welcome](https://gitlab.gnome.org/Infrastructure/gimp-web/-/issues/new).*
</small>

