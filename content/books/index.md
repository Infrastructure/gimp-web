Title: Recent Books About GIMP
Date: 2015-08-14T13:58:40-05:00
Author: Pat David
Summary: Non-exhaustive list of recently published books about GIMP
Status: hidden

This page is a non-exhaustive list of **books published after GIMP <!-- DELIMITER_VERSION --> release** (<!-- DELIMITER_DATE -->).
We also have a [page on older books](/books/older.html).</br>
We welcome [missing books reports](https://gitlab.gnome.org/Infrastructure/gimp-web/-/issues/new).

ℹ️  These books are not endorsed by the GIMP project. We simply list what
we are notified of. Descriptions are publisher's and do not reflect developers
opinions.

<!-- NEW_BOOKS -->

<hr/>
<small>
🛈  *List generated on <!-- DATE --> from <!-- N_NEW_BOOKS --> [reported books](https://gitlab.gnome.org/Infrastructure/gimp-web/-/blob/testing/content/books/index_data.json)
published after <!-- DELIMITER_DATE -->.*
<br/>
🖝  *[Reports of missing books welcome](https://gitlab.gnome.org/Infrastructure/gimp-web/-/issues/new).*
</small>
