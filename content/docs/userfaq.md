Title: Frequently Asked Questions
Date: 2015-08-14T15:07:00-05:00
Author: Alexandre Prokoudine
lang: en
slug: userfaq

[TOC]

## General questions

### Is there a company or a foundation behind GIMP?

No. We are a diverse group of volunteers from around the world who work on this
project in their spare time. Code-wise most of project contributions come from
Europe where the current GIMP maintainers live. Our translators are an even
more diverse group of contributors, since GIMP is available in
80 languages/locales.

### Can I use GIMP commercially?

Yes, you can. GIMP is free software, it doesn't put restrictions on the kind
of work you produce with it.

### What's the GIMP's license, and how do I comply with it?

GIMP is distributed under terms of General Public License v3 and later. In a
nutshell, this means:

* You are free to use GIMP, for any purpose
* You are free to distribute GIMP
* You can study how GIMP works and change it
* You can distribute changed versions of GIMP

Note that once you distribute modified version of GIMP, you also must publish
your changes to the source code under GPLv3+ as well.

### Someone sold or tried to sell me GIMP on a 3rd party website. Is this legal?

Yes, under terms of the General Public License this is perfectly legal,
provided that the seller also gave you the source code of GIMP and any
modifications he/she introduced. Please see
[this page](https://www.gimp.org/about/selling.html) for more information.

### Are you trying to develop a Photoshop killer app?

No. Most generic image editors look like Photoshop simply because Adobe's
application was among the first image editors as we know them now, so
developers tend to stick to what people know&nbsp;&mdash; in general terms.

What we aim to do is to create a high-end image manipulation application that
is free to use and modify by everyone, ever.

Feature-wise, the proposed "high-end" status does automatically put GIMP into
direct comparison against Photoshop, but we don't think about competition
much. We have too many ideas of our own to implement, and too many things to
improve before the notion of competition begins to make the slightest sense.

We do, however, acknowledge the fact that people will treat GIMP as Photoshop
replacement no matter what we tell them, and that's all right with us. You
own this software, it's up to you to decide how you make use of it.

### I don't like GIMP's user interface. Why can't you just copy Adobe Photoshop?

In the past, the development in the project was somewhat erratic with regards
to taking usability into consideration, which is rather typical for free
software projects in their early years.

Between 2006 and 2013, we worked with Peter Sikking of Man+Machine Works, a
professional usability architect, who helped us shape the project vision for
GIMP, interviews professional users to better understand their workflows and
demands, and wrote functional specifications for various GIMP features.

[This collaboration](https://gui.gimp.org) resulted in major improvements of
GIMP's usability, in particular: the rectangular-based selection/cropping
tools, the unified free/polyline selection tool, the single-window mode, the
upcoming unified transformation tool etc.

While working on functional specifications, Peter researched how various
features are implemented in applications with a partially matching feature set
(such as Adobe Photoshop), but the final design was made to help actual users
complete their tasks as fast as possible.

Nowadays we have a
[`gimp-ux`](https://gitlab.gnome.org/Teams/GIMP/Design/gimp-ux/)
repository dedicated to gather feedbacks, discuss about interface,
experience and write down specifications of features with well thought
rationals.

This is exactly the kind of approach to designing interfaces that we
consider to be superior to merely copying others' user interaction
decisions.

### I don't like the name GIMP. Will you change it?

With all due respect, no.

We've been using the name GIMP for more than 20 years and it's widely known.

The name was originally (and remains) an acronym; although the word "gimp" can
be used offensively in some cultures, that is not our intent.

On top of that, we feel that in the long run, sterilization of language will
do more harm than good. GIMP has been quite popular for a long time in search
engine results compared to the use of the word "gimp". So we think we are on
the right track to make a positive change and make "gimp" something people
actually feel good about. Especially if we add all the features we've been
meaning to implement and fix the user interface.

Finally, if you still have strong feelings about the name "GIMP", you should
feel free to promote the use of the long form GNU Image Manipulation Program
or exercise your software freedom to fork and rebrand GIMP.

## Releases

### When do you release the next version of GIMP?

We release both updates to the current stable version and development versions.

We cut new updates of the stable version in two cases: 1) some newly introduced
bug is knowingly affecting a lot of users; 2) the amount of improvements and
bug fixes is large enough to justify an update&nbsp;&mdash; typically, a few
dozens of each, but there is no rule.

### Why can't you announce dates of future releases?

We are a team of volunteers with day jobs, families, and personal interests
beyond development of software. Given that, we try to avoid the situation when
we cannot deliver a release, because something else at work/in family came up.

Instead we provide a [feature-based roadmap](https://developer.gimp.org/core/roadmap)
that roughly outlines, in what order we will be implementing various popular
requests made by users.

Note that we tried a few times to give date estimates but it always ends
up as being taken as some kind of "promises" and relayed in news
outlets, finally backfiring into having people telling us we don't keep
our promises. Anyone working in software development knows that
estimates are just an attempt to organize. This is why we are wary about
publishing dates in announcements anymore.

### Aren't you interested in doing paid development of GIMP via crowdfunding?

Some developers have jobs they love and contribute in their spare time, while
others actively try to make a living with Free Software development. GIMP
project actively encourages personal fundraisers by trusted contributors. There
are two such campaigns running at the moment. You can learn more about them on
the [Donate](https://www.gimp.org/donating/) page.

If you are willing to launch a campaign and develop some features for GIMP,
talk to us about changes you are about to propose. We'll help you to flesh
out your idea and promote it to a larger community.

### Will you release GIMP for Android or iOS devices?

Apps for mobile devices imply a different approach to designing interfaces.
Since most of GIMP's source code is related to the user interface one way or
another, it means that we would have to design and then develop a whole new
application. Given the current manpower, we'd rather focus on delivering a
great image manipulation program for desktop users.

However, this has been a topic of interest to several core developers
across the years. We might release some day a GIMP-branded image
manipulation program for Android. Though **no promises!**

As for iOS, please note that GIMP is licensed under GNU GPL v3+ which
conflicts with Apple's Terms of Service. For a full story, please read
[this article](https://www.engadget.com/2011/01/09/the-gpl-the-app-store-and-you/)
by Richard Gaywood.

## Features
### I do not recognize GIMP's user interface. It's different from what I see in all tutorials. What do I do?

We switched to a dark user interface theme and a symbolic icon theme by default
in version 2.10, we also introduced tool groups. If you absolutely want to revert
GIMP to colorful icons, bright/system user interface, and no tool groups, here is
what you do:

1. Go to `Edit > Preferences`, in the newly opened window go to `Interface > Theme`
and change the theme from _Dark_ to _System_.

2. Switch to the `Interface > Icon Theme` page nearby and change the theme
from _Symbolic_ to _Legacy_.

3. Finally, go to `Interface > Toolbox` in the very same _Preferences_ dialog and
tick off the _Use tool groups_ setting.

Changes will apply immediately. Click _OK_ to confirm.

### When will GIMP support HDR imaging and processing with 16bit per color channel precision?

GIMP 2.10 was released in April 2018 and is the first version of the program
to feature processing with precision of 16-bit and 32-bit per color channel.

### When will GIMP support any kind of non-destructive editing like adjustment layers, layer filters, and/or full-blown node-based editing?

Rejoice: GIMP 3.0 has non-destructive layer effects! 🎉

It is a first version, with a still basic and limited UI. More improvements are
yet to come.

### Will GIMP open raw (.NEF, .CR2 etc.) files from my camera?

Starting with version 2.10, GIMP features plug-ins for using
[darktable](https://www.darktable.org/) and
[RawTherapee](https://rawtherapee.com/) to process raw images, as well as a
preference for a default raw processing plug-in.

### I do a lot of desktop publishing related work. Will you ever support CMYK?

Yes, better support for CMYK has been on our roadmap for a long time. One of the
ideas, how we want to make this work, was introduced by user interaction
architect Peter Sikking at Libre Graphics Meeting 2009 and later&nbsp;&mdash;
in his two-part article in his company's blog:
[1](http://blog.mmiworks.net/2009/05/gimp-enter.html),
[2](http://blog.mmiworks.net/2009/06/gimp-squaring-cmyk-circle.html). Please
take some time to read up on that.

We needed to finalize transition to the new image processing engine, GEGL,
before attempting to introduce features like CMYK support. Now that it is
complete, initial work on CMYK support has been done in the babl and GEGL
libraries that GIMP relies on. This is partially
[funded by the GIMP community](https://www.patreon.com/pippin/posts),
you can join in, too.

More work happened in the GIMP 2.99.12 development iteration, as a [GSoC
project](https://www.gimp.org/news/2022/06/03/cmyk-in-gsoc-2022/). This
allowed or improved importing and exporting CMYK images in various file
formats (converted to and from RGB as storage format) and expose CMYK
pixel information within GIMP (e.g. when color picking) within the
context of a soft-proof color profile.

As for making CMYK a core image format (such as RGB, Grayscale or
indexed), we have no timeline on this but we also wish it to be possible
some day.
Also, please note that we are not planning advanced features such as GCR support
for now. This will most likely require a new dedicated developer in the team.

Should a new developer join the team to specifically work on
CMYK-related features, we will do our best to help him/her to complete this
project and get it to our users as soon as possible.

### I don't like some changes you introduced in recent GIMP versions, like the Save/Export separation. Why can't you add a checkbox to disable them?

We realize that some changes are disruptive to some groups of users,
especially those who got used to GIMP as an image editor for doing quick fixes
to lossy files such as JPEG, PNG etc. (i.e. files that cannot store layers,
masks, custom channels, paths).

However, adding a switch for every change we make adds numerous levels of
complexity that we'd rather avoid. Additionally, it would lead to dramatically
changing the way we mean GIMP to work. Hence we respectfully disagree to make
extra behaviour switches.

At the same time, if you don't wish to abandon GIMP completely, we recommend
having a look at the
[Saver and Save/export clean plug-ins](https://www.shallowsky.com/software/gimp-save/)
by Akkana Peck, as well as at various GIMP forks on GitHub, although typically
they aren't maintained up to date with regards to bugfixes.

### Why doesn't GIMP use GTK4?

We only just ported GIMP to GTK+3. Please give us some time to breathe
before starting pressuring us into yet another toolkit port.

Note also that following GTK evolution is obviously planned, but this is
not necessarily a priority for us or our users. Most people doing
advanced usage of GIMP don't care about the toolkit and have even no
idea what GTK is. They want to work on image manipulation and none of
related features are on GTK side.

Still it will come some day, and the
[roadmap](https://developer.gimp.org/core/roadmap) might be a good place
to look at for reference.

## Tips

### How do I draw a straight line?

In any of the drawing tools (Pen, Pencil, etc.), click on one endpoint of the
line. Then hold the shift key and click on the other endpoint.

### How do I draw a circle or square?

In the Rectangular or Elliptical selection tool, click in one corner of your
square or circle, then press Shift while dragging toward the other corner. Or
enable the checkbox for Fixed: Aspect Ratio in tool options and make sure the
aspect ratio is set to 1:1 before starting your square or circular selection.

Once you have a selection, `Edit->Stroke Selection...` will draw a line the
shape of the selection you just made.

For curved selections, like circles, stroking with the Paintbrush paint tool
will usually give a smoother looking line. You can get an even smoother line
by converting the selection to a path (`Select->To Path`), then using
`Edit->Stroke Path...` instead of `Stroke Selection...`

As a general rule though, if you mostly need to work with shapes, maybe
GIMP is not the most adapted tool. This is typically a usage for vector
applications. We recommend for instance [Inkscape](https://inkscape.org/)
which is a great Free Software for vector imagery.

We are planning to eventually have a shape tool, but having vector
layers (which is also planned) will have to be the first step.

## Troubleshooting

### My graphic tablet doesn't work on Windows/Mac. Does GIMP support advanced input devices such as Wacom?

Yes, GIMP does support graphic tablets and maps pressure, stroke speed, and
other events to its advanced brush engine properties.

### Under MacOS GIMP fonts are garbled

Some issues were fixed in Pango (the library we use for text layout)
[recently](https://www.gimp.org/news/2024/12/27/gimp-3-0-RC2-released/#missing-gui-font-issues-on-macos).
You should update to the latest version of GIMP.

As a general rule, updates of macOS often break previously working
versions of many software, including GIMP. You should consider always
using the latest version of GIMP (this is true in general, but has
proven even more important on macOS).

## Contributing

### I'm a developer. How do I help you?

Great! Please check the [developer website](https://developer.gimp.org/)
for introduction on GIMP development and talk to us on
[IRC](https://www.gimp.org/discuss.html).

### I'm not a developer. Can I still help you somehow?

Absolutely! Our community includes artists, designers, translators, documenters,
writers, webmasters and much more!
Please check the [Get Involved](/develop/) pages for ways you can help.
Here are some of the ways you can help us:
