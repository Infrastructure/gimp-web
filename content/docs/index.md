Title: Documentation
Date: 2015-08-14T14:39:59-05:00
Author: Pat David

## GIMP User Manual

GIMP comes with a built-in help system, available in dozens of languages. Once
you have started the program, press <kbd>F1</kbd> for context-sensitive help.
You may have to install the help pages from a separate package (`gimp-help`),
depending on how your version of GIMP was packaged.

The manual is also available online on a [dedicated website](https://docs.gimp.org/).

## Tutorials

[Tutorials on gimp.org](/tutorials/)

## Books

[Books about GIMP](/books/)

## GIMP User FAQ

[Frequently Asked Questions](userfaq.html)
