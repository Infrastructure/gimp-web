Title: Citing GIMP and Linking to Us
Date: 2015-08-12T16:18:27-05:00
Modified: 2015-08-12T16:18:33-05:00
Author: Pat David

Citing GIMP or linking to our website from other sites is very much encouraged.
Here are a few simple guidelines that should help you.

[TOC]

## Linking to our pages

If you want to add a link to the official GIMP web site, we recommend that you link to our home page: <kbd>https://www.gimp.org/</kbd>.

You can link to other pages such as the
[download](/downloads/) or [donation](/donating/) pages but be aware
that the <span class="help" title="deep link: a link to a page that is
several clicks away from the home page">deeper you link</span>, the
higher the risks that the page could be moved or removed in the future.
We try to avoid breaking links, but it is difficult to guarantee. So
your best bet for a stable link is the home page, or some of the main
pages featured in the navigation menu.

## Reusing our images

The GIMP project favors artworks under *Libre Art* licenses, which means
most images on news articles or on the main pages can be copied and used
freely elsewhere. This includes our logo (Wilber) shown below.

Here are a few usage how-to:

* Even licensed as *Libre Art*, images are still copyrighted by their
  respective authors.<br/>
  Most images without licensing information (e.g. screenshots in news)
  are [Creative Commons Attribution-ShareAlike 4.0 International
  License](https://creativecommons.org/licenses/by-sa/4.0/) by default
  on this website.<br/>
  It means you must mention the author when cited ("GIMP team"
  otherwise), the license and any modified version must be released in a
  compatible license.
* To use our images, we recommend that you copy them to your own server
  instead of linking directly from this site.
* Some images may not be under Libre Art licenses and cannot be copied
  without permission. This includes most images in our [Legacy
  tutorials](https://www.gimp.org/tutorials/list-all.html). It may also
  happen for instance if we were to feature an artist in a news article.
  We would make a particular mention when such case arises.

## Wilber, the GIMP mascot

The currently used version of Wilber, the GIMP mascot, has a [vector image source (SVG)
available](https://gitlab.gnome.org/GNOME/gimp-data/-/blob/main/images/logo/gimp-logo.svg) by Aryeom Han, available as [Creative Commons Attribution-ShareAlike 4.0
International](https://creativecommons.org/licenses/by-sa/4.0/):

[![Wilber, the GIMP mascot (current version](https://gitlab.gnome.org/GNOME/gimp-data/-/raw/main/images/logo/gimp-logo.svg)](https://gitlab.gnome.org/GNOME/gimp-data/-/blob/main/images/logo/gimp-logo.svg)

All past versions of Wilber are still [available from our `gimp-data`
repository](https://gitlab.gnome.org/GNOME/gimp-data/-/blob/main/images/logo-log.md),
including the original version created by Tuomas Kuosmanen (tigert).

## Citing GIMP in research

Researchers sometimes want to cite GIMP in their thesis when the
software is used for their research. Some [interesting
links](https://www.software.ac.uk/publication/how-cite-and-describe-software)
can be found online on the topic of properly citing software.

As far as our project is concerned, we think that a good citation could
include:

* The name: GIMP (GNU Image Manipulation Program)
* The website: `https://gimp.org`
* The fact it is **Community, Free Software**
* The main license: GPLv3

For people formatting their paper with LaTeX, this `BibTex` entry could
for instance be pasted into your bibliography so that it gets formatted
according to your chosen style (of course, you may want to change the
used version if not the latest stable):

<!-- RESEARCH_CITATION -->
