Title: Selling GIMP
Date: 2015-08-13T13:02:14-05:00
Modified: 2015-08-13T13:02:23-05:00
Author: Pat David


## Introduction

From time to time, we get questions from users who are surprised to see some individuals or companies selling GIMP from their web site or as part of an auction. Sometimes the software is advertised as being GIMP but in some cases there is no mention of the fact that what is sold is GIMP, and GIMP is <span class="help" title="software that may be modified and distributed freely, as long
as you do not deny other users these freedoms">Free Software</span>. This page tries to answer some of these questions and provides suggestions for those who would like to sell GIMP.

## Is it legal to sell copies of GIMP?

Yes. The terms and conditions for copying, distribution and modification of GIMP are clearly listed in the [GNU General Public License](/about/COPYING). Redistribution for free or for profit is specifically allowed, as long as the license is included and the source code is made available. See also [Selling Free Software](https://www.gnu.org/philosophy/selling.html) on the <span class="help" title="Free Software
Foundation">FSF</span> site.

Besides the rights and conditions given by the GPL, it would also be nice (but not required) to mention in any advertising that the product being sold is GIMP (or a bundle including GIMP) or is derived from it.

## Can it sometimes be illegal?

We are no lawyers, but there are clearly some cases where vendors are
bordering on illegality (sometimes even clearly illegal). We encountered
many such cases across the years. If you do too, you would be right to
contact the vendor first. Maybe they didn't do it maliciously and will
be happy to comply swiftly.

If the vendors do not react positively, contacting the selling platform
might be the next right step. Unfortunately some platforms are happy to
ignore their customers, but if enough people complain, they may react.

Here is a non-exhaustive list of problematic cases:

* Per the license, the vendor must keep the license, include it with the
  copies and display appropriate copyright notices in a conspicuous
  manner. See [sections
  4.](https://www.gnu.org/licenses/gpl-3.0.en.html#section4) and [5. of
  the GNU General Public
  License](https://www.gnu.org/licenses/gpl-3.0.en.html#section5).
* Per the license, the vendor must provide the source code (original or
  modified one depending on whether it is a verbatim or modified copy of
  GIMP). See [section 6. of the GNU General Public
  License](https://www.gnu.org/licenses/gpl-3.0.en.html#section6).
* Our logo/mascot, Wilber, also has a [license and
  author](/about/linking.html#wilber-the-gimp-mascot) which must be
  respected if the image is reused and/or modified. In particular, the
  current version is licensed as [Creative Commons by-sa
  3.0](https://creativecommons.org/licenses/by-sa/3.0/), so the original
  author must be given credit in all distributions and any modified
  version must be in the same license.
* Regardless of any license, third party vendors should not pretend to
  be the core GIMP team or to be mandated by us. If they do, this is
  impersonation of other people, which is illegal in most countries. In
  the best case, they should honestly display that they are packaging a
  software developed by others with a link to
  [gimp.org](https://www.gimp.org/). Often though, many vendors remain
  vague, which is not illegal, though clearly not the most good faith
  approach.
* If any modification of the software is itself illegal or unsafe, it
  could be a cause for taking it down. For instance, we saw some vendors
  providing GIMP with modified heinous texts and on remote servers with
  no secure separation between various people's documents (which raise
  privacy and data safety concerns).

## Are the developers associated with companies selling GIMP?

No. The GIMP developers are not associated with the companies selling copies of GIMP. We may be in contact with some of them from time to time when they contribute some improvements to GIMP (though it hasn't happened for many years, as far as we know). Some of them have also made [donations](/donating/) to the GIMP developers (sponsoring for the GIMP developers' conference, though again this was a far away past as far as we know). But none of the developers has direct financial interest in these companies.

## I bought GIMP without knowing that it was Free Software and available free-of-charge

Unfortunately, some companies selling GIMP do not always mention in
their advertising the origin of this software. In particular, they
don't mention the licensing or that the software that they are selling
can also be obtained for free. Sometimes GIMP is combined with
additional software or artwork that adds value to the package, but
sometimes what you get is not much more than what could otherwise be
downloaded for free. If you have purchased such a copy of GIMP and you
feel cheated, then you should complain to the vendor.

Note also that in the European Union, you have specific rights [when
buying things
online](https://europa.eu/youreurope/citizens/consumers/shopping/index_en.htm),
and in particular a 14-day right of withdrawal (even up to 30 days in
some countries). Other countries in the world may have similar laws too.

However, if the vendor did not make the complete corresponding source
code for their version of GIMP available to you as required by the GPL,
then the vendor may have violated the license and may be liable for
copyright infringment. Please try to clarify the situation with the
vendor _before_ reporting any GPL violation to the GIMP developers. In
any case, you should refrain from making public statements about the
potential GPL violation before you have contacted the vendor. Notifying
the developers too early or making public statements about the potential
GPL violation may limit the legal options available to the developers
(copyright owners).

## Where can I buy a copy of GIMP?

We do not sell GIMP from this web site. We provide it free-of-charge on
our [download page](/downloads/) for several platforms. Other platforms
for which we don't create builds often have their own distribution
methods with their third-party GIMP package.

If you believe our work deserves to be funded, we welcome
[donations](/donating/). This helps us improving GIMP further and is our
alternative to "buying" the program.

## Recommendations for those who sell copies of GIMP

If you or your company intend to sell GIMP, it would be nice to follow these guidelines:

1.  **Be honest**. Do not try to hide the fact that the product that you are selling is or contains GIMP (the GNU Image Manipulation Program). Mention it in any advertisement. Also state explicitly that GIMP is developed by other people, ideally with a link to [our website *gimp.org*](https://www.gimp.org/).
2.  **Add value**. Try to provide more than what can be found in the default GIMP package. Include a nice installer, additional plug-ins, some nice artwork, some custom brushes and textures, your own tutorials and documentation, printed copies of the documentation, etc. There are many ways to add value to GIMP and to make your customers happy.
3.  **Respect the GPL**. The GPL requires you to make the source code available. The best solution is to include the source code on the same medium as the GIMP installation package, but you can also include a written offer to supply the source code on request. Note that you cannot simply give a link to the GIMP mirrors: it should be the exact source code that was used to compile the binary package that you are selling and you have to cover the costs of redistribution yourself. If you sell and distribute the binaries online, the GPL requires you to make the source code available "from the same place" so giving a link to the GIMP mirrors is not sufficient (see also [this section of the GPL FAQ](https://www.gnu.org/copyleft/gpl-faq.html#SourceAndBinaryOnDifferentSites)).
4.  **Support your users**. If the version of GIMP that you are selling is modified in any way, you should inform your users and try to handle the support requests related to that version. Providing good support is another way to make your customers happy.

Finally, think about giving something back. If the software created by many volunteers helps your business, it would be nice to return the favor by helping the developers. You can contribute by sending some improvements to the code or by sponsoring some events such as the GIMP developer's conference. This is not required, but happy developers are more likely to create a better product that you can sell later...
