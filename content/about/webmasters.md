Title: Contacting the Webmasters
Date: 2015-08-12T15:47:05-05:00
Author: Pat David
url: webmasters.html
save_as: webmasters.html
status: hidden


This site is maintained by a team of volunteers who are constantly trying to improve it. We are all GIMP users and some of us are also GIMP developers. If you have a question or a comment about GIMP, we will do our best to help you. However, we get a _lot_ of mail and we may not be able to answer all queries. The best way to get support is to post your questions and comments to one of the [mailing lists or IRC channels](/discuss.html).

If you have a problem with GIMP and you think that you have found a bug or if you would like to suggest a new feature, you can also take a look at our [bugs page](/bugs/). Your problem may have been reported or solved already, so please [search and browse](https://gitlab.gnome.org/GNOME/gimp/issues) for existing reports first.

If you have the feeling that your report will require (longer) discussion - this is true for most feature requests - then you should bring this topic up on the gimp-developer mailing list first.


## Problems on this web site?

If you detected a problem on one of our pages, such as a broken link or some incorrect information, please use GNOME Gitlab to [submit a bug report for this web site](https://gitlab.gnome.org/Infrastructure/gimp-web/issues). Several GIMP contributors are keeping an eye on the bug database regularly so this is probably the best way to tell us about these problems and to get a timely response.

You can also subscribe to the [gimp-web mailing list](/discuss.html) and then submit your improvement proposals to the list.

## Problems with download mirrors?

You can also use GNOME Gitlab to [notify
us](https://gitlab.gnome.org/Infrastructure/gimp-web/issues?scope=all&utf8=%E2%9C%93&state=opened&search=mirrors)
if you experience problems with a mirror of download.gimp.org.
If you want to set up a new mirror, you may just [follow the new mirror
procedure](https://gitlab.gnome.org/Infrastructure/Infrastructure/issues/new?issuable_template).
