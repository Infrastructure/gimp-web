Title: GIMP team's Privacy Policy
Date: 2022-06-08
Modified: 2022-06-08
Author: GIMP team

## Team statement

**GIMP, as a software and as a community of people, does not, in any
way, collect, transmit, share or use any Personal Data.**

## No trackers, no ads, no data share

GIMP is community-developed Free Software. The community does not
condone usage of private data of others so our software does not gather
or send data.

This website is mostly static, it does not contain any trackers, neither
ours nor third-party. Furthermore no cookies are stored in your browser.

The website does not contain advertisements.

The software does not contain advertisements or trackers either.

## Caveats

* GIMP is able to load or save files on remote access (for some
  protocols and platforms). If you choose to load or save a remote file,
  your IP or other private data might be shared as part of the normal
  connection flow for the given protocol. This is out of our control and
  it is up to you to decide whether you trust a remote host.

* GIMP is meant to manipulate image files which usually contain all
  types of metadata. It is your responsibility to verify the metadata
  contained in your files before you share them with others.

* While running and for subsequent runs, GIMP uses local persistent storage
  for logs, configuration files, cache, thumbnails, recently accessed
  files and other information which may contain private data. This stays
  on local storage.

* When reading the online version of the User Manual within GIMP, manual
  contents is requested through `HTTPS` connections.

* The Windows universal installer and macOS packages are built with an
  "update check" option to verify, through an `HTTPS` connection
  requesting a static file, if new versions have been released, no more
  than once a week. Checking for new releases is the only purpose and
  nothing else is done.

* When we make GIMP officially available through a third-party
  distribution service (a.k.a. "store" or "repository"), such as the
  Microsoft Store or Flathub, then these service may gather some data as
  a generic usage.  Your should check their terms of service and see if
  you agree with them.

* GIMP is Free Software and therefore may be packaged by other people,
  who may include additional software or modify the source code. We do
  not vouch for these third-party packages and cannot tell you what they
  contain and what they do regarding your privacy. The official packages
  are explicitly listed in our [download
  page](https://www.gimp.org/downloads/).
