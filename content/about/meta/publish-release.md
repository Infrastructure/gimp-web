Title: Publishing a Release
Date: 2018-11-12T09:50:48-06:00 
Author: Pat David
Summary: Steps for publishing a release on the website

These are the steps for publishing a new release on the website.

## GIMP_VERSIONS

All of the releases are listed in this JSON file in the root of the gimp-web directory.
The website builds will parse this file to include the relevant information in various places (the downloads page in particular).

The website build system _does not check_ for existence of the binaries on download.gimp.org.
File will need to be uploaded here before continuing.
Find a GIMPer with appropriate permissions to push files up to download.gimp.org before continuing.


## Source

When a new tarball is ready, make sure the file is available on download.gimp.org and the SHA512 and SHA256 sums have been updated as well.

Now update GIMP_VERSIONS:

```
{
  "STABLE":  {
    "2.XX.XX": {
      "date": "YYYY-MM-DD",
      "source": {
        "gimp-2.XX.XX.tar.bz2": {
          "sha512": "...",
          "sha256": "..."
        }
      },
...
```

Replace `X` where appropriate.
Hopefully adding an entry is obvious, but as an example, adding a new version for a fake `2.10.10` release would look like this:


```
{
  "STABLE":  {
    "2.10.10": {
      "date": "2018-11-12",
      "source": {
        "gimp-2.10.10.tar.bz2": {
          "sha512": "...",
          "sha256": "..."
        }
      }
    },
    "2.10.8": {
      "date": "2018-11-08",
      "source": {
        "gimp-2.10.8.tar.bz2": {
          "sha512": "...",
          "sha256": "..."
        }
      }
    }
...
```


## Windows and macOS Binaries

The same steps should be used for binaries, but with the addition of including a torrent for the file as well.
As before, make sure that the binaries are already uploaded to download.gimp.org and the SHA512 and SHA256 hashes have been created.


### Create .torrent file

To create a new torrent file, download the binary locally.
Make sure that your torrent creation tool allows webseeds.
We've been using `mktorrent`, specifically:

```
$ mktorrent -v
mktorrent 1.0 (c) 2007, 2009 Emil Renner Berthing
```

You can grab one of the previous .torrent files to see what is used for trackers and webseeds.
(You can use a tool like `transmission-show` to view the .torrent file info.)

Here is the .torrent file for a windows release:

```
$ transmission-show gimp-2.10.8-setup-1.exe.torrent
Name: gimp-2.10.8-setup-1.exe
File: gimp-2.10.8-setup-1.exe.torrent

GENERAL

  Name: gimp-2.10.8-setup-1.exe
  Hash: cbf0a8124f9f99e4d2b29f8c4da18fde2e41dc83
  Created by: mktorrent 1.0
  Created on: Sat Nov 10 17:58:00 2018
  Comment: GIMP 2.10.8 Installer for Microsoft Windows - 32 and 64 Bit
  Piece Count: 778
  Piece Size: 256.0 KiB
  Total Size: 203.8 MB
  Privacy: Public torrent

TRACKERS

  Tier #1
  udp://tracker.coppersurfer.tk:6969

  Tier #2
  udp://tracker.leechers-paradise.org:6969

  Tier #3
  udp://tracker.openbittorrent.com:80

  Tier #4
  udp://open.demonii.com:1337

WEBSEEDS

  http://gimper.net/downloads/pub/gimp/v2.10/osx/
  http://gimp.afri.cc/pub/gimp/v2.10/osx/
  http://de-mirror.gimper.net/pub/gimp/v2.10/osx/
  http://mirrors-br.go-parts.com/gimp/v2.10/osx/
  http://gimp.parentingamerica.com/v2.10/osx/
  http://gimp.raffsoftware.com/v2.10/osx/
  http://gimp.skazkaforyou.com/v2.10/osx/
  http://ftp.iut-bm.univ-fcomte.fr/gimp/v2.10/osx/
  http://mirror.ibcp.fr/pub/gimp/v2.10/osx/
  http://servingzone.com/mirrors/gimp/v2.10/osx/
  http://artfiles.org/gimp.org/v2.10/osx/
  http://gimp.cybermirror.org/v2.10/osx/
  http://ftp.fernuni-hagen.de/ftp-dir/pub/mirrors/www.gimp.org/v2.10/osx/
  http://ftp.gwdg.de/pub/misc/grafik/gimp/v2.10/osx/
  http://mirrors.zerg.biz/gimp/v2.10/osx/
  http://ftp.cc.uoc.gr/mirrors/gimp/v2.10/osx/
  http://ftp.heanet.ie/mirrors/ftp.gimp.org/pub/gimp/v2.10/osx/
  http://www.ring.gr.jp/pub/graphics/gimp/v2.10/osx/
  http://ftp.snt.utwente.nl/pub/software/gimp/gimp/v2.10/osx/
  http://ftp.nluug.nl/graphics/gimp/v2.10/osx/
  http://piotrkosoft.net/pub/mirrors/ftp.gimp.org/v2.10/osx/
  http://mirrors.dominios.pt/gimpv2.10/osx/
  http://mirrors.fe.up.pt/mirrors/ftp.gimp.org/v2.10/osx/
  http://mirrors.serverhost.ro/gimp/v2.10/osx/
  http://sunsite.rediris.es/mirror/gimp/v2.10/osx/
  http://ftp.sunet.se/pub/gnu/gimp/v2.10/osx/
  http://www.mirrorservice.org/sites/ftp.gimp.org/pub/gimp/v2.10/osx/
  http://gimp.cp-dev.com/v2.10/osx/
  http://mirror.hessmo.com/gimp/v2.10/osx/
  http://gimp.mirrors.hoobly.com/gimp/v2.10/osx/
  http://mirror.umd.edu/gimp/gimp/v2.10/osx/
  http://mirrors.zerg.biz/gimp/v2.10/osx/
  http://mirrors.xmission.com/gimp/v2.10/osx/
  http://download.gimp.org/gimp/v2.10/osx/

FILES

  gimp-2.10.8-setup-1.exe (203.8 MB)
```

To create a new torrent, invoke `mktorrent` like this:

```
$ mktorrent -a "tracker1" -a "tracker2" -a "trackerN" \
 -w "webseed1" -w "webseed2" -w "webseedN" \
 -c "COMMENT (ie: GIMP-2.10.8 Windows 64/32bit Installer)" \
 {file}
```

Where `trackerN` and `webseedN` are the appropriate URLs.

Make sure the new .torrent file gets uploaded to download.gimp.org as well.

Save changes to GIMP_VERSIONS and push!
