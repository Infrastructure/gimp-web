Title: Discuss
Date: 2021-11-10
Author: Jehan
url: discuss.html
save_as: discuss.html
status: hidden

To discuss features, tips, tricks or contribute to development, the
below discussion channels are available.

We only list spaces moderated by the GIMP team.
Our project encourages community process so many community-led spaces
exist to discuss about GIMP as well (forums, chat systems…) without our
supervision.

In any of the official channels, you are expected to follow our [code of
conduct](#code-of-conduct).

[TOC]

⚠️ Warning: these discussion channels are _public_ and often _archived_ for
__anyone__ to read. In some cases they are even archived in places outside of
our control.
Don't send any private or sensitive information! ⚠️

## Forums

GIMP team has presence in a few forums:

* [forum hosted by *pixls.us*](https://discuss.pixls.us/gimp/), the community
  for Free/Open Source Photography.
* [forum hosted by the *GNOME* project](https://discourse.gnome.org/tag/gimp),
  the Free Software desktop by the GNOME Foundation.
* [Reddit](https://www.reddit.com/r/GIMP/)

## IRC / Matrix

The official GIMP IRC channels are on GIMPNet
([irc.gimp.org](irc://irc.gimp.org/)) and have existed for more than 20
years.

Matrix is an alternative real-time discussion option, with many more
people to ask questions to, though less developers are active.

irc://irc.gimp.org/[#gimp-users](irc://irc.gimp.org/gimp-users) | alt: [GIMP users Matrix channel](https://matrix.to/#/#gimp-users:gnome.org)
: Main support channel for GIMP users.

irc://irc.gimp.org/[#gimp](irc://irc.gimp.org/gimp) | alt: [GIMP Matrix channel](https://matrix.to/#/#gimp:gnome.org)
: Original channel, created in early 1997 for users and developers.
Discussions often focus on GIMP development.

irc://irc.gimp.org/[#gimp-web](irc://irc.gimp.org/gimp-web)
: Channel devoted to this web site and related GIMP sites.

ℹ️  *Note: there used to be a Matrix bridge. It was taken down and we are not
sure if it will get back. So the IRC and Matrix channels are not communicating
anymore.*

## Discord

A [Discord server](https://discord.gg/kHBNw2B) (another real-time
discussion channel) is also available. Supports Chat, Voice and Video
streaming.

## Social Networks

GIMP has official accounts on the following social networks:

* [Mastodon](https://floss.social/@GIMP)
* [Bluesky](https://gimpofficial.bsky.social)
* [Facebook](https://www.facebook.com/gimpofficial/)
* [Twitter/X](https://x.com/gimp_official)

## Code of Conduct

Communication channels between contributors and users of GIMP are
important. Therefore we urge you to follow the following common
etiquette rules. Failure to observe these or instructions from the
moderators may be grounds for reprimand, probation, or removal.

* **Be considerate and respectful**. Every message in our most popular
  discussion channels may be read by thousands of subscribers and then
  aggregated to be seen by an even larger audience.
  Please make sure that you add value to the discussion, avoid repetitive
  arguments, flamewars, trolling, and personal attacks.
* **Write in English**. While the GIMP community is multinational, we
  need a _lingua franca_ for communication which happens to be
  English. It's OK to have bad English skills as long as you do your
  best. Nobody will mock your English (this would go against the first
  rule of being respectful).
* **Do basic research**. Chances are that your questions, feature
  requests, or bug reports have already been thoroughly documented and
  discussed. See the list of [sources](#research-sources) for reference
  below.
* **Be specific**. When describing a problem, please always mention:
  operating system and its version, architecture (32bit, 64bit…), exact
  version of GIMP (use `Help->About` for reference).
* For real-time channels (IRC, Discord) specifically:
    - **Be patient**. IRC offers real-time communication, but be aware
      that there may be long periods of silence in the channels when other
      people are busy or away from their keyboards. Receiving answers
      after a few hours is not unheard of. Therefore asking, then doing
      something else while leaving the window opened and checking from
      time to time if there are answers is not considered rude
      because everyone is aware that others have lives too.
    - **Don't ask to ask, just ask**. Questions like "*May I ask a
      question?*" are redundant. Questions like "*Can someone help me?*"
      cannot be answered until you have described your problem. Be
      polite yet direct and describe your problem or question
      immediately.
    - **Don't expect to be greeted**. You are free to say hello but
      receiving no answers does not mean anything bad. Talking directly or
      just listening is perfectly acceptable behavior too.


<span id="research-sources">*List of sources to check for information on
the project, roadmap, feature requests, bug reports etc. before asking
questions*</span>:

* *Your favorite web search engine*.
* [Documentation](https://www.gimp.org/docs/).
* [Bug tracker](https://gitlab.gnome.org/GNOME/gimp/issues). Useful for checking bug reports and feature requests.
* [FAQ](https://www.gimp.org/docs/userfaq.html). Some frequently asked questions we already replied.
* [Roadmap](https://developer.gimp.org/core/roadmap/). Where some of the plans are outlined.
* [Usability](https://gui.gimp.org). Ongoing work to improve user experience of GIMP.
* [Development](https://developer.gimp.org/). If you wish to contribute, please check the developer website.

We also suggest checking [archives of the mailing lists](#mailing-lists-archives) for existing discussions.

## Mailing Lists Archives

We used to have mailing lists. These were
[discontinued](https://mail.gnome.org/archives/gimp-user-list/2022-October/msg00007.html)
end of October 2022.

For people who wish to use our forums through emails, this
[post](https://discourse.gnome.org/t/welcome-to-gimp-forum-on-gnome-discourse/11534/5)
might be of interest (the instructions would work for both our
[forums](#forums), simply "Watching" a category on `pixls.us` forum instead of a tag).

Mailing list archives are still available:

* *GIMP User* (for questions about using GIMP) archives:
  [GNOME](https://mail.gnome.org/archives/gimp-user-list/),
  [mail-archive](https://www.mail-archive.com/gimp-user-list%40gnome.org/),
  [spinics](https://www.spinics.net/lists/gimp/) / Old Archives:
  [mail-archive](https://www.mail-archive.com/gimp-user%40lists.xcf.berkeley.edu/)
* *GIMP Developer* (for plug-in developers and core program developers; discussion
  about the source code) archives:
  [GNOME](https://mail.gnome.org/archives/gimp-developer-list/),
  [mail-archive](https://www.mail-archive.com/gimp-developer-list%40gnome.org/)
  / Old Archives:
  [mail-archive](https://www.mail-archive.com/gimp-developer%40lists.xcf.berkeley.edu/)
* *GEGL Developer* (for developers interested in contributing to
  [GEGL](http://www.gegl.org/), the new architecture for image processing)
  archives: [GNOME](https://mail.gnome.org/archives/gegl-developer-list/),
  [mail-archive](https://www.mail-archive.com/gegl-developer-list%40gnome.org/)
  / Old Archives:
  [mail-archive](https://www.mail-archive.com/gegl-developer%40lists.xcf.berkeley.edu/)
* *GIMP GUI* (discussions around UI/UX) archives:
  [GNOME](https://mail.gnome.org/archives/gimp-gui-list/),
[mail-archive](https://www.mail-archive.com/gimp-gui-list%40gnome.org/)
* *GIMP Web* (discussions about contents and structure of this web site)
  archives: [GNOME](https://mail.gnome.org/archives/gimp-web-list/),
  [mail-archive](https://www.mail-archive.com/gimp-web-list%40gnome.org/)
* *GIMP Docs* (discussions about [GIMP User Manual](https://docs.gimp.org) and
  all derivative works) archives:
  [GNOME](https://mail.gnome.org/archives/gimp-docs-list/),
  [mail-archive](https://www.mail-archive.com/gimp-docs-list%40gnome.org/) / Old
  Archives:
  [mail-archive](https://www.mail-archive.com/gimp-docs%40lists.xcf.berkeley.edu/)
