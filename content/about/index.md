Title: About GIMP
Date: 2015-07-29T14:40:35-05:00
Modified: 2015-07-29T14:40:43-05:00
Author: Pat David

## Introduction

GIMP is an acronym for GNU Image Manipulation Program.
It is a freely distributed program for such tasks as photo retouching, image composition and image authoring.

The software are developed as **Community, Free Software** by the
following [GIMP authors and contributors]({filename}authors.md) under
the [GNU General Public License](/about/COPYING).

## More about GIMP

* [Citing or Linking GIMP, and using images]({filename}linking.md)
* [Frequently Asked Questions (FAQ)]({filename}../docs/userfaq.md)
* [Is it legal to sell copies of GIMP?]({filename}selling.md)

## GIMP History

* [Prehistory][] - until GIMP 0.54 in 1996
* [Ancient History][] - until GIMP 1.0 in 1998
* [History][] - after GIMP 1.0

[Prehistory]: {filename}prehistory.md
[Ancient History]: {filename}ancient_history.md
[History]: {filename}history.md


## GIMP Manual Pages

The `man` pages for command line usage of GIMP:

*   [<tt>gimp</tt> (1)](/man/gimp.html)
*   [<tt>gimprc</tt> (5)](/man/gimprc.html)
*   [<tt>gimptool-2.0</tt> (1)](/man/gimptool.html)


## Important GIMP Links

* [GIMP Privacy Policy][] - www.gimp.org/about/privacy.html
* [GIMP Developer Website][] - developer.gimp.org
* [GIMP Issues][] - GNOME Gitlab
* [GIMP ToolKit][] - www.gtk.org
* [GNU Project][] - www.gnu.org

[GIMP Privacy Policy]: /about/privacy.html
[GIMP Developer Website]: https://developer.gimp.org
[GIMP Issues]: https://gitlab.gnome.org/GNOME/gimp/issues
[GIMP ToolKit]: //www.gtk.org
[GNU Project]: //www.gnu.org


## GIMP RSS Feeds

* [Atom Feed][]
* [RSS Feed][]
* [GIMP Active Bugs][]

[Atom Feed]: /feeds/atom.xml
[RSS Feed]: /feeds/rss.xml
[GIMP Active Bugs]: https://gitlab.gnome.org/GNOME/gimp/issues.atom?state=opened
