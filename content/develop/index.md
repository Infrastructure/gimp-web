Title: Get Involved
Date: 2015-08-14T14:18:56-05:00
Author: Pat David

<figure>
<script type='text/javascript' src='https://www.openhub.net/p/gimp/widgets/project_basic_stats?format=js'></script>
</figure>

GIMP is <span class="help" title="software that may be modified and distributed
freely, as long as you do not deny other users these freedoms">Free
Software</span>, mostly licensed under the [GNU General Public License
(GPL)](/about/COPYING) license, and historically part of the [GNU
Project](https://www.gnu.org/).

In the free software world, there is generally no distinction between users and
developers. As in a **friendly neighborhood**, everybody pitches in to help their
neighbors. Please consider the time you give in assistance to others as payment.

Ways in which you can help:

[Fix bugs or program](https://developer.gimp.org/core/submit-patch/) new features
: Our [Developer Website](https://developer.gimp.org/) is where you want to
start learning about being a code contributor.

Report [bugs](/bugs/) (errors in the program)
: As any application, GIMP is not bug-free, so [reporting bugs](/bugs/)
that you encounter is very important to the development.

Test existing features and provide feedback
: If you think a feature can be improved somehow, you can discuss
the improvements on the [forums](/discuss.html#forums) and
create [enhancement requests on our UX
tracker](https://gitlab.gnome.org/Teams/GIMP/Design/gimp-ux/-/issues/).

Write or improve the [documentation](https://docs.gimp.org/)
: The [Documentation websites tells you all about
contributing](https://docs.gimp.org/help.html).

Translate GIMP or documentation to your own language
: Contact the respective [translation team](https://l10n.gnome.org/teams/) for
your language. The [Translation
Project](https://wiki.gnome.org/TranslationProject) page of GNOME is also of
interest.

Write [tutorials](/tutorials/) on this website
: We have a [procedure for writing tutorials](/tutorials/#contributing-new-tutorials-or-fixes) too!

Improve this website
: Creating websites that contain useful information is very important. It is
just as important as doing bug reports. A website contains a lot of information
that are needed for the development to move on and it also contains information
that will help the public to understand what the application is all about. We
explain how to help us [in a dedicated
tutorial](https://developer.gimp.org/core/wgo/).

Make artwork for GIMP
: After all, GIMP is for artists, designers, photographers and other creators.
So if you want to contribute your art under a Libre license, feel free to [reach
out](/discuss.html).

Let people know you used GIMP for your artwork
: On your website, social networks or whatnot!

Give away copies of GIMP
: This is Free Software, it is made to be shared, just like knowledge and
know-how. Give GIMP to your neighbors, your friend, your family, everyone!

Help others to learn GIMP
: Write [books about GIMP](/books/), teach it [in
university](/news/2018/11/08/gimp-2-10-8-released/#gimp-in-universite-de-cergy-pontoise),
in websites, video tutorial, local community workshops, everywhere! GIMP is free
to use and will never try to tamper with your work.

Help new GIMP users in online forums
: See the [Discuss](https://www.gimp.org/discuss.html) page for a list of
official discussion venues for GIMP. Helping in non-official forums, for
instance in your native language is also great!

etc.
: As you can see, anyone can help. This is what a community is about.

Once you've figured out what to do, be bold and get to work!

GIMP's community is a friendly one, but it probably is still worth saying this: try not to take critiques personally. We all just want GIMP to be the best that it can be. Once approved, your edits will be merged into the code base, making you an official GIMP contributor. And if you keep up the good work, not only will this process get easier with practice, your administrative privileges in GIMP development will also increase too.
