Title: GIMP Organization Links
Date: 2015-08-17T11:31:37-05:00
Modified: 2015-08-17T11:31:42-05:00
Authors: Pat David
Status: hidden


## The GIMP Organization

[gitlab.gnome.org/GNOME/gimp/](https://gitlab.gnome.org/GNOME/gimp/)
 Take your bugs here

[developer.gimp.org](https://developer.gimp.org)
 GIMP developers website

[gui.gimp.org](https://gui.gimp.org "GIMP Usability")
 GIMP UI redesign Wiki

[docs.gimp.org](https://docs.gimp.org)
 GIMP documentation

[registry.gimp.org](http://registry.gimp.org)
 Plug-ins & scripts

[gimpmagazine.org](https://web.archive.org/web/20180502224330/https://gimpmagazine.org/)
 GIMP Magazine by Steve Czajka

[gimpfoo.de](http://gimpfoo.de)
 Personal blog of Michael Natterer, GIMP's maintainer

[vidar.gimp.org](http://vidar.botfu.org)
 Author of GIMPressionist and Fire Annimation plug-ins
