Title: GIMP Links
Date: 2015-08-17T11:31:37-05:00
Modified: 2015-08-17T11:31:42-05:00
Authors: Pat David
Status: hidden


This page contains the best links to interesting GIMP-related sites. If you want to link to us instead, please have a look at [this other page](/about/linking.html).

## Important GIMP Links

*   [The GNU Image Manipulation Program](https://www.gimp.org/) - www.gimp.org
*   [GIMP Developer website](https://developer.gimp.org/) - developer.gimp.org
*   [GIMP bugtracker](https://gitlab.gnome.org/GNOME/gimp/) - gitlab.gnome.org/GNOME/gimp/
*   [The GIMP Toolkit](https://www.gtk.org/) - www.gtk.org
*   [GNU Project](https://www.gnu.org/) - www.gnu.org
*   [GNU General Public License](/about/COPYING) - GPL

## GIMP RSS feeds

*   [GIMP Active Bugs](https://gitlab.gnome.org/GNOME/gimp/-/issues.atom)

## Other Useful Links

*   [gimp.org](#org)
*   [Tutorials](#tuts)
*   [Communities](#clubs)
*   [Development](#development)
*   [Created with GIMP](#created)
*   [More GIMP-links](#more)
*   [Other Graphics Applications](#other)

## <a name="org"></a>gimp.org

[gitlab.gnome.org/GNOME/gimp/](https://gitlab.gnome.org/GNOME/gimp/)
 Take your bugs here.

[gui.gimp.org](https://gui.gimp.org "GIMP Usability")
 GIMP UI Redesign Wiki

[docs.gimp.org](https://docs.gimp.org)
 GIMP Documentation

[registry.gimp.org](https://registry.gimp.org)
 Plug-ins & Scripts

[More ...](org.html)

## <a name="development"></a>GIMP Development Links

[GIMP developers](https://developer.gimp.org)
 The main GIMP development website.

[Martin Nordholts' GIMP Blog](https://setofskills.com/)
 What happens in GIMP development.

[GEGL](http://www.gegl.org/)
 An image processing library based on GObjects.

[GNOME GIT](https://git.gnome.org/)
 GNOME GIT source code repository

## <a name="tuts"></a>Tutorials

[Getting Around in GIMP](https://blog.patdavid.net/p/getting-around-in-gimp.html)
 A collection of tutorials aimed at photography and retouching.

[Search GIMP tutorials](https://www.pixel2life.com/tutorials/gimp/)
 This site lets you find and rate GIMP tutorials hosted on other sites (note: with advertisements).

[Gimp.Tutorials](https://www.ghuj.com/)
 This site lets you find and comment on GIMP tutorials hosted on other sites (note: with advertisements).

[gimpusers.com Tutorials](https://www.gimpusers.com/tutorials.php)
 Tutorials on gimpusers.com (English + German)

[gimps.de](http://gimps.de/)
 has lots of GIMP tutorials; most of the texts are in German and English.

[Gimpology](http://gimpology.com/)
 Tutorials for the gimp

[gimp-tutorials.net](http://gimp-tutorials.net/)
 More GIMP tutorials

## <a name="more"></a>More GIMP-links

[GIMP-startpagina](https://gimp.startpagina.nl) <small>(Dutch version)</small>
 Just one page, but contains the most up-to-date collection of links to GIMP-sites.

[GIMP-startpage](http://gimp.start4all.com) <small>(English version)</small>
 Essentially the same as the one above, but this time in English.

[GIMP-category from ODP](http://www.dmoz.org/Computers/Software/Graphics/Image_Editing/The_GIMP/)
 This OpenDirectoryProject-category holds links to GIMP-sites (like this page).

[Meet the GIMP!](http://meetthegimp.org)
 A video podcast about the free graphics program GIMP

[DeviantArt](https://www.deviantart.com/resources/applications/)
 Resources (brushes, scripts, plug-ins) for GIMP and other applications

## <a name="clubs"></a>Clubs & Communities

[English GIMP-club](http://tech.groups.yahoo.com/group/thegimp2/)
 English-spoken community to discuss usage of GIMP

[GIMP in Russian](http://www.gimp.ru)
 Russian site about GIMP. Articles, tutorials, FAQ, forum.

[Linuxartist.org](https://www.linuxartist.org)
 A resource for artist using GNU/Linux.

[gimpforum.de](http://www.gimpforum.de/)
 German-spoken forum focusing on GIMP. News, tutorials, user-to-user help and galleries.

[gimpusers.de](https://www.gimpusers.de/)
 German GIMP platform offering tips & information about GIMP and a growing tutorials database.

[gimpusers.com](https://www.gimpusers.com/)
 An international community site offering tips, news and a growing tutorials database.

[GIMP in Lithuanian](http://gimp.akl.lt/)
 Lithuanian site about GIMP, featuring news, tutorials, forum and galleries.

[GIMP.org.es](http://www.gimp.org.es)
 News, tutorials, forum, galleries for Spanish-speaking users.

[GIMP.hu](http://www.gimp.hu)
 News, blog and forum in Magyar (Hungarian).

[gimpuj.info](https://www.gimpuj.info/)
 GIMP for Polish-speaking users: news, forum, tutorials, gallery.

[www.gimp.no](http://www.gimp.no)
 Forum and galleries in Norwegian.

[GUG - Gimp User Group](http://gug.criticalhit.dk/)
 Forum, tutorials, galleries.

[GimpChat.com](http://gimpchat.com/)
 Forum, tutorials, galleries and resources.

[Gimper](https://gimper.net/)
 Forum, tutorials, galleries and resources.

## <a name="created"></a>Created with GIMP

[Computer-graphics with GIMP](http://www.dmoz.org/Arts/Visual_Arts/Computer_Graphics/The_GIMP/)
 This OpenDirectoryProject-category holds links to galleries of images created with GIMP

## <a name="other"></a>Other Graphics Applications

[ImageMagick](https://www.imagemagick.org/)
 A collection of tools and libraries that work at the command line as well.

[Tux Paint](http://www.tuxpaint.org/)
 A drawing program for young children.

[Krita](https://www.krita.org/)
 Krita is a KDE program for sketching and painting, offering an end–to–end solution for creating digital painting files from scratch by masters.

[XV](http://www.trilon.com/xv/)
 One of the original Unix graphics applications.

[Skencil](http://www.skencil.org/) (formerly known as Sketch)
 A vector drawing program for Unix.

[Inkscape](https://www.inkscape.org/)
 Very powerful [SVG](https://www.w3.org/Graphics/SVG/)-based illustration package distributed under the GNU GPL.

[Blender](https://www.blender.org/)
 Multi-platform full featured 3D content creation suite, also distributed under the GPL.
