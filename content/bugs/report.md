Title: Reporting an Issue
Date: 2015-08-17T11:09:31-05:00
Modified: 2018-06-08T11:46:47-06:00
Authors: Pat David
Status: hidden

Making good bug reports is a great way to help increase the software quality of GIMP. A good bug report provides:

* unambiguous step-by-step instructions on how to reproduce the bug;
* what result is expected;
* and what the actual result is.

This makes it easy for the developers to reproduce and fix the bug.

Related to bug reports are enhancement requests. It is recommended to discuss
enhancements with developers first, for instance on [IRC](/discuss.html#irc-matrix)
or in the [forums](/discuss.html#forums). This is to make sure that the
enhancement requests that are filed are well-specified and aligned with
the overall goals the developers have for GIMP.

[TOC]

## Reporting a Bug

The goal is to give the developers as much information as possible to help them understand what needs to be fixed:

1.  Use `gimp --version` or the about dialog (`Help → About GIMP`) to check your
    GIMP version. Next compare to the most recent GIMP release on
    [www.gimp.org](https://www.gimp.org/). If your GIMP is old, update and try
    to reproduce the bug. Your bug may have been fixed in a recent release.

2.  Attempt to reproduce the problem. Go do what you were doing when it happened and see if you can trigger the bug again.

    If you know how to do so, start the program from a terminal, or you can follow the [instructions](https://www.gimp.org/bugs/verbose.html).
    Sometimes it will output helpful error messages. After reproducing
    the bug, copy the error messages from your terminal so you can paste
    them in the bug report. It is better to give too much information
    than not enough.

    To narrow down the exact cause of the problem, attempt to reproduce
    it in other ways. Prepare yourself to explain how to reproduce it in
    your bug report. If you are running GIMP in another language, try
    [switching GIMP to
    English](https://docs.gimp.org/en/gimp-fire-up.html#gimp-concepts-running-language)
    so you can report menu items exactly with the English menu item
    name. It simplifies developers' task.

    If you cannot reproduce the bug and don't have any reliable information, reporting might be fruitless and only waste everyone's time.
    If it recurs, consult with appropriate [discussion channels](/discuss.html). Perhaps someone else can find the key to reproducing it.

3.  Go to [GIMP issues](https://gitlab.gnome.org/GNOME/gimp/issues/). If you don't have a login yet, follow the directions to create one.

4.  Select "New issue".
    <figure>
    <img src="{static}new-issue.png">
    </figure>

    Choose a template to pre-populate the _Description_ field with important information to help the developers track down the problem:

    <figure>
    <img src="{static}new-issue-template.png">
    </figure>

    Give your bug a good Title and if you chose a template you should see some text in the Description field already.

5.  Here in the Description you'll have to tell the developers everything about your system, your version of GIMP, and your bug. Just do your best to tell them about it. A crappy bug report is better than no report at all, but if you write down everything clearly you'll be going a long way to helping get the bug addressed.

    <figure>
    <img src="{static}new-issue-description.png">
    </figure>

    Remember, try to be as clear and concise as possible!  This is immensely helpful in tracking down what the problem is and how to possibly fix it.

    Finally, please add the output you get from running `gimp --version
    --verbose` to your bug report, and publish the report by clicking the blue
    "*Create issue*" button.

6.  You should not expect immediate responses as contributors have a lot
    of tasks, though they try to be reactive when possible. They may ask you for
    more information and will tell you what to do to get it (you should get
    email notifications). We don't expect you to do answer immediately as well,
    though we expect you to follow up eventually.

7.  If you later get more information (like a more specific error message or
    fancy stuff like a trace), add a comment to your original bug with that
    information.

8.  It is especially important to add a comment if you somehow resolve
    your bug. For example, you update something else on your system and
    the bug no longer appears. In that case, add a comment describing
    what you updated from what version to what version. This helps everyone and
    saves a lot of time which can be used to fix other bugs or implement nice
    features.

## The Wait Patiently Part

*Whee!!* You survived! If you managed to get through all this and submit your
bug report, be happy. Be proud as a contributor to the community!

You will later get e-mails about your bug. It may not be immediate. Since GIMP
is developed by a community, by volunteers, sometimes it may take some time.
Though we try to always answer.
Similarly when we ask you questions, we don't expect you to answer immediately,
though we expect you to answer eventually. Otherwise it makes no sense. The
basis is that we acknowledge that everyone has a life outside of GIMP, you as
much as the developers.

Answers might include a request for more information. If you get something that
says your bug is not a bug, was already fixed or is a duplicate to another
report, do not be discouraged from reporting in the future. Next time it might
be. Submitting careful bug reports and providing additional information where
possible helps make GIMP better. The day will come where you submit a bug and
later get an e-mail that says your bug is "FIXED" or "RESOLVED". Then you will
know that some developer out there found your bug, reproduced it, and fixed it.
🎉

## Security Bugs

Some bug reports may be related to security issues.
For example, a file plug-in may be vulnerable to a buffer overflow allowing arbitrary code execution when loading an image.

We believe that the best way to report these vulnerabilities is still through
our [GitLab issues](https://gitlab.gnome.org/GNOME/gimp/-/issues), as described
above. This will ensure that the bug is reviewed and handled quickly.  The only
difference is that you will want to check the box "*This issue is confidential
and should only be visible to team members with at least Reporter access.*" in
the bug report creation form. This will hide the bug report from anyone else but
core members.

But if you really do not want to use GitLab for security reports and you do not
mind some extra delay, you can also contact a limited set of GIMP developers by
mail `@gimp.org`, using the special alias `security`.  What will happen next is
that a developer will review the issue and submit it in GitLab, usually as a
hidden bug report only visible to developers.

This will take a bit longer than if you directly submit the bug yourself, but we
know that some people have a policy of not disclosing security vulnerabilities
publicly, so we provide that address for their convenience.
