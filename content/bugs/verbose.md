Title: Obtain Stack Trace
Date: 2023-06-03T12:24:32-05:00
Modified: 2023-07-08T12:24:36-05:00
Author: lillolollo


To find out the cause for troubles in GIMP it's sometimes necessary to find out some internal information from GIMP. You can get them with the following steps, especially if you are asked for it on Gitlab: 

* [Windows](#on-windows)
* [Linux](#on-linux)
* [Linux with Flatpak](#on-linux-with-flatpak)
  * [Flatpak GIMP 2-10](#for-gimp-2-10)
  * [Flatpak GIMP 2-99](#for-gimp-2-99)
  * [Flatpak GIMP nightly](#for-gimp-nightly)
* [MacOS](#on-macos-x)

## On Windows

1. Open the Windows command prompt window:
 
* **Windows Vista and Windows 7** users: Click start, type cmd and then press <Enter>. 
   
* **Windows 8** users: on the start screen with the tiles: enter cmd and then press <Enter>. 
   
* **Windows 10** users: Click start, type cmd and then press <Enter>. 
   
* **Windows 11** users: Click start, type cmd in the search bar. You will see the command prompt    appear as the best match.       click on it or hit Enter to open it. 
   
   A black window will open - the Windows command prompt window. 

2. Enter `"c:\program files\gimp 2\bin\gimp-2.10" --verbose --console-messages`
   
   After a moment a black command prompt window will open, having the title 'GIMP output. You can minimize this window, but don't close it'. Note that the start might take a bit longer because GIMP now writes output data. 

3. When the GIMP application has started, execute the steps to reproduce the error.

4. Select the GIMP output and copy it the clipboard: click on the GIMP icon in the upper left window corner, then Edit/Select All, then Edit/Copy.

5. Open your Internet browser, open your particular bug and paste the message.

## On Linux

1. Open a terminal window.

2. Enter `gimp --verbose --console-messages`
   
   <p>Note that the start might take a bit longer because GIMP now writes output data.</P>

3. When the GIMP application has started, execute the steps to reproduce the error.

4. Select the GIMP output from the terminal window and copy it the clipboard.

5. Open your Internet browser, open your particular bug and paste the message.

## On Linux with Flatpak
Copy and paste all the commands

```sh
flatpak list |grep org.gimp.GIMP
```
This command returns information about GIMP-related packages. 

You need to check where GIMP is installed, as shown in the column Installation. This way you will know whether to install or update in `--system` or `--user`.

If a flatpak command asks where to install a component, always choose the same installation target.

You must therefore follow the instructions below depending on the version of GIMP you have installed 

### For GIMP 2-10

```sh
flatpak install flathub org.gimp.GIMP.Debug
```
In the command below, replace `--system` with `--user` if GIMP was installed as "user" installation (cf. result of above command).
```sh
FLATPAK_SDK=`flatpak --system remote-info flathub org.gimp.GIMP//stable | grep Sdk: |sed 's/^ *Sdk: *\(.*\)/\1/'`
```
```sh
FLATPAK_SDK_DEBUG=`echo $FLATPAK_SDK | sed 's$\([^/]*\)/\(.*\)$\1.Debug/\2$'`

```
```sh
flatpak install flathub $FLATPAK_SDK $FLATPAK_SDK_DEBUG
```

<p>Then run GIMP with the command </p>

```sh
flatpak run --devel org.gimp.GIMP
```
try to replicate the crash and add the obtained stacktrace to the issue report

### For GIMP 2-99
     
```sh
flatpak install flathub-beta  org.gimp.GIMP.Debug//beta
```
In the command below, replace `--system` with `--user` if GIMP was installed as "user" installation (cf. result of above command).
```sh
FLATPAK_SDK=`flatpak remote-info flathub-beta org.gimp.GIMP//beta | grep Sdk: |sed 's/^ *Sdk: *\(.*\)/\1/'`
```
```sh
FLATPAK_SDK_DEBUG=`echo $FLATPAK_SDK | sed 's$\([^/]*\)/\(.*\)$\1.Debug/\2$'`

```
```sh
flatpak install flathub $FLATPAK_SDK $FLATPAK_SDK_DEBUG
```

<p>then run GIMP with the command</p>

```sh
flatpak run --devel org.gimp.GIMP//beta
```
try to replicate the crash and add the obtained stacktrace to the issue report

### For GIMP nightly

```sh
flatpak install gnome-nightly  org.gimp.GIMP.Debug
```
```sh
flatpak remote-info gnome-nightly org.gimp.GIMP//master | grep Sdk: |sed 's/^ *Sdk: *\(.*\)/\1/'
```
```sh
flatpak install gnome-nightly org.gnome.Sdk.Debug//master
```
<p>then run GIMP with the command</p>

```sh
flatpak run --devel org.gimp.GIMP//nightly
```
try to replicate the crash and add the obtained stacktrace to the issue report

## On MacOS X

1. Go to Applications/Utilities, then Terminal.

2. Enter `/Applications/GIMP.app/Contents/MacOS/gimp --verbose --console-messages`
  Note that the start might take a bit longer because GIMP now writes output data.

3. When the GIMP application has started, execute the steps to reproduce the error.

4. Select the GIMP output from the terminal window and copy it the clipboard. In the Mac OS X terminal use Edit/Select All, then Edit/Copy.

5. Open your Internet browser, open your particular bug and paste the message.
