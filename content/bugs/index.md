Title: Bugs
Date: 2015-08-17T11:09:31-05:00
Modified: 2019-03-14T11:06:49-06:00
Authors: Pat David
Status: hidden

If you think you found a bug, it is very important to report it. If the
developers don't know about what might be broken, they can't fix
it. So there you are at your computer trying to do something with GIMP
and it freaks out at you. It can be a frightening experience at times.

## First, Next, Third

First
: Get out a piece of paper or open a text file and scribble down everything you can remember about what you were doing when it happened. Also write down the exact wording of any error messages you received. If relevant, take a screenshot.

Next
: Go away and yell and scream and do whatever you need to do to relax again.

Third
: Check if your bug has been reported already by searching if any report in the
[Current Issue Lists](#checking-existing-reports) looks like it.
If it has, you can still help. See the section: [Enhancing Bug
Reports](#enhancing-bug-reports). Otherwise you will need to [report
it](#reporting-a-bug).

## Checking Existing Reports

<form action="https://gitlab.gnome.org/GNOME/gimp/issues?scope=all&utf8=%E2%9C%93">
<label>Search for an existing bug in GIMP:<br/>
<input type="text" name="search" placeholder="e.g.: 'font loading'" id='bugInput' /></label>
<input type="hidden" name="state" value="all">
<input type="submit" value="Submit"/>
</form>

Below are different links into GitLab which show you lists of open and closed
bugs for GIMP itself, its user manual or its website. You can search manually,
but these links are often a convenient start. With the help of the lists below
you should be able to see if the bug you have found has already been reported.

If it has been reported, and you can give more information, please [add a
comment with the additional information](#enhancing-bug-reports)!

### GIMP bugs

*   [Open issues (all)](https://gitlab.gnome.org/GNOME/gimp/issues?scope=all&utf8=%E2%9C%93&state=opened)
    *   [on Linux](https://gitlab.gnome.org/GNOME/gimp/issues?label_name%5B%5D=OS%3A+Linux)

    *   [on Windows](https://gitlab.gnome.org/GNOME/gimp/issues?label_name%5B%5D=OS%3A+Windows)

    *   [on macOS](https://gitlab.gnome.org/GNOME/gimp/issues?label_name%5B%5D=OS%3A+macOS+%2F+OSX)

*   [Issues that are easy to fix](https://gitlab.gnome.org/GNOME/gimp/issues?label_name%5B%5D=4.+Newcomers)

*   [Closed issues](https://gitlab.gnome.org/GNOME/gimp/issues?scope=all&utf8=%E2%9C%93&state=closed)

*   [Feature proposals](https://gitlab.gnome.org/GNOME/gimp/issues?label_name%5B%5D=1.+Feature)

### GIMP Manual bugs

*   [Open issues](https://gitlab.gnome.org/GNOME/gimp-help/issues)

### GIMP Website bugs

*   [Open issues](https://gitlab.gnome.org/Infrastructure/gimp-web/issues)

## Reporting a Bug

If you already know GIMP's bug reporting procedure, just:

* <a href="https://gitlab.gnome.org/GNOME/gimp/issues/new" title="Report a bug for GIMP!" class='reportBug'>
report an issue in GIMP
</a>
* <a href="https://gitlab.gnome.org/GNOME/gimp-help/issues/new" title="Report a bug for GIMP Manual!" class='reportBug'>
report an issue in GIMP Manual
</a>
* <a href="https://gitlab.gnome.org/Infrastructure/gimp-web/issues/new" title="Report a bug for GIMP Website!" class='reportBug'>
report an issue in GIMP Website
</a>

Otherwise please read our tutorial: [How To Report an Issue]({filename}report.md)

ℹ️  Note that if you are looking for fixing the problem yourself, you might
rather be interested by our [tutorial on the 🧑‍💻 developer
website](https://developer.gimp.org/core/submit-patch/), on how to submit your
first patch.

## Enhancing Bug Reports

If someone has already reported the bug you encounter:

* Read the bug report and the comments carefully. Make sure every bit of
  information you know about the bug is in there. If your version is
  different or you had a slightly different experience with the bug, add
  a comment providing your information.

* Check the status of the bug carefully. If developers are requesting
  some information and the original reporter doesn't answer, see if you
  can provide the needed information.

* Do not add a "*me too*" comment unless your comment provides additional
  information that might be helpful for debugging.

**Note**: if you are unsure the existing report really describes the
same bug, it is better to create a new report, though it is a good idea
to mention the report ID of this other bug which looks alike. Other
contributors may be able to make sense of whether it is a duplicate or
not. If so, they will simply close as duplicate.
