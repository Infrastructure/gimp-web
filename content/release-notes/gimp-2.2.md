Title: GIMP 2.2 Release Notes
Date: 2015-08-17T14:15:58-05:00
Modified: 2015-08-17T14:16:03-05:00
Authors: Pat David
Status: hidden


The GIMP developers are proud to release version 2.2 of the **GNU Image Manipulation Program**. About nine months after version 2.0 hit the road, we have completed another development cycle and can bring a new stable GIMP to our users' desktops.

## About GIMP

The GNU Image Manipulation Program is ideal for amateur photographers, web designers, and many other people who want to create and edit digital images. It is a very powerful application, with features including: channels, layers and masks; colour operations such as levels, curves and threshhold; advanced selection operations using intelligent scissors and selection channels; and much more.

GIMP is free software, which means it can be freely distributed and modified. This makes it well suited to be included on cover disks of magazines dedicated to digital photography or image editing, or to be included with digital cameras, scanners, printers etc. As free software, its capabilities are ultimately limited only by the collective imagination of the community of free software contributors. More information about GIMP is available at [www.gimp.org](https://www.gimp.org/).

## Download

GIMP can be downloaded as source code from ftp.gimp.org or from one of the mirrors listed in the [Downloads](/downloads/) section. We expect that binary packages for the different supported platforms and distributions will show up soon. Unless you are experienced with building software from source, you should wait until binary packages become available.

## Upgrading from GIMP 2.0

Version 2.2 is an update on GIMP 2.0\. GIMP 2.2 is fully backward compatible to GIMP 2.0\. Plug-ins and scripts written for GIMP 2.0 will continue to work and don't need to be changed nor recompiled to be used with GIMP 2.2\. We do however hope that plug-in authors will update their plug-ins for GIMP 2.2 and adapt the GUI changes we did with this version.

GIMP 2.2 replaces GIMP 2.0\. It is advised that you uninstall GIMP 2.0 before installing GIMP 2.2\. If you want to keep GIMP 2.0 installed in parallel to GIMP 2.2, you have to choose a separate prefix which is not in your default library search path.

## Help

The GIMP user manual has been improved a lot over the last couple of months. We stronly suggest that you install the latest version of the gimp-help-2 package together with GIMP 2.2\. You will then have context-sensitive help in almost all dialogs by hitting the F1 key or by pressing the Help button which has been added to most dialogs.

gimp-help-2 is available from [ftp.gimp.org](ftp://ftp.gimp.org/pub/gimp/help/). The manual can also be accessed online at [docs.gimp.org](https://docs.gimp.org/).

## Bugs

Thanks to the pre-releases we did over the last month, this code has already seen quite some testing but a lot has changed since version GIMP 2.0 and we could very well have introduced new bugs. If you find any , make sure you report them at [bugzilla.gnome.org](https://bugzilla.gnome.org/) (after checking that it isn't reported already).

## What's New in GIMP 2.2

Here is a brief summary of some of the most important new features
introduced in GIMP
2.2. There are many other smaller changes that long-time users will notice
and appreciate (or complain about!). There are also important changes at
the level of plug-in programming and Script-Fu creating that are not
covered here.

For a detailed list of changes, please have a look at the NEWS file, which is contained in the tarball and available [online](https://gitlab.gnome.org/GNOME/gimp/raw/gimp-2-2/NEWS).

### Interoperability and Standards Support

- You can drag-and-drop or copy-and-paste image data from the
GIMP to
any application which supports image/png drops (currently
*Abiword* and *Kword* at least) and image/xml+svg drops
(*Inkscape*
supports this one). So you can copy-and-paste curves into the
GIMP from *Inkscape*, and then drag a
selection into *Abiword* to include it
inline in your document.
- Patterns can now be any supported *GtkPixbuf*
format, including png, jpeg, xbm and others.
- GIMP can load gradients from SVG files, and
palettes from ACT and RIFF files.
- Drag-and-drop support has been extended. You can now drop
files and URIs onto an image window, where they will be opened
in the existing image as new layers.
Please note, that Drag and Drop will not work for Apple Mac OS X
between GIMP and the finder. This is due to a lack of
functionality on Apples X11.app

### Shortcut Editor

You can now edit your shortcuts in a dedicated dialog, as well as
continue to use the little-known dynamic shortcuts feature (which has
been there since 1.2).

### Plug-in Previews

We have provided a standard preview widget for plug-in authors which
greatly reduces the amount of code required to support previews. David
Odin has integrated this widget into all the current filters, so that
now many more filters in the GIMP include a preview
which updates in
real time, and the various previews behave much more consistently.

### Real-Time Previews of Transform Operations

The transform tools (shear, scale, perspective and rotate) can
now show a real-time preview of the result of the operation when
the tool is in *Traditional* mode.  Previously, only a
transforming grid was shown.

### GNOME Human Interface Guide Conformance

A lot of work has been done on making the GIMP's
interface simpler and
more usable for newcomers. Most dialogs now follow the
GNOME HIG to
the best of our knowledge. In addition, dialogs have separated out or
removed many *Advanced* options, and replaced them with
sane defaults or hidden them in an expander.

### GTK+ 2.4 Migration

- Menus use the *GtkUIManager* to
generate menu structure dynamically from XML data files.
- A completely revamped File Chooser is used everywhere in the
GIMP
for opening or saving files. The best thing about it is that it
lets you create a set of *bookmarks*, making it possible to
navigate quickly and easily to commonly used directories.
- GIMP now supports fancy ARGB cursors when they
are available on the system.

### Basic Vector Support

Using the GFig plug-in, the GIMP now supports the
basic functionality
of vector layers. The GFig plug-in supports a number of vector
graphics features such as gradient fills, Bezier curves and curve
stroking. It is also the easiest way to create regular or irregular
polygons in the GIMP. In the
GIMP 2.2, you can create GFig layers, and
re-edit these layers in GFig afterwards. This level of vector support
is still quite primitive, however, in comparison to dedicated
vector-graphics programs such as *Inkscape*.

### Also...

- There are many other smaller user-visible features. A rapid-fire list
of some of those features is below.
- It is now possible to run the GIMP in batch
mode without an X server.
- We have a GIMP binary
(GIMP-console) which is not linked to GTK+ at all.
- Improved interface for extended input devices
- Editable toolbox: You can now decide which tools should be shown
in the Toolbox, and their order. In particular, you can add any or
all of the Color Tools to the Toolbox if you wish to.
- Histogram overlays R, G and B histograms on the Value histogram, and
calculates the histogram only for the contents of the selection.
- Shortcuts are now shared across all GIMP windows.

## What's Next

You can expect updated versions of gimp-gap, gimp-freetype, gimp-plugin-template and hopefully also gimp-perl to appear over the next weeks. The GIMP source tree will soon be branched so that development can continue towards GIMP 2.4\. We haven't yet made up a detailed roadmap, but we will try to publish one soon.

Lots of people have helped to make this release happen. Thanks to all the contributors who wrote code, sent patches, worked on the docs, translated the user interface, reported bugs or just made helpful suggestions. GIMP 2.2 wouldn't exist without your help. Keep it coming!

_Happy GIMPing,
 The GIMP developers_
