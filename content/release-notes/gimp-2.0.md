Title: GIMP 2.0 Release Notes
Date: 2024-03-16T00:00:00+01:00
Authors: Wilber
Status: Published

First, a statistic: the GIMP code base
contains about 230,000 lines of C code, and
most of these lines were rewritten in the
evolution from 1.2 to 2.0. From the user's point of view, however,
GIMP 2 is fundamentally similar to
GIMP 1; the features are similar enough that
GIMP 1 users won't be lost. As part of the
restructuring work, the developers cleaned up the code greatly, an
investment that, while not directly visible to the user, will ease
maintenance and make future additions less painful. Thus, the
GIMP
2 code base is significantly better organized and more
maintainable than was the case for GIMP 1.2.

## Basic tools

The basic tools in GIMP 2 are not very
different from their predecessors in
GIMP 1. The
*Select Regions by Color* tool
is now shown in the GIMP toolbox, but
was already included in GIMP 1 as a
menu option in the Select menu. The Transform
tool has been divided into several specialized tools:
Rotation, Scale, Shearing and Perspective. Color operations
are now associated with layers in the menu
*Layer \> Colors*,
but this is merely a cleanup: they were already present in
the Image menu (illogically, since they are layer
operations). Thus no completely new tools appear in this
release, but two of the tools have been totally revamped
compared to the older versions: the Text tool and the Path
tool. More on this below.

The user interface for tools has also changed significantly.
The *Tool Options* dialog box was modified to
not resize
itself when a new tool is chosen. Most users felt that the
window changing size when a new tool was selected was
annoying. Now, by default the *Tool Options*
dialog is
constantly open and docked under the toolbox, where it can
easily be found.

## Tool options

The *Tool Options* for many tools have new
possibilities that weren't available in
GIMP 1. Without being exhaustive,
here are the most noticeable improvements.

All selection tools now have mode buttons: Replace, Add,
Subtract and Intersect. In GIMP 1 the
only way to change the
selection mode was to use the
`Ctrl` or `Shift`
buttons, which
could get very confusing because those buttons also had
other functions. For example, pressing and holding the
`Shift`
key while using the Rectangle selection tool forces the
rectangle to be a square. Thus, to add a square selection
you would first press `Shift`, then click
the mouse, then release `Shift`, then press
`Shift` again, then sweep out the
selection with the mouse, then release `Shift`.
It can now be done more easily.

For transformation tools, buttons now control which object
(layer, selection or path) is affected by the
transformation. You can for example transform a rectangular
selection to various quadrilateral shapes. Path
transformation in particular is now easier than it was
before.

*Fade out* and
*Paint Using Gradient* are now available for
all drawing tools. In fact, all drawing tools now have their
own individual brush, gradient and pattern settings, in
contrast to GIMP 1 where there was a
single global setting
that applied to all drawing tools. Now you can select
different brushes for the Pencil and the Paint Brush, or
different patterns for the Clone and Fill tools. You can
change these setting by using your mouse wheel over the
relevant resource button (this is most useful for quickly
and easily choosing a brush).

## User Interface

The most visible changes in GIMP 2
concern the user interface. GIMP now
uses the GTK2+ graphical toolkit in
place of GTK+. One of the nice features
brought by the new
libraries is dockable dialogs, and tab navigation between
dialogs docked in the same window — a feature present in
several popular web browsers. GIMP 1
was famous for opening dialogs anywhere on your screen;
GIMP 2 can be told to use
fixed boxes. Dialogs now include a little tab-customization
menu, which provides maximum flexibility in organizing your
workspace.

The Image window has some interesting new features. These
are not necessarily activated by default, but they can be
checked as options in the
*Preferences \> Interface \> Image Windows*
menu. *Show Brush Outline*, for example,
allows you
to see the outline of the brush when using drawing tools. In
the *Appearance* sub-section, you can toggle
whether a menu
bar is present at the top of image windows. You can set an
option to work with the new fullscreen mode. Viewing options
are also available from all image windows using right click
to bring up the menu, then selecting *View*.
The so-called *image* menu is also available
by clicking on a little
triangle in the top left corner of the drawing space. The
setting you choose in the *Preferences*
dialog is used as
the default value, and options you set from an image are
used only for that image. (You can also toggle fullscreen
mode by using the `F11` key; the
`Esc` key also exits fullscreen mode).

GIMP
2 features keyboard accelerators to ease menu access. If you
find that navigating through menus using your mouse is
onerous, the solution may be to use the keyboard. For
example, if the menu bar is present, to create a new image
just hit
`Alt`+`F`+`N`. Without the menu bar, hit
`Shift`+`F10`
to open the top-left menu, and use direction keys or
`F` then `N`
to create the new image. Keyboard accelerators are different
from shortcuts: accelerators are useful to navigate through
menus, whereas shortcuts call a specific menu item directly.
For example,
`Ctrl`+`N`
is a shortcut, and the quickest way to open a new image.

To ease access to your most commonly used menu items, the
GIMP has provided dynamic shortcuts for
many years. When a
menu is open, you can hover over the desired menu item and
hold down your shortcut combination. This feature is still
present, but is deactivated by default in the
GIMP 2.0, to
avoid accidental re-assigning of existing shortcuts.

The GIMP also ships with a number of
sets of key-bindings
for its menus. If you would like to replace the default
GIMP
keybindings by Photoshop bindings, for example, you can move
the file `menurc` in your user data
directory to `oldmenurc`, rename
`ps-menurc` to
`menurc` and restart
GIMP.

## Handling Tabs and Docks

The GIMP
2.0 introduces a system of tabbed dialogs to allow you to make your
workspace look the way you want it to be. Almost all dialogs can be
dragged to another dialog window and dropped to make a tabbed dialog
set.

Furthermore, at the bottom of each dialog, there is a dockable area:
drag and drop tabs here to attach dialogs beneath the bottom tab
group.

## Scripting

*Python-fu* is now the standard external
scripting interface for GIMP 2. This
means that you can now use GIMP
functions in Python scripts, or
conversely use Python to write
GIMP plug-ins. Python
is relatively easy to understand even for a
beginner, especially in comparison to the
Lisp-like Scheme
language used for Script-Fu in GIMP 1.
The Python bindings
are augmented by a set of classes for common operations, so
you are not forced to search through the complete
GIMP
Procedural Database in order to carry out basic operations.
Moreover, Python has integrated
development environments and
a gigantic library, and runs not only on
Linux but also on
Microsoft Windows and Apples Mac OS X. The biggest drawback,
for GIMP 2.0, is that the standard user
interface offered in
Python-fu does not use the complete power of the
Python
language. The interface is currently designed to support
simple scripts, but a more sophisticated version is a goal
of future development.

GIMP-Perl is no longer distributed with
the standard GIMP 2
distribution, but is available as a separate package.
Currently, GIMP-Perl is supported only
on Unix-like
operating systems. It includes both a simple scripting
language, and the ability to code more polished interfaces
using the Gtk2 perl module. Direct
pixel manipulation is
available through the use of PDL.

Script-Fu, based on *Scheme*,
has the same drawbacks as before: not intuitive, hard to use
and lacking a real development environment. It does, however,
have one major advantage compared to Python-fu: Script-Fu
scripts are directly interpreted by GIMP
and do not require any additional software installation.
Python-fu requires that you install a package for the Python
language.

## The Text Tool

The big problem with the standard text tool in
GIMP
1 was that text could not be modified after it was rendered.
If you wanted to change anything about the text, all you could
do was *undo*
and try again (if you were lucky enough to have sufficient
undo history available, and then of course you would also undo
any other work you had done in the meantime). In
GIMP 1.2 there was also a
*dynamic text*
plug-in that allowed you to create special text layers and
keep them around indefinitely, in a modifiable form, but it
was buggy and awkward to use. The second generation Text tool
is an enhanced combination of the old Text tool and the
Dynamic Text plugin. Now all options are available in the
*Tool Options*
: font, font size, text color, justify, antialiasing, indent,
spacing. To create a new text item, click in the image and a
little editor pops up. Text appears on the image while you are
editing (and carriage returns are handled properly!). A new
dedicated layer is created; this layer resizes dynamically to
match the text you key in. You can import plain text from a
file, and you can even do things like writing from right to
left in Arabic. If you select a text layer, clicking on it
opens the editor, and you can then modify your text.

## The Path Tool

The second generation Path tool has a completely new interface. The
first major difference you notice is that paths are no longer
required to be closed. A path can be made up of a number of disjoint
curve segments. The next major difference is that now the path tool
has three different modes, Design, Edit and Move.

In Design mode, you can create a path, add nodes to an
existing path and modify the shape of a curve either by
dragging edges of the curve or dragging the
*handles* of a node.

In Edit mode, you can add nodes in the middle of curve edges, and
remove nodes or edges, as well as change the shape of the curve. You
can also connect two path components.

The third mode, Move, is, as you might expect, used to move
path components. If your path has several components, you
can move each path component separately. To move all
components at once, use the `Shift` key.

Two other path-related features are new in the
GIMP 2.0. The GIMP
can not only import an SVG image as a raster image, but
can also keep SVG paths intact as GIMP
paths. This means that the GIMP is now
more able than ever to complement your
favorite vector drawing tool. The other feature which has
made the path tool much better is the introduction of
vector-based stroking. In previous versions, stroking paths
and selections was a matter of drawing a brush-stroke along
the path. This mode is still available, but it is now
possible to stroke a curve accurately, using the vector
library libart.

## Other improvements

Some other improvements in brief:

- Higher-quality antialiasing in some places — most notably in the Text tool.
- Icons and menus are skinnable. You can create your own icon set and apply it to the toolbox using the
*Preferences \> Interface* menu option. A theme called *small* is
included with the standard distribution.
- An image can be saved as a template and used to create new
images.
- There are four new combination modes for layers that lie
one on top of another within an image:
*Hard Light*, *Soft Light*, *Grain Extract* and *Grain Merge*.
- If there is an active selection, you can crop the image
directly to the selection size using image menu
*Image \> Crop*.
- As well as being able to create guides, there's now a grid
functionality in GIMP.
It is complementary to the guides functionality and makes it
easier to position objects so that they align perfectly.
- The Layers dialog is more coherent, in that there are no more
hidden functions accessed only with right click on the miniature
image of the layer that appears there. You can now handle layer
operations directly from the image menu: Layer Mask,
Transparency, Transformation and Layer Color operations are
directly in the Layer submenu.
- Color display filters are now available from the image menu
*View \> Display Filters*.
Using them, you can
simulate different gamma values, different contrasts, or
even color deficient vision, without altering your
original image. This actually has been a feature of the
GIMP developer versions for a long
time, but it has never been stable enough to appear in
a stable version of the GIMP before.
- The color selection dialog has a new CMYK mode, associated with
the printer icon.
- Data stored in EXIF
tags by digital cameras are now handled in read and write mode
for JPEG files.
- MNG animations are now supported. The MNG file format can be
considered as animated PNG. It has all the advantages of PNG
over GIF, such as more colors, 256 levels of transparency, and
perhaps most importantly, lack of patent encumbrance. The format
is a web standard and all recent popular web browsers support
it.
- The GIMP Animation package now does
onion-skinning, a
bluescreen feature was added as well as audio support.
- A channel mixer filter, previously available from the web as an
add-on, appears in *Filters \> Colors*.

