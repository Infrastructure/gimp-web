Title: Downloads
Date: 2015-08-14T14:18:56-05:00
Author: Pat David
Template: downloads-comments

The official download page for all things GIMP!

Please only use the official binaries provided here unless you really, really know what you're doing (it's the only way to be safe).

We try to provide binaries in-time with regular releases, but may occasionally be delayed as the packagers pull everything together, please be patient.


--- 

The actual page content for /downloads/ has been moved into its own template
file at `./themes/newgimp/templates/downloads-comments.html`.

If you want to edit the text of the /downloads/ page, please see the above
template file.
