Title: 💘 Thank you for your support! 💌
Date: 2021-07-17T14:18:56-05:00
Author: nmat
Status: hidden

*(Your download should begin shortly. It didn't start? [Then go back to the
download page and try again](/downloads/))*

**Did you know: GIMP is built with love _entirely by volunteers_.** 👷
<br/>**Please consider helping us to build a better GIMP for everyone!**

<p class='buttons'>
  <a href="https://www.gimp.org/donating/">
    <span class='donate-button'>
    Support us by<br/> <strong>Donating</strong>
    </span>
  </a>
</p>

Donations help us to sustain the project by supporting developers, community, maintaining hardware…
Every little bit helps and is a wonderful way to show some love for the project!

## Learn GIMP's basics

Check out the [tutorials](/tutorials/) or [documentation](/docs/) to learn more.

## Contribute

<p class='buttons'>
  <a href="https://gitlab.gnome.org/GNOME/gimp/">
    <span class='donate-button'>
    Help with<br/> <strong>GIMP development</strong>
    </span>
  </a>
</p>

You don't have to be a developer to contribute to the project!

There are other ways to join the project including:

 * Writing [documentation](https://docs.gimp.org/)
 * Creating [tutorials](/tutorials)
 * [Bug reporting and triaging](https://gitlab.gnome.org/GNOME/gimp/-/issues)
 * Feature testing
 * Translating GIMP and documentation
 * Create cool things (and tell people about it)

Even simply talking about GIMP and amplifying posts/social interactions goes a long way to helping raise awareness of the project.

Find out [more about getting involved!](/develop/)
