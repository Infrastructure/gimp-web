Title: Heart Shape
Date: 2021-07-16T16:40:00-05:00
Modified: 2021-07-16T16:40:05-05:00
Author: GIMP Team
Summary: Create a heart shape

## Intention

This tutorial is aimed to teach you to create a heart shape in GIMP using selections.

## Step 1

Fire up GIMP and create a new image. I used a white background at 400px by 400px and no specific other settings changed.
<figure>
<img src="{static}00-new-image.png" alt="Heart Shape" />
</figure>

## Step 2

Create a new transparent layer.
<figure>
<img src="{static}0a-new-transparent-layer.png" alt="Heart Shape" />
</figure>

## Step 3

Select the tool (**Ellipse Select Tool**) or on keyboard press <kbd>E</kbd>
In the tool options select **'Fixed' - Aspect ratio**

Draw first circle shape and fill selection with red color. See below image:

<figure>
<img src="{static}01-circle-shape-red.png" alt="Heart Shape" />
</figure>

## Step 4

Deselect the shape by going to: 
<div class="MenuCmd"><span>right click -> Select -> None</span></div>

or <kbd>Shift+Ctrl+A</kbd> on keyboard.

## Step 5

Create another transparent layer

## Step 6

Select the **Rectangle Select Tool** or press <kbd>R</kbd> on keyboard

Make the selection from the middle of the circle and down. And fill with red color:

<figure>
<img src="{static}02-rectangle-shape-red.png" alt="Heart Shape" />
</figure>

## Step 7

Deselect the shape by going to: 
<div class="MenuCmd"><span>right click -> Select -> None</span></div>

or <kbd>Shift+Ctrl+A</kbd> on keyboard.

## Step 8

Right click the layer for the rectangle shape and select **"Merge down"** so it merges with the layer
of the circle shape. See layers dialog example below:

<figure>
<img src="{static}03-layers-dialog-after-merge.png" alt="Heart Shape" />
</figure>

## Step 9

Select the tool **"Rotate Tool"** or press <kbd>Shift+R</kbd> on keyboard 
and click the layer to rotate which is the red shape dialog.

In the dialog, change the **Angle** to be **45**. See image below:

<figure>
<img src="{static}04-rotate-shape.png" alt="Heart Shape" />
</figure>

NOTE! I have changed the settings for the tool to be "Direction: Normal (Forward)" therefore my shape is
forwarded rotated then backward. In general it does not matter because we are going to duplicate and move
layers around in next step. The main part is only to have it rotated.

## Step 10

In layers dialog duplicate the shape layer. See image below:

<figure>
<img src="{static}05-duplicated-layers.png" alt="Heart Shape" />
</figure>

## Step 11

Select the tool: **Flip Tool** or press <kbd>Shift+F</kbd> on keyboard and Flip the new shape. See below:

<figure>
<img src="{static}06-layers-flipped.png" alt="Heart Shape" />
</figure>
<figure>
<img src="{static}07-image-flipped.png" alt="Heart Shape" />
</figure>

## Step 12

Select the tool: Move Tool or press <kbd>M</kbd> on keyboard.

Move one of the layers so they align at the bottom of the shape
(NOTE: you can move the layer with the arrow keys on the keyboard also). See below:

<figure>
<img src="{static}08-moved-layers-to-align-shape.png" alt="Heart Shape" />
</figure>

Almost done! Now lets merge the shape together.

## Step 13

In the top layer for the shape. Right click the layer and press **"Merge Down"**.

<figure>
<img src="{static}09-final-shape-layers.png" alt="Heart Shape" />
</figure>

## Final step

Last let's fix the layer boundaries. Right click the shape layer and press **"Layer to Image Size"**

<figure>
<img src="{static}10-final-result.png" alt="Heart Shape" />
</figure>

<small>
<a href='https://creativecommons.org/licenses/by-sa/3.0/deed.en_US'>
<img class='cc-badge' src='/images/creativecommons/by-sa-80x15.png' alt='Creative Commons By Share Alike'/>
</a>
<span xmlns:dct="http://purl.org/dc/terms/">GIMP Tutorial - Heart Shape</span> by [GIMP Team](https://www.gimp.org/)
Licensed under a [Creative Commons Attribution-ShareAlike 3.0 Unported License](https://creativecommons.org/licenses/by-sa/3.0/deed.en_US).
</small>
