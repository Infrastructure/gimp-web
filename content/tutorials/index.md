Title: Tutorials 
Date: 2015-07-29T14:40:35-05:00
Modified: 2015-07-29T14:40:43-05:00
Authors: Pat David
Summary: The tutorials page.

🛈 As everything created by the GIMP project, these tutorials are fully
community-written. Therefore if you find any tutorial incomplete,
or if it contains errors or outdated information, the best thing is to
contribute a fix. Moreover we warmly welcome new tutorials. [See below
for the full contribution
procedure](#contributing-new-tutorials-or-fixes).🛈

## Beginner

[GIMP Quickies][]  
Use GIMP for simple graphics needs without having to learn advanced image manipulation methods.

[Simple Floating Logo][]  
This tutorial walks through some basic image and layer manipulation techniques.

[Making a Circle-Shaped Image][]  
How to create a circular-shaped image.

[Making a Heart Shape with Selections][]  
How to create a heart-shape with selections.

[Layer Masks][]  
An introduction to using layer masks to modify the opacity of a layer.

[Basic Color Curves][]  
A first look at the Curves tool and adjusting color tones in an image.

[Your GIMP Profile (and You)][]  
What the GIMP Profile is and how to use it.

[Image Formats Overview][]  
Selecting the best image format for your purposes.

[Asset Folders][]  
Extending GIMP with new plug-ins, scripts, brushes, and more.

[GIMP Quickies]: {filename}GIMP_Quickies/index.md
[Simple Floating Logo]: {filename}Floating_Logo/index.md
[Layer Masks]: {filename}Layer_Masks/index.md
[Basic Color Curves]: {filename}Basic_Color_Curves/index.md
[Your GIMP Profile (and You)]: {filename}GIMPProfile/index.md
[Making a Circle-Shaped Image]: {filename}CircleImage/index.md
[Making a Heart Shape with Selections]: {filename}Heart_Shape/index.md
[Image Formats Overview]: {filename}ImageFormats/index.md
[Asset Folders]: {filename}Asset_Folders/index.md


## Photo Editing

[Digital B&W Conversion][]  
Detailed conversion tutorial for generating a B&W result from a color image.

[Luminosity Masks][]  
Using multiple layer masks to isolate specific tones in your image for editing.

[Tone Mapping with 'Colors/Exposure'][]  
Using high bit depth GIMP's 'Colors/Exposure' operation to add exposure compensation to shadows and midtones while retaining highlight details.

[Focus Group][]  
Layer masking and creative filter applications.

[Digital B&W Conversion]: {filename}Digital_Black_and_White_Conversion/index.md
[Luminosity Masks]: {filename}Luminosity_Masks/index.md
[Tone Mapping with 'Colors/Exposure']: {filename}Tone_Mapping_Using_GIMP_Levels/index.md
[Focus Group]: {filename}Focus_Group/index.md


## Painting

[Parametric Brushes][]  
A look at the advantages and flexibility of using Parametric Brushes.

[Parametric Brushes]: {filename}Parametric_Brush/index.md

## Configuration

[How to set the tile cache](/unix/howtos/tile_cache.html)  
The tile cache tells GIMP how much memory it can use before swapping some image
data to disk. Setting the size of the tile cache can be critical for the
performance of your system.

[How to install fonts for use with GIMP](/unix/fonts.html)  

[How to use MIDI devices with GIMP](/unix/howtos/gimp-midi.html)  
GNU/Linux only procedure.

## Programming

[Basic GIMP Perl][]  
Learn how to write simple perl scripts using the gimp-perl module (GNU/Linux users only).

[Automate Editing][]  
Using GIMP Python to automate a workflow.

[Automatic Creation of XCF from JPG][]  
Import XCF images a directory at a time.

## Contributing new tutorials or fixes

You don't have to be a developer to participate to the GIMP project.
Some team members help with design, community management, bug triaging…
and some help with documentation and tutorials!

The only thing we ask of contributors is to use *Libre* licenses, which
allow anyone to share, redistribute and modify the tutorials. It enables
others to fix tutorials, even years later, even if the original author
is not around anymore. This is the same rules as for our code, making
GIMP a Free Software.

<span id="recommended-licenses">**Recommended licenses**</a> are:

* [Free Art 1.3](https://artlibre.org/licence/lal/en/)
* Creative Commons [Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/)
* Creative Commons [Attribution 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/)
* [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/).

So you want to contribute? The process is simple, here is an example for
the hypothetical case where you found an error in an existing tutorial.
In such a case of editing existing text, your change will be automatically
considered to be in the same license as chosen by the first author:

1. All tutorial contents can be found on our [website's
   repository](https://gitlab.gnome.org/Infrastructure/gimp-web/-/tree/testing/content/tutorials)
   (*Gitlab*). After clicking this link, feel free to browse to understand the file structure: Each
   tutorial is listed in a separate folder and each folder has a file called `index.md` which
   contains the text of the tutorial. There are many other files that accompany the
   tutorial inside the folder, such as images, screenshots and
   various assets.
2. Once you found a tutorial, which you want to edit, click the
   `index.md` link, and you will see a blue button saying "Edit". Click
   it (obviously you need to have created a login first and be
   connected).
3. *Gitlab* will propose you to "fork" the project because of missing
   permissions.  Press "fork".
4. Once this is done, make your changes using the
   [Markdown](https://daringfireball.net/projects/markdown/syntax)
   syntax.
5. When you are happy with your edits, proceed to the bottom of the edit page, notice the "Commit message" field.
   Fill in relevant information explaining what you did. Click "Commit
   changes".
6. A "New merge request" page will appear. Click "Create merge request".
7. It's done! Now all you have to do is wait for one of the core members
   to review and validate your changes.

If you just need to change an image (e.g. because it shows an outdated
version of GIMP), you may select the image in *Gitlab* and click the
"Replace" button. It will propose you to upload a new version and to
write a "Commit message" to explain the change. Please write down the
[desired *Libre* license](#recommended-licenses) for the image in the
commit message. Then hit "Replace file".

For more complicated changes, or if you want to propose new tutorials,
it might be necessary to understand at least the basics of the *git*
source versionning system we use.
Alternatively, you may simply write your tutorial in a new text file on
your machine (following *Markdown* syntax), then simply [open a
report](https://gitlab.gnome.org/Infrastructure/gimp-web/-/issues/new)
and upload all the files (the text file as well as images and other
resources) in this report. Do not forget to tell us the license you
chose from [the list given above](#recommended-licenses).

---

<small>
A list of legacy tutorials can be found here:
[All (Legacy) Tutorials List]({filename}list-all.md)

Bear in mind that this list is being provided for legacy reasons only.
They were originally made for very old hence outdated versions of GIMP
and most of them do no use Libre licenses allowing editing.
</small>

[Basic GIMP Perl]: {filename}Basic_Perl/index.md
[Automate Editing]: {filename}Automate_Editing_in_GIMP/index.md
[Automatic Creation of XCF from JPG]: {filename}AutomatedJpgToXcf/index.md


