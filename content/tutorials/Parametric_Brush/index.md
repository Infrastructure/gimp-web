Title: Parametric Brushes
Date: 2018-07-19T11:43:37-06:00
Author: Americo Gobbo
Status: published
Template: page_author

<figure><img src="parametric-brushes.assets/parametric-brushes-personaggio-bizarro.jpg" width='530' height='507'>
<figcaption>A fast sample made in 2013 by Americo Gobbo with Dry Media paint dynamics and parametric brushes.</figcaption>
</figure>

## GIMP Brushes
GIMP provides two very different types of brushes: the parametric (.vbr) and raster (.gbr and .gih).
The scope of this article mainly demonstrates the quality and possibility of parametric brushes. I am writing a dedicated article about the GIMP brushes and I have pasted the short intro about them:

> The parametric brush is a vector brush and it has the great possibilities and flexibility for many different tasks and branches (graphic design, photography, digital painting, etc...)
The raster brush is also versatile and has great pictorial qualities, but is more complex to build because it is managed as an image.
Probably the choice, between raster and/or parametric brush, is done by the kind of task, the painter's style and of his/her artistic background/knowledge/taste, etc.


## The Parametric Brush and Painting Techniques
It seems artists haven't yet realized the full usefulness and versatility of GIMP's parametric brushes, especially when paired with the proper dynamics. This article shows how to use parametric brushes to make convincing emulations of dry media such as pencil, color pencil, charcoal, chalk and pastel.

<figure><img src="parametric-brushes.assets/parametric-brushes-dialog-editor.png" width='410' height='442' title="Brush Set and Parametric (.vbr) Brush Editor." /><figcaption>Brush set and Parametric Editor (.vbr).</figcaption></figure>

The default shapes are round, square and diamond. It's also possible create a great variation of the shapes using the different parameters to control the shape and its aspect (radius, spikes, hardness, aspect ratio, angle and spacing).

Some of the advantages of parametric brushes are:

* The stroke of the parametric brush is sharpest and hardest when we use hardness equal to 1 than with the raster brushes,
* It's easy to increase/decrease the hardness, applied correctly, via own editor or via Tool Options. The .gbr and .gih brushes, when we increase/decrease the hardness, is deformed by a blur algorithm: [Comparison test between hardness and Force with .gbr and .vbr brushes](https://gitlab.gnome.org/GNOME/gimp/issues/1081 "Issue #1081")

### Dry Media Experiments
In 2013, I began to study emulating dry media with parametric brushes, they are essentially the drawing tools such as pencil, crayon, charcoal and pastel.
I have also invited Gustavo Deveze and Elle Stone to use my parametric brush set with 2 paint dynamics dedicated exclusively to dry media.

<figure>
<img src="parametric-brushes.assets/parametric-brushes-deveze-dry-media-test.png" width='800' height='600' title="Dry Media by Gustavo Deveze" />
<figcaption>Gustavo Deveze drawing with the Dry Media paint dynamics with the brush set of parametric brushes.
</figcaption>
</figure>

#### Paintbrush and Airbrush Tool
To explore the dry media effects with parametric brushes, I have revised initially in 2013, the Pencil Generic Paint Dynamics to adapt better it to other dry media.
After working with the variations of Pencil Generic, I have found a good compromise, but the methodology was complex and based only on trial and error.

> I have thought it interesting to understand better real media and to try a way of modeling the effects via paint dynamic making a good attempt of real variables on these media. The first steps have demonstrated good results, and I will describe this methodology in another article.

I have also verified that it's possible to use different tools, pencil, paintbrush and airbrush. Effectively the airbrush is most interesting mainly due to the Rate and Flow controls.

<figure><img src="parametric-brushes.assets/parametric-brushes-dry-media-airbrush-x-paintbrush.png" width='724' height='831' title="Parametric brush with Dry Media Paint Dynamics - Paintbrush Tool versus Airbrush Tool" /><figcaption>Samples using round parametric brushes of the set with my paint dynamic Dry Media.</figcaption></figure>

In general, all brush sizes are working well, but mainly between 8 ~ 64 pixels seems the best compromise. The large brushes are interesting for covering large areas but it's not possible to have a good shape (they are appearing a bit soft).

## Parametric Basic Brush Set and Paint Dynamics | Download

<figure><img src="parametric-brushes.assets/parametric-brushes-shapes-and-strokes-dry-media.png" width='971' height='991' title="Parametric brush with Dry Media Paint Dynamics with Airbrush Tool" /><figcaption>Samples using round parametric brushes with different shapes with Airbrush tool. Tool.</figcaption></figure>

The brush set, in the 'B0' folder, contains the .vbr basic brushes, round and block (hard and soft versions). There are two paint dynamics: one for graphics tablet's stylus with tilt and not. The version with tilt is fun to use and it's possible to emulate well the effect when you are inclining the tool in real cases.

> In general, it seems that users prefer to pick the brushes directly on the dialog palette. The parametric brush is so easy to modify and create our own set... but many people don't have time and 'energy' to build own our brushes. To those, that they think that is a large set they could delete the brushes not usable for them.

Download [Brush Set + Paint Dynamics](parametric-brushes.assets/parametric-brushes-Brushset-and-paint-dynamics.zip "Parametric Brush Set + Dry Media Paint Dynamics").

## Tool Options Settings
To emulate better the dry media I recommend using the Airbrush Tool with these settings on Tool Options:

* Opacity 75 ~ 100
* Enable the Jitter and use values between 0.5 ~ 0.75 circa.
* Force¹ at 100% (normally is 50% by default).
* Rate between 75 to 125
* Flow around 50
* Motion Only, read this [excerpt](https://goo.gl/PLTUKt "About Motion Only to the Airbrush tool") of The Book of GIMP: A Complete Guide to Nearly Everything. By Olivier Lecarme, Karine Delvare.

*¹ Only in GIMP 2.10 and 2.99 Devel.*

<figure><img src="parametric-brushes.assets/parametric-brushes-tool-options-settings-dry-media.png" width='256' height='758' title="Tool Options Settings" /><figcaption>Airbrush, my proposal of the settings for the dry media effects.</figcaption></figure>

We have tested on GIMP 2.10, 2.99 and 2.8 with good results.


### Acknowledgements
To [Elle Stone](https://ninedegreesbelow.com/) and Gustavo Deveze who helped me with paint dynamics tests and images to illustrate this document. Also, thanks to [Pat David](https://patdavid.net) for giving me support with GitLab and formatting of the article for www.gimp.org.

### Author, License (Brush Set and Paint Dynamics)
<small>
<a href='https://creativecommons.org/publicdomain/zero/1.0/'>
<img class='cc-badge' src='/images/creativecommons/publicdomain.svg' alt='Creative Commons Public Domain Dedication'/>
</a>
<span xmlns:dct="http://purl.org/dc/terms/">GIMP Tutorial - Parametric Brushes (text, images, brushes)</span> by [Americo Gobbo](http://americogobbo.com.br/) is licensed under a [CC0 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
</small>

