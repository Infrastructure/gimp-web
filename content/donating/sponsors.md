Title: Sponsors
Date: 2015-08-14T15:54:25-05:00
Authors: Pat David, Aryeom, Jehan

💌 The GIMP team would like to thank all the sponsors for the help
given. **Thank you!** 💌

*Note: the following lists in no way reflect the full list of sponsors
of GIMP across the years. In particular, we want to thank the numerous
people who made small [donations](/donating/). With small drops, you
created a huge river!*

## Official Mirrors

<figure>
<img src="/images/2021-wilber_and_co-mirror.jpg" alt="Mirrored Wilber - Wilber and co. comics strip"/>
<figcaption>
<em>"Mirrored Wilber" by <a href="https://film.zemarmot.net">Aryeom</a>, Creative Commons by-sa 4.0</em>
</figcaption>
</figure>

*Entities sponsoring GIMP by mirroring the downloads, hence lessening
burden on our servers (last update: <!-- DATE -->).*

<!-- MIRRORS -->

If you wish to be a part of this list by creating an official
GIMP mirror and get rsync credentials, follow our [official mirror
process](/news/2024/12/27/gimp-3-0-RC2-released/#how-to-be-an-official-mirror-procedure-update)

## Infrastructure Sponsors

These sponsors make it possible to build, test builds and distribute
executables:

[Circle CI](https://circleci.com/)
: MacOS x86_64 builds and CI

[MacStadium](https://www.macstadium.com/)
: Apple Silicon build runners

[Arm Ltd.](https://www.arm.com/)
: 3 Windows/Aarch64 CI build runners

*Note: [Microsoft](https://www.microsoft.com/) sponsored the
registration fee for Microsoft Partner Center in 2022.*

## Hardware Sponsors

These sponsors donated some hardware to contributors for development:

* 2023: [Arm Ltd.](https://www.arm.com/) donated a Windows Dev Kit 2023.
* 2021: [Purism](https://puri.sm/) donated a Librem Mini.

## Past Sponsors

*The GIMP team remains grateful for the past assistance of the
following:*

Philantropist
: Lars Mathiassen
: Hiroyuki Ikezoe

Patron
: Free Software Foundation
: O'Reilly
: BackupAssist
: Shawn Collier

Sponsor
: EU Edge
: Adrian Likins
: Distrowatch
: MacGIMP

Associate
: Gimp-fr.org, Peter Jensen, James Yenne, Roger Mattson, Jonathan Grant, Peter Kupec, Lex Biesenbeek, Soeren Fanoe, Thomas Zehetner-Schatzl, Eric Pierce, Ken Colette, Cedric Gemy, Jaral Kijkanjanapaiboon, Margaret Wong, Barton Woolery, Gordon Rios, Nathaniel Friedman, Tord Lindner, Marco Candrian, Mark Slagell, Manuel Schuhmacher, Jared Stookey, Darren Dale, James Eadon, Yasuhisa Minagawa, Wayne Harris, Jay Patel, Curdegn Bandli, Reinhard Moser, Adrian Ulrich, Reinhard Lehar, David Kuntz, Cristopher Eisley, John Woltman, Nigel Ecclesfield.
