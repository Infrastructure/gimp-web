Title: Donate
Date: 2015-08-14T15:54:25-05:00
Author: Pat David
Status: hidden

Donating money is important: it makes GIMP sustainable.

## Fund Core Team Developers Directly

We don't raise funds to sponsor development as an organization yet and encourage contributor fundraisers:

### Patreon

<p class='buttons'>
  <a href="https://www.patreon.com/pippin">
    <span class='donate-button'>
      Fund Øyvind Kolås<sup>1</sup><br/>
      <strong>GEGL development</strong>
    </span>
  </a>
  <a href="https://www.patreon.com/zemarmot">
    <span class='donate-button'>
      Fund ZeMarmot<sup>2</sup><br/>
      <strong>GIMP development</strong>
    </span>
  </a>
</p>

1. **Øyvind Kolås** [raises funds](https://www.patreon.com/pippin) for his work on [GEGL](http://gegl.org/), GIMP's image processing core. Øyvind is GEGL maintainer and primary developer.

2. **Aryeom and Jehan** [raise funds](https://www.patreon.com/zemarmot) for GIMP development through an animated movie: **ZeMarmot** (<abbr class="cc-by-sa" title="Creative Commons by-sa"><span>CC by-sa</span></abbr> 4.0). Jehan is GIMP maintainer and primary developer; Aryeom designs GIMP features and tests them in production.

### Liberapay

<p class='buttons'>
  <a href="https://liberapay.com/GIMP/donate"><img alt="Donate using Liberapay" src="/images/liberapay-donate.svg"></a>
</p>

[Donating to GIMP on Liberapay][liberapay] funds developers too.
This platform is run by a non-profit organization allowing weekly crowdfunding.

[liberapay]: https://liberapay.com/GIMP/

## Donate to the Project

Contributions can be made by donating to the [GNOME Foundation](https://foundation.gnome.org/) (which acts as our fiscal agents) and **specifying GIMP project as the recipient**. The GNOME Foundation is a tax-exempt, non-profit 501(c)(3) organization so all donations are tax-deductible in the USA.

**Donations through GNOME Foundation can only be used for community needs** (conferences, developer meetings, material renewal…). [If you would rather fund paid development, favour Core Team developer funding above](#fund-core-team-developers-directly).

If applicable, please specify whether you want to remain anonymous. While we usually don't publish the names of donors, this may be considered for exceptional donations.

### Paypal

<div class='buttons'>
  <form action="https://www.paypal.com/cgi-bin/webscr" method="post"><input name="cmd" type="hidden" value="_xclick"><input name="business" type="hidden" value="gimp@gnome.org"><input name="item_name" type="hidden" value="Donation to GIMP Project"><input name="item_number" type="hidden" value="106"><input name="no_shipping" type="hidden" value="1"><input alt="Support GIMP" name="submit" src="donate.png" type="image"></form>
</div>

[Donate to gimp@gnome.org][]: this will notify us and the GNOME board that funds have been donated to the GIMP project. Credit card donations are also accepted via this route.


[Donate to gimp@gnome.org]: https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=gimp%40gnome%2eorg&lc=US&item_name=Donation%20to%20GIMP%20Project&item_number=106&currency_code=USD

### Cheque

Cheque, made payable to the [GNOME Foundation](https://www.gnome.org/), and sent to:

> GNOME Foundation<br/>
>  #117<br/>
> 21 Orinda Way Ste. C<br/>
> Orinda, CA 94563<br/>
> USA

… with a cover-letter (or a note in the "memo" field) saying it is for the GIMP project.

[lgm]: https://libregraphicsmeeting.org/
