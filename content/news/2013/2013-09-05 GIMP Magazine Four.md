Title: GIMP Magazine #4 published
Date: 2013-09-05
Category: News
Authors: Alexandre Prokoudine

The fourth issue of [GIMP Magazine](https://web.archive.org/web/20180502224330/https://gimpmagazine.org/) is now out and available as a digital download, a printed copy, and an e-zine for iPad. In just a year the project got a huge following, and now it'd a 100 pages large magazine with interviews, tutorials, a gallery section, and a graphic novel of its own.

Creating a full-fledged magazine takes a lot of time, so while GIMP Magazine is a free download, we kindly suggest you to consider [premium options](https://web.archive.org/web/20240628021943/https://gimpmagazine.magcloud.com/). That way you will ensure its continuity.
