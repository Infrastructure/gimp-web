Title: GIMP Magazine Issue #6 Released
Date: 2014-11-19
Category: News
Authors: Wilber Gimp

The newly released issue #6 of GIMP Magazine features a "Using GIMP for portrait and fashion photography" master class by Aaron Tyree who uses GIMP professionally, and a gallery of other artworks and photos made or processed with GIMP.

The team is planning to switch to monthly releases, however they need your support to cover the costs of publishing a free magazine. You can sponsor the project at [Patreon](https://www.patreon.com/gimpmagazine) or visit the magazine's [gift shop](https://web.archive.org/web/20180213180947/https://gimpmagazine.org/giftshop/) to make a donation.
