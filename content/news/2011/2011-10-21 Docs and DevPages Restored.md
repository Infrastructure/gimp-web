Title: Online Docs and Developer Pages Restored
Date: 2011-10-21
Category: News
Authors: Michael Schumacher

The online docs and developer pages are available again:

* Online Docs ([docs.gimp.org](https://docs.gimp.org/))
* Developer Pages ([developer.gimp.org](https://developer.gimp.org/))
