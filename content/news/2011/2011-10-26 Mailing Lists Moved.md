Title: Mailing Lists Moved to GNOME Mailing List Server
Date: 2011-10-26
Category: News
Authors: Michael Schumacher

Our mailing lists have moved to the GNOME list server. All previously subscribed users have automatically been added to the new lists.

The old list mail addresses are no longer valid, please use the new ones from now–note that in addition to the changed domain, the list names got a "-list" appended to them.

The subscription management pages are accessible via the following links:

* [gimp-developer-list](https://mail.gnome.org/mailman/listinfo/gimp-developer-list)
* [gimp-user-list](https://mail.gnome.org/mailman/listinfo/gimp-user-list)
* [gimp-docs-list](https://mail.gnome.org/mailman/listinfo/gimp-docs-list)
* [gimp-web-list](https://mail.gnome.org/mailman/listinfo/gimp-web-list)
* [gegl-developer-list](https://mail.gnome.org/mailman/listinfo/gegl-developer-list)

The list archives will be restored at gnome.org as soon as we get the files.

The gimp-announce, gimp-film, and gimp-win-user lists don't exist any longer.
