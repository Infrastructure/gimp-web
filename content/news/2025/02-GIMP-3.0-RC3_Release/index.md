Title: GIMP 3.0 RC3 Released
Date: 2025-02-10
Category: News
Authors: GIMP Team
Slug: gimp-3-0-RC3-released
Summary: Release news for version GIMP 3.0 RC3
Image: /news/2024/12/27/gimp-3-0-RC2-released/gimp-3.0.RC2-splash.jpg

We're excited to share the third release candidate of GIMP 3.0 for what (we hope) is the final round of community testing before the stable version! This release follows the recent _GIMP 3 and Beyond_ talk by **Jehan** at [FOSDEM 2025](https://www.gimp.org/news/2025/01/27/gimp-team-zemarmot-fosdem-2025/).

[TOC]

# Important Bug Fixes and Changes

While resolving the last few major bugs for 3.0, we've made some changes that we feel need more community review. While trying out this release candidate, please keep an eye out for the following:

## New GTK3 Version

Just in time for GIMP 3.0, a new version of GTK3 has been released!
Among other changes, GTK 3.24.48 includes fixes for several bugs
affecting GIMP with patches initially contributed by **Jehan**, such as
a crash in Wayland when dragging layers and text glitches in certain
widgets with Right-To-Left languages.
We want to thank **Carlos Garnacho** and **Matthias Clasen** for their
help on these respective patches.

GTK 3.24.48 also adds support to the version 2 of `xdg_foreign` for
Wayland (v1 stays supported as fallback). Specifically the absence of
this support was causing GIMP to freeze with certain actions on
KDE/Wayland, which is now fixed.

As a consequence of these issues — some of them really making GIMP
unstable on Wayland — we recommend packagers to update to the latest
version of GTK3 when packaging our RC3.
However, please let us know if you notice any regressions or other issues as a result of the new GTK3 version.

## Image Graph Improvements

With non-destructive editing in GIMP, users can now stack multiple filters on top of each other. These filters usually work in high bit-depth format so image information is not lost. However, each filter's output was converted to and from the original image's bit-depth when stacked – so if the image was only 8-bit, a great deal of information was lost in these constant conversions. **Jehan** fixed this problem by only converting to the image's format when the filter is meant to be merged in, rather than in non-destructive stacks. Since this is a big change in how filters work, we want to have more users test this change for any possible regressions.

## Thread-safe Projection Changes

When changes are made to an image (such as painting), the image projection needs to be "flushed" to display new changes to the screen. Some aspects of this process were not "thread-safe", which means that when your computer used multiple threads to speed up the work, they might conflict with each other and cause a crash. This was observed in our [auto-expanding layer](https://www.gimp.org/news/2024/02/21/gimp-2-99-18-released/#auto-expanding-layers) feature. **Jehan** fixed the function to be entirely thread-safe. However, changes to multi-threading can leave some well-hidden bugs, so more community testing would be helpful.

## Private Procedures

The GIMP Procedural DataBase browser shows plug-in and script developers all the functions they can access. Until now, it also showed "private" functions that are only used internally. **Jehan** added a flag to hide these functions. We initially cast too wide of a net and hid some important public functions. While we fixed these instances, we'd like more review from the community to make sure we didn't miss any mislabeled public functions.

# Enhancements

While we are still in major feature-freeze until the stable release of GIMP 3.0, some small and self-contained enhancements have been made to plug-ins.

## Script-fu
### Filter API

The new `(gimp-drawable-merge-filter)` PDB call allows Script-fu writers to use labels to specify filter properties.
This will give Script-fu users the same flexibility with calling and updating filters that C and Python plug-in developers
have in the GIMP 3.0 API. As an example, here is a call to the `Emboss` filter:

```
(gimp-drawable-merge-new-filter mask-emboss "gegl:emboss" 0 LAYER-MODE-REPLACE 1.0 "azimuth" 315.0 "elevation" 45.0 "depth" 7 "type" "emboss")
```

You can see more examples in our [Script repository](https://gitlab.gnome.org/GNOME/gimp/-/tree/master/plug-ins/script-fu/scripts).

### New named-arguments syntax

In Script-Fu, all the functions generated from plug-ins' PDB procedure
must now be called with a brand new named-argument syntax, inspired by
the Racket Scheme variant.

For instance, say your plug-in wants to call the Foggify plug-in, instead of calling:

```scm
(python-fu-foggify RUN-NONINTERACTIVE 1 (car (gimp-image-get-layers 1)) "Clouds" '(50 4 4) 1.0 50.0)
```

You should now call:

```scm
(python-fu-foggify #:image 1 #:drawables (car (gimp-image-get-layers 1)) #:opacity 50.0 #:color '(50 4 4))
```

This has a few advantages:

* Much more self-documented calls, especially as some plug-ins have a
  lot of arguments (so we could end up having functions with a dozen of
  integers or floats and that was very confusing).
* The order of arguments doesn't matter anymore.
* You can ignore arguments when you call them with default values.
* It allows to improve plug-in procedures in the future by adding new
  arguments without breaking existing scripts.

This last point in particular is important, and orders of arguments did
not matter anymore when calling PDB procedures from the C API, as well
as all introspected bindings. Script-Fu was the only remaining interface
we had which still cared about argument orders and numbers. This is not
true anymore and is therefore a huge step towards a much more robust API
for GIMP 3!

## File Formats

All changes to image loading plug-ins are checked with the [automated testing framework](https://gitlab.gnome.org/Infrastructure/gimp-test-images) built by **Jacob Boerema** to prevent regressions.

### PSD

In addition to bug fixes such as saving CMYK merged images properly, **Jacob Boerema** has added support for loading 16-bits-per-channel LAB PSDs. He also updated the PSD export dialog to use GIMP's built-in metadata export features.

### DDS

Much-requested support for loading DDS images with BC7 support has been implemented by **CMYK Student**. **Jacob Boerema**
worked to fix compatibility with DDS files exported from older versions of GIMP.

## AppImage is now Official

After nine months of incubation (the number is a mere coincidence 🙂), we present
a "new" distribution format for Linux users: `.AppImage`. Initially we used it as
an internal format for testing, as
[already covered in previous posts](https://www.gimp.org/news/2024/05/28/experiments-appimage/).
**Bruno Lopes**' efforts have allowed us to improve the build process. We now feel
confident with the generated AppImage and so we aim to make it official.

As an official upstream package, no fancy third party plug-ins or other arbitrary binaries
that are not GIMP dependencies are added to "bloat" it. It is what some people call
"vanilla" GIMP, a clean but complete GIMP for production (aka for general use).

Like any packaging format, it has its own characteristics and limitations. In the
case of GIMP's AppImage, included tools such as `gimp-console*` and `gimp-debug-tool*`
require prior extraction of the `.AppImage` file with `--appimage-extract` command.
Also, partly due to AppImage's design, commands that points to `$PWD` will not work.
These two are the only known feature limitations so far. So, if you find any others
or even bugs, please report them on [our tracker](https://www.gimp.org/bugs/).

## Miscellaneous

- It is now easier to load images from Google Drive and other remote or cloud platforms without having to manually select a
file format to try opening it with.

- Our build process now generates additional icons with the `-rtl` extension, which are automatically used with Right-to-Left languages. An example of this is the left and right arrow icons; they now face the correct direction in both language types.

- Plug-in developers no longer have to make custom file chooser buttons - `GimpProcedureDialog` now automatically creates them when a File type parameter is used. You can also specify whether the button is for opening or saving files and folders.

- **Rupert Weber** continued his effects in cleaning up our BMP plug-in. Additionally, he has in-progress work to add support
for importing color profiles in BMPs, which will hopefully be ready in a future release.

- **CMYK Student** updated the ICNS plug-in with new support for `ic05` icon types and ARGB icon formats. They also fixed a bug when loading older ICNS formats with no transparency mask. **Lukas Oberhuber** assisted with diagnosing and resolving
[a known bug in the ICNS format](https://en.wikipedia.org/wiki/Apple_Icon_Image_format#Known_issues) that caused our macOS icon
to show garbled pixels at small sizes.

# GEGL

The GEGL 0.4.54 release also contains some new enhancements and bugfixes. **Thomas Manni** updated the Noise Spread filter to
prevent bugs when applied to empty layer groups. **Jonny Robbie** added new option and paper types to the Negative Darkroom filter,
and optimized some floating point operations in GEGL as a whole.

# Release Stats

Since GIMP 3.0 RC2, in the main GIMP repository:

* 85 reports were closed as FIXED.
* 56 merge requests were merged.
* 335 commits were pushed.
* 19 translations were updated: Basque, Bulgarian, Catalan, Chinese
  (China), Danish, Dutch, Finnish, Georgian, Italian, Norwegian Nynorsk,
  Persian, Portuguese, Slovak, Slovenian, Spanish, Swedish, Turkish,
  Ukrainian, Vietnamese.

33 people contributed changes or fixes to GIMP 3.0.0 RC3 codebase (order
is determined by number of commits; some people are in several groups):

* 13 developers to core code: Jehan, Alx Sa, Jacob Boerema, lloyd
  konneker, Anders Jonsson, Thomas Manni, Bruno, Daniele Forsi, Lloyd
  Konneker, Lukas Oberhuber, Rupert, cheesequake, Øyvind Kolås.
* 10 developers to plug-ins or modules: Alx Sa, Jacob Boerema, Jehan,
  Rupert, lloyd konneker, Anders Jonsson, Bruno, Daniel Novomeský,
  Daniele Forsi, lillolollo.
* 19 translators: Alan Mortensen, Alexander Shopov, Nathan Follens,
  Kolbjørn Stuestøl, Hugo Carvalho, Asier Sarasua Garmendia, Ngọc Quân
  Trần, Jordi Mas, Marco Ciampa, Sabri Ünal, Anders Jonsson, Danial
  Behzadi, Ekaterine Papava, Jiri Grönroos, Jose Riha, Luming Zh,
  Martin, Rodrigo Lledó, Yuri Chornoivan.
* 1 Theme designer: Alx Sa.
* 6 build, packaging or CI contributors: Bruno, Jehan, lloyd konneker,
  Alx Sa, Rupert, Jacob Boerema.

Contributions on other repositories in the GIMPverse (order is determined by
number of commits):

* GEGL 0.4.54 is made of 11 commits by 16 contributors: Øyvind Kolås,
  Alexander Shopov, Hugo Carvalho, JonnyRobbie, Alan Mortensen, Anders
  Jonsson, Asier Sarasua Garmendia, Bartłomiej Piotrowski, Jehan,
  Martin, Nathan Follens, Nils Philippsen, Rodrigo Lledó, Sam L, Thomas
  Manni, Yuri Chornoivan.
* [ctx](https://ctx.graphics/) had 233 commits since RC2 release by 1
  contributor: Øyvind Kolås.
* `gimp-data` had 6 commits by 4 contributors: Bruno, Jehan, Alx Sa,
  Andre Klapper.
* `gimp-test-images` (new repository for image support testing) had 5
  commits by 2 contributors: Jacob Boerema, Alx Sa.
* The `gimp-macos-build` (macOS packaging scripts) release had 6
  commits by 2 contributors: Lukas Oberhuber, Bruno.
* The flatpak release had 12 commits by 3 contributors after RC2
  release: Bruno Lopes, Jehan, Hubert Figuière.
* Our main website (what you are reading right now) had 42 commits by 6
  contributors: Jehan, Alx Sa, Bruno, Jacob Boerema, Andre Klapper, Petr
  Vorel.
* Our [developer website](https://developer.gimp.org/) had 18 commits by
  5 contributors: Jehan, Bruno, Lukas Oberhuber, Alx Sa, Anders Jonsson.
* Our [3.0 documentation](https://testing.docs.gimp.org/) had 373
  commits by 13 contributors: Andre Klapper, Kolbjørn Stuestøl, Nathan
  Follens, Jacob Boerema, Alan Mortensen, Yuri Chornoivan, Dick
  Groskamp, Jordi Mas, Alevtina Karashokova, Alx Sa, Anders Jonsson,
  Daniele Forsi, Hugo Carvalho.

Let's not forget to thank all the people who help us triaging in Gitlab, report
bugs and discuss possible improvements with us.
Our community is deeply thankful as well to the internet warriors who manage our
various [discussion channels](https://www.gimp.org/discuss.html) or social
network accounts such as Ville Pätsi, Liam Quin, Michael Schumacher and Sevenix!

*Note: considering the number of parts in GIMP and around, and how we
get statistics through `git` scripting, errors may slip inside these
stats. Feel free to tell us if we missed or mis-categorized some
contributors or contributions.*

# Around GIMP

## Download Mirrors

Since the [3.0RC2 news post](https://www.gimp.org/news/2024/12/27/gimp-3-0-RC2-released/), two new mirrors have been contributed:

- Saswata Sarkar, Gurugram, India
- Hoobly Classifieds, USA

Mirrors are important as they help the project by sharing the load for dozens of thousands of daily downloads. Moreover by having mirrors spread across the globe, we ensure that everyone can have fast download access to GIMP.

## How to Cite GIMP in Research

GIMP is often used in research, and therefore it is cited in various
science publications. A researcher using GIMP for astronomical image
processing [approached
us](https://gitlab.gnome.org/GNOME/gimp/-/issues/12828) to know how to
cite GIMP properly, even more as they say it is used to perform an
important step in their algorithm.

Since it seems like an interesting question, we updated our "*Citing
GIMP and Linking to Us*" page with a new "[Citing GIMP in
research](https://www.gimp.org/about/linking.html#citing-gimp-in-research)"
subsection containing the conclusion of this discussion.

In particular, a `BibTex` entry, for researchers using `LaTeX` to manage
their bibliography, is available on this link to simplify your work. For
instance, say you use this RC3 for your research, you may cite GIMP with
this entry:

```tex
@software{GIMP,
    author = {{The GIMP Development Team}},
    title = {GNU Image Manipulation Program (GIMP), Version 3.0.0-RC3. Community, Free Software (license GPLv3)},
    year = {2025},
    url = {https://gimp.org/},
    note = {Version 3.0.0-RC3, Free Software}
}
```

Thank you to Cameron Leahy for this piece of BibTex code!

# Downloading GIMP 3.0 RC3

You will find all our official builds on [GIMP official website (gimp.org)](https://www.gimp.org/downloads/devel/):

- Linux AppImages for x86 and ARM (64-bit)
- Linux Flatpaks for x86 and ARM (64-bit)
- Universal Windows installer for x86 (32 and 64-bit) and for ARM (64-bit)
- MSIX package (GIMP Preview) for x86 and ARM (64-bit)
- macOS DMG packages for Intel hardware
- macOS DMG packages for Apple Silicon hardware

Other packages made by third-parties are obviously expected to follow (Linux or \*BSD distributions' packages, etc).

# What's Next

We really appreciate all the community testing and feedback we've received during the last two release candidates!
This will hopefully be the final release candidate before the stable 3.0 version. Our focus now is to finish
resolving the [few remaining bugs in our 3.0 milestone list](https://gitlab.gnome.org/GNOME/gimp/-/milestones/3#tab-issues),
while keeping an eye out for any new reports resulting from the changes in RC3.

Don’t forget you can [donate and personally fund GIMP developers](https://www.gimp.org/donating/), as a way to
give back and accelerate the development of GIMP. Community commitment helps the project to grow stronger!
