Title: GIMP team at FOSDEM 2025 (talk and keynote)
Date: 2025-01-27
Category: News
Authors: GIMP Team
Slug: gimp-team-zemarmot-fosdem-2025
Summary: The team will be present with 2 talks!
Image: gimp-zemarmot-fosdem-2025.jpg

Several members of the GIMP team will be present next weekend
(**1<sup>st</sup> and 2<sup>nd</sup> of February 2025**) at
[FOSDEM](https://fosdem.org/2025/), a conference in **Brussels,
Belgium**. This event describes itself this way:

> FOSDEM is a free event for software developers to meet, share ideas
> and collaborate. Every year, thousands of developers of free and open
> source software from all over the world gather at the event in
> Brussels. You don't need to register. Just turn up and join in!

<figure>
<img src="{attach}gimp-zemarmot-fosdem-2025.jpg"
     alt="GIMP team and ZeMarmot will be at FOSDEM'25 on Sunday, 2nd of February, for a talk and a keynote!"/>
</figure>

Needless to say, with over 8000 people expected, it is one of the
biggest event in the Free Software ecosystem.

On **Sunday morning, from 10 to 11:50 AM**, we have **2 talks** lined up,
in the main track and biggest room ([Janson](https://fosdem.org/2025/schedule/room/janson/)
in building J, with over 1400 people of capacity!), and one of these
talks is in fact a **keynote**:

* ["GIMP 3 and beyond"](https://fosdem.org/2025/schedule/event/fosdem-2025-6344-gimp-3-and-beyond/)  
  **Date/Time**: Sunday, February 2, from 10 to 10:50 AM  
  **Main track talk**  
  **Speakers**: Jehan (maintainer) and other GIMP team members  
  **Room**: Janson, building J  
  **Streaming**: [live.fosdem.org/watch/janson](https://live.fosdem.org/watch/janson)
* ["Early Screening of "ZeMarmot" animation film (work-in-progress) with live
  music"](https://fosdem.org/2025/schedule/event/fosdem-2025-6370-early-screening-of-zemarmot-animation-film-work-in-progress-with-live-music/)  
  **Date/Time**: Sunday, February 2, from 11 to 11:50 AM  
  **Keynote**  
  **Speakers**: Aryeom (film director), Jehan (scenarist, technical) and ORL (composer)  
  **Musicians**: Pelin, Adrien, ORL  
  **Room**: Janson, building J  
  **Streaming**: [live.fosdem.org/watch/janson](https://live.fosdem.org/watch/janson)

The keynote in particular will be a *work-in-progress* screening of
*ZeMarmot* short animation film, which is worked on by 2 major contributors of GIMP
(Jehan, maintainer, and Aryeom, designer for GIMP, and film director of
*ZeMarmot*) within the non-profit production
[LILA](https://libreart.info/) which produces Libre Art movies.

Not only this, you may notice the presence of musicians, since the music
will be played live by 3 musicians from the [AMMD](https://ammd.net/)
non-profit, producing Libre Art music, concerts and recording. The film
is a short (about 10 minutes), then it will be followed by a talk
presenting our work.

As for the GIMP talk, we will showcase the soon-to-be-released GIMP 3!

In the end of both talks, you will be able to ask questions.

We hope to see many people there. See you soon! 🤗
