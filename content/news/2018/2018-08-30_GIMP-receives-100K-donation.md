Title: GIMP receives a $100K donation
Date: 2018-08-30
Category: News
Authors: Wilber
Slug: handshake-gnome-donation
Summary: Handshake and GNOME Foundation donated $100,000 to GIMP.

Earlier this month, GNOME Foundation announced that they received a $400,000
donation from Handshake.org, of which $100,000 they transferred to GIMP's
account.

We thank both Handshake.org and GNOME Foundation for the generous donation and
will use the money to do much overdue hardware upgrade for the core team
members and organize the next hackfest to bring the team together, as well as
sponsor the next instance of [Libre Graphics Meeting][].

[Libre Graphics Meeting]: https://libregraphicsmeeting.org

[Handshake][] is a decentralized, permissionless naming protocol compatible
with DNS where every peer is validating and in charge of managing the root
zone with the goal of creating an alternative to existing Certificate
Authorities. Its purpose is not to replace the DNS protocol, but to replace
the root zone file and the root servers with a public commons.

[Handshake]: https://handshake.org

GNOME Foundation is a non-profit organization that furthers the goals of the
[GNOME Project][], helping it to create a free software computing platform for
the general public that is designed to be elegant, efficient, and easy to use.

[GNOME Project]: https://www.gnome.org/
