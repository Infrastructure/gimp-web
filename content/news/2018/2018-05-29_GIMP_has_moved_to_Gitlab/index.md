Title: GIMP has moved to Gitlab
Date: 2018-05-31
Category: News
Authors: Alexandre Prokoudine
Slug: gimp-has-moved-to-gitlab
Summary: GIMP has moved to new Gitlab-based infrastructure provided by GNOME.

Along with the GEGL and babl libraries, GIMP has moved to a new collaborative
programming infrastructure based on [Gitlab][] and hosted by [GNOME][]. The new URLs
are:

* <https://gitlab.gnome.org/GNOME/gimp>
* <https://gitlab.gnome.org/GNOME/gegl>
* <https://gitlab.gnome.org/GNOME/babl>

[Gitlab]: https://www.gitlab.com "GitLab website"
[GNOME]: https://www.gnome.org/ "GNOME website"

On the end-user side, this mostly means an improved bug reporting experience.
The submission is easier to fill in, and we provide two templates — one for bug
reports and one for feature requests.

<figure>
<img src="{attach}gitlab-new-issue-form.png" alt="New issue form on Gitlab" width='879' height='873'>
<figcaption>
New issue form on Gitlab.
</figcaption>
</figure>

For developers, it means simplified contribution, as you can simply fork the
GIMP repository, commit changes, and send a merge request. Please note that
while we accept merge requests, we only do that in cases when patches can be
fast-forwarded. That means you need to rebase your fork on the master branch
(we'll see if we can do merge requests for the 'gimp-2-10' branch).

In the meantime, work continues in both 'master' branch (GTK+3) porting and
the 'gimp-2-10' branch. Most notably, Ell and Jehan Pagès have been improving
the user-perceivable time it takes GIMP to load fonts by adding the
asynchronous loading of resources on startup.

What it means is that font loading does not block startup anymore, but if you
have a lot of fonts and you want to immediately use the *Text* tool, you might
have to wait.

<p>
<video width="830" height="467" controls>
  <source src="https://download.gimp.org/gimp/video/v2.99/gimp-2-99-1-fonts-loading-warning.webm" type="video/webm">
  <source src="https://download.gimp.org/gimp/video/v2.99/gimp-2-99-1-fonts-loading-warning.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>
</p>

The API is general rather than fonts-specific and can be further used to add
the background loading of brushes, palettes, patterns etc.
