Title: GIMP 2.10.8 Released
Date: 2018-11-08
Category: News
Authors: Wilber
Slug: gimp-2-10-8-released
Summary: GIMP 2.10.8 released: bug fixes and optimizations

Though the updated GIMP release policy allows cool new features in micro releases,
we also take pride on the stability of our software (so that you can edit images feeling that your work is safe).

In this spirit, GIMP 2.10.8 is mostly the result of dozens of bug fixes and optimizations.

<figure>
<a href="{attach}wilber-co-Aryeom_stability.png">
<img src="{attach}wilber-co-Aryeom_stability.png" alt="Stable GIMP, Wilber and Co."/>
</a>
<figcaption>
<em>Wilber and Co.</em> strip, by <a href="https://girinstud.io/">Aryeom and Jehan</a>, 2013
</figcaption>
</figure>

## Notable improvements

In particular, chunk size of image projections are now determined
dynamically depending on processing speed, allowing better
responsiveness of GIMP on less powerful machines whereas processing
would be faster on more powerful ones.

Moreover various tools have been added to generate performance logs,
which will allow us to optimize GIMP even more in the future.
As with most recent optimizations of GIMP, these are the results of Ell's
awesomeness. Thanks Ell!

In the meantime, various bugs have been fixed in 
`wavelet-decompose`, the new vertical text feature (including text
along path), selection tools, and more. On Windows, we also improved
RawTherapee detection (for RawTherapee 5.5 and over), working in sync
with the developers of this very nice RAW processing software.
And many, many more fixes, here and there…

The Save dialog also got a bit of retouching as it now shows more
prominently the features preventing backward compatibility (in case you
wish to send images to someone using an older version of GIMP). Of course,
we want to stress that we absolutely recommend to always use the latest
version of GIMP. But life is what it is, so we know that sometimes you
have no choice. Now it will be easier to make your XCF backward compatible
(which means, of course, that some new features must not be used).

<figure>
<img src="{attach}save-dialog-compatibility-list.jpg" alt="Compatibility issues in Save dialog"/>
<figcaption>
Save dialog shows compatibility issues when applicable
</figcaption>
</figure>

Thanks to Ell, the **Gradient** tool now supports multi-color hard-edge gradient
fills. This feature is available as a new *Step* gradient-segment blending
mode. This creates a hard-edge transition between the two adjacent color
stops at the midpoint.

<figure>
<img src="{attach}gimp-2-10-8-gradients-step-blending.png" alt="Step blending in gradient fills"/>
<figcaption>
Newly added Step blending in gradient fills
</figcaption>
</figure>

On the usability end of things, all transform tools now apply changes when
you save or export/overwrite an image without pressing **Enter** first
to confirm changes. Ell also fixed the color of selected text which wasn't
very visible when e.g. renaming a layer.

## CIE xyY support

Thanks to Elle Stone, GIMP now features initial support for color readouts
in the CIE xyY color space. You can see these values in the *Info* window
of the **Color Picker** tool and in the *Sample Points* dock. Most of the
related code went into the babl library.

Much like CIE LAB, this color space is a derivative of CIE XYZ. The *Y* channel
separates luminance information from chromaticity information in the *x* and
*y* channels. You might be (unknowingly) familiar with this color space if you
ever looked at a [horseshoe diagram of an ICC profile](https://ninedegreesbelow.com/photography/all-the-colors.html).

CIE xyY is useful to explore various color-related topics like the
[Abney effect](https://en.wikipedia.org/wiki/Abney_effect). See
[this Pixls.us thread](https://discuss.pixls.us/t/what-are-the-lch-and-jch-values-for-the-srgb-blue-primary/8796)
for an example of what people do with this kind of information.

## Improved GIMP experience on macOS

Our new macOS contributor, Alex Samorukov, has been very hard at work
improving the macOS/OSX package, debugging and patching both GIMP, GEGL,
and the [gtk-osx project](https://gitlab.gnome.org/GNOME/gtk-osx/merge_requests/1).

Some of the macOS specific bugs he fixed are artifacts while zooming,
the windows focus bug in plug-ins, and a non-functional support for some
non-Wacom tablets. Jehan, Ell, and Øyvind actively participated in fixing these
and other macOS issues.

We also thank CircleCI for providing their infrastructure to us free of charge.
This helps us automatically building GIMP for macOS.

That said, please keep in mind that we have very few developers for macOS and
Windows. If you want GIMP to be well supported on your operating system of
choice, we do welcome new contributors!

Also, see the [NEWS][] file for more information on the new GIMP release,
and the [commit history][] for even more details.

[NEWS]: https://gitlab.gnome.org/GNOME/gimp/blob/c6f1196721d9e8d1179a09d91557656895e6086d/NEWS#L11
[commit history]: https://gitlab.gnome.org/GNOME/gimp/commits/gimp-2-10

## Around GIMP
### GEGL and babl

The babl library got an important fix that directly affects GIMP users:
the color of transparent pixels is now [preserved][] during conversion
to premultiplied alpha. This means all transform and deformation operations
now maintain color for fully transparent pixels, making unerase and curves
manipulation of alpha channel more reliable.

[preserved]: https://www.patreon.com/posts/premultiplied-in-21014115

On the GEGL side, a new buffer iterator API was added (GIMP code has been
ported to this improved interface as well). Additionally, new GEGL_TILE_COPY
command was added to backends to make buffer duplication/copies more efficient.

Recently, Øyvind Kolås has been working again on [multispectral/hyperspectral processing in GEGL](https://www.patreon.com/posts/camayakaa-float-22446330),
which happens to be the groundwork for CMYK processing. This is therefore
the first steps for better CMYK support in GIMP! We hope that anyone who wants
to see this happening will support
[Øyvind on Patreon](https://www.patreon.com/pippin)!

### GIMP in Université de Cergy-Pontoise

Aryeom, well known around here for being the director of [ZeMarmot
movie](https://film.zemarmot.net/en/), a skilled illustrator, and a
contributor to GIMP has given a graphics course with GIMP as a guest
teacher for nearly a week at the *Université de Cergy-Pontoise* in
France, mid-October.

She taught to two classes: a computer graphics class and a 3D heritage one,
focusing on digital illustration for the former and retouching for the latter.

<figure>
<img src="{attach}gimp-2-10-8-Univ_Cergy.jpg" alt="Students being taught computer graphics with GIMP"/>
<figcaption>
<a href="https://twitter.com/zemarmot/status/1053584659400015872">Aryeom and her students</a>
in University of Cergy-Pontoise
</figcaption>
</figure>

This is a good hint that GIMP is getting more recognition as it now
gets taught in universities. Students were very happy overall, and we
could conclude by quoting one of them at the end of a 3-day course:

<blockquote>I didn't know that GIMP was the Blender for 2D; now this is one more
software in my toolbox!</blockquote>

We remind that you can also support [Aryeom's work on
Patreon](https://www.patreon.com/zemarmot), on
[Tipeee](https://fr.tipeee.com/zemarmot) or [by others
means](https://film.zemarmot.net/en/donate)!

### Flatpak statistics

Although Flathub does not (yet) provide any public statistics for packages,
an internal source told us that there have been over 214,000 downloads of GIMP
since its existence (October 2017). This is more than 500 downloads a day, and
by far the most downloaded application there!

Flathub is a new kind of application repository for GNU/Linux, so of
course these numbers are not representative of all downloads.
In particular, we don't have statistics for Windows and macOS.
Even for Linux, every distribution out there makes its own package of GIMP.

So this is a small share, and a nice one at that, of the full usage of
GIMP around the globe!

### GIF is dead? Long live WebP!

The GIF format is the only animated image format which is visible in any
web browser, making it the de-facto format for basic animation on the
web, despite terrible quality (256 colors!), binary transparency (no
partial transparency), and not so good compression.

Well, this may change! A few days ago, WebP reached support in most major
browsers (Mozilla Firefox, Google Chrome, Microsoft Edge, Opera),
when a [2-year old feature request for Mozilla Firefox got closed as
"FIXED"](https://bugzilla.mozilla.org/show_bug.cgi?id=1294490). This
will be available for Firefox 65.

Therefore, we surely hope web platforms will take this new format into consideration,
and that everyone will stop creating GIF images now that there are actual
alternatives in most browsers!

And last but not least, we remind everyone that GIMP has already had WebP support
since [GIMP 2.10.0](https://www.gimp.org/news/2018/04/27/gimp-2-10-0-released/)!

<figure>
<img src="{attach}gimp-2-10-8-ZeMarmot-frama.webp"
alt="If you see this text (instead of an animation), your browser
does not support WebP yet!"/>
<figcaption>
A WebP animation (done in GIMP), by Aryeom, featuring ZeMarmot and a penguin.
</figcaption>
</figure>

*Disclaimer*: the GIMP team is neutral towards formats. We are aware of
other animated image formats, such as APNG or MNG, and wish them all the
best as well! We would also be very happy to support them in GIMP, if
contributors show up with working patches.

## What's next

We've been running late with this release, so we haven't included some of the
improvements available in the main development branch of GIMP. And there are
even more changes coming!

Here is what you can expect in GIMP 2.10.10 when it's out.

* ACES RRT display filter that can be used in scene-referred imaging workflows.
Technically, it's a luminance-only approximation of the ACES filmic HDR-to-SDR
proofing mapping [originally written][] in The Baking Lab project.
* Space invasion: essentially you can now take an image that's originally
in e.g. ProPhotoRGB, process it in the CIE LAB color space, and the resulted
image will be in ProPhotoRGB again, with all color data correctly mapped to
the original space / ICC profile. This is a complicated topic, we'll talk more
about it when it's time to release 2.10.10.

[originally written]: https://github.com/TheRealMJP/BakingLab/blob/master/BakingLab/ACES.hlsl

Another new feature we expect to merge to a public branch soon is smart
colorization based on the original implementation in the ever-popular GMIC
filter.

<div class='fluid-video'>
<iframe width="560" height="315" src="https://www.youtube.com/embed/2SXaoqiIYas" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

Given quickly approaching winter holidays and all the busy time that comes with it,
we can't 100% guarantee another stable release this year, but we'll do our best
to keep 'em coming regularly!

## Conclusion

We wish you a lot of fun with GIMP, as it becomes more stable every day!
