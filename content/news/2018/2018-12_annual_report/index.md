Title: GIMP and GEGL in 2018
Date: 2019-01-02
Category: News
Authors: Wilber
Slug: gimp-and-gegl-in-2018
Summary: In this post, we are looking back at 2018 and then forward to outline future work on GIMP, GEGL, and babl.

In this post, we are looking back at 2018 and then forward to outline future work on GIMP,
GEGL, and babl.

## Version 2.10 release and point releases

In April, we released the much anticipated version 2.10, featuring updated user
interface, high bit depth support, multi-threading, linear color space workflow,
revamped color management, new transformation tools, and many more changes, as
outlined in the
[release notes](https://www.gimp.org/release-notes/gimp-2.10.html).

Given that the next big update, v3.0, is likely far ahead, we now also allow new
features in the stable series (2.10.2, 2.10.4 etc.). Which means, you don't have
to wait years for new features anymore. Instead, we make a new release every 1-2
months, and it usually comes with many bugfixes as well as some new stuff.

<figure>
<img src="{attach}gimp-2-10-9-main-window.jpg" alt="Unstable GIMP, main window"/>
<figcaption>
<em>Nightscape, by Filip Bulovic, CC BY-SA 4.0</em>
</figcaption>
</figure>

Among the new features in 2.10 updates this year:

- support for [HEIF][] images
- simple horizon straightening
- vertical text
- new filters

[HEIF]: https://en.wikipedia.org/wiki/High_Efficiency_Image_File_Format "High Efficienty Image File Format"

## Development focus

So what have we been busy with after releasing 2.10?

**Refactoring.** Most work is happening in the main development branch leading
up to version 3.0. GIMP 3.0 will be relying on GTK3, a much newer version of
the toolkit. And we want to arrive to working on non-destructive editing with a
much leaner code base. This means a lot still needs to change. The majority of
changes here was contributed by GIMP maintainer Michael Natterer.

**Usability.** There have been numerous fixes to address various usability
issues in GIMP. We eliminated duplicated file type selector in the exporting
dialog, added an explanation for why a file might not be entirely readable in
older versions of GIMP, and fixed quite a few other things.

<figure>
<img src="/news/2018/11/08/gimp-2-10-8-released/save-dialog-compatibility-list.jpg" alt="Compatibility warning"/>
<figcaption>
<em>Compatibility warning</em>
</figcaption>
</figure>

**Smart colorization.** This major new feature greatly simplifies filling inked
sketches with color, where areas are not completely closed. It was added by
Jehan Pagès, with contributions from Ell, and will be available in GIMP 2.10.10.

<figure>
<img src="{attach}gimp-2-10-9-smart-colorization.gif" alt="Smart colorization demo"/>
<figcaption>
<em>Smart colorization demo</em>
</figcaption>
</figure>

**Extension management.** *ZeMarmot* project has started implementing
[extension management within
GIMP](https://girinstud.io/news/2018/07/crowdfunding-for-extension-management-in-gimp-and-other-improvements/),
which will allow to search, install, uninstall and update extensions
directly within GIMP. An extension is meant to be any data already
installable (manually currently) in GIMP, such as plug-ins,
icons, brushes, and more.

<figure>
<img src="/news/2018/08/19/gimp-2-10-6-released/gimp-2-10-6-ongoing-dev-extensions.jpg" alt="Extension management"/>
<figcaption>
<em>Extensions dialog</em>
</figcaption>
</figure>

**Performance and async jobs.** There are several attack vectors towards subpar
performance of GIMP. Ell fixed some of the big issues by introducing async
operations like lazy loading of fonts (which effectively fixed the long startup
times for GIMP on Windows), and then moved all parallel processing in multiple
threads over to GEGL. Both Ell and Øyvind Kolås contributed to improving
performance of downscaling with bilinear and bicubic samplers and other aspects
of GEGL and babl.

**Space invasion.** GIMP used to have the sRGB color space hardcoded into all
processing. This couldn't work for everyone, and we introduced some changes to
support any RGB color spaces in 2.10. Space invasion is the next step towards
that goal. The 'master' git branch of GIMP now allows taking an image that’s
originally in e.g. ProPhotoRGB, processing it in a different color space (e.g.
CIE LAB), and the resulted image will be in ProPhotoRGB again, with all color
data correctly mapped to the original space / ICC profile. This isn't yet
polished and thus not read for prime-time use. Most of the work was done by
Øyvind Kolås and Michael Natterer.

**CMYK.** Øyvind made CMYK a first-class citizen in GEGL, thus laying the
foundation for respective changes in GIMP. GEGL now can e.g. open a CMYK JPEG
file, composite an RGB PNG file with an alpha channel on top of it, then write
a CMYK TIFF file to the output, tagged with a user-submitted CMYK ICC profile.
This and other work can be sponsored by you
[via Patreon](https://www.patreon.com/pippin/).

**Bugfixing.** This is the boring part that is, however, absolutely crucial for
making any software usable. Due to the switch to GNOME-hosted Gitlab instance,
we cannot give the exact number, but there have been a few hundreds of bugfixes
done by many contributors throughout the year.

## 2019 outlook

We expect to be shipping 2.10.x updates throughout 2019, starting with the
version 2.10.10 currently expected in January/February. This version will
feature faster layer groups rendering, smart colorization with the *Bucket Fill*
tool, and various usability improvements.

We are also planning the first unstable release of GIMP that will have version
2.99.2, eventually leading up to version 3.0. The prerequisite for releasing
that version will be the completion of the space invasion (see above).

*ZeMarmot* project (which can be supported on
[Patreon](https://www.patreon.com/zemarmot) or
[Tipeee](https://tipeee.com/zemarmot)) is also planning to focus a bit more
on better canvas interactions, as well as animation support improvements,
starting from merging existing work.

On the GEGL and babl front, we expect to continue working towards better CMYK
support and performance.

## Where help is wanted

There are many ways you can contribute to making GIMP better.

**Usability.** Historically, GIMP has been created by programmers rather than UI
designers. Although we don't have the manpower to implement every single
proposal, we do appreciated structured proposals, explaining a problem and
suggesting ways to fix it. We also welcome contributions improving the design of
symbolic icons which have their usability issues.

**User manual.** Currently, most of the original writing is done by a single
person, so the user manual is not yet complete to cover all changes in the
version 2.10. You don't need to know DocBook/XML to contribute (it's not hard to
learn though), we welcome new text
[submitted in any file format](https://gitlab.gnome.org/GNOME/gimp-help/issues).

**User interface translations.** Out of
[81 languages](https://l10n.gnome.org/module/gimp/) supported in the stable
branch of GIMP, only 20 translations into other languages are currently more
than 90% complete, and 15 translation are only 10% or less complete. If this is
something you would like to work on, please join your
[local team](https://l10n.gnome.org/teams/) and start contributing translation
updates.

**Tutorials.** There will never be enough tutorials explaining how to use GIMP
to accomplish various tasks, especially if we are talking about high-quality
results. If you don't like watching and making *video* tutorials, you can write
text with illustrations. And if you don't have your own blog, we are open to
submissions for the
[official tutorials section at gimp.org](https://www.gimp.org/tutorials/) too.

**Programming.** If you are interested in fun things, pretty much all of the
[*Future* section of the roadmap](https://wiki.gimp.org/wiki/Roadmap#Future) can
be done for 3.0 and a lot — even for 2.10.x (do talk to us first just in case
though). There are even more
[feature requests in the tracker](https://gitlab.gnome.org/GNOME/gimp/issues?label_name%5B%5D=1.+Feature).
And one thing we are particularly interested in for 3.0 is fixing the broken
Python support.

On top of that, we still need to make public API for text layers, so that the
PSD plug-in could be patched to read and write text as text. And our EXR and
RGBE plug-ins are currently rather simplistic, so that's another area of
improvement.

There are all sorts of other interesting ideas for plug-ins, like UV unwrapping
from OBJ files for texturing. And we are still missing a developer with a game
design bug to work on the DDS plug-in that currently lives in a
[dedicated git branch](https://gitlab.gnome.org/GNOME/gimp/tree/wip/dds).

## Donations

In August 2018, we received a $100K donation from Handshake.org via GNOME
Foundation. So far, we are using this money for long overdue hardware upgrades.

Our core developers, [Øyvind Kolås](https://www.patreon.com/pippin) and
[Jehan Pagès](https://www.patreon.com/zemarmot), continue running their
personal crowdfunding via Patreon, although in case of Jehan the money is
actually split two-ways, because his campaign is targeted at funding
a short anomation movie made with GIMP (development assists that project,
like in Blender Institute's open movies).

## Team acknowledgment

While we often mention GIMP, GEGL, and babl separately, all work on GEGL and
babl directly affects the evolution of GIMP, both in terms of features and
performance. Thus, all GEGL and babl contributors are GIMP contributors too. 

The vast majority of the work in GIMP's git repository is currently done by
Ell, Jehan Pagès, and Michael Natterer, who share a nearly equal percentage
of commits and spend more or less the same amount of time hacking on the code.
Additional contributions came from Simon Budig, Massimo Valentini, Ono Yoshio,
and others.

Øyvind Kolås, Ell, and Debarshi Ray are major developers of GEGL. Additional
contributions came from Thomas Manni, Félix Piédallu, Simon Budig, and others.

Most of the work on babl in 2018 was done by Øyvind Kolås, with contributions
from Félix Piédallu and Debarshi Ray.

We thank Julien Hardelin for his tireless work on the user manual and all the
translators who contribute their updates. We also thank Elle Stone on her
insight into all things color management and contributions to making GIMP play
better with CIE color spaces.

We thank Pat David and Michael Schumacher on both their work on the website and
user support on various social channels.

We also thank Jernej Simončič and Alex Samorukov for providing Windows and macOS
builds and fixing platform-specific bugs.

And we can't thank enough all the translators who have been closely following
development of GIMP to make the program perfectly available in their respective
languages: Piotr Drąg, Nils Philippsen, Sveinn í Felli, Tim Sabsch, Marco
Ciampa, Claude Paroz, Daniel Korostil, Alan Mortensen, Anders Jonsson, Dimitris
Spingos, Snehalata B Shirude, Martin Srebotnjak, and others.

<div class='fluid-video'>
<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/uaZRcZf6lg0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>
<figcaption>
<em>Happy Holidays, from GIMP and <a href="https://film.zemarmot.net/">ZeMarmot</a> team, by Aryeom</em>
</figcaption>
<br>
