Title: GIMP 2.10.6 Released
Date: 2018-08-19
Category: News
Authors: Wilber
Slug: gimp-2-10-6-released
Summary: GIMP 2.10.6 released with vertical text layers, faster previews, Marathi translation, and more!

Almost four months have passed since [GIMP 2.10.0 release][], and this
is already the fourth version in the series, bringing you bug fixes,
optimizations, and new features.

[GIMP 2.10.0 release]: https://www.gimp.org/news/2018/04/27/gimp-2-10-0-released/

The most notable changes are listed below (see also the [NEWS][] file).

[NEWS]: https://gitlab.gnome.org/GNOME/gimp/blob/3bbebaf6e06229ac893c1d4a34295a156ab28a06/NEWS#L11

# Main changes
## Vertical text layers

GIMP finally gets support for vertical text (top-to-bottom writing)!
This is a particularly anticipated feature for several
[East-Asian writing systems][], but also for anyone wishing to design
fancy vertical text.

<figure>
<img src="{attach}gimp-2-10-6-vertical-text.jpg" alt="Vertical text">
<figcaption>
Vertical text in GIMP 2.10.6.
</figcaption>
</figure>

For this reason, GIMP provides several variants of vertical text, with
mixed orientation (as is typical in East-Asian vertical writing) or
upright orientation (more common for Western vertical writing), with
right-to-left, as well as left-to-right columns.

Thanks to [Yoshio Ono](http://reddog.s35.xrea.com) for the vertical text implementation!

[East-Asian writing systems]: https://en.wikipedia.org/wiki/Horizontal_and_vertical_writing_in_East_Asian_scripts

## New filters

Two new filters make an entrance in this release:

### Little Planet

This new filter is built on top of the pre-existing 
`gegl:stereographic-projection` operation and is finetuned to create
"little planets" from 360×180° equirectangular panorama images.

<figure>
<img src="{attach}gimp-2-10-6-little-planet.jpg" alt="Little Planet filter">
<figcaption>
Little Planet filter in GIMP 2.10.6.<br/>
<a href="https://commons.wikimedia.org/wiki/Category:Panoramas#/media/File:Ettling_Isar_Panorama.jpg">Image on canvas</a>: <em>Luftbild Panorama der Isar bei Ettling in Niederbayern</em>, by Simon Waldherr,
(<a href="https://creativecommons.org/licenses/by-sa/4.0">CC by-sa 4.0</a>).
</figcaption>
</figure>

### Long Shadow

This new GEGL-based filter simplifies creating long shadows in several visual
styles.

<p>
<video width="960" height="492" controls>
  <source src="https://download.gimp.org/gimp/video/v2.10/gimp-2-10-6-long-shadow.webm" type="video/webm">
  <source src="https://download.gimp.org/gimp/video/v2.10/gimp-2-10-6-long-shadow.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>
</p>

There is a handful of configurable options, all helping you to cut extra steps
from getting the desired effect.

The feature was contributed by Ell.

## Improved straightening in the Measure tool

A lot of people appreciated the new [Horizon Straightening][] feature added in
GIMP 2.10.4. Yet many of you wanted vertical straightening as well. This is now
possible.

[Horizon Straightening]: https://www.gimp.org/news/2018/07/04/gimp-2-10-4-released/#simple-horizon-straightening

<figure>
<img src="{attach}gimp-2-10-6-vertical-straighten.jpg" alt="Vertical Straightening">
<figcaption>
Vertical straightening in GIMP 2.10.6.<br/>
<a href="https://en.wikipedia.org/wiki/Wall#/media/File:Western_side_of_the_Great_Mosque_of_Kairouan.jpg">Image on canvas</a>: <em>View of the western enclosing wall of the Great Mosque of Kairouan</em>, by Moumou82,
(<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC by-sa 2.0</a>).
</figcaption>
</figure>

In the _Auto_ mode (default), _Straighten_ will snap to the smaller angle to
decide for vertical or horizontal straightening. You can override this behavior
by specifying explicitly which it should be.

## Optimized drawable preview rendering

Most creators working on complex projects in GIMP have had bad days when there
are many layers in a large image, and GIMP can't keep up with scrolling the
layers list or showing/hiding layers.

Part of the reason was that GIMP couldn't update user interface until it was
done rendering layer previews. Ell again did some miracles here by having most
drawable previews render asynchronously.

For now, the only exception to that are layer groups. Rendering them
asynchronously is still not possible, so until we deal with this too, we made
it possible for you to disable rendering layer group previews completely. Head
over to _Preferences > Interface_ and tick off the respective checkbox.

<figure>
<img src="{attach}gimp-2-10-6-layer-group-preview.jpg" alt="Disable preview of layer groups">
<figcaption>
Disable preview of layer groups in GIMP 2.10.6.
</figcaption>
</figure>

One more thing to mention here. For technically-minded users, the _Dashboard_
dockable dialog  ([introduced in GIMP 2.10.0][]) now displays the number of
async operations running in the _Misc_ group.

[introduced in GIMP 2.10.0]: https://www.gimp.org/news/2018/03/26/gimp-2-10-0-rc1-released/#dashboard-dockable

## A new localization: Marathi

GIMP was already available in 80 languages. Well, it's [81 languages][] now!

A team from the *North Maharashtra University*, Jalgaon, worked on a
[Marathi][] translation and contributed a [nearly full translation][]
of GIMP.

Of course, we should not forget all the other translators who do a
wonderful work on GIMP. In this release, 13 other translations were
updated: Brazilian Portuguese, Dutch, French, German, Greek, Italian,
Latvian, Polish, Romanian, Russian, Slovenian, Spanish, and Swedish.

Thanks everyone!

[81 languages]: https://l10n.gnome.org/module/gimp/#gimp-2-10
[Marathi]: https://en.wikipedia.org/wiki/Marathi_language
[nearly full translation]: https://l10n.gnome.org/languages/mr/gnome-gimp/ui/

<figure>
<img src="{attach}gimp-2-10-6-marathi-gimp.jpg" alt="GIMP in Marathi">
<figcaption>
Marathi translation in GIMP 2.10.6.
</figcaption>
</figure>

## File dialog filtering simplified

A common cause of confusion in the file dialogs (opening, saving, exporting…)
was the presence of two file format lists, one for displaying files with
a specific extension, the other for the actual file format choice. So we
streamlined this.

There is just one list available now, and it works as both the filter for
displayed images and the file format selector for the image you are about to
save or export.

<figure>
<img src="{attach}gimp-2-10-6-file-dialog.jpg" alt="File Dialog">
<figcaption>
File dialog in GIMP 2.10.6.
</figcaption>
</figure>

Additionally, a new checkbox allows you to display the full list of
files, regardless of the currently chosen file format. This could be
useful when you want to enforce an unusual file extension or reuse an
existing file's name by choosing it in the list and then appending your
extension.

## The end of DLL hell? A note to plug-in developers…

A major problem over the years, on Windows, was what developers call the
[DLL hell](https://en.wikipedia.org/wiki/DLL_Hell). This was mostly
caused either by third-party software installing libraries in system
folders or by third-party plug-ins installing themselves with shared
libraries interfering with other plug-ins.

The former had already been mostly fixed by tweaking the DLL search priority
order. This release provides an additional fix by taking into account 32-bit
plug-ins running on 64-bit Windows systems
([WoW64 mode](https://en.wikipedia.org/wiki/WoW64)).

The latter was fixed already since GIMP 2.10.0 **if** you installed your
plug-ins in its own directory (which is not compulsory yet, but will be in
GIMP 3).

E.g. if you have a plug-in named `myplugin.exe`, please install it under
`plug-ins/myplugin/myplugin.exe`. This way, not only you won't pollute other
plug-ins if you ever included libraries, but your plug-in won't be prevented
from running by unwanted libraries as well. All our core plug-ins are now installed this way. Any third-party plug-ins should be as well.

# Ongoing Development

## Prepare for the space invasion!

Meanwhile, taking place simultaneously on the babl, GEGL, and GIMP 2.99 fronts, pippin and Mitch embarked on a project internally nicknamed the "[space invasion](https://www.patreon.com/posts/20264674)", the end goal of which is to simplify and improve color management in GIMP, as well as other GEGL-based projects.

<figure>
<img src="{attach}gimp-2-10-6-ongoing-dev-space-invasion.jpg"
     alt="Space invasion (ongoing development)">
<figcaption>
Mutant goats from outer space, soon landing in GIMP.
</figcaption>
</figure>

About a year ago, babl, the library used by GIMP and GEGL to perform color conversions, [gained the ability](https://www.patreon.com/posts/babl-primaries-13975931) to tie arbitrary RGB color spaces to existing pixels formats.
This, in turn, allowed GIMP to start using babl for performing conversions between certain classes of color profiles, instead of relying solely on the LCMS library, greatly improving performance.
However, these conversions would only take place at the edges between the internal image representation used by GIMP, and the outside world; internally, the actual color profile of the image had limited effect, leading to inconsistent or incorrect results for certain image-processing operations.

The current effort seeks to change that, by having all image data carry around the information regarding its color profile internally.
When properly handled by GEGL and GIMP, this allows babl to perform the right conversions at the right time, letting all image-processing operations be applied in the correct color space.

While the ongoing work toward this goal is already available in the mainline babl and GEGL versions, we are currently restricting it to the GIMP 2.99 development version (to become GIMP 3.0), but it will most likely make its way into a future GIMP 2.10.x release.

## GIMP extensions

Lastly Jehan, from [ZeMarmot project](https://www.patreon.com/zemarmot),
has been working on [extensions in
GIMP](https://girinstud.io/news/2018/07/crowdfunding-for-extension-management-in-gimp-and-other-improvements/).
An extension could be anything from plug-ins to splash images, patterns,
brushes, gradients… Basically anything which could be created and added
by anyone. The end goal would be to allow creators of such extensions to
upload them on public repositories, and for anyone to search and install
them in a few clicks, with version management, updates, etc.

<figure>
<img src="{attach}gimp-2-10-6-ongoing-dev-extensions.jpg"
     alt="Extension (Ongoing development)">
<figcaption>
Extension manager in future GIMP
</figcaption>
</figure>

This work is also only in the development branch for the time being,
but should make it to a GIMP 2.10.x release at some point in the future
as well.

## Helping development

Keep in mind that pippin and Jehan are able to work on GEGL and GIMP thanks
to crowdfunding and the support of the community. 
Every little bit helps to support their work and helps to make GIMP/GEGL even
more awesome!
If you have a moment, check out their support pages:

* [pippin Patreon](https://www.patreon.com/pippin "Øyvind Kolås Patreon Page")
* [Jehan Patreon](https://www.patreon.com/zemarmot "ZeMarmot Patreon Page") (ZeMarmot)

Most importantly: have fun with GIMP everyone!
