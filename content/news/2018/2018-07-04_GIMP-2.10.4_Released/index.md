Title: GIMP 2.10.4 Released
Date: 2018-07-04
Category: News
Authors: Alexandre Prokoudine
Slug: gimp-2-10-4-released
Summary: The latest update of the stable series delivers bugfixes, simple horizon straightening, async fonts loading, fonts tagging, and more new features.

The latest update of GIMP's new stable series delivers bugfixes, simple horizon straightening, async fonts loading, fonts tagging, and more new features.

## Simple Horizon Straightening

A common use case for the *Measure* tool is getting GIMP to calculate the angle
of rotation, when horizon is uneven on a photo. GIMP now removes the extra step
of performing rotation manually: after measuring the angle, just click the
newly added **Straighten** button in the tool's settings dialog.

<figure>
<img src="{attach}gimp-2-10-4-straighten.jpg" alt="Straightening images">
<figcaption>
Straightening images in GIMP 2.10.4.
</figcaption>
</figure>

## Asynchronous Fonts Loading

Loading all available fonts on start-up can take quite a while, because as soon
as you add new fonts or remove existing ones, fontconfig (a 3rd party utility
GIMP uses) has to rebuild the fonts cache. Windows and macOS users suffered the
most from it.

Thanks to Jehan Pagès and Ell, GIMP now performs the loading of fonts in a
parallel process, which dramatically improves startup time. The caveat is that
in case you need to immediately use the *Text* tool, you might have to wait till
all fonts complete loading. GIMP will notify you of that.

<p>
<video width="960" height="492" controls>
  <source src="https://download.gimp.org/gimp/video/v2.10/gimp-2-10-4-background-fonts-loading.webm" type="video/webm">
  <source src="https://download.gimp.org/gimp/video/v2.10/gimp-2-10-4-background-fonts-loading.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>
</p>

## Fonts Tagging

Michael Natterer introduced some internal changes to make fonts taggable.
The user interface is the same as for brushes, patterns, and gradients.

GIMP doesn't yet automatically generate any tags from fonts metadata, but this
is something we keep on our radar. Ideas and, better yet, patches are welcome!

## Dashboard Updates

Ell added several new features to the *Dashboard* dockable dialog that helps
debugging GIMP and GEGL or, for end-users, finetune the use of cache and swap.

New *Memory* group of widgets shows currently used memory size, the available
physical memory size, and the total physical memory size. It can also show the
tile-cache size, for comparison against the other memory stats.

<figure>
<img src="{attach}gimp-2-10-4-dashboard.jpg" alt="Updated Dashboard">
<figcaption>
Updated Dashboard in GIMP 2.10.4.
</figcaption>
</figure>

Note that the upper-bound of the meter is the physical memory size, so the
memory usage may be over 100% when GIMP uses the swap.

The *Swap* group now features "read" and "written" fields which report the total
amount of data read-from/written-to the tile swap, respectively. Additionally,
the swap busy indicator has been improved, so that it's active whenever data has
been read-from/written-to the swap during the last sampling interval, rather
than at the point of sampling.

## PSD Loader Improvements

While we cannot yet support PSD features such as adjustment layers, there is one
thing we can do for users who just need a file to render correctly in GIMP.
Thanks to Ell, GIMP now can load a "merged", pre-composited version of the
image, that becomes available when a PSD file was saved with "Maximize
Compatibility" option enabled in Photoshop.

This option is currently exposed as an additional file type ("Photoshop image
(merged)"), which has to be explicitly selected from the filetype list when
opening the image. GIMP then will render the file correctly, but drop certain
additional data from the file, such as channels, paths, and guides, while
retaining metadata.

## Builds for macOS Make a Comeback

[Beta builds][] of GIMP 2.10 for macOS are available now. We haven't eliminated
all issues yet, and we appreciate your feedback.

[beta builds]: https://download.gimp.org/gimp/v2.10/osx/testing/

## GEGL and babl

Ell further improved the *Recursive Transform* operation, allowing multiple
transformations to be applied simultaneously. He also fixed the trimming of
tile xache into the swap.

<p>
<video width="960" height="636" controls>
  <source src="https://download.gimp.org/gimp/video/v2.10/gimp-2-10-4-recursive-multiple-transform.webm" type="video/webm">
  <source src="https://download.gimp.org/gimp/video/v2.10/gimp-2-10-4-recursive-multiple-transform.mp4" type="video/mp4">
Your browser does not support the video tag.
</video>
</p>

New *Selective Hue-Saturation* operation by Miroslav Talasek is now available in
the workshop. The idea is that you can choose a hue, then select width of the
hues range around that base hue, then tweak saturation of all affected pixels.

Øyvind Kolås applied various fixes to the *Pixelize* operation and added the
"needs-alpha" meta-data to *Color to Alpha* and *svg-luminancetoalpha*
operations. He also added a *Threshold* setting to the *Unsharp Mask* filter
(now called *Sharpen (Unsharp Mask)*) to restore and improve the legacy
*Unsharp Mask* implementation from GIMP prior to v2.10.

In babl, Ell introduced various improvements to the babl-palette code, including
making the default palette initialization thread-safe. Øyvind Kolås added an
R~G~B~ set of spaces (which for all BablSpaces mean use sRGB TRC), definitions
of ACEScg and ACES2065-1 spaces, and made various clean-ups. Elle Stone
contributed a fix for fixed-to-double conversions.

## Ongoing Development

While we spend as much time on bugfixing in 2.10.x as we can, our main goal
is to complete the GTK+3 port as soon as possible. There is a side effect of
this work: we keep discovering old subpar solutions that frustrate us until
we fix them. So there is both GTK+3 porting and refactoring, which means we
can't predict when it'll be done.

Recently, we also revitalized an outdated subproject called 'gimp-data-extras'
with the sole purpose of keeping the Alpha-to-Logo scripts that we removed
from 2.10 due to poor graphics quality. Since some users miss those scripts, 
there is now a simple way to get them back: download 
[gimp-data-extras v2.0.4][], unpack the archive, and copy all '.scm' files
from the 'scripts' folder to your local GIMP's 'scripts' folder.

[gimp-data-extras v2.0.4]: https://download.gimp.org/gimp/extras/
