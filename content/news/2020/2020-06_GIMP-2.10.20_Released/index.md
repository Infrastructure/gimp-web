Title: GIMP 2.10.20 Released
Date: 2020-06-11
Category: News
Authors: Wilber
Slug: gimp-2-10-20-released
Summary: "GIMP 2.10.20 comes with new features as well as important bugfixes."

GIMP 2.10.20 comes with new features as well as important bugfixes.

Release highlights:

- Tool-group menus can now expand on hover
- Non-destructive cropping now available by cropping the canvas rather than actual pixels
- Better PSD support: exporting of 16-bit files now available, reading/writing channels in the right order
- On-canvas controls for the _Vignette_ filter
- New filters: _Bloom_, _Focus Blur_, _Lens Blur_, _Variable Blur_
- Blending options now built into filter dialogs
- Over 30 bugfixes

<figure>
<img src="{attach}20200607-covid19.jpg" alt="We wish you all the best health! - Wilber and co. comics strip"/>
<figcaption>
<em>We wish you all the best health! by <a href="https://film.zemarmot.net">Aryeom</a>, Creative Commons by-sa 4.0</em>
</figcaption>
</figure>

# Toolbox updates 

We listened to users' feedback on introducing tool groups in the toolbox in the previous release. A lot of people told us they appreciated the change in general but were quite averse to having to click to open the list of tools in a group. The new release adds the option to show the tool-group menu as soon as the mouse hovers over the toolbox button, without having to click it. This option is enabled by default when the toolbox is arranged in a single column, but it can be enabled for arbitrary toolbox layouts, or disabled entirely, through the _Toolbox_ page of the _Preferences_ dialog.

Additionally, when not using the new behavior, toolbox tooltips now list all the tools in a group, to improve their discoverability.

<figure>
<img src="{attach}gimp-2-10-20-toolbox-tooltip.jpg" 
alt="List of tools in a tool group"/>
</figure>

# Basic non-destructive cropping

GIMP now provides a kind of a non-destructive cropping behavior by default. Instead of deleting pixels that you cropped out and thus changing both the layer and the canvas, it will simply resize the canvas. If you export such an image, the resulted file will only have what you see within canvas boundaries.

<figure>
<img src="{attach}gimp-2-10-20-non-destructive-crop.jpg" 
alt="Non-destructive cropping"/>
</figure>

The benefit of that is (at least) threefold:

- You can revert to the original uncropped version by going to `Image -> Fit Canvas to Layers`. None of your edits between cropping and uncropping will disappear.
- If you save your project as an XCF file, you can close the file and even quit GIMP and still be able to remove cropping and then crop differently at any time later. 
- When you are on the fence about your cropping decision, you can view pixels that you cropped out by going to `View -> Show All`.

If you want the old "destructive" behavior back, simply tick the 'Delete cropped pixels' checkbox in _Crop_ tool's settings.

# New and improved filters

The _Vignette_ filter now has on-canvas controls to visually manipulate the geometry of the vignette rather than enter numeric values in a dialog.

Whichever vignette shape you pick, you will always have control for the inner area that stays unchanged, the border of the vignette where pixels stop changing, and the medium point between the two. Dragging the mouse pointer anywhere outside of the outer control will result in rotating the vignette shape.

In addition, there are two new shapes,  'Horizontal' and 'Vertical'.

<figure>
<img src="{attach}gimp-2-10-20-vignette.jpg" 
alt="Vignette filter"/>
</figure>

There are three new filters all related to imitating out-of-focus blur.

_Variable Blur_ takes a layer or channel as an input mask to decide which pixels should be blurred (at what percentage of user-defined maximum blur intensity) and what pixels should stay unchanged, and then blurs pixels with Gaussian Blur.

<figure>
<img src="{attach}gimp-2-10-20-variable-blur.jpg"
alt="Variable Blur filter"/>
<figcaption>
<em>Featured image by <a href="https://500px.com/Armadeus">Raphaël Bacco</a>.</em>
</figcaption>
</figure>

_Lens Blur_ does the same but provides a far more realistic imitation of the out-of-focus blur, including the partial occlusion of sharp foreground objects by highlights blurred in the background. You also have control of how much highlights are affected.

<figure>
<img src="{attach}gimp-2-10-20-lens-blur.jpg" 
alt="Lens Blur filter"/>
</figure>

_Focus Blur_ provides a simple user interface to out-of-focus blurring using the same on-canvas controls that the updated _Vignette_ filter has. You can choose between _Gaussian Blur_ and _Lens Blur_ as blurring methods. One of the uses for the filter is creating miniature fakes out of regular photos — the effect originally achieved by using a tilt-shift lens that changes the plane of focus.

<figure>
<img src="{attach}gimp-2-10-20-focus-blur.jpg" 
alt="Focus Blur filter"/>
<figcaption>
<em>Featured image by Matt McNulty.</em>
</figcaption>
</figure>

New _Bloom_ filter applies an effect that looks a lot like what you can get with the _Soft Glow_ filter but, unlike, _Soft Glow_, it doesn't decrease saturation. Technically, _Bloom_ isolates the highlights region, feathers it, and then recombines it with the original image.

<figure>
<img src="{attach}gimp-2-10-20-bloom.jpg"
alt="Bloom filter"/>
<figcaption>
<em>Featured image by <a href="https://sites.google.com/site/thebrockeninglory/">Brocken Inaglory</a>.</em>
</figcaption>
</figure>

# Filter dialog improvements

The options dialog of GEGL filters now provides a new _Blending Options_ section, which allows controlling the blending-mode and opacity with which the filter is applied.
This replaces the _Fade_ functionality, which was removed in version 2.10.10.

Filter previews now remain cached even when toggled off, allowing for quickly switching between the original and filtered views.

# Better PSD support

While GIMP could already load 16-bits-per-channel PSD files, high-bit-depth images can now be exported using 16-bits per channel as well.

In addition, channels are now exported in the correct order, and with their original colors.

# Other changes

- Painting tools can now save and load opacity and blending mode to/from presets.
- Canon CR3 files are now properly recognized by GIMP and sent to your raw developer software of choice.
- The TWAIN plug-in used for acquiring images via scanners has been slightly refactored and now supports 16-bit RGB/grayscale images.
- PNG and TIFF plug-ins now default to not saving color values when alpha channel is present and 0 itself. This is to address security concerns about using simple cutting to remove sensitive information.
- The PDF plug-in now imports multi-page documents in bottom-first order, similar to animated formats, and also similar to defaults for PDF exporting. This brings consistency but breaks existing behavior in 3rd party scripts.

# GEGL and babl

As usual, new GIMP release is accompanied by new releases of [babl](http://gegl.org/babl/) and [GEGL](http://gegl.org/) libraries.

The new version of babl comes mostly with bug fixes and performance improvements like the use of AVX2 instructions to convert gamma u8 data to linear float (making those conversions between 1.25x and 2.2x faster). It also features VAPI file generation for Vala integration as the unstable branch of GIMP now supports creating new plug-ins with Vala.

The most important changes in GEGL are:

- New meta-data API that permits handling non-Exif metadata in different file loaders
and savers in a generic manner
- Faster and softer cubic interpolation.
- GEGL now gracefully fails when running out of swap space.
- The `hue-chroma` operation has been fixed to avoid modifying hue/chroma of neutrals
- The `dropshadow` operation now has an option to grow the shadow. This means, with zero shift and larger shadow, you can stroke a text or a bitmap layer.

On top of that, GEGL got several new operations, some of which we already described above. Others are:

- `border-align`: places a buffer within the borders of another one.
- `pack`: joins two buffers into one, with optional gap.
- `piecewise-blend`: uses a grayscale map as index into array of buffers used as LUT (required by new blur filters)
- `reset-origin`: moves upper left of extent to 0,0
- `band-tune`: parametric band equalizer for tuning frequency bands of image.

# Contributors

Contributors to this release are: Ell, Jehan, Jernej Simončič, lillolollo, luz.paz, Marco Ciampa, Michael Natterer, Michael Schumacher, Nikc, Øyvind Kolås, pesder, Sabri Ünal, Salamandar, Sergio Jiménez Herena, Simon Budig, T Collins, woob.

Translators: Alexandre Prokoudine, Anders Jonsson, Bruce Cowan, Cristian Secară, Daniel Korostil, Daniel Șerbănescu, Dimitris Spingos, Jiri Grönroos, Jordi Mas, Nathan Follens, Piotr Drąg, Rodrigo Lledó Milanca, Sabri Ünal, Seong-ho Cho, Tim Sabsch, Yuri Chornoivan, Georgy Timofeevsky.

We also thank lillolollo, nmat, and Michael Schumacher for triaging bug reports, and Julien Hardelin for updating the user manual.

# What's next

We are wrapping up the development in the `master` branch for v2.99.2, the first unstable release in the 2.99 series leading up to v3.0 some time in the future.

We know that this release is anticipated by various demographics of our users, from people doing digital painting (you can hotplug a graphic tablet now) to photographers using a 4K display (the icon size is right for them now) to people generally wishing to drop GTK2 and Python 2 from their operating systems.

Having said that, there is a warning to make as well.

While we are looking forward to feedback and bug reports, we do not recommend upcoming v2.99.2 for use in production. There are both improvements and regressions from the 2.10.x series. Full details will be published when the release is out (soon).

For the time being, don't forget you can [donate to the project and personally
fund several GIMP developers](https://www.gimp.org/donating/), as a way to give
back, and to accelerate the development of GIMP.
