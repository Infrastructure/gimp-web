Title: GIMP 2.10.22 Released for macOS
Date: 2020-12-25T19:00:00+01:00
Category: News
Authors: Wilber
Slug: gimp-2-10-22-released-macos
Summary: "GIMP 2.10.22 available for macOS - and what is going on with Big Sur"

GIMP 2.10.22 is now available as a DMG file from our [downloads page](https://www.gimp.org/downloads).

Many thanks to Des McGuinness, who updated the build enviroment created by
Alex Samorukov and succeeded in getting the current stable code built and
notarized!

This brings all the changes and fixes since GIMP 2.10.14 to macOS users, who
had been limited to this increasingly outdated version for far too long. Several
of the changes are quite visible and noticable to users, so it is a good idea
to check the release notes for GIMP
[2.10.18](https://www.gimp.org/news/2020/02/24/gimp-2-10-18-released/),
[2.10.20](https://www.gimp.org/news/2020/06/11/gimp-2-10-20-released/) and
[2.10.22](https://www.gimp.org/news/2020/10/07/gimp-2-10-22-released/) to get up
to date with the current versions.

# What's going on with Big Sur

It is not all well on the platform yet, though - with users upgrading to the latest macOS release, Big Sur, we started getting reports about performance and user interface issues.

[GIMP being very slow](https://gitlab.gnome.org/GNOME/gimp/-/issues/5989) and [invisible selection outlines](https://gitlab.gnome.org/GNOME/gimp/-/issues/5952) are reported most frequently. It is likely both are symptoms of the same underlying technical issue, that being the image window content being updated completely and far too frequently than necessary.

It feels really good to have active contributors to the macOS platform again, this gives us confidence that the issues can be investigated properly and, hopefully, mitigated or completely solved.

If you encounter any issue in addition to the two linked above, please let us know; the [bugs](/bugs/) page explains how to do this. If you are unsure whether a bug is already known, you can search for them there, or have a look at [all the issues reported for the macOS platform](https://gitlab.gnome.org/GNOME/gimp/issues?label_name%5B%5D=OS%3A+macOS+%2F+OSX).
When in doubt, report it anyway, we will figure out duplicates and mark them accordingly.

Finally, please don't forget you can [donate to the project and personally fund several GIMP developers](https://www.gimp.org/donating/), as a way to give back and accelerate the development of GIMP.
