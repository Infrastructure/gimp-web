Title: GIMP and GEGL in 2019
Date: 2020-01-04
Category: News
Authors: Wilber
Slug: gimp-and-gegl-in-2019
Summary: Here is what we did in 2019 and what we are planning to do in 2020.

Here is what we did in 2019 and what we are planning to do in 2020.

<figure>
<img src="{attach}2020happynewyear.jpg" alt="Happy New Year 2020 from GIMP and ZeMarmot, by Aryeom"/>
<figcaption>
<em>"Happy New Year 2020", by <a href="https://film.zemarmot.net">Aryeom</a>, Creative Commons by-sa 4.0</em>
</figcaption>
</figure>

# New releases and features

2019 was the second year in a row where we shipped updates with new features in the stable branch. Our assumption was that this could change the public's perception of the ongoing development efforts and shift the balance towards having more contributors. Here is why.

Between 2012 and 2018 (v2.8 and v2.10 releases respectively), we worked hard and added a ton of improvements and new features, we demoed them on social networks, mentioned them in annual reports etc., and yet we kept hearing how GIMP was dead because those changes were not in any stable releases. The same thing was happening before in the four years between v2.6 and v2.8.

Moreover, this was preventing people from contributing as they would have to wait a long time to see their contribution actually used. That wasn't sparking an interest really.

Hence, after the v2.10 release, we kept adding new features at the same pace and started producing regular updates with those features, and all of a sudden we started hearing how we "picked up the pace"!

So this could be a lesson for other projects: arguing against the irrational is futile. Just don't keep people waiting. If you did something good, share it!

Either way, there have been three updates in 2019. In terms of focus, here is what we targeted. The list below is far from being complete, these are just the most obvious changes.

## Usability improvements

- GIMP is finally able to optionally display and edit pixels outside the canvas boundaries.
- There's a new preference to allow the editing of hidden layers.
- On-canvas handles of transformation tools can now be readjusted after zooming in or out.
- The _Free Select_ tool now creates a preliminary selection so that you could both copy and paste the selection and tweak positions of nodes when you do a polygonal selection. 
- You can now switch to a particular layer by pointing at its pixels and pressing `Alt + middle click`.
- The _Curves_ tool/filter now allows for more predictable editing and comes with two types of nodes instead of one to make more useful, more sophisticated curves.

<figure>
<img src="{attach}2019-report-curve-tool-update.jpg" alt="Updated Curve tool"/>
</figure>

## Better tools

- Parametric brushes now have 32-bit per channel precision.
- The _Bucket Fill_ tool got a new mode for smart colorization of line art, this is handy for comic artists.
- The new _Sample Merged_ option in the _Heal_ tool allows non-destructive retouching of photos on a separate layer.
- The _Foreground Select_ tool got a new _Grayscale_ preview mode.
- The New _Offset_ tool makes it possible to shift pixels and optionally wrap them around the edges so that you could create repeatable patterns.

## Better performance

- Faster painting: GIMP now doesn’t replace the paint buffer on every dab if the paint color/pixmap hasn’t changed.
- Faster saving/exporting and layer groups rendering.
- Grayscale workflows are now an order of magnitude faster thanks to changes in the babl library.

## Improved file formats support

- Due to popular demand, we merged the existing DDS plug-in originally developed by Shawn Kirst. But as none of us is a game developer, we appreciate further contributions from the community.
- Layers are now optionally preserved when exporting to TIFF, and if you load a TIFF file with an unspecified channel, GIMP will now ask you how to handle it.
- GIMP now supports ICC profiles in HEIF images at both loading and exporting time when built with libheif v1.4.0 and newer. 

<figure>
<img src="{attach}2019-report-dds-exporting-options.png" alt="DDS exporting options"/>
</figure>

## New filters

A major addition here is the _Normal Map_ filter that generates [normal maps](https://en.wikipedia.org/wiki/Normal_mapping) commonly used in production of 3G graphics and games. It's already usable for some workflows, and there's a plan for further improvements.

We also provided direct access to more filters, developed in GEGL a couple of years ago:

- _Bayer Matrix_ (for ordered dithering) and _Linear Sinusoid_ (useful for halftoning)
- _Newsprint_, a GEGL version of the old GIMP filter for halftoning, plus quite a few extras
- _Mean Curvature Blur_ for edge-preserving blurring

A few more filters, namely _Neon_, _Stretch Contrast_, and _Oilify_ are finally on par with the original implementations and thus replaced them.

<figure>
<img src="{attach}2019-report-normal-map.jpg" alt="New Normal Map filter"/>
</figure>

# Build systems, CI, and packaging

During 2019, we ported babl, GEGL, and the master branch of GIMP to the [Meson](https://mesonbuild.com/) build system. While it's of little relevance to end-users who rely on ready-to-use builds of GIMP, to developers it means much faster compilation time.

We also set up Gitlab-based continuous integration (CI) for all three projects, which gives us a few bonuses over the previously used Jenkins-based solution. One of them is that Windows builds are now available for every successful commit to GIMP's master branch.

Over the year, Alex Samorukov contributed a bunch of improvements to the macOS version, including support for Catalina.

# Development in the unstable branch

As you probably already know from the previous report, we manage to backport most of the new features form the unstable branch to the stable one. However, some changes are the result of refactoring and some changes rely on the tech not available in the GTK2 stack.

One of the latter things is the entirely new object-based plug-ins API, with an extra bonus of preserving plugins' settings across sessions. We expect more to happen there. This part of the work was done mostly by Michael Natterer and Jehan Pages.

The use of GObject introspection has more implications: we switched to Python 3 and made it possible to write new plug-ins in Lua and JavaScript.

Most recently, Jehan started working on a new feature that checks for availability of a newer version of the program and suggests downloading an update. It also integrates into the bug reporting dialog to tell the user if the version currently in use is out of date and the update may not have that bug anymore.

# What's new in GEGL and babl

There have been numerous changes in both GEGL and babl:

- further work on space invasion to streamline the handling of color spaces, in collaboration with Elle Stone and others;
- a ton of changes in the standalone GEGL tool, making it suitable for node-based compositing and video playback;
- far more sophisticated CMYK support, up to blending CMYK and RGB pixel data, reading and writing CMYK TIFF and JPEG files;
- a better use of available CPU cores on more operations thanks to newly added dynamic computation of per-operation thread cost;
- better memory management: when you close large images in GIMP, it will now free used memory a lot faster.
- sharper output when downscaling images with the cubic sampler.

Øyvind Kolås also started [ctx](https://pippin.gimp.org/ctx/), a new vector rasterizer project with API inspired by Cairo and HTML5 canvas’ 2D rendering context, and sufficiently small resource footprint to provide modern vector graphics on many microcontrollers.

The ctx library already has support for floating point pixel formats, and that support is geared to end up generic for gray, RGB, CMYK and other multi-component formats. Lack of support for color spaces and pixel encodings beyond 8-bit sRGB in cairo has been a gap for GEGL/GIMP that ctx can end up filling.

# Team changes

Most of the source code changes in all three projects are still coming from the usual suspects: Michael Natterer, Jehan Pages, Ell, Øyvind Kolås, and Thomas Manni. Nevertheless, there have been a few new contributors who seem interested in sticking around.

A significant part of the contributions towards the new build system and CI support came from Félix Piédallu and Salamandar.

We now also have a small dedicated team of people, frogonia and nmat, who triage bug reports on a regular basis and do user support on the IRC channel. Liam Quin who has been team member for a long time, now does the vast majority of user support on Facebook.

We are still lucky to have Julien Hardelin as the author of the user manual.

And, of course, we keep getting no small amount of translation updates from new and seasoned translators. In fact, since the v2.10.14 release, we now mention non-code contributors to the project in release notes.

# What's next in 2020

There is an agreement among team members that 2.99.x releases are long overdue. We are likely to tie loose ends and start shipping them soon.

# How you can help

As usual, we appreciate both coding and non-coding contributions to GIMP, GEGL, and babl.

There are many things we want to improve and even redo completely. And while we do say that certain changes are targeted for v3.2 and beyond,  anything goes really, especially with the master branch.

UI/UX redesign? Shapes drawing? Clips for layers? Better text tool? Layer filters? Vector layers? You name it. Talk to us, if you want to work on any of that.

And if you don't do programming, there's plenty of work to do all around: updating and translating the [user manual](https://gitlab.gnome.org/GNOME/gimp-help), writing new tutorials, updating [incomplete translations](https://l10n.gnome.org/module/gimp/#gimp-2-10), even making great art with GIMP and telling people about it.