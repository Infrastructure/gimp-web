Title: GIMP 2.10.18 Released
Date: 2020-02-24
Category: News
Authors: Wilber
Slug: gimp-2-10-18-released
Summary: "We skipped announcing 2.10.16 due to a critical bug. Together, the two updates deliver several major usability improvements, a new tool for transformations in 3D space, new release checker, and the usual amount of bug fixes."

We skipped announcing 2.10.16 due to a critical bug. Together, the two updates deliver several major usability improvements, a new tool for transformations in 3D space, new release checker, and the usual amount of bug fixes.

Here are release highlights:

- Tools are now grouped in the toolbox by default
- Sliders now use a compact style with improved user interaction
- Vastly improved user experience for the transformation preview
- Dockable areas now highlighted when a dockable dialog is being dragged
- New 3D Transform tool to rotate and pan items
- Much smoother brush outline preview motion on the canvas
- Symmetry painting enhancements
- Faster loading of ABR brushes
- PSD support improvements
- Consolidated user interface for merging down and anchoring layers
- Update check to notify users of new releases available
- 28 bug fixes, 15 translation updates

<figure>
<img src="{attach}gimp-2-10-18-update-tools-group.jpg" alt="GIMP update brings tool groups, by Aryeom"/>
<figcaption>
<em>"GIMP update brings tool groups, by <a href="https://film.zemarmot.net">Aryeom</a>, Creative Commons by-sa 4.0</em>
</figcaption>
</figure>

# Tools now grouped by default 

Tools can now be grouped in the toolbox, and this option is enabled
by default.

<figure>
<img src="{attach}gimp-2-10-18-tools-groups-ui.jpg" 
alt="New compact style for sliders"/>
</figure>

You can customize groups by creating new ones and dragging tools between
them. The changes will take effect immediately. Or you can disable
the grouping entirely. You'll find configuration options on the
_Interface/Toolbox_ page of the _Preferences_ dialog.

Please note that the default order of tools in the toolbox is now different.
You can customize it as well. 

# Compact sliders

Sliders typically used in GEGL-based filters and tools' options now have
a compact style by default: they take a lot less space vertically and have
a vastly improved interaction model.

<figure>
<img src="{attach}gimp-2-10-18-compact-sliders-style.jpg" 
alt="New compact style for sliders"/>
</figure>

You can use multiple modifiers with either left-click or mouse wheel
scrolling:

- __Left-click + drag__ changes a value with a default increment
- __Shift + left-click + drag__ (or __right-click + drag__) changes a value
with a smaller step
- __Ctrl + left-click + drag__ change a value with a larger step

Here is the full reference:

<figure>
<img src="{attach}gimp-2-10-18-new-slider-interaction_v6.png" 
alt="New interaction model for the slider widget"/>
</figure>

# Docking UX improvements

The 'You can drop dockable dialogs here' message is now gone from the
toolbox, and other empty dockable areas. This used to annoy quite a few users
who used a single/double-column layout for the left panel.

But since we still need to hint where you can dock dialogs, whenever you drag
a dialog, all dockable areas will be highlighted.

<div class='fluid-video'>
<video width="1280" height="720" controls>
  <source src="https://download.gimp.org/gimp/video/v2.10/gimp-2-10-16-docking-highlight.mp4" type="video/mp4">
  <source src="https://download.gimp.org/gimp/video/v2.10/gimp-2-10-16-docking-highlight.webm" type="video/webm">
Your browser does not support the video tag.
</video>
</div>

# High-contrast symbolic theme now available

Since releasing 2.10 in 2018, we received a lot of feedback that the symbolic
icon theme used by default doesn't have enough contrast. We recently did
a quick poll on Twitter showing people a variation of the theme with more
foreground/background color contrast, and that certainly clicked with users.

Some of the feedback suggested, however, that a part of the demographic likes
the current contrast. So instead of pushing changes for everyone, we
introduced a new high-contrast symbolic theme. You can choose it in the
_Preferences_ dialog, just like any other icon theme.

<figure>
<img src="{attach}gimp-2-10-18-high-contrast-icons.png" 
alt="New high-contrast icon themes"/>
</figure>

The contrast is a compile-time variable that you can change prior to building
GIMP from source code. We see this as more of a dirty temporary solution.

With GTK3, it's going to be a lot easier to make things configurable. In
particular, upcoming Inkscape 1.0 is featuring a new icon theme called
'multicolor' that makes a great use of CSS in GTK3 and, while staying symbolic,
uses some color where it matters. We will be definitely looking closer at that
approach.

To complement the new high-contrast variation of the symbolic theme, GIMP now
also draws a double black/white border around FG/BG indicator in the toolbox
to make that border more legible, especially in dark themes.

# Transformation preview improvements

A new option called _Composited Preview_ is now available for most
transformation tools. It enables the rendering of the transform preview
with the right position of the modified layer in the layers stack, as well
as with the correct blending mode.

<div class='fluid-video'>
<video width="1280" height="720" controls>
  <source src="https://download.gimp.org/gimp/video/v2.10/gimp-2-10-16-composited-preview.mp4" type="video/mp4">
  <source src="https://download.gimp.org/gimp/video/v2.10/gimp-2-10-16-composited-preview.webm" type="video/webm">
Your browser does not support the video tag.
</video>
</div>

It comes with two sub-options.

_Preview linked items_ enables previewing changes to all linked items
like layers rather than just the currently selected item.

<div class='fluid-video'>
<video width="1280" height="720" controls>
  <source src="https://download.gimp.org/gimp/video/v2.10/gimp-2-10-16-linked-layers-transformation.mp4" type="video/mp4">
  <source src="https://download.gimp.org/gimp/video/v2.10/gimp-2-10-16-linked-layers-transformation.webm" type="video/webm">
Your browser does not support the video tag.
</video>
</div>

_Synchronous preview_ render the preview as you move the mouse/stylus
pointer to change the transform instead of waiting for the movement to stop.
This provides instant feedback when GIMP can update the display fast enough,
which is usually not the case with large layers.

GIMP now also automatically previews the clipping of transformed layers.
This allows reducing the amount of trial and error when e.g. rotating. 

# New 3D Transform tool

A new transform tool helps changing the perspective of a layer or panning
it in 3D space. You can set a vanishing point, then rotate the layer
in X, Y, and Z axes.

<div class='fluid-video'>
<video  width="1280" height="867" controls>
  <source src="https://download.gimp.org/gimp/video/v2.10/gimp-2-10-16-new-3d-transform-tool.mp4" type="video/mp4">
  <source src="https://download.gimp.org/gimp/video/v2.10/gimp-2-10-16-new-3d-transform-tool.webm" type="video/webm">
Your browser does not support the video tag.
</video>
</div>

Multiple modifiers are available to constrain rotation and panning to just one
axis. The _Unified interaction_ checkbox allows shifting the vanishing, as well
as panning and rotating without switching between tabs on the on-canvas
settings dialog. Finally, the _Local frame_ option allows controlling the
transformation in the layer's local frame of reference, instead of the global
one.

# Brush outline and quality improvements

The brush outline motion now feels smoother thanks to raising the refresh
rate from 20 FPS to a maximum of 120 FPS, as well as disabling the snapping to
dabs (new option, off by default). The former became a sensible idea thanks
to painting parallelization introduced by Ell several releases ago.
The snapping to brush dabs can be re-enabled on the _Image Windows_ page
of the _Preferences_ dialog. 

<div class='fluid-video'>
<video width="1280" height="720" controls>
  <source src="https://download.gimp.org/gimp/video/v2.10/gimp-2-10-16-smooth-brush-outline-motion.mp4" type="video/mp4">
  <source src="https://download.gimp.org/gimp/video/v2.10/gimp-2-10-16-smooth-brush-outline-motion.webm" type="video/webm">
Your browser does not support the video tag.
</video>
</div>

Additionally, the paint rate of the _Airbrush_ tool was increased from 15 to a
maximum of 60 stamps per second, making paint buildup smoother. Note that, as a
result, the relation between the tool's _Rate_ parameter and the actual paint
buildup rate is now different, which may affect existing tool presets.

It's also worth mentioning that the _Warp Transform_ tool now respects
mouse pointer settings defined on the _Image Windows_ page of the
_Preferences_ dialog.

Furthermore, in order to improve the quality of downscaled brushes, paint tools
now use mipmaps for downscaling bitmap brushes. Instead of resampling
downscaled brushes directly from their original size, GIMP now creates a
box-filtered mipmap hierarchy for the original brush on demand, and uses the
closest mipmap level as the resampling source for downscaled brushes. This
significantly reduces aliasing in downscaled brushes, producing smoother
results.

# Symmetry painting enhancements

The _Mandala_ symmetry painting mode now has a _Kaleidoscope_ option, which
combines both rotation and reflection.

<div class='fluid-video'>
<video width="1280" height="720" controls>
  <source src="https://download.gimp.org/gimp/video/v2.10/gimp-2-10-16-kaleidoscope.mp4" type="video/mp4">
  <source src="https://download.gimp.org/gimp/video/v2.10/gimp-2-10-16-kaleidoscope.webm" type="video/webm">
Your browser does not support the video tag.
</video>
</div>

When the _Kaleidoscope_ option is enabled, subsequent strokes are mirrored
across the symmetry slice edges.

# Faster loading of ABR brushes

GIMP now spends a lot less time loading Photoshop's brushes (ABR).
So if you use a lot of those, the startup time will become pleasantly
smaller, by order of a magnitude.

The technical explanation is that GIMP used to read the stream of ABR
data byte by byte, and now it uses scanline reading instead.

# PSD support improvements

PSD files now load faster mostly by eliminating excessive copies between
the original file and the project representation inside GIMP. For large
PSD files, the loading is now ~1.5 to ~2 times faster.

Moreover, GIMP is now capable of loading CMYK(A) PSD files (only 8-bit per
channel for now). It does so by converting pixels to RGB(A) float using
sRGB as the profile which, we know, is not good enough for serious work.

However, the plug-in is already using babl formats to specify and communicate
CMYK pixel format encodings with GIMP. This is a good first step towards better
CMYK support. It can be improved both on its own as well as integrate with
the ongoing work enabling general color-space support for babl formats in the
development branch.

# Layers dock improvements

The _Layers_ dialog finally consolidates the UI for merging layers and
attaching floating selections.

The bottom toolbar will now display a button for anchoring a floating
selection only when there is one. Otherwise, it will display a button
for merging layers.

<figure>
<img src="{attach}gimp-2-10-18-consolidated-merging-anchoring-ui.png" 
alt="Consolidated UI for merging layers and anchoring floating selections"/>
</figure>

You can also use several modifiers:

- __Shift__ will merge a layer group
- __Ctrl__ will merge all visible layers
- __Ctrl + Shift__ will merge all visible layers with last used values

# Update check to notify users of new releases available

GIMP will now check every time on the start up if the version of GIMP
you have is the latest one available. It will do so by pinging
GIMP's server for the version of the latest release, then comparing it
to the one installed on your computer.

GIMP will also compare revisions of the installers so that users would
be aware of updated installers available. This is typically useful when
we update installers to provide fixes in 3rd party components that we use.

Finally, this feature is used when constructing a crash report. If you
experience a crash while using an outdated version of the program, GIMP
will now tell you so.

You can disable this feature on the _System Resources_ page of the
_Preferences_ dialog, and manually use the __Check for updates__ button in the
_About_ dialog.

It is also possible to build GIMP without this feature by passing the
`--disable-check-update` argument to the configure script.

# Continuous Integration

Work on our continuous integration goes forward. We now implemented
automatic compilation of the main development branch both with the
legacy `autotools` build system and the new `meson` one. We also produce
an alternative build with the `Clang` compiler (additionally to the GNU
compiler `gcc`).

Moreover, for cross-platform development, we now produce Windows builds,
both for 32-bit and 64-bit, cross-compiled with the
`crossroad`/`Mingw-w64` tools.

All these automatic builds allow us to catch early on specific bugs
which may affect only certain configurations or platforms.

We hope it could also attract new developers wishing to dabble in
contributing. Looking at compilation warnings and trying to fix them may
be a very good first step into GIMP code. It would be much less
overwhelming than actually trying to dive into our huge code from
scratch.

If you are interested, look into our [CI
pipelines](https://gitlab.gnome.org/GNOME/gimp/pipelines) and look at
the last ones (preferably the `master` branch), then into one of the
various compilation jobs. We will be waiting for your
[patches](https://gitlab.gnome.org/GNOME/gimp/merge_requests)!

# GEGL and babl changes

There have been several improvements in GEGL since the release
that accompanied GIMP 2.10.14:

- Fixed potential data-corruption/crash in the tile-swap back-end on Windows
(the fix has already been made available in an updated installer for 2.10.14),
and improved tile-queuing speed.
- The GEGL core now avoids running more thread jobs than there are
pixels to process.
- The teardown of buffer caches is now faster when bounding box
shrinks.
- In-place processing now only happens if region of interest fits in
input abyss.
- Edge handling was improved for gegl:distance-transform operation

The babl library got build fixes, improved host CPU detection,
macOS-specific fixes, and Clang warning squelches.

# Contributors

Code contributors to this release are: Alex Samorukov, Anders Jonsson,
band-a-prend, Cyril Richard, Elad Shahar, Ell, Elle Stone, Félix Piédallu, Jehan
Pagès, Jernej Simončič, lillolollo, Massimo Valentini, Michael Natterer, Nikc,
Øyvind Kolås, Pascal Terjan, woob.

Translators: Alan Mortensen, Alexandre Prokoudine, Anders Jonsson, Asier Sarasua
Garmendia, Balázs Meskó, Balázs Úr, Bruce Cowan, Daniel Korostil, Jordi Mas,
Julien Hardelin, Marco Ciampa, Piotr Drąg, Rodrigo Lledó Milanca, Ryuta Fujii,
Sabri Ünal, sicklylife, Sveinn í Felli, Tim Sabsch, Zander Brown.

As usual, we thank lillolollo, nmat, and Michael Schumacher for triaging
bug reports, and Julien Hardelin for keeping the user manual up to date.

# What's next

Our main objective is still completing the GTK3 port and releasing GIMP 3.0.
This will take a while.

One of the ideas we are also exploring is improving the default
single-window layout and introducing named workspaces streamlined
for common use cases such as general editing, web design, digital
photography, painting etc.

If you customized your default GIMP layout, we encourage you to post
a screenshot and tell us about your use cases for GIMP that affected this
customization. You can do that either
[on Twitter](https://twitter.com/GIMP_Official) or in the mailing list
for users.

Once we have a representative amount of samples for each common use case,
we will analyze the data and see if we can create default workspaces that
you can further tweak to your liking.

For the time being, don't forget you can [donate to the project and personally
fund several GIMP developers](https://www.gimp.org/donating/), as a way to give
back, and to accelerate the development of GIMP.
