Title: This is 25
Date: 2020-11-21
Category: News
Authors: Wilber
Slug: 25-years-of-gimp
Summary: Exactly 25 years ago, Peter Mattis wrote a message to several newsgroups announcing a new image editor called GIMP. 

Exactly 25 years ago, Peter Mattis [wrote a message][] to several newsgroups announcing a new image editor called GIMP. 

[wrote a message]: /about/prehistory.html#november-1995-an-announcement "November 1995 Announcement"

<figure>
<img src="{attach}2020-GIMP-25-th-birthday.jpg" alt="Happy 25th birthday GIMP! - Wilber and co. comics strip"/>
<figcaption>
<em>"Happy 25th birthday GIMP!" by <a href="https://film.zemarmot.net">Aryeom</a>, Creative Commons by-sa 4.0</em>
</figcaption>
</figure>

We've been really busy ever since!

- Had to come up with GTK, a user interface toolkit of our own. Did not expect whole desktop environments, like GNOME and Xfce, to become the result of that. GTK is now a self-contained project used by thousands of developers.

- Surprisingly, got a few developers from Hollywood to write the beginnings of what became a new image processing engine called GEGL, now used by a few more software projects too. We still have barely scratched the surface of what's possible with GEGL.

- Introduced Wilber, a little cute mascot who traveled the world and, admittedly, did some kinky things. They grow up so fast! ([Check out the splash screen archive](/about/splash/))

- Helped kickstart [Libre Graphics Meeting][] as an extended version of our annual meetup in 2006. Made a lot of new friends every year since then.

[Libre Graphics Meeting]: https://libregraphicsmeeting.org "Libre Graphics Meeting Website"

- Did our best to provide a sensible workflow to users by using common user interface patterns. That gave us a few questionable monikers like 'Photoshop for Linux', 'free Photoshop', and 'that ugly piece of software'. We still can wholeheartedly agree with the latter one only!

- Tried to do too many things at once with too few active developers to realistically get things done in a sensible timeframe. Made a lot of people think the project died while we were slaving away really. So we introduced some planning. It's been paying off so far.

- Made more people angry with software's quirks than we'd like to. Got help on that from more passionate contributors than we expected to. We can certainly use more help still.

- Got ourselves an animation project called [ZeMarmot][] to make a positive feedback loop involving artists and developers. Continue using our chat for conversation with artists, some of which put GIMP through *a lot*. That really helps.

[ZeMarmot]: https://film.zemarmot.net/en/ "ZeMarmot animation project"

- Every day, we are one step closer to completing the boring yet extremely important work on refactoring GIMP to make way for great new things. Things that we've been meaning to do for a long time. Things that users have been expecting for an even longer time.

The world is definitely a different place 25 years later. Louder, noisier, more demanding. Definitely less safe. But also full with warmth and humanity. We've seen waves of that washing up and down the rocky shores of GIMP.

We don't really have any kind of big news for you to commemorate the anniversary. Sorry about that. We keep slaving away — in a more intelligent way these days, hopefully. But there might be cake.
