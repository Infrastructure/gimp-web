Title: Happy 27!
Date: 2022-11-21
Category: News
Authors: Jehan
Slug: gimp-27-birthday
Summary: Happy 27th birthday to the GNU Image Manipulation Program
Image: gimp-27th-birthday.jpg

Today, on 21<sup>st</sup> of November 2022, the **GNU Image Manipulation Program
turned 27** (cf. the [first release announcement on
1995-11-21](https://www.gimp.org/about/prehistory.html#november-1995-an-announcement)).

To celebrate, [Aryeom (*ZeMarmot*'s director)](https://film.zemarmot.net/) drew
this nice birthday illustration (fully drawn within GIMP, and under Creative
Commons by-sa 4.0 license, of course!):

<figure>
<a href="{attach}gimp-27th-birthday.jpg">
<img src="{attach}gimp-27th-birthday.jpg" alt="Happy 27th birthday! - Wilber and co. comics strip by Aryeom"/>
</a>
<figcaption>
<em>"Happy 27th birthday!" by <a href="https://film.zemarmot.net">Aryeom</a>
(also a <a href="{attach}gimp-27th-birthday-bg.jpg">Wilber-less version</a> as
temporary gimp.org header), Creative Commons by-sa 4.0</em> </figcaption>
</figure>

For both Aryeom and I (Jehan), this is our tenth year of continued contribution,
since a first *commit* in September 2012 (basic icon-changing patch in the
animation playback plug-in, soon followed by more… many more patches…). Back
then, never would we have imagined sticking for so long around this nice core
community (regarding this point, we thank the other contributors for their
welcomeness, and in particular the wonderful
[mitch](https://www.gimp.org/news/2017/03/01/an-interview-with-michael-natterer-gimp-maintainer/))
and contributing litterally thousands of patches in GIMP! So it's also a pretty
big personal milestone.

It is also my second year maintaining GIMP. And to be fair, Aryeom has a huge
role in my maintenance with constant reviewing, testing my code (and other
contributor's code), following up with feedback, specifying behaviors (while
always caring about others' usage! One of her main rule when she helps designing
changes is researching and wondering what worflows others have). So much is
being done in the shadow to keep it all together.

But GIMP is not only us. What would we do without Øyvind Kolås in particular?
Nowadays he is carrying most of our core flow-based graphics engine maintenance,
[GEGL](https://gegl.org/), and its sister projects ([babl](https://gegl.org/babl/)
and [ctx](https://ctx.graphics/)).

Of course, I can't forget all other awesome contributors: developers, packagers,
community support, translators (GIMP is more than 90% translated in [27
languages](https://l10n.gnome.org/module/gimp/) among 84 languages we currently
support!), documenters, website contributors, tutorial writers… We should also
thank the GNOME Infrastructure team for being so helpful of course. And many
many more! What would GIMP be without all of you? 💌

I will likely do a more detailed report later (like I did [last
year](https://gimp.org/news/2021/12/31/gimp-2021-annual-report/)) to sum up 2022
events, so I'll stay short in this news. For once!

All in all, we wish to remind that **GIMP is Community, Free Software**. It
is what [we make of it together](https://www.gimp.org/develop/). We welcome
contributors very warmly 🤗!

Finally if you can't contribute your time, in these year-end times of giving, don't forget
that you may [**support financially GIMP developers**](https://www.gimp.org/donating/).
GIMP project actively supports its contributors willing to make a living with
their Free Software contribution. Right now it means 3 people: Aryeom and myself
(through *ZeMarmot* project) as [GIMP maintainers](https://www.patreon.com/zemarmot)
and Øyvind as [GEGL maintainer](https://www.patreon.com/pippin). If you
appreciate what we do and wish to **give back**, funding us is an excellent way.
It is part of what makes GIMP development sustainable.

And for everyone who is really eager to see **GIMP 3.0** out, it should go
without saying that funding developers is what accelerates the development of
GIMP.

🎁 **So GIMP, and
[Wilber](https://www.gimp.org/about/linking.html#wilber-the-gimp-mascot), we wish you a very happy 27!** 🎂

And to every member of the community: **thank you all for sticking with this
project for all or part of these 27 years!** GIMP would not be where it is today
without all of you! 💌
