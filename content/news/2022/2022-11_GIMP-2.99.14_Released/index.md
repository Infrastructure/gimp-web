Title: Development version: GIMP 2.99.14 Released
Date: 2022-11-18
Category: News
Authors: Jehan
Slug: gimp-2-99-14-released
Summary: Release news for development version GIMP 2.99.14

The GIMP team is happy to release GIMP 2.99.14 with a lot of nice milestones on
the route to GIMP 3.0.

We are getting into deep changes, so we hope you will all test thoroughly and
we remind you that it is an unstable version meant for testing and reporting
issues.

[TOC]

To get a more complete list of changes, you should refer to the
[NEWS](https://gitlab.gnome.org/GNOME/gimp/-/blob/f7f92b61e1d703ac4bf878b305019c98674c4745/NEWS#L9)
file or look at the [commit
history](https://gitlab.gnome.org/GNOME/gimp/-/commits/master).

## Tools
### Align and Distribute tool: fully reworked interaction

The Alignment tool was very hard to use, with complicated on-canvas interaction
to select the target items (and never being too sure if we selected right!).
Thanks to the core [multiple layer
selection](https://www.gimp.org/news/2020/11/06/gimp-2-99-2-released/#multi-layer-selection)
which GIMP is now capable of, we greatly simplified the tool:

* Target items to align or distribute are now the selected layers and/or paths
  in their respective dockable, as set in the "Targets" section in tool options.

<figure>
<img src="{attach}gimp-2.99.14-alignment-targets.png" alt="GIMP 2.99.14: alignment tool target selection"/>
<figcaption>
<em>Targets in alignment tool options - GIMP 2.99.14</em>
</figcaption>
</figure>

* For layers in particular, a new option "*Use extents of layer contents*" allow
  to align or distribute target layers based on their pixel contents, not the
  layer bounds (which typically might be bigger than the pixel data). This is
  similar to running "*Crop to Content*" before aligning, except that we don't
  actually crop.
* Guides still need to be selected on-canvas if you want to align or distribute
  them. The tool options hints at the modifiers: `Alt` or `Shift-Alt`. Moreover
  the selected guide color will change, giving a visual feedback of selected
  guides.

<figure>
<img src="{attach}gimp-2.99.14-align-guides.gif" alt="GIMP 2.99.14: aligning and distributing guides (screencast)"/>
<figcaption>
<em>Aligning and distributing guides - GIMP 2.99.14</em>
</figcaption>
</figure>

* Simple clicks (no modifiers) in the canvas is now only used to pick the
  reference object for alignment, if "*Picked reference object*" is set in the
  "*Relative to*" dropdown menu. In such case, you can pick as reference any
  layer, path or guide. The 2 other dropdown choices are "*Image*" and
  "*Selection*" in order respectively to use the current image or selection as
  alignment reference.
* Your reference object shows on-canvas handles as visual feedback.
* In the "Targets" section of the tool options, you can also choose your item
  anchor points: left, right, top, bottom and center. Therefore if you align
  2-dimension targets and reference, you may align e.g. the left side of targets
  to the left, middle or right side of your reference. Any combination is
  possible.
* The distribute actions do not use the reference object anymore. Instead they
  use the leftest/rightest or top/bottom object as reference (i.e. that the 2
  extreme position targets don't move). This is consistent with how other
  software, e.g. Inkscape, handle distribution.
* 2 types of distribution actions are proposed:

    - *Distribute anchor points evenly in the horizontal/vertical*: the distance
      between the anchor point of each target stays the same, e.g. the distance
      between the left side of each object.
    - *Distribute horizontally/vertically with even horizontal/vertical gaps*:
      the distance between the right side of one object and the left side of the
      next (in horizontal distribution) statys the same.

<figure>
<img src="{attach}gimp-2.99.14-align-items.gif" alt="GIMP 2.99.14: aligning and distributing layers (screencast)"/>
<figcaption>
<em>Align <a
href="https://www.gimp.org/about/linking.html#wilber-the-gimp-mascot">Wilber</a>
and <a href="https://film.zemarmot.net/">ZeMarmot</a> relatively to Wilber's
center point, then objects tops under Wilber, before distributing them - GIMP
2.99.14</em>
</figcaption>
</figure>

### Text tool: new outline options

The Text tool now gains non-destructive outline and fill options.

This is implemented as a new "Style" setting in the tool options, with 3
choices:

* *Filled*: the original style;
* *Outlined*: you can choose an outline color or pattern, antialiasing, a line
  width and style. The character inside will be see-through.
* *Outlined and filled*: identical to "*Outlined*" except for the fact that
  characters inside will be filled by the text color.

<figure>
<img src="{attach}gimp-2.99.14-text-outline.gif" alt="GIMP 2.99.14: outline feature in text tool options (screencast)"/>
<figcaption>
<em>Outline feature in text tool options - GIMP 2.99.14</em>
</figcaption>
</figure>

### Transform tools activated automatically

All transform tools (Unified Transform, Rotate, Scale…) needed an explicit click
on canvas before their handle showed up on the canvas when activated with the
tool box or shortcut, which was not consistent with their activation through the
Tools menu, and with how some other tools worked.

As this change was requested, we decided to experiment with directly activating
the handles as soon as the tool is selected.

## Usability and User Interface
### Floating selection concept reviewed

The "Floating selection" concept has been a huge topic across the years,
especially because it was quite unexpected by many people.

After discussing the matters, we came to the conclusion that we should
experiment limiting its usage.

Nevertheless we are also deeply aware that this feature can be a huge time saver
and a much better interface for some types of interaction. In particular, the
quick on-canvas copy|cut-paste with the `Alt` modifier (`Ctrl-Alt` to cut-paste
or `Shift-Alt` to copy-paste) heavily relies on the floating selection to
extremely quickly move bits of a layer.
Obviously the explicit "*Float*" action (equivalent to a cut-paste) is in a
similar situation.

For pasting inside a layer mask, it is even mandatory because it allows to edit
the pasted data — e.g. positioning appropriately, transforming it… — before
actually merging into the mask which may already contain mask data. Note that if
some day, layers were allowed to contain several masks, this would not be
necessary anymore.

For this reason, the 3 cases where we still have floating items are:

* when pasting into a layer mask;
* when doing quick copy|cut-paste on canvas with the `Alt` modifiers;
* when floating layers explicitly with the "*Float*" action.

There is still a case which we need to discuss as it also creates floating
selections: transform tools when there is a selection.
For other common types of data pasting, they will now create new layers
directly.

As a side change, when the "floating selection" happens on a layer mask, we now
call it "floating mask" and shows it above the mask in the Layers dockable (it
used to be above the layer at all times). This should make this specific case
less confusing.

### Copy-paste re-specified

In the light of multi-layer selection, we have been wondering how the various
types copy-paste cases should work. In particular when copying several layers,
should you paste several layers or a merged copy? And when copying pieces
(through a selection) of several layers?

This is still a
[work-in-progress](https://developer.gimp.org/core/specifications/copy-paste/)
but we are trying to properly specify consistent and reasonable behavior for the
many sub-cases. In particular now, we always paste as many layers as was copied,
even when we copied from a selection (in which case, the new layers will be the
size of the selection bounding box).

For the merging case, we add 2 new actions called "*Paste as Single Layer*" and
"*Paste as Single Layer in Place*" in the `Edit > Paste as` submenu. As the
names imply, they paste the merged down version of your copied contents. It's a
bit similar to "Copy Visible", except that it only applies to the selected
layers and can be chosen at paste time.

### New "Gray" theme

GIMP now comes with a "Gray" theme based on a 18.42% luminance middle-gray
background, which should be a good neutral environment for professionnal color
work.

<figure>
<img src="{attach}gimp-2.99.14-gray-theme.jpg" alt="GIMP 2.99.14: gray theme"/>
<figcaption>
<em>Focusing on your artwork color with a middle-gray 18.42% luminance theme - GIMP 2.99.14</em>
</figcaption>
</figure>

### Theme override icon size settings

We now provide a theme-override icon size selection in `Preferences > Themes`
with conceptual sizes: small, medium, large and huge. The following widgets are
so far modified: toolbox icons, fg/bg editor in toolbox, foreground/background
editor in Colors dockable, dockables tab icons, bottom buttons (in the button
box) of dockables, header eye and lock icons above item trees, and eye and lock
icon switches in item tree cells.

<figure>
<img src="{attach}gimp-2.99.14-icon-size-override.gif" alt="GIMP 2.99.14: override theme icon sizes"/>
<figcaption>
<em>Overriding theme-set icon sizes - GIMP 2.99.14</em>
</figcaption>
</figure>

You may recall that we have a similar setting in GIMP 2.10 stable branch, which
was initially removed for GIMP 3.0, because our updated toolkit has high-density
display awareness and will already resize all the interface following your
"scale factor" settings (as set in your system).
Nevertheless we realized it was not enough. First of all, because this single
settings cannot take all special cases into consideration and some people still
wanted even bigger icons because they were watching their display from far away,
or simply prefered small icons, or any other reason.

This is the rationale for adding this override of icon size, thus bypassing the
system settings. As a nice aside, it will work with any theme. So you don't have
to discard a theme you appreciate just because the chosen icons are not the size
you want.

## Core changes
### Much faster XCF save

Saving with RLE (default) and zlib (the "*better but slower compression*"
checkbox in the Save dialog) is now multi-threaded (following the settings in
Preferences), which makes it a lot faster.

In the best case scenario, we saw up to 70% save time (e.g. a 276-layer, 115MiB,
was reliably saved in about 50 seconds before, and 15 seconds after the change,
on the same test machine), though other tests would be around 1/3 save time
(another 180MiB XCF was saving in 1m30s before and 1min after the change on a
same machine). On any case, it's a great news for people working on big images,
who hopefully won't have to wait so long. Or even small images anyway!

This work was initially contributed by suzu_11 and further improved upon.

### Vectors (paths) structure in XCF

A further change in the XCF format, which warranted bumping the format version,
was that paths now have a proper structure in the XCF specification instead of
just being "properties" on images.

What it means especially is that the XCF format will now store locks and color
tags on paths, but also the path selection (whether several paths were selected
in their dockable). It will also make path items easier to evolve in the future
as we add new features, instead of being stuck on some old, outdated and
non-evolvable format.

As an aside, the XCF format specification had been stored inside the source
repository ever since 1997 (2006 in its detailed version). We moved the file to
the new developer website:
[ Documentation of the XCF file format ](https://testing.developer.gimp.org/core/standards/xcf/).
It should make it easier to read, with markdown formatting and generated table
of contents.

### Moving to `GApplication` and `GtkApplication`

This is a technical information which possibly only developers would understand:
the main process is now run as a `GimpApp` which is a new class derived from
`GtkApplication`. The main process of `gimp-console` on the other hand is a
`GimpConsoleApp` which is derived from `GApplication`. Both new classes share a
same `GimpCoreApp` interface.

This is a main step for the GTK+3 port as it should allow us to work with
`GMenu` next.

## File format support
### PDF

Among other improvements, the PDF export now provides an option "*Root layers
only*" available when "*Layers as pages*" is checked.

<figure>
<img src="{attach}gimp-2.99.14-pdf-root-layers.png" alt="GIMP 2.99.14: root layers only option in PDF export"/>
<figcaption>
<em>Root layers only option in PDF export - GIMP 2.99.14</em>
</figcaption>
</figure>

This option considers root layers only as PDF pages, and not their children,
which means you can more cleanly organize your PDF pages into layer groups.

### AVIF

We improved AVIF compatibility with Safari on iOS 16.0. Some AVIF images are
indeed rendered differently in Apple's implementation compared to implementations
of Google and Mozilla (See [upstream report](https://github.com/AOMediaCodec/av1-avif/issues/195)).

This changes requires libheif 1.10.0 though the plug-in can still build with
older libheif.

### PSD

Two important changes were implemented:

* export of CMYK(A) files added, with 8 or 16-bit precision per channel, using a
  CMYK soft-proof profile for conversion.
* Paths are now exported with PSD files.

<figure>
<img src="{attach}gimp-2.99.14-psd-cmyk.png" alt="GIMP 2.99.14: CMYK PSD export"/>
<figcaption>
<em>Exporting PSD images as CMYK using the soft-proof profile - GIMP 2.99.14</em>
</figcaption>
</figure>

As a reminder, proper [CMYK PSD
import](https://www.gimp.org/news/2022/08/27/gimp-2-99-12-released/#export-formats-with-new-or-improved-cmyk-support)
was improved in GIMP 2.99.12, storing the CMYK profile from the PSD as
soft-proof profile, making round-trips easier (passing through a RGB conversion
in GIMP).

### JPEG-XL

Metadata import and export are now supported.

### ICNS

GIMP now has initial support for loading and exporting <abbr title="Apple Icon
Image">ICNS</abbr> files, the icon format by Apple.

It will also warn you when one of your layer is not a valid icon size for the
ICNS format.

<figure>
<img src="{attach}gimp-2.99.14-icns-support.png" alt="GIMP 2.99.14: ICNS support"/>
<figcaption>
<em>ICNS support - GIMP 2.99.14</em>
</figcaption>
</figure>

### TIFF

The TIFF format has a concept of "reduced page". Until now, we were assuming
pages tagged as "reduced" to be thumbnails. Yet this is not always the case. For
instance we had feedbacks from makers of medical devices which were using
"reduced pages" as sub-sampled images generated by said devices. They needed
GIMP to be able to load all the pages as layers (the main images and the
sub-sampled ones).

<figure>
<img src="{attach}gimp-2.99.14-import-tiff-reduced.gif" alt="GIMP 2.99.14: importing reduced pages of TIFF files"/>
<figcaption>
<em>Importing reduced pages of TIFF files - GIMP 2.99.14</em>
</figcaption>
</figure>

This is why we added a new option called "*Show reduced images*" which lets you
decide whether you want to load these or not. The option will be checked by
default through a small heuristic: if there is only 1 reduced page and it's in
the second position, then it's probably a thumbnail (as per common usage across
software); in which case we disable the checkbox by default. Still in the end,
the choice is yours!

## API

Several API improvements are present in this release:

* We have a better `GimpPickButton` implementation for Windows, which should
  work better than the existing implementation, thanks to Luca Bacci.
* Text layers now have a dedicated class `GimpTextLayer`.
* Various functions were added to get lists of selected items (as per 2.99
  ability to select multiple items).
* `gimp_vectors_stroke_translate()` now uses `double` offsets (instead of
  integer).
* There is a new function `gimp_text_layer_set_markup()`, contributed by Ian
  Munsie, which allows to set [Pango
  markup](https://docs.gtk.org/Pango/pango_markup.html) directly in text layers.
  It is a powerful function because it allows to render texts even with features
  not supported in the text tool <abbr title="Graphical User
  Interface">GUI</abbr>.

    For instance, this is a text layer with a double underline and an overline
    on "*Hello*", "*under*" in subscript, and "*2*" in superscript all of which
    are supported features in Pango, but not in our text tool GUI, as set
    through the Python binding of our API:

    ```
    txt_layer.set_markup('<span underline="double" overline="single">Hello</span> <sub>under</sub>world<sup>2</sup>')
    ```

    As a note of interest, any styling unsupported by the GUI will be discarded
    if you edit the text layer through the GUI.

<figure>
<img src="{attach}gimp-2.99.14-text-layer-set-markup.png" alt="GIMP 2.99.14: text layer styled with gimp_text_layer_set_markup()"/>
<figcaption>
<em>Text layer styled with <code>gimp_text_layer_set_markup()</code> - GIMP 2.99.14</em>
</figcaption>
</figure>

Though this release is not the most packed with API changes, a lot of background
work is in-progress and in particular Lloyd Konneker is very actively
participating to the work. We should hopefully see the result in the next
development release.

## macOS

Though the macOS build, still has some issues, major advances happened on macOS
side, thanks to Lukas Oberhuber, once again.

### Pointer click bug with macOS Ventura

Let's start with the one bad news (before going to the good ones): there seems
to be a major hover and click bug in GTK on macOS Ventura, the last version of
macOS released a few weeks ago. It basically makes all GTK+3 application
unusable, including GIMP. Every new release of this operating system seems to
bring its load of (bad) surprises! 😓

As of now, no solutions exist yet as GTK developers are still looking for the
cause. In any case, you may want to hold onto updating your OS if some GTK+3
applications (e.g. GIMP, Inkscape, Siril…) are a major part of your workflow.

### GIMP has an Apple Silicon package!

The biggest news is that we now have a DMG package for Apple Silicon machines
(M1, M2…)! 🥳

Be careful, you should take this as an **experimental** 🥼 package of an
**experimental** 🧪 GIMP version. So issues are expected. As usual, we welcome
any [issue
report](https://gitlab.gnome.org/GNOME/gimp/-/issues) you would get with GIMP
(on macOS or any other platform by the way).

### MacPorts-based builds

The second big change on macOS land, less visible yet quite major as an
infrastructure change: Lukas ported the build to `MacPorts` away from the
historical `JHBuild` scripts.

The main reason was that we could take advantage of the bigger "port"
maintaining community for our dependencies, instead of building everything
ourselves. This can be compared to using `MSYS2` on Windows or the runtime
system of flatpak. This improves the build time, but also the maintenance load
as Lukas is still alone to maintain all this.

The drawbacks are that it makes it a bit harder to tweak dependencies when
needed (as usual when you rely on an upstream), but also that the DMG packages
are now bigger in file size. This is unfortunate, but considering that the
alternative might be to wear our macOS maintainer out and have no package at
all, we consider it to be worth it.

### HTTPS for the update check

The "update check" — i.e. the ability to verify if new versions of GIMP were
released, i.e. that you are running outdated GIMP — was never working on macOS
because of the lack of HTTPS support for macOS in `GIO`, our backend library to
handle all input/output transparently.

Lukas Oberhuber implemented a work-around for this, based on a macOS system API
(no additional dependency), which we may backport later to `GIO`. Maybe macOS
will eventually have the ability to open HTTPS remote images at some point!

## Build and documentation

### meson (message to packagers)

As told when [releasing GIMP
2.99.12](https://www.gimp.org/news/2022/08/27/gimp-2-99-12-released/#meson-message-to-packagers),
we entered an intensive testing phase for our meson build. We received useful
reports and feedbacks, which allowed us to get the meson build even closer to
finalization.

This release might be the last one when we will provide both an autotools and
meson tarball for packagers. If all goes well, we may decide to phase out our
autotools build after GIMP 2.99.14, and only provide a meson tarball.

So if any packager finds any issue, please, now is the time to tell us about it!

### API reference tarball

Our developer website now provides online [`libgimp` and `libgimpui` 2.99 API
reference](https://developer.gimp.org/api/3.0/), generated by `gi-docgen`.

This is the experimental version of what should become the `libgimp` 3.0 API. Of
course, it's still a moving target, so you should not expect it to be stable
yet, up until we officially release GIMP 3.0. Still, plug-in developers are
welcome to experiment already in order to prepare their plug-in for the new
major version (several well known plug-ins already do have versions usable with
our 2.99 experimental API).

An API reference tarball is generated as part of the continuous integration
process and we now distribute them on [our download
server](https://download.gimp.org/pub/gimp/v2.99/api-docs/) for anyone who
prefers to read the documentation offline.

## babl and GEGL

As usual, this release is supplemented with the releases of
[babl](https://gegl.org/babl/) 0.1.98 and [GEGL](https://gegl.org/) 0.4.40.

Some race conditions are now avoided in the LUT cache [introduced in GIMP
2.99.10](https://www.gimp.org/news/2022/02/25/gimp-2-99-10-released/#automatic-lut-creation-for-conversions-in-babl).

ICC tags handling was improved as well in babl and the `newsprint` GEGL
operation was improved so that it does not drop the alpha channel in RGB modes.

## Team news

There is no specific team news, except that we are getting a solid core team,
with the usual people steadily contributing. 🧑‍💻

Our [GSoC student](https://gimp.org/news/2022/06/03/cmyk-in-gsoc-2022/), Nikc,
stayed around and is clearly getting used to our codebase as they contribute
more and more, which is pleasing to see!

## Mirror news

4 organizations contributed [download
mirrors](https://www.gimp.org/donating/sponsors.html#official-mirrors) to
distribute GIMP.

Thanks to *metanet.ch* (Zürich, Switzerland), the *Fremont Cabal Internet
Exchange* (7 mirrors across the United States and Canada!), the *LIP6,
Sorbonne université* (Paris, France) and *EdgeUno* (Bogotá, Colombia; our
first mirror in South America, at least since the renewed mirror procedure!).

We remind that mirrors are important as they help the project by sharing the
load for dozens of thousands of daily downloads. Moreover by having mirrors
spread across the globe, we ensure that everyone can have fast download access
to GIMP.

## Book news

One more self-published third-party book about GIMP, in English, was added to
the [books page](https://www.gimp.org/books/):

* [*The Ultimate GIMP 2.10 Guide: Learn Professional photo editing*, by Bernard 't Hooft](https://www.gimp.org/books/#gimp-210-das-umfassende-handbuch)
  (2018, in English)

We remind that we welcome book additions. Whether you wrote it or just
read it, if you know of a book about GIMP, just [report the same
information as other books in the list](https://gitlab.gnome.org/Infrastructure/gimp-web/-/issues/new).
Thanks!

## Release stats

31 people contributed to the [main
repository](https://gitlab.gnome.org/GNOME/gimp/) for GIMP 2.99.14:

* 16 developers contributed to `GIMP` code base for this micro version:
    - 1 developer with more than 100 commits: Jehan.
    - 3 developers with 10 to 20 commits: Jacob Boerema, Nikc and Daniel
      Novomeský.
    - 12 developers with less than 10 commits: Lukas Oberhuber, Hanabishi, Luca
      Bacci, Øyvind Kolås, Gotam Gorabh, Ian Munsie, Michael Schumacher, Niels
      De Graef, suzu_11, Hanabishi, Niels De Graef and lloyd konneker.
* 15 translations were updated: Basque, Catalan, Chinese (China), Galician,
  Georgian, German, Hungarian, Icelandic, Polish, Portuguese, Russian,
  Slovenian, Spanish, Swedish, Ukrainian.
* 17 translators contributed: Hugo Carvalho, Yuri Chornoivan, Martin, Zurab
  Kargareteli, Sveinn í Felli, Alexandre Prokoudine, Anders Jonsson, Balázs Úr,
  Jordi Mas, Boyuan Yang, Luming Zh, Rodrigo Lledó, Asier Sarasua Garmendia,
  Fran Dieguez, Piotr Drąg, Balázs Meskó, Tim Sabsch.
* 1 person contributed to icons and themes: Jehan.
* 10 people contributed build-related updates: Jehan, Alx Sa, Hanabishi, Øyvind
  Kolås, Daniel Novomeský, Ian Munsie, Luca Bacci, Lukas Oberhuber, Niels De
  Graef, Thomas Klausner.

These are the stats on [babl](https://gitlab.gnome.org/GNOME/babl/),
[GEGL](https://gitlab.gnome.org/GNOME/gegl/) and
[ctx](https://ctx.graphics/files.html) repositories:

* 1 contributors to `babl` 0.1.98 with 5 commits: Øyvind Kolås
* 12 contributors to `GEGL` 0.4.40:
    - 4 code contributors: Øyvind Kolås, Jehan, Sam James, nikita.
    - 8 translators: Marco Ciampa, Asier Sarasua Garmendia, Enrico Nicoletto,
      Fran Dieguez, Jordi Mas, Luming Zh, Matheus Barbosa, Sabri Ünal.
* `ctx` doesn't have releases per-se as it is project-embedded code.
  In the time frame between GIMP 2.99.12 and 2.99.14, there were 247 commits by
  1 contributor: Øyvind Kolås.

On the [documentation
repository](https://gitlab.gnome.org/GNOME/gimp-help/), in the GIMP 2.99.12 to
2.99.14 timeframe, 5 people contributed:

* Main contributor on documentation and script with 57 commits: Jacob Boerema.
* 1 additional contributor on documentation: Tim Sabsch.
* 4 translators: Tim Sabsch, Andre Klapper, Hugo Carvalho, Rodrigo Lledó.

On the [main website
repository](https://gitlab.gnome.org/Infrastructure/gimp-web/), in the GIMP
2.99.12 to 2.99.14 timeframe, 1 contributor contributed 89 commits: Jehan.

On the [macOS build
repository](https://gitlab.gnome.org/Infrastructure/gimp-macos-build), in the
GIMP 2.99.12 to 2.99.14 timeframe, 1 contributor contributed 43 commits: Lukas
Oberhuber.

On the [developer website
repository](https://gitlab.gnome.org/Infrastructure/gimp-web-devel/), since the
[relevant
news](https://www.gimp.org/news/2022/10/16/gimp-developer-website-rewrite/), 1
contributor contributed 8 commits: Jehan.

*Note: considering the number of parts in GIMP and around, and how we
get statistics through `git` scripting and manual tweaking, errors may
slip inside these stats. Feel free to tell us if we missed or
mis-categorized some contributors or contributions because we try to
acknowledge every contributor for being a part of GIMP!*

## Downloading GIMP 2.99.14

As usual, GIMP 2.99.14 is available on [GIMP official website
(gimp.org)](https://www.gimp.org/downloads/devel/), now in 4 package formats, as
we got the new macOS on Apple Silicon package:

* Linux development flatpak
* Windows installer
* macOS DMG package for Intel
* macOS DMG package for Apple Silicon

Other packages made by third-party are obviously expected to follow
(Linux distributions, \*BSD distributions' packages, etc.).

## What's next

With this release, we are really approaching GIMP 3.0 release, as can be seen
from the [roadmap of 3.0](https://developer.gimp.org/core/roadmap/#gimp-30)
where most items are "nearly done" or "done". We are clearly reaching this part
of development when we start targetting specific pain points, which means a lot.

Nice milestones in this release:

* we now have all themes we absolutely needed (neutral dark, light and
  middle-gray themes);
* macOS builds are getting on-par with other builds;
* usability is being finalized, after the multi-item selection really changed
  the whole paradigm of how GIMP interacts with layers changed;
* the space invasion project is currently running strong. Even though this
  release doesn't show as much consequences of it yet as we hoped, the next
  release should;
* developer documentation, for onboarding of new contributors, is finally
  getting somewhere.

Don't forget you can [donate and personally fund GIMP
developers](https://www.gimp.org/donating/), as a way to
give back and **accelerate the development of GIMP**.
Community commitment helps the project to grow stronger! 💪🥳
