Title: Revival of the GIMP developer website
Date: 2022-10-16
Category: News
Authors: Wilber
Slug: gimp-developer-website-rewrite
Summary: Revival of the GIMP developer website
Image: gimp-developer-website-revival-2022.png

This weekend, we published a new [developer 🧑‍💻 website](https://developer.gimp.org/)! 🥳

<figure>
<a href="https://developer.gimp.org/">
<img src="{attach}gimp-developer-website-revival-2022.png" alt="Screenshot of the new developer website of GIMP"/>
</a>
<figcaption>
<em>Screenshot of the new developer website of GIMP</em>
</figcaption>
</figure>

[TOC]

As the name indicates, it is targetted at developers, both core contributors
(people making GIMP itself) and third-party developers (people
making plug-ins and publishing them on the side). This is why the website
has 2 main sections:

* [Core](https://developer.gimp.org/core/): for core developers, with roadmaps,
  information on how to build GIMP, coding style guidelines…
* [Resources](https://developer.gimp.org/resource/): for third-party developers,
  with links to the public `libgimp` <abbr title="Application Programming
  Interface">API</abbr>, tutorials to make plug-ins…<br/>
  We call this section "*Resources*" as in the future, it may also contain info
  on how to make brushes or other data for GIMP.

## A bit of history

GIMP has had a [developer website](https://developer.gimp.org/) for at least 2
decades (the Internet Archive traces back an early page in 2001), yet mostly
unmaintained ever since 2009, which is a shame.

Since then, documentation for developers was scattered on the [general
website](https://www.gimp.org/), the source repository itself and 2 wikis
(developer and GUI wiki). As you may know, the [developer wiki encountered
problems](https://www.gimp.org/news/2022/08/27/gimp-2-99-12-released/#development-website)
recently. As for the GUI wiki, it is still there, though we plan to merge both
wikis into our new developer website.

Rather than having duplicate documents all over the place, we want to
consolidate developer documentation into a single point of entry.

## Work in progress

This new website is still a work-in-progress. Contents is still incomplete and
often outdated. We decided to publish it in its current state and update it as
we go, rather than wait forever.

As usual, we stick to only serving static pages, no server-side scripting. It's
simpler and safer as we don't want to spend our time administering a webpage (we
develop GIMP, not webpages).

What has been done so far:

* We ported the website from DockBook to [Hugo](https://gohugo.io/), especially
  thanks to Robin Swift, with help from Pat David. It has a few advantages:
    - The markdown syntax is less powerful yet so far sufficient and simpler
      that DockBook XML. It should facilitate
      [contributions](https://gitlab.gnome.org/Infrastructure/gimp-web-devel/).
    - Hugo websites are easier to build and test, with nice immediate feedback
      loop when using the hugo webserver during development.
    - Hugo has a nice organization where file structure also decides of the
      website structure.
* Contents was reorganized, reviewed, partially rewritten or merged. Some
  outdated documents were kept for historical interest, yet may not be as
  relevant for modern GIMP usage.<br/>
  We identified 2 main sections — as explained above: *Core* and *Resources* —
  with an additional *Conferences* section where we mostly keep track of
  historical meetings where developers made some decisions on the future of
  GIMP. We try to organize documents in these sections and subsections when
  relevant.
* We progressively improve automatization of the website publishing, for
  instance automatic grab of the newest `libgimp` library documentation, and
  early error detection.
* We created a testing website to validate changes before publishing to the main
  website (similar as what we do for `gimp.org`). Both websites will be
  automatically published daily from their own branch and can be triggered
  manually through Continuous Integration jobs on *Gitlab*.
* All documents from the old developer website were salvaged, ported to
  markdown, reorganized or discarded after examination.
* Several development-related documents from the main `gimp.org` website were
  moved to `developer.gimp.org` or merged into others. For instance, we had
  redundant pages about contributing code for GIMP. There is now (unless we
  missed some) only a single tutorial:
  [Submit your first patch](https://developer.gimp.org/core/submit-patch/).
* Dozens of the old wiki documents were ported to markdown and moved or
  discarded after examination. In particular, some of the most requested pages
  have a home again:
    - [GIMP roadmaps](https://developer.gimp.org/core/roadmap/)
    - [GIMP's compilation tutorials](https://developer.gimp.org/core/setup/)
* Some documents from the main source repository of GIMP were moved. An
  important document is the [GIMP Coding Style
  guide](https://developer.gimp.org/core/coding_style/).
* Redirections were created, because "[*Cool URIs don't
  change*](https://www.w3.org/Provider/Style/URI)" and links to moved pages can
  be found in many third-party websites. For instance old links on the roadmap
  (which used to be at
  [wiki.gimp.org/wiki/Roadmap](https://wiki.gimp.org/wiki/Roadmap)) now
  redirect to the new roadmap.

## What's next

More documents need to move, be rewritten or fixed. This is only a start.

Also the website style is currently pretty simple and bare. On one hand, maybe
it's not too bad for a development website. On the other hand, discussions have
happened proposing to make the website look a little more lively and less
austere 🧐. We'll see!

## Why this website?
### Improving onboarding

By having a single point of entry, we hope it will feel less heavy for newcomers
to understand where to go for building GIMP and contributing patches.

Now if anyone asks, tell them to look at [the Core section of GIMP's developer
website](https://developer.gimp.org/core/).

### Simplifying plug-in development

We strive for a lively third-party plug-in ecosystem. For this to happen, we
want to help third-party developers. There are a lot of documentation and
tutorials about plug-in developement, but they are spread across the web, many
links are dead, a good part of the documents are unmaintained and therefore
partly outdated.

This new website doesn't bring much **yet** on this side, though by making
plug-in development one of the 2 main sections, we clearly intend to change this
fact for the upcoming GIMP 3.0. You should not expect new tutorials for GIMP
2.10 plug-in development, but this is definitely where you should keep an eye on
if you are interested by plug-in creation for GIMP 3.0: [Resources section of
GIMP's developer website](https://developer.gimp.org/resource/)

## Statistics

The main work happened on the [`gimp-web-devel`
repository](https://gitlab.gnome.org/Infrastructure/gimp-web-devel/).

Since mid-July, when we started the website renewal, the following contributors
participated:

* Jehan: 86 commits
* Pat David: 38 commits
* Robin Swift: 15 commits
* Lukas Oberhuber: 1 commit

Some more work, unlisted here, happened on [`gimp-web` (main website)
repository](https://gitlab.gnome.org/Infrastructure/gimp-web/).

## Contributing to and Funding GIMP

As usual, we remind that GIMP is Community, Free and Libre Software. It is what
we all make of it, together. This is a main reason of such a developer website.
Hopefully you will find it useful. If not, don't remember that the website
itself is community-made. So feel free to [propose developer tutorials and fixes
or updates to existing ones](https://gitlab.gnome.org/Infrastructure/gimp-web-devel/).

<p class='buttons'>
  <a href="https://developer.gimp.org/core/">
    <span class='donate-button'>
    Help with<br/> <strong>GIMP development</strong>
    </span>
  </a>
</p>

Last but not least, if GIMP is useful to you and **you wish to fund
development**, you are very welcome to [**donate** to the project and fund core
GIMP developers](https://www.gimp.org/donating/), as a way to give back
and accelerate the development of GIMP.

<p class='buttons'>
  <a href="https://www.gimp.org/donating/">
    <span class='donate-button'>
    Support us by<br/> <strong>Donating</strong>
    </span>
  </a>
</p>
