Title: Conference "GIMP and ZeMarmot" in Vandœuvre-lès-Nancy (France)
Date: 2022-10-28
Category: News
Authors: Jehan
Slug: conference-zemarmot-fccl
Summary: Talk about GIMP and the sister project ZeMarmot in Vandœuvre-lès-Nancy
Image: affiche_conference_gimp_zemarmot_2022-11-04-724x1024.jpg

Next **Friday, the 4th of November 2022, from 6PM to 8PM CET**, Aryeom (with her
hats of film director of "[ZeMarmot](https://film.zemarmot.net/)" and GIMP
contributor) and myself (Jehan, with my hats of main developer/maintainer of
GIMP and technical operations in "ZeMarmot"), will host a conference at the
*Jules Verne* library in **Vandœuvre-lès-Nancy (France)**.

<figure>
<a href="https://fccl-vandoeuvre.fr/conference-gimp-et-zemarmot/">
<img src="{attach}affiche_conference_gimp_zemarmot_2022-11-04-724x1024.jpg" alt="Poster for the talk 'GIMP and ZeMarmot' of 4 November 2022 in Vandœuvre-lès-Nancy"/>
</a>
<figcaption>
<em>Poster for the talk "GIMP and ZeMarmot" of 4 November 2022 in Vandœuvre-lès-Nancy</em>
</figcaption>
</figure>

Location
: Médiathèque Jules Verne  
  2 rue de Malines  
  54500 Vandoeuvre-lès-Nancy  
  France  
  +33 (0)3 83 54 85 53

Time
: Friday, November 4, 2022 - from 6PM to 8PM (CET, French time)

* [Event page on FCCL website](https://fccl-vandoeuvre.fr/conference-gimp-et-zemarmot/)
* [Online map for directions](https://www.qwant.com/maps/place/osm:way:51030446)
* [Live streaming link](https://fccl-vandoeuvre.fr/film)

The event is organized by a public body whose name could be translated as
something like "the [Collective Factory of the Libre
Culture](https://fccl-vandoeuvre.fr/)" (<abbr title="Fabrique Collective de la
Culture du libre ">FCCL</abbr>), which is a cool name, right? It's rare enough
to have public institutions funded by a city (in this case: Vandœuvre-lès-Nancy)
encouraging Free Software usage, and this is why we accepted their invitation.
As far as we know, ever since the global health crisis, it will be the first
physical conference with contributors of GIMP.
Quite strange to prepare talks again after 3 years! 🥲

We will obviously talk about GIMP and how it is developed as a community,
since the concept of "Community, Free Software" is dear to me. We will also talk
about "[ZeMarmot](https://film.zemarmot.net/)", the Free/Libre Animation Film
produced by the non-profit [LILA](https://libreart.info/) which
[crowd-funds](https://film.zemarmot.net/en/donate) our work (for the movie and GIMP
development). So it will be the opportunity to discuss about various interesting
topics, based on our concrete experience of running FLOSS and Libre Art
projects.

We may also present some other Free/Libre movies we produced, such as the ones
made on account of [Framasoft](https://framasoft.org/): "[What is
Peertube?](https://framatube.org/w/kkGMgK9ZtnKfYAgnEtQxbv)" and "[What is the
Fediverse?](https://framatube.org/w/9dRFC6Ya11NCVeYKn8ZhiD)").

So if you are in France around Vandœuvre-lès-Nancy on November 4, we hope we'll
see you there. 🤗 Otherwise, I am told that the talk will be [streamed
live](https://fccl-vandoeuvre.fr/film) 🖥️ (in French of course).
