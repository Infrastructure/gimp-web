Title: GIMP is a GSoC 2022 mentor organization
Date: 2022-03-08
Category: News
Authors: Wilber
Slug: gimp-in-gsoc-2022
Summary: GIMP has been accepted as Google Summer of Code 2022 mentor organization
Image: /news/2020/10/07/gimp-2-10-22-released/202010-wilber-and-co.jpg

Up till 2013, GIMP was a regular at the Summer of Code. Ever since then
we haven't applied.
Nine years have passed, so we decided to give it a new try and 2 days
ago, we received an email:
GNU Image Manipulation program is officially a [Google Summer
of Code](https://summerofcode.withgoogle.com/) 2022 [mentor
organization](https://summerofcode.withgoogle.com/programs/2022/organizations/gnu-image-manipulation-program)!

If anyone is interested, it could be a good opportunity to jump into the
development of a huge desktop program used by millions of people. Here
are some ideas of what you could possibly work on: [wiki with list of
possible project
ideas](https://wiki.gimp.org/wiki/Hacking:GSoC/Future/Ideas).

On our side, we are interested in realistic projects which can really be
finished or at least broken down in usable parts. Our list of ideas is
mostly informative and we very much welcome people coming with their own
ideas. If you want to participate, come [discuss with us on
IRC](https://www.gimp.org/discuss.html#irc-matrix).

## Requirements

GIMP is a huge codebase with high quality level requirements before
acceptance of any code. It will require any participant to be thorough,
listen well to our reviews and have some perseverance.

To increase acceptance chances, you could pick some bugs to fix or some
easy features to implement (look in our [newcomers
list](https://gitlab.gnome.org/GNOME/gimp/-/issues?scope=all&state=opened&label_name[]=4.%20Newcomers)
or simply in the [full
list](https://gitlab.gnome.org/GNOME/gimp/-/issues) of reports; possibly
if you encounter an issue yourself, you could just come and fix that!),
make a merge request with us and see how it goes. This would be the best
kind of first contact before submitting your application.

Last but not least: we welcome contributors from all walks of life, 
regardless of origin, skin color, gender etc. All we ask is that you remain 
courteous and respectful to your fellow developers while working with us, 
and we will of course return the courtesy. If you're looking for a
welcoming environment, our project may be the perfect fit for you! 🤗

See you on the other side?!
