Title: GIMP 2.10.32 Released
Date: 2022-06-14
Category: News
Authors: Wilber
Slug: gimp-2-10-32-released
Summary: Release news for version GIMP 2.10.32

GIMP continues strengthening its bases as this new version 2.10.32 is
quite heavy on bug fixes and improves our support for many image file
formats.

[TOC]

*This news lists the most notable and visible changes. In particular, we
do not list here the bug fixes.
To get a more complete list of changes, you should refer to the
[NEWS](https://gitlab.gnome.org/GNOME/gimp/-/blob/2ea287125f6af3cf9d35ea607071e648dcf4465e/NEWS#L11)
file or look at the [commit
history](https://gitlab.gnome.org/GNOME/gimp/-/commits/master).*

## File Formats
### TIFF and BigTIFF

GIMP now supports **8 and 16-bit CMYK(A) TIFF** files on import. This is
one of the first consequences (already!) of the [GSoC
project](https://www.gimp.org/news/2022/06/03/cmyk-in-gsoc-2022/)
by NikcDC under mentorship of Jehan.

Moreover the **BigTIFF format is now supported**, both for import and
export. BigTIFF is an evolution of the original TIFF format allowing
files bigger than 4GiB by using 64-bit internal offsets.

The TIFF export dialog exposes a checkbox to enable this format variant.

<figure>
<img src="{attach}gimp-2-10-32-bigtiff-export.png" alt="BigTIFF export GIMP 2.10.32"/>
<figcaption>
<em>BigTIFF export in GIMP 2.10.32: new option in the TIFF export dialog</em>
</figcaption>
</figure>

Moreover, if you try to export as a TIFF file and if it fails because of
the maximum file size being exceeded, it will propose you to retry
either as BigTIFF or with another compression algorithm.

Finally additional special cases in TIFF format are now taken into
account, making loading TIFF files more robust.

### JPEG XL

JPEG XL file format has been supported in development code ever since
[GIMP
2.99.8](https://www.gimp.org/news/2021/10/20/gimp-2-99-8-released/#improved-file-formats-support-jpeg-xl-psdpsb-and-more).

Daniel Novomeský **backported the import** code to the stable branch.

We remind that a [third-party
plug-in](https://github.com/libjxl/libjxl/tree/main/plugins/gimp)
already exists, by developers of the `libjxl` library. It is a different
codebase and should still work with GIMP 2.10.32. The main advantage of
the third-party plug-in is that it has also export capabilities (which
our stable plug-in doesn't have yet), though with some unsolved issues
causing some images to fail to load or export correctly.

You are welcome to continue using the third-party plug-in if you prefer.
For instance the `MSYS2` project now has it built-in within the `libjxl`
package. If you install it, it will take precedence over our new core
plug-in.

### DDS

Some game engines require DDS images to be flipped vertically. Until
now, asset creators had to flip the image in GIMP, export, then undo,
which was not a very practical workflow.

This is why NikcDC added a **new "*Flip the image vertically on export*"
option** allowing you to flip your image at export time.

Similarly, there is now a **"*Visible layers*" option** (additionally to
"*Selected layer*", "*As cube map*"…) allowing you to export the whole
image render, not a single layer. So depending on your work usage, you
won't have to flatten your image (to later undo this action) anymore
before export.

<figure>
<img src="{attach}gimp-2-10-32-dds-export.png" alt="DDS export GIMP 2.10.32"/>
<figcaption>
<em>DDS export in GIMP 2.10.32: new "flip" and "All visible layers" options</em>
</figcaption>
</figure>

### Metadata handling (PSD…)

Several metadata handling improvements were made. One of the most
important changes is that GIMP will start dropping
`Xmp.photoshop.DocumentAncestors` tags if more than a thousand of them
are found.

This case is very likely due to a bug in some versions of Photoshop or
other software creating `PSD` files as we encountered over 100,000 such
tags in some files (it probably makes no sense that a document could
have that many ancestor documents), which was seriously slowing down
GIMP to the point it looked frozen.

### XCF

XCF import code also received various improvements, making the software
more robust to invalid files. In some cases, it allows to salvage more
data from corrupted XCF files, hence protecting your work.

### Other formats

More format handling were improved:

BMP
: A new `PDB` procedure `file-bmp-save2` was added for plug-in
  developers. It supports all the options available interactively.

DICOM
: Fixed endian conversion for photometric interpretation `MONOCHROME1`.

EPS
: Loading transparent EPS files is now supported.

RAW
: "*RGB Save Type*" confusing dialog label renamed to "*Palette Type*"
  as on the main dev branch.

TGA
: Improved support of indexed images with alpha channel (both import
  and export).

WebP
: Export has a new IPTC checkbox (saved through XMP) as well as a
  thumbnail checkbox. (backported from dev branch, since 2.99.8)

## Text tool: localized glyph variants

The Text tool now supports localized glyphs (`locl`) depending on the
value of the "Language" tool option.

It is useful if you write in languages with regional variant forms (e.g.
Serbian or Ukrainian use the same Cyrillic alphabet yet with 5 letters
having regional variants).

*If your chosen font supports several variants*, you can now
tell the internal formatting system through the "Language" settings
which one needs to be used.

<figure>
<img src="{attach}gimp-2-10-32-locl.png" alt="locl support in GIMP 2.10.32"/>
<figcaption>
<em>'locl' support in GIMP 2.10.32: same characters in (left) Serbian (right) Ukrainian<br/>(top) upright (bottom) italic</em>
</figcaption>
</figure>

## Usability: themes and icons

* All official themes now have on-hover indicator around the eye 👁️ and
  link 🔗 toggles in Layer/Channel/Path Dialog tree-views.
<figure>
<img src="{attach}gimp-2-10-32-eye-link-toggles.gif" alt="On hover effect on eye and link toggles in GIMP 2.10.32"/>
<figcaption>
<em>On-hover effect on eye and link toggles in GIMP 2.10.32</em>
</figcaption>
</figure>
* In the Dark theme, a new on-hover effect on radio menu items was added
  to improve readability.
<figure>
<img src="{attach}gimp-2-10-32-on-hover-menu-radio-item.gif" alt="On hover effect on radio item in GIMP 2.10.32"/>
<figcaption>
<em>On-hover effect on radio menu items in GIMP 2.10.32</em>
</figcaption>
</figure>
* In the Color icon theme, thin contrast borders were added to the
  'close' and 'detach' icons to improve their readability against dark
  backgrounds on mouse-hover.
<figure>
<img src="{attach}gimp-2-10-32-color-theme-contrast-icons.png" alt="More readable icons in GIMP 2.10.32"/>
<figcaption>
<em>More readable icons in GIMP 2.10.32</em>
</figcaption>
</figure>
* In the Color icon theme, horizontal and vertical chain ⛓️ icons are
  more distinguishable between their broken and not-broken variants.
<figure>
<img src="{attach}gimp-2-10-32-color-chain-icons.png" alt="Chain icons in Color icon theme in GIMP 2.10.32"/>
<figcaption>
<em>Left: GIMP 2.10.30 - Right GIMP 2.10.32</em>
</figcaption>
</figure>

All these changes are works by Stanislav Grinkov.

## Improved Screenshot on Windows

The Screenshot plug-in was missing the "*Include mouse pointer*" option
on Windows.

This long missing feature, which was available on other platforms (such
as Linux X11/Wayland, \*BSD or macOS) is now implemented on Windows as
well!

<figure>
<img src="{attach}gimp-2-10-32-screenshot-windows.png" alt="Screenshot plug-in on Windows in GIMP 2.10.32"/>
<figcaption>
<em>Screenshot plug-in on Windows in GIMP 2.10.32: new "Include mouse pointer" options</em>
</figcaption>
</figure>

## GEGL, babl

GIMP 2.10.32 is the first stable release with the nice optimizations
brought since [babl 0.1.90 and GEGL
0.4.36](https://www.gimp.org/news/2022/02/25/gimp-2-99-10-released/#babl-and-gegl),
now that they have been battle-tested and debugged within the
development release GIMP 2.99.10.
As a consequence, `babl` 0.1.92 has been released, fixing in particular
SIMD discovery ability, which was misbehaving on Windows.

These improvements are important enough that we felt their 2.99.10
description deserved to be copied on the stable news to make clear this
applies to GIMP 2.10.32 now.

### Automatic LUT creation for conversions in babl

Citing Øyvind in a recent [report](https://www.patreon.com/posts/babl-lutefisk-62286666):

> babl works as a universal pixel encoding translator by having - a not
> necessarily fast reference conversion able to deal with everything - and
> a collection of conversions that can be chained together into what babl
> calls fishes. The fishes are raced against each other to find the best
> conversion babl is able to create.

In some situations, a [LUT (Look-Up
Table)](https://en.wikipedia.org/wiki/Lookup_table#Lookup_tables_in_image_processing)
will perform better, we now know what the performance will be of using a
LUT for various combinations of input and output bits per pixel; and
mark fishes that are better as LUTs on creation time. If such a fish is
used more than a minimum, LUT population is triggered.

Right now one drawback is that it might actually slow down slightly the
first conversion happening in a given `input > output` conversion couple.
The solution which might come later would be to create the LUTs in the
background in a thread and continue doing non-LUT conversions until it
is ready.

Note that the created LUTs are also garbage-collected after a few
minutes of non-usage, to avoid filling memory with single use LUTs.

### SIMD builds for x86_64 and ARM neon (ctx, babl and GEGL)

All of babl, GEGL and ctx have gotten similar SIMD build and dispatch changes
applied.

Image processing code written to be portable and performant caters well to the
auto-vectorization support for SIMD in modern compilers. The way it is done is
mostly by changes to the build system. For now the code remains the same for
all targets but the approach can be extended with conditional intrinsics.

To see the impact this can have, here is a test of filling a circle with ctx
and its different pixel encoding targets, on a Raspberry Pi with default
compiler flags (without neon support). Note that the cairo reference
comparison is the BGRA8 format (the only other format proposed by Cairo is A8):

<figure>
<img src="https://www.gimp.org/news/2022/02/25/gimp-2-99-10-released/GEGL-0-4-36-arm-without-neon.png" alt="Test results without SIMD"/>
<figcaption>
<em>Filling a 256px radius circle in a 512x512 buffer a number of times without Neon (smaller is better)</em>
</figcaption>
</figure>

And the same test, with the compiler allowed to use Neon instructions:

<figure>
<img src="https://www.gimp.org/news/2022/02/25/gimp-2-99-10-released/GEGL-0-4-36-arm-with-neon.png" alt="Test results with SIMD"/>
<figcaption>
<em>Filling a 256px radius circle in a 512x512 buffer a number of times without Neon (smaller is better)</em>
</figcaption>
</figure>

Note well that both the optimized and non-optimized cases are built-in,
and availability of relevant instructions determined by runtime tests.
This makes these optimizations very portable, despite being based on
recent architecture instructions.

ℹ️ Note that these changes (both the automatic LUT generation and the SIMD
dispatch) will apply to GIMP 2.10 versions later. For now they are
available in 2.99 as a form of beta test. We welcome feedbacks if you
encounter any misbehavior.

### Other improvements

`babl` now also comes with pre-defined CIE Lab u8 and CIE Lab u16 formats.


## Team news

lukaso was already considered co-maintainer of the macOS build
repository of GIMP (`gimp-macos-build`) since they have done most heavy
lifting on GIMP for macOS for many months. This has been made official
by changing their account to `maintainer` on this build repository.

Lloyd Konneker has been given developer git access to the main GIMP
repository, in particular for their nice work so far on script-fu (which
was in a state where dropping script-fu for GIMP 3.0 would have been
considered otherwise).

Kevin Cozens, a long term GIMP contributor and script-fu maintainer got
back recently and was given git access again to the repository. He will
help co-maintain script-fu with Lloyd.

Ondřej Míchal has been given "Reporter" rights on the main GIMP
repository (allowing to triage reports: labelling, closing,
reopening, and moving reports…).

After being given "Reporter" rights initially, NikcDC has eventually
been given developer git access in order to facilitate the work on
feature branches for their [GSoC
project](https://www.gimp.org/news/2022/06/03/cmyk-in-gsoc-2022/).

## Release stats

10 people contributed changes or fixes to GIMP 2.10.32 codebase:

* 3 people contributed several code changes or fixes: Jacob Boerema,
  Jehan and Nikc.
* 1 person contributed several theme and icon improvements: Stanislav
  Grinkov.
* 6 people contributed single commits: Sabri Ünal, Daniel Novomeský,
  Lukas Oberhuber, Rafał Mikrut, smlu and Øyvind Kolås.

About translations of GIMP itself:

* 24 translators contributed: Anders Jonsson, Sveinn í Felli, Alan
  Mortensen, Aleksandr Melman, Yuri Chornoivan, Hugo Carvalho, Luming
  Zh, Rodrigo Lledó, Jordi Mas, Martin, Piotr Drąg, Balázs Úr, Jiri
  Grönroos, Zurab Kargareteli, Boyuan Yang, Marco Ciampa, Matej
  Urbančič, Milo Ivir, Sabri Ünal, Tim Sabsch , Balázs Meskó, Charles
  Monzat, Fran Dieguez, Hannie Dumoleyn.
* 20 translations were updated: Catalan, Chinese (China), Croatian,
  Danish, Dutch, Finnish, French, Georgian, German, Hungarian,
  Icelandic, Italian, Polish, Portuguese, Russian, Slovenian, Spanish,
  Swedish, Turkish, Ukrainian.
* GIMP 2.10.32 includes a new Georgian translation, incrementing the
  number of available languages for the stable version to 83.
* GIMP was already localized in Galician, but not the Windows
  installer… until this version!

Contributions on other repositories in the GIMPverse:

* 1 contributor to `babl` 0.1.92: Øyvind Kolås.
* 5 contributors to `babl` 0.1.90: Øyvind Kolås, Mingye Wang, Jehan,
  Tomasz Golinski and Andrzej Hunt.
* 6 contributors to `GEGL` 0.4.36: Øyvind Kolås, Anders Jonsson, Jehan,
  Alan Mortensen, Caleb Xu and zamfofex.
* `ctx` contributors in the same timeframe: Øyvind Kolås, and Jehan.
* 2 contributors to `gimp-macos-build` (macOS build scripts): Lukas
  Oberhuber and Jehan.
* 4 contributors to `org.gimp.GIMP` (stable flatpak): Hubert Figuière,
  Ondřej Míchal, Jehan and Rebecca Wallander.
* 1 Windows packager: Jernej Simončič.
* Contributors to `gimp-help` (GIMP manual) since [last
  stats](https://www.gimp.org/news/2022/02/25/gimp-2-99-10-released/#release-stats):
    - 2 technical contributors (build scripts, continuous
      integrations…): Jacob Boerema and Jehan.
    - 5 documenters (original English text): Jacob Boerema, Anders
      Jonsson, Andre Klapper, Jehan and Marco Ciampa.
    - 20 translators: Rodrigo Lledó, Jordi Mas, Nathan Follens, Yuri
      Chornoivan, Anders Jonsson, Hugo Carvalho, Marco Ciampa, Tim
      Sabsch, Balázs Úr, dimspingos, Alan Mortensen, Aleksandr Melman,
      Charles Monzat, Claude Paroz, Kjartan Maraas, Luming Zh, Martin,
      Matheus Barbosa, Milo Ivir, Ulf-D. Ehlert.
* 6 contributors to `gimp-web` (website) since last stats: Jehan,
  Alexandre Prokoudine, Anders Jonsson, Tom Gooding, Alberto Garcia
  Briz, Babs Keeley.

Then let's not forget to thank all the people who help us triaging in
Gitlab, report bugs and discuss evolutions with us.
And of course, our community is deeply thankful to the internet warriors
who manage our various [discussion
channels](https://www.gimp.org/discuss.html) or social network accounts
such as Ville Pätsi, Alexandre Prokoudine, Liam Quin, Michael Schumacher
and Sevenix!

*Note: considering the number of parts in GIMP and around, and how we
get statistics through `git` scripting, errors may slip inside these
stats. Feel free to tell us if we missed or mis-categorized some
contributors or contributions.*

## Around GIMP
### Mirror news

Ever since our new [official mirror
procedure](https://www.gimp.org/news/2021/10/06/official-mirror-policy/),
the following mirrors have been added to our rotation scripts:

* Get Hosted Online (Rothwell, United Kingdom)
* Aceldama Systems (Montreal, Canada)
* Open Computing Facility (Berkeley, CA, United States)
* Freedif (Singapore)

One mirror from the previous list had to be removed because of
performance issues, and a mirror acceptance is still on-hold because of
the conflict and unstability in East Europe.

As a fun note, GIMP is now back to Berkeley through the *Open Computing
Facility* (student organization for *all University of California,
Berkeley students, faculty, and staff*). For anyone wondering why it's
fun, you may read with interest the [birth
story](https://www.gimp.org/about/prehistory.html) of the GNU
Image Manipulation Program.

<abbr title="Too Long; Didn't Read">TLDR;</abbr> GIMP was originally
born in the University of California. I guess it's good to be home,
right?

### Book news

Our [books page](https://www.gimp.org/books/) was in a badly maintained
state. 4 books in Spanish were added to the list recently:

* [*Blanco y Negro con GIMP* (2 ed.), by Alberto Garcia Briz
  (2022)](https://testing.gimp.org/books/#blanco-y-negro-con-gimp-2-ed)
* [*Niveles y Curvas con GIMP* (2 ed.), by Alberto Garcia Briz
  (2013)](https://testing.gimp.org/books/#niveles-y-curvas-con-gimp-2-ed)
* [*Domine GIMP. Manual práctico*, by Laura Raya González and José Luis
  Raya Cabrera
  (2013)](https://testing.gimp.org/books/#domine-gimp-manual-practico)
* [*Aprende GIMP*, by Jan Smith and Roman Joost
  (2012)](https://testing.gimp.org/books/#aprende-gimp)

We took the opportunity to restructure the page with per-language
section.

This is also a good time to remind that it is up to everyone to help us
keep this page updated. We know many more books have been published
about GIMP across the years so the list is very incomplete. Therefore if
you wrote or know of a book absent from the page, please feel free to
[report the same information as other books on the
page](https://gitlab.gnome.org/Infrastructure/gimp-web/-/issues/new) so
that we can add them. Thanks!

## Downloading GIMP 2.10.32

As usual, GIMP 2.10.32 is available on [GIMP official website
(gimp.org)](https://www.gimp.org/downloads/) in 3 package formats:

* Linux development flatpak
* Windows installer
* macOS DMG package

Other packages made by third-party are obviously expected to follow
(Linux or \*BSD distributions' packages, etc.).

## What's next

You may notice that stable releases are slowing down ([GIMP
2.10.30](https://www.gimp.org/news/2021/12/21/gimp-2-10-30-released/)
happened 6 months ago) as we are reaching a good stability overall on
this branch, despite being held back by our very old toolkit.
Nowadays most of our focus is on the development branch. Yet we will
continue to provide updates and bug fixes to the 2.10 branch until GIMP
3.0 is out.

Don't forget you can [donate to the project and personally fund several
GIMP developers](https://www.gimp.org/donating/), as a way to give back
and accelerate the development of GIMP. As you know, the [maintainers of
GEGL and GIMP are crowdfunding to be able to work full-time on free
software](https://www.gimp.org/news/2021/07/27/support-gimp-developers-sustainable-development/). 🥳
