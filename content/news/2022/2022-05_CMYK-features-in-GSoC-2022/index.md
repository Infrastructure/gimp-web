Title: GSoC 2022 project announced: CMYK features
Date: 2022-06-03
Category: News
Authors: Wilber
Slug: cmyk-in-gsoc-2022
Summary: CMYK features will be added to GIMP as part of the Google Summer of Code 2022 program

The cat is out of the bag: Nikc, a Google Summer of Code student, is working
this year on **[getting CMYK features into
GIMP](https://summerofcode.withgoogle.com/programs/2022/projects/x9DLkUcC)**.
Let's talk about this in a little more detail.

For the record, we only requested this unique project slot to Google.
Nikc discussed beforehand with us to understand the needs, the project
current state as well as the wanted direction. They also contributed
patches before GSoC selection so we knew how they interact during code
review. If anyone is interested in GSoC in future years, please consider
communicating with us like this, not just dropping a proposal without
contacting us on our [mailing
lists](https://www.gimp.org/discuss.html#mailing-lists) or
[IRC](https://www.gimp.org/discuss.html#irc-matrix). 😉

## What's up with GIMP and CMYK anyway?

Historically, it was complicated to implement CMYK support in GIMP as
the program used to be hardwired to use sRGB color space for pretty much
everything. Supporting any RGB color space has been work in progress for
many years and it got much better already in the 2.10 stable series.
On the other hand, print was not facilitated as a main workflow provided
by our project for decades.

Since late 2000s, we've been considering a late binding workflow for CMYK,
i.e. where you work in RGB, softproof in CMYK and then export to CMYK. Peter
Sikking, a UX architect we worked with, [came up with a
plan](http://blog.mmiworks.net/2009/05/gimp-enter.html) for that, but the
image processing engine was missing the required changes, and someone would
have to work on exposing everything in GUI.

We ultimately lacked contributors interested into prioritizing that, and
[one patch](https://gitlab.gnome.org/GNOME/gimp/-/issues/356) we got
from a contributor some years ago couldn't be merged for architectural
reasons, it had to be redone completely to play well with the rest of
GIMP's code.

A few years back, Øyvind Kolås finally [added the missing
bits](https://www.patreon.com/posts/cmyk-progress-22901518) to GEGL, our image
processing engine, that made it possible to do things like blending a CMYK
image with an RGBA image and writing the result as a TIFF CMYK file.
This paved the way to this particular GSoC project.

## What is the objective of the GSoC project?

First and foremost, we are not yet talking about a CMYK image mode, akin to
'RGB', 'Greyscale', and 'Indexed' like what you have today. Here is what it
will focus on instead.

**Importing and exporting images in a CMYK color space**. GIMP will open
images in a CMYK color space and convert them to RGB(A) for viewing and
editing. You will also be able to export images to CMYK. We are currently
targeting TIFF, PSD, PDF, EPS, AI, and PDF file formats.

While you won't be able to edit CMYK images in a non-native color space
yet, being able to both open (if only to view) and export CMYK data is
clearly a step forward. Moreover while working with a CMYK backend is
useful for some use cases (controlling your black colors or other
accurate color mix as per printer instructions, ink coverage, trapping,
overprinting and whatnot), working with \*RGB images, then moving
to CMYK in the end, is still a recommended workflow for many types of
works. For instance, graphics professionals in the GIMP team and around
worked with a lot of success with a mix of GIMP and
[Scribus](https://www.scribus.net/), which is a software we warmly
recommend.

<figure>
<a href="{attach}gimp-libre-calendar-2015-printed.jpg">
<img src="{attach}gimp-libre-calendar-2015-printed.jpg" alt="Libre Calendar 2015, physical print project by LILA non-profit"/>
</a>
<figcaption>
<em>Example of printing with FLOSS: the "<a href="https://librecal2015.libreart.info/">Libre Calendar 2015</a>" was a collaborative project by <a href="https://libreart.info/">LILA</a>, with several artists (illustrators, photographers and 3D), processed with GIMP, Inkscape and Scribus, Creative Commons by 2.0</em>
</figcaption>
</figure>

Here is the current progress regarding CMYK support in file formats:

- JPEG:
    * CMYK JPEG loading has been supported in GIMP for many years.
    * CMYK JPEG loading has been recently ported to using `babl` for
      conversion by Jehan Pagès.
    * CMYK JPEG exporting has been implemented as well by Jehan Pagès.
    * These improvements will be available in upcoming GIMP 2.99.12 and
      were implemented as a demonstration sample for Nikc since
      [Jehan](https://film.zemarmot.net/) is the project mentor.
- PSD:
    * CMYK PSD 8-<abbr title="Bits per Channel">bpc</abbr> CMYK loading
      has already been available since GIMP 2.10.18 thanks to Massimo
      Valentini.
    * The code has been updated by [Jacob Boerema](https://www.jacobboerema.nl/en/)
      to load 16-<abbr title="Bits per Channel">bpc</abbr> CMYK PSD files
      in GIMP 2.99.10.
    * Work is in progress by Nikc to port CMYK PSD load code to use the
      `babl` library for conversion (should be available for 2.99.12,
      provided it passes review).
    * Exporting CMYK files in the PSD format could also be within the
      scope of this GSoC project.
- CMYK TIFF loading and exporting is a work in progress by Nikc and
  might be available in GIMP 2.99.12 if it passes review on time.
- Other formats with CMYK support will be considered.

**A new dockable dialog for color management**. To aid the late binding
workflow, we need to be able to easily switch soft-proofing (simulating
the CMYK result). Right now switching it ON/OFF and choosing profiles is
done through menus which is cumbersome.

Nikc will develop a new dockable dialog to convert between color
spaces via ICC profiles, control soft-proofing, etc. This is the next step
after CMYK support in various file formats. We'll feel comfortable talking
about this more once it's being actively worked on.

**Soft-proof profile(s) as first-class citizen**: currently the
soft-proof profile is attached to the view, which means it disappears
and needs to be set up again in-between sessions. Also the selected
profile is not available to plug-ins which makes our CMYK export
capabilities much less interesting until it gets implemented. This is
why we want to move the soft-proof profile to become an image data
instead, which will be stored in the XCF file. Nikc announced to have
already started to work on this.

Discussions are ongoing to make this change generic to allow storing
even several simulation profiles in the long run. This will make sense
in particular when GIMP will support the concept of multiple export
targets, as was [theorized in an early blog
post](https://girinstud.io/news/2015/09/improving-the-export-process-in-gimp/).
Indeed a single image can be targetted differently for low-quality web
previewing, for high-quality digital viewing, for printing… And it can
even have different print targets. This is not in the scope of this GSoC
project but are things we need to take into account early on when
modifying the core code.

**Identifying and fixing issues with color management**. There are still all
sorts of bugs and imperfections in GIMP's color management implementation. We
already know about some of them and we have no doubt that others will manifest
themselves as the work on this GSoC project continues. Among the obvious
areas to look on in details are: color picking and [sample
points](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/589),
[color
selection](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/633),
soft-proofing, various types of previews, maybe improved ways to work on
specific color channels…
So fixing or improving those is definitely part of the project. As you
can see from the links above, work has already been started on some of
these areas, and porting more code to using
[babl](https://gegl.org/babl/) for streamlined conversion is a big part
of the project.

Note that all improvements are not necessarily about CMYK-only in the
end, since we have been working to make our color code more generic,
which means that some changes may need to happen on a lower level, hence
enhancing the workflow, efficiency and accuracy with any color model.

## When are we shipping the results of the GSoC project?

Currently, we expect to make all new features and improvements developed by Nikc
this spring/summer to be part of GIMP 3.0.

## Will there be a CMYK mode?

The short answer is 'eventually'.

Previously, we talked about *anyRGB* approach to editing that was within the
scope of the Space Invasion initiative (end-to-end ICC profile attribution for
an image while passing it through various operations in the node composition).
Essentially, you should be able to open an image in e.g. *AdobeRGB* color space
and never convert it to e.g. *sRGB* unless a particular GEGL operation requires
that.

We revised the *anyRGB* plan to extend it to *anyModel* (RGB, CMYK, LAB
etc.). This is going to be a major undertaking, we do not expect to ship
this in GIMP 3.0 and we'd rather not give any estimations at this time.

Even staying within the scope of the printing world, hardcoding the
specific CMYK model, rather than having generic core, would make no
sense. What if you could also add spot color channels to your image for
instance? This *anyModel* core is the direction we are heading for and
the result of the groundwork which has been slowly built up by several
contributors for many years now.
