Title: GIMP 2.10.32 on Apple Silicon
Date: 2022-12-02
Category: News
Authors: Jehan
Slug: gimp-2.10.32-apple-silicon
Summary: The first package of a stable version of GIMP for Apple Silicon is available!
Image: /downloads/downloadsplash-aryeom.jpg

It is a bit of an early Chistmas 🎅 for people using Apple Silicon machines
(Apple M1, M2…) as we release for the first time ever a stable version of GIMP
for this architecture!

It is a revision package for GIMP 2.10.32, already released a [few months
ago](https://www.gimp.org/news/2022/06/14/gimp-2-10-32-released/), re-built with
our new [MacPorts-based
infrastructure](https://www.gimp.org/news/2022/11/18/gimp-2-99-14-released/#macports-based-builds)
on both `x86_64` ("macOS on Intel" architecture) and `AArch64` ("macOS on Apple
Silicon").

Note that we provide 2 DMG packages now, one for each architecture, not a single
universal package. The website will try and detect which architecture you are
on, but if it fails to detect properly (detection is not as easy on some
browsers), be careful to choose the version for the correct hardware ("*for
Intel*" or "*for Apple Silicon*").

Additionally in this revised package, dependencies have been updated, in
particular babl and GEGL. It means that even for macOS on Intel, you will get
the [recent
fix](https://www.gimp.org/news/2022/11/18/gimp-2-99-14-released/#babl-and-gegl)
to the race condition bug which was sometimes causing crashes of GIMP (somehow
we mostly saw it happen on macOS).

This is why we recommend every person on macOS (whichever your hardware) to
update GIMP with this revision 1 of GIMP 2.10.32.

As usual, get these updates on [GIMP's download page](/downloads/)!
