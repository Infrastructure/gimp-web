Title: Development version: GIMP 2.99.10 Released
Date: 2022-02-25
Category: News
Authors: Wilber
Slug: gimp-2-99-10-released
Summary: Release news for development version GIMP 2.99.10
Image: gimp-2-99-10-20220221-experiments.jpg

GIMP 2.99.10 is once again a pretty massive step in our development
plans toward GIMP 3.0. We redesigned some core concepts of GIMP ("linked
layers", item locks' GUI, dynamics switch), substantially improve the
new API further into releasable state, babl and GEGL gets crazy
optimizations going on, macOS and Wayland get more love, all the while
improving file formats… And more!

The below report will go through the most interesting changes.

<figure>
<a href="{attach}gimp-2-99-10-20220221-experiments.jpg">
<img src="{attach}gimp-2-99-10-20220221-experiments.jpg" alt="Experiments - Wilber and co. comics strip by Aryeom"/>
</a>
<figcaption>
<em>"Experiments" by <a href="https://film.zemarmot.net">Aryeom</a>, Creative Commons by-sa 4.0 - GIMP 2.99.10</em>
</figcaption>
</figure>

[TOC]

To get a more complete list of changes, you should refer to the
[NEWS](https://gitlab.gnome.org/GNOME/gimp/-/blob/82af34e4941603ba81ff625e3e11731f86ba3457/NEWS#L9)
file or look at the [commit
history](https://gitlab.gnome.org/GNOME/gimp/-/commits/master).

## Redesigned Core Features
### "Linked layers" superseded by "layer sets"

Our stable series (2.10) still uses a concept of "linked layers", which
is mostly for transforming layers together. For instance, say you want
to translate, rotate or shear several layers the exact same way. You
would link them with the "chain" 🔗 icon next to each layer in the
Layers dockable.

In GIMP 3.0, as we have multi-selection of layers and all the transform
tools were already made to work on multiple layers at once, this concept
was redundant and confusing. Nevertheless there was still one added
value: as a way to mark and store a relation between layers.

This is why we remove the link feature and added instead a "layer set"
feature. A "layer set" is a selection of layers stored with a name.

<figure>
<img src="{attach}gimp-2-99-10-layer-set.png" alt="Layer sets"/>
<figcaption>
<em>Layer set popover at bottom of Layers dockable</em>
</figcaption>
</figure>

For instance, you could select a few layers together and store this
selection as a layer set. Anytime in the future, you can select the same
layers again in one click, without going through the slow process of
clicking them one by one. In the below example, I am storing a semantic
name for several layers featuring sleeping marmots:

<figure>
<img src="{attach}gimp-2-99-10-storing-layer-set.png" alt="Storing Layer sets"/>
<figcaption>
<em>Left: select some layers and write a set name - Right: the set can now be selected again anytime</em>
</figcaption>
</figure>

There is more! The same dialog allows you to search through layers by
names. Say I want to select all layers containing "marmot" or "flower",
I could just search said keyword.

<figure>
<img src="{attach}gimp-2-99-10-search-layers.png" alt="Searching layers"/>
<figcaption>
<em>Searching layers with "flower" in the name - 3 were found in this huge layer list</em>
</figcaption>
</figure>

For the geekiest of us: by default, this uses simple text search, but in
Preferences, tab "Interface", you can also set the search entry
to use the Glob or Regular Expression syntaxes!

<figure>
<img src="{attach}gimp-2-99-10-layer-set-preferences.png" alt="Layer sets settings"/>
<figcaption>
<em>Settings for search syntax: text search, glob pattern or regular expression</em>
</figcaption>
</figure>

### Item locks redesigned

The locks (position, contents and alpha) used to be at the top of the
Layers or Channels dockables. It meant that to know whether a specific
layer was locked, you had to select it. Worse, when selecting several
layers, the buttons could end up in an inconsistent state (if some
layers are locked while others not).

So we moved the locks to the space left vacant by the now-defunct "link"
🔗 icon, next to each item. Clicking it will open a small popover dialog
where you can select the lock type.
Selecting a single lock will show it next to the visibility (eye 👁️)
icon, selecting 2 locks or more will show a generic multi-locks icon.
Each of the new icons, specific or generic, were designed by Aryeom.
Now you know in a single glimpse to the layers list which ones are
locked or not.

<figure>
<img src="{attach}gimp-2-99-10-locks.png" alt="Locking a layer"/>
<figcaption>
<em>Left: popover on "lock" area click -
Middle: lock position (specific icon shown) -
Right: lock contents (2 locks in use: multi-locks icon shown)
</em>
</figcaption>
</figure>

Among other advantages, the lock icons now allow the `Shift-click`
interaction, same as the visibility icon, so that one can switch many
items' locks at once. We also created a new `Alt-click` interaction to
switch locks within all selected layers. So we now have:

* Simple `click` on a lock or visibility icon: the clicked item's
  visibility or lock state is switched.
* `Shift-click`: the clicked item stays visible/locked while the
  visibility or lock state of all **other items on the same level** in
  the layer tree is switched ON or OFF.
* `Alt-click`: the clicked item stays visible/locked while the
  visibility or lock state of all **other selected items** is switched
  ON or OFF.

You will notice that we also added a new lock, the "Visibility Lock". As
the name implies, it prevents the visibility status to be modified. This
is especially interesting together with features allowing to change
multiple items at once, like the `Shift-click` or `Alt-Click` actions
explained above. You could for instance have a background image locked
to be always visible, then you could switch other foreground layers'
visibility while always keeping the background visible. Oppositely you
could prevent your initial croquis for instance from being shown by
mistake.

<figure>
<img src="{attach}gimp-2-99-10-lock-visibility.png" alt="Locking visibility of background"/>
<figcaption>
<em>Locking visibility of background layers to always visible and
croquis layer to never visible.</em>
</figcaption>
</figure>

Among other changes in this area, the alpha and position locks can now
be set on Layer groups:

* Alpha lock on groups mostly works like contents lock on groups (except
  that it's for the alpha channel only), i.e. that it forbids to change
  the alpha channel from any child layer.
* Position lock work both ways by forbidding moving child layers but
  also parent layers.

A document was also written to better [specify the behaviors of various
locks](https://gitlab.gnome.org/GNOME/gimp/-/blob/master/devel-docs/specifications/locks.md).

Last, but not least, two changes were made to improve discoverability of
the visibility and especially the lock buttons:

* The visibility and lock columns in the dockable now have icon headers,
  making their presence more obvious.
* Our default theme will now provide visible hints around inactive
  button areas while hovering these clickable areas.

<figure>
<img src="{attach}gimp-2-99-10-layer-dockable-icon-hints.png" alt="Hints for discoverability of clickable areas"/>
<figcaption>
<em>Points to notice: "visibility" and "lock" icon headers and border around clickable area when hovering with pointer</em>
</figcaption>
</figure>

### Enabling/Disabling dynamics simplified

It is now possible to enable and disable dynamics in a single checkbox,
instead of tediously having to select the "*Dynamics Off*" neutral
dynamics. Obviously this neutral dynamics has been removed from the list
of installed data.

It allows to switch dynamics painting in a single click, and without
having to remember which dynamics you were using when switching back.

<figure>
<img src="{attach}gimp-2-99-10-dynamics-enabling.webp" alt="Enabling/disabling dynamics in a click"/>
<figcaption>
<em>Enabling and disabling dynamics in one click</em>
</figcaption>
</figure>

For advanced user, we also set up the new action
"context-dynamics-toggle". Thus you can create a shortcut (`Edit >
Keyboard Shortcuts` menu) to switch the dynamics painting for even
faster work.

<figure>
<img src="{attach}gimp-2-99-10-dynamics-shortcut.png" alt="Shortcut settings for enabling/disabling dynamics"/>
<figcaption>
<em>Setting a shortcut to enable or disable dynamics in the Keyboard
Shortcuts dialog</em>
</figcaption>
</figure>

## New Core Features
### New option in "Line Art" mode of bucket fill tool

We added a new option "*Allow closing lines in selected layer*" in the
"Fill by line art detection" mode of the bucket fill tool. The name is
not necessarily final as naming this was hard.

Anyway here is what it does: when you choose the "*Layer above|below the
selected one*" source, the selected layer is not used as line art source
(as the title says). Yet with this option, you add the ability to paint
with your fill color directly in the selected layer and union this data
as closure data.

Once again, it is **not** a source for line art detection, which means
it doesn't trigger any recomputing. It is additional data for manual
line closure only *after* line art and closure computation.

One of the main usage could be to set the "*Maximum gap length*" to 0,
which totally disables line art closure computation, hence having much
less computing (i.e. faster on big line art images). Then you'd rely
solely on manually closed line arts. Unfortunately sometimes you don't
want to close a line art with your **line** color/brush/style, you want
to close it with your **fill** color/brush/style. This is when this
option comes into play.

For instance, here is how you could fill the line art with the smart
closing which could oversegment if you don't tweak properly the
"*Maximum gap length*". Additionally the closure (here at the leg
bottoms of the character) would likely not be the expected style:

<div class='fluid-video'>
<video width="1280" height="720" controls>
  <source src="https://download.gimp.org/gimp/video/v2.99/gimp-2-99-10-line-art-smart-closing.mp4" type="video/mp4">
  <source src="https://download.gimp.org/gimp/video/v2.99/gimp-2-99-10-line-art-smart-closing.webm" type="video/webm">
Your browser does not support the video tag.
</video>
</div>

Now let's disable the smart closing algorithm and use the new option
instead. We will simply paint with our expected fill color and style to
close the line art and it will be considered as "line" without
recomputation. This way, the fill is instant and the closure style is
exactly the one you expect:

<div class='fluid-video'>
<video width="1280" height="720" controls>
  <source src="https://download.gimp.org/gimp/video/v2.99/gimp-2-99-10-line-art-manual-closing.mp4" type="video/mp4">
  <source src="https://download.gimp.org/gimp/video/v2.99/gimp-2-99-10-line-art-manual-closing.webm" type="video/webm">
Your browser does not support the video tag.
</video>
</div>

This new option is originally based on an advanced digital painter tip
of Aryeom on how to colorize efficiently a croquis (she was using this
long before the "fill by line art detection" feature in GIMP, using
*Fuzzy Select* in sample merged mode). Closing line arts with fill style
is one of the many techniques she teaches to digital painting students at
university. So we thought integrating this into our line art fill tool
would be a definite gain as Aryeom was disappointed by the absence of
style and control with the smart closing algorithm.

*Note: we are aware the settings in this tool are a bit complicated to
apprehend. We will hopefully do a pass of redesigning soon.*

### Welcome Dialog

GIMP now gets a `Welcome Dialog` which will show up only once after a
new installation or an update. It will display the splash image, some
short text and links to documentations and alike.

It also has a "Release Notes" tab containing the Release Notes of the
new version so that anyone can peek at a listing of novelties.

<figure>
<img src="{attach}gimp-2-99-10-welcome-dialog.jpg" alt="Welcome Dialog"/>
<figcaption>
<em>Welcome Dialog at first launch after installation or update</em>
</figcaption>
</figure>

*Note: the currently proposed Windows installer misses the release note.
We will fix this shortly.*

The website's (the one you are reading on `gimp.org`) news will still
provide nicer longer texts with explicative screenshots. But at least,
the Welcome Dialog will be here for people not reading the website.

An advantage of the Welcome Dialog's release notes though is that it can
be fully localized by our translators (unlike the website in its
current version). For instance, here are the release notes showing on a
Portuguese-localized GIMP:

<figure>
<img src="{attach}gimp-2-99-10-welcome-dialog-pt.png" alt="Welcome Dialog in Portuguese"/>
<figcaption>
<em>Welcome Dialog's release notes are localized; e.g. here in Portuguese</em>
</figcaption>
</figure>

Some more work might happen on this Welcome Dialog. For instance,
a "Customization" tab for some of the more controversial settings (such
as themes and icons) will likely be added.

There is also the question about adding more "everyday use" features,
such as a list of the last opened images, or a "New Image" option,
avoiding going in menus (or using shortcuts).
Of course it means giving the ability to open the dialog at every start
as an option (unlike now where it will only open once after a new
update).

## Going further with our compact slider widget

Our compact slider was only available to [core
code](https://www.gimp.org/news/2020/11/06/gimp-2-99-2-released/#compact-sliders)
until recently. It means that all plug-ins in particular could not use
of this slider.
So the widget has now been moved to the `libgimpui` library, making it
usable by any plug-in which wants a nice looking and efficent slider
with slow relative moves, fast drag moves and direct keyboard edits (see
some of the [usability work in an older
news](https://www.gimp.org/news/2020/12/25/gimp-2-99-4-released/#slider-widget)),
all the while maximizing the space.

A few of our official plug-ins already make use of it now.

<figure>
<img src="{attach}gimp-2-99-10-compact-slider-in-png-export.png" alt="Locking a layer"/>
<figcaption>
<em>The PNG export dialog with compact slider
</em>
</figcaption>
</figure>

Additionally our default `System` theme made it even more compact,
because there were some complaints that this said *compact* slider was
not compact enough!

## Official plug-ins
### PSD

As often lately, our PSD plug-in got some love to widen our support
abilities:

* new support for loading 16-bit per channel CMYK images.
* new support for files in LAB colorspace.
* new support for loading 32-bit per channel images (some code
  existed yet may have never really worked).
* Add extra layer groups when loading PSD images with clipping
  layers: Photoshop handles clipping layers in a different way than
  GIMP. The only way to have it look the same is by adding extra
  layer groups. PSD layers that have clipping set, combined with the
  first non clipping layer below it are grouped together in a new
  layer group. Doing this results in the same image as the PSD
  merged image unless there are other PSD elements in play that we
  don't handle yet.
* PSD layers with clipping set need clip to backdrop as composite
  mode: certain PSD layers have a flag called clipping set to 1. We
  were not handling this flag and had some reports about colors
  bleeding where there were not supposed to. We are going to set
  GIMP's layer composite mode to "Clip to Backdrop" which is the closest
  we can get to Photoshop's clipping. With this, the colors won't bleed
  anymore.

### JPEG-XL

Our recent JPEG-XL support also continues to improve:

* Bit depth now selectable in JXL export.
* Import in 8-bit and 16-bit integer precision now possible for
  lossless images (GIMP used to import all JXL images as 32-bit float
  precision images).
* New very fast export settings: thunder and lightning (fastest).
* Compression slider is disabled for lossless.

### Microsoft Windows Cursor (`.cur`)

GIMP is now able to load and export Microsoft Windows Cursors, including
the ability to read at import, pass on and edit at export the hot spot
location for each cursor in the file.

### Screenshot

We definitely removed support for the **KDE** and **GNOME** portals for
screenshots. On recent versions of these portals, new security
restrictions were making them unusable for GIMP which didn't have the
permissions.

We now use only the Freedesktop portal on **Linux** (unless the direct X11
codepath is available, i.e. on Wayland) which is the new recommended
logic by both the GNOME and KDE teams.

On **Windows**, the Screenshot plug-in finally gets an "Include mouse
pointer" option.

### HEIF

The HEIF plug-in used to have some heuristic for the choice of the
bit-depth defaults when exporting, depending on the image's encoding
settings. This heuristic didn't agree with everyone's logic.

Also it was going against the settings storage which are now a basic
feature of most plug-ins.

So we decided to just drop this heuristic and leave the user completely
responsible for chosing the bit-depth they wish.

### Deprecating `help-browser` and `webpage` plug-ins

We have 2 plug-ins depending on the WebkitGTK library:

1. `help-browser`: for displaying the html docs. Nowadays every desktop
   machine has a browser, so it feels redundant. Its main advantage
   though was its very nice "topic tree", which is basically a side menu
   to navigate the documentation quite efficiently.
2. `web-page`: a webpage screenshot plug-in allowing to shot the whole
   page, not just what is visible. This is a very neat feature, yet it
   also appeared on most web browsers by default these days. For
   instance on Mozilla Firefox, you can *right-click* then select "Take
   Screenshot" in the contextual menu of a page.

On the other hand:

1. WebkitGTK is very hard and long to build. More than once we
   encountered issues and are dreading them. Actually our macOS package
   don't build it right now because of such difficulties.
2. From what we gather, recent versions are officially unsupported on
   Windows right now and we don't know when or if it will change soon.

So we have 2 of our main platforms not using it, it makes a lot of
packaging problems, all this for features which are a bit redundant with
browser nowadays anyway. This is why we decided to drop the ball, at
least for now, by deprecating these 2 plug-ins. Unless situation changes
in any positive way, we are not recommending packagers to build these
anymore.

## Build and documentation
### Icon theme handling and refactoring

Our icon theme handling code was a bit of a mess because of too many
build cases, so we refactored it:

* We now use the exact same list for the Color and Symbolic icon themes,
  both in their SVG and PNG variants.
* The PNG variants for both themes are now fully generated from the SVG
  files instead of duplicating everything and risking strange bugs with
  specific build combinations.
* Our Continuous Integration now has a new weekly job to verify that the
  meson and autotools build rules are perfectly synced regarding icons.
* We started to group icons in semantic lists when possible to help with
  deciding the right sizes to generate in the PNG case.

By the way, why we keep the PNG variant option needs to be explained. We
were originally planning to drop it, but it turns out that the `librsvg`
dependency used as `GdkPixbuf` loader, ported to Rust in its later
versions, doesn't build on all platforms and architectures (as Rust is
not fully portable yet).
It means that some platforms may need to either use very old versions of
librsvg or may even prefer to use PNG-only icons. This is why we keep
the PNG variant for now. It is suboptimal and on platforms where
`librsvg` is not a problem, we definitely advise to use SVG icons. But
at least the option exists.

Note though that there is another case where we depend on `librsvg`: the
SVG import plug-in and this one had been made a mandatory one. We are
not changing this because as graphics professionals, we consider that
even in a raster program, you need to be able to import SVG nowadays.
This format is simply a professional standards now and not supporting it
seems absurd.
Yet we understand if it is a problem on some platforms so, packagers,
feel free to comment out the SVG plug-in if really this is a problem to
you, as we have been told. It should be quite easy to achieve.

### Clang-format check in Continuous Integration

A new `.clang-format` file was added to our repository and now every
merge request will trigger a CI job to verify coding-style of your
change.

Now to be perfectly fair, we are not completely happy with the check
results, which is why the job failures are not fatale, mostly
informational. Let's say this is a first step, hoping this coding style
check will get better.

### Tool to bisect-test with older versions of flatpak

*Note: this is developer fanciness and may not be of high interest to
others.*

Flatpak has limitations but also quite some interesting features. One of
them is the ability to install older versions of the package easily. It
can be a pretty neat developer tool when we want to test something on an
older version of GIMP, a kind of `git bisect` but with flatpak packages.
Nevertheless listing the old versions, uninstalling your current
version, installing the relevant older version, and remembering the
command lines for all this (since we don't do this daily either) was
making it hardly usable in real life.

This is where this tool comes in:

```
$ tools/flatpak-releases  -h
Usage: ./flathub-releases [--beta] [--system] [-X] [org.example.app]

List all flatpak builds stored on Flathub or GNOME repository.
The builds for org.gimp.GIMP are looked up by default unless
you provide explicitely another AppStream ID.

Adding a -X number as option install the numbered release
instead of listing them.

Options:

-0: install the last build.
-1: install the previous build.
-2: install the before-previous build (and so on).

--beta: list or install a beta release
--nightly: list or install a nightly release
--system: install as system flatpak (default to user install)
```

Say I want to know all the beta release still stored on Flathub:

```
$ tools/flatpak-releases --beta
 0: Working around weird install path of appstream file. (be96fed3) [2022-02-24 00:37:25 +0000]
 1: Update dependencies (127a0fa7) [2022-01-13 16:59:43 +0000]
 2: Issue #106:  File->Create->From Screenshot not working. (9c831b14) [2021-12-14 21:46:52 +0000]
 3: Release GIMP 2.99.8. (908bf5b0) [2021-10-20 20:29:00 +0000]
 4: Release GIMP 2.99.6. (e04355dd) [2021-04-26 14:08:32 +0000]
 5: Make sure the default path for plug-ins and scripts point to the extension point (8b02ea26) [2021-03-31 16:12:06 +0000]
 6: Build Little-CMS 2.12 ourselves. (ae60863e) [2021-03-29 16:03:51 +0000]
 7: Add extension point for Gimp3 (34b2f8c0) [2021-03-27 12:40:57 +0000]
 8: Release GIMP 2.99.4. (20a0a962) [2020-12-22 23:45:19 +0000]
 9: Update to GNOME Platform 3.38. (a84da0fa) [2020-11-14 23:08:38 +0000]
10: Use org.gnome.Platform//3.36beta. (12017e04) [2020-11-06 22:59:59 +0000]
11: Release GIMP 2.99.2! (58fef365) [2020-10-25 22:20:18 +0000]
12: Add shared module for intltool. (a1d2b211) [2020-06-01 18:34:15 +0000]
```

The number `0` is the GIMP 2.99.10 release (the package stored the last
commit message, which might not be as relevant as we'd want).
Now say I want to install the GIMP 2.99.6 release for a test:

```
$ tools/flatpak-releases --beta -4
[1/2] Installing org.gimp.GIMP
Looking for matches…
Skipping: org.gimp.GIMP/x86_64/beta is already installed
[2/2] Updating to commit '9165cae20b6ad8549ff8053385b0facd15bb11fc8733e0b9c50aed0e961a6c3e' (4's previous build)
Looking for updates…


        ID                     Branch         Op         Remote               Download
 1. [✓] org.gimp.GIMP          beta           u          flathub-beta         43.4 MB / 75.5 MB

Updates complete.
Build 4 released on 2021-04-26 14:08:32 +0000 was installed.
Build subject: Release GIMP 2.99.6. (e04355dd)
Build commit on flathub-beta: 9165cae20b6ad8549ff8053385b0facd15bb11fc8733e0b9c50aed0e961a6c3e
```

Done. In 2 commands and less than a minute, I am now able to run our
GIMP 2.99.6 flatpak beta package which was 4 packages ago.

This command allows me to do this for the stable, the beta and the
nightly packages.

### New contributor documentation

Our developer document was a bit sparse and not too maintained these
days, so a new version is starting to get written. It's still widely
incomplete and some part are based on older docs which might be
outdated, or at least would deserve a review. In any case, work is in
progress.

For anyone wishing to contribute, [this is where you should
start](https://gitlab.gnome.org/GNOME/gimp/-/blob/master/devel-docs/README.md).

## Improved API for plug-ins

The work on the API is still going full on. Here are the most noteworthy
changes which happened during GIMP 2.99.10 time frame.

Changes in `libgimp`:

* `GimpStringArray` type was removed in favor of `GStrv`. Various
  libgimp API were updated to use `GStrv`, and relevant plug-in procedures
  with
  `GStrv` arguments or return values were updated as well.
* New functions:
    + `gimp_context_are_dynamics_enabled()`
    + `gimp_context_enable_dynamics()`
    + `gimp_item_get_lock_visibility()`
    + `gimp_item_set_lock_visibility()`
    + `gimp_pdb_run_procedure_config()`
* Removed functions:
    + `gimp_item_get_linked()`
    + `gimp_item_set_linked()`

Changes in `libgimpui`:

* New widgets:
    + `GimpLabelColor` (now used by default for `GimpRGB` properties in
      `GimpProcedureDialog)
    + `GimpLabelEntry` (now used by default for string properties in
      `GimpProcedureDialog)
    + `GimpSpinScale` (see [above](#going-further-with-our-compact-slider-widget))
* New functions:
    + `gimp_color_area_enable_drag()`
    + `gimp_event_triggers_context_menu()`: alternative to
      `gdk_event_triggers_context_menu()` with the additional ability of
      using button release events as contextual menu triggering
      (instead of press events), which might be prefered in some
      cases. Other than this, it uses exactly the same conditions as
      its GDK counterpart.
    + `gimp_procedure_dialog_get_spin_scale()`
    + `gimp_prop_label_color_new()`
    + `gimp_prop_label_entry_new()`
    + `gimp_prop_spin_scale_new()`
    + `gimp_prop_widget_set_factor()`
* Improved functions:
    + `gimp_procedure_dialog_get_widget()` can now generate widgets of
      type `GimpSpinScale` (for int/double properties) and GimpLabelColor
      or `GimpColorButton` (for `GimpRGB` properties).
    + `gimp_procedure_dialog_get_color_widget()` now only return
      `GimpLabelColor` widgets (editable or not).

Also the Vala bindings `gimp-3.vapi` and `gimp-ui-3.vapi` were renamed
to `gimp-3.0.vapi` and `gimp-ui-3.0.vapi` respectively in the autotools
build (now consistent with meson).

By the way, it seems we have issues with the Vala and Lua plug-ins on
Windows in the installer we are providing. We don't know why yet and
still need to investigate.

## Wayland and macOS support

Various bugs and improvements have been made specifically to Wayland and
macOS support. These often go together as we notice that many of the
issues which appear on one of these also appear on the other.

On macOS in particular though, there were massive slowness issues of the
2.99 development series, so much that at some point, our packager feared
that GIMP 3.0 on macOS would never be usable.
Well we are proud to announce this was premature despair as we finally
found some solutions for some things, workarounds for others, sometimes
not too pretty yet enough for GIMP to be finally usable and even faster
than the 2.10 series in our various tests! Our only uncertainty is that
the code was mostly tested on macOS Monterey. We don't know the
situation on Big Sur where the problems initially started.

Several parts of the solutions lead us to GTK patches. Not everything is
upstream yet, but we already use our current satisfying patch set in our
own DMG package. This will help other projects using GTK3 and
experiencing similar issues.

## babl and GEGL

As usual, this release is supplemented with the releases of
[babl](https://gegl.org/babl/) 0.1.90 and [GEGL](https://gegl.org/)
0.4.36.

And these are particularly exciting for this release.

### Automatic LUT creation for conversions in babl

Citing Øyvind in a recent [report](https://www.patreon.com/posts/babl-lutefisk-62286666):

> babl works as a universal pixel encoding translator by having - a not
> necessarily fast reference conversion able to deal with everything - and
> a collection of conversions that can be chained together into what babl
> calls fishes. The fishes are raced against each other to find the best
> conversion babl is able to create.

In some situations, a [LUT (Look-Up
Table)](https://en.wikipedia.org/wiki/Lookup_table#Lookup_tables_in_image_processing)
will perform better, we now know what the performance will be of using a
LUT for various combinations of input and output bits per pixel; and
mark fishes that are better as LUTs on creation time. If such a fish is
used more than a minimum, LUT population is triggered.

Right now one drawback is that it might actually slow down slightly the
first conversion happening in a given `input > output` conversion couple.
The solution which might come later would be to create the LUTs in the
background in a thread and continue doing non-LUT conversions until it
is ready.

Note that the created LUTs are also garbage-collected after a few
minutes of non-usage, to avoid filling memory with single use LUTs.

### SIMD builds for x86_64 and ARM neon (ctx, babl and GEGL)

All of babl, GEGL and ctx have gotten similar SIMD build and dispatch changes
applied.

Image processing code written to be portable and performant caters well to the
auto-vectorization support for SIMD in modern compilers. The way it is done is
mostly by changes to the build system. For now the code remains the same for
all targets but the approach can be extended with conditional intrinsics.

To see the impact this can have, here is a test of filling a circle with ctx
and its different pixel encoding targets, on a Raspberry Pi with default
compiler flags (without neon support). Note that the cairo reference
comparison is the BGRA8 format (the only other format proposed by Cairo is A8):

<figure>
<img src="{attach}GEGL-0-4-36-arm-without-neon.png" alt="Test results without SIMD"/>
<figcaption>
<em>Filling a 256px radius circle in a 512x512 buffer a number of times without Neon (smaller is better)</em>
</figcaption>
</figure>

And the same test, with the compiler allowed to use Neon instructions:

<figure>
<img src="{attach}GEGL-0-4-36-arm-with-neon.png" alt="Test results with SIMD"/>
<figcaption>
<em>Filling a 256px radius circle in a 512x512 buffer a number of times without Neon (smaller is better)</em>
</figcaption>
</figure>

Note well that both the optimized and non-optimized cases are built-in,
and availability of relevant instructions determined by runtime tests.
This makes these optimizations very portable, despite being based on
recent architecture instructions.

ℹ️ Note that these changes (both the automatic LUT generation and the SIMD
dispatch) will apply to GIMP 2.10 versions later. For now they are
available in 2.99 as a form of beta test. We welcome feedbacks if you
encounter any misbehavior.

### Other improvements

`babl` now also comes with pre-defined CIE Lab u8 and CIE Lab u16 formats.

## Team news

On our main source repository, "*reporter*" access (allowing to triage
reports: labelling, closing, reopening, and moving reports…) have been
given to nmat and Liam Quin.

Ondřej Míchal has now been given write access to the package repository
for our stable and beta flatpak on Flathub, thus joining Hubert Figuière
and Jehan for maintenance, after their various improvements to our
flatpak packages, including some very nice work for automatic detection
of outdated dependencies, thanks to the `flatpak-external-data-checker`
tool.

## Release stats

42 people contributed to GIMP 2.99.10:

* 17 developers contributed to `GIMP` code base for this micro version:
    - 1 developer with more than 300 commits (Jehan)
    - 4 developers with 10 to 30 commits (Jacob Boerema, Niels De Graef,
      Lukas Oberhuber and Daniel Novomeský)
    - 12 developers with 1 to 4 commits: Øyvind Kolås, Anders Jonsson,
      Asalle, Emily Gonyer, Luca Bacci, Markus Volk, Ondřej Míchal,
      Stanislav Grinkov, Yoshinori Yamakawa, bartoszek, Nikc and Tomasz
      Goliński.
* 20 translations were updated: Basque, British English, Catalan,
  Chinese (China), Danish, German, Greek, Hungarian, Italian, Kabyle,
  Latvian, Lithuanian, Polish, Portuguese, Russian, Slovenian, Spanish,
  Swedish, Ukrainian, Vietnamese.
* 24 translators contributed: Yuri Chornoivan, Hugo Carvalho, Anders
  Jonsson Matej Urbančič Rodrigo Lledó, Rūdolfs Mazurs, Asier Sarasua
  Garmendia, Luming Zh, Bruce Cowan, Jordi Mas, Piotr Drąg, Ask Hjorth
  Larsen, Boyuan Yang, Marco Ciampa, Alan Mortensen, Daniel Mustieles,
  Yacine Bouklif, Alexandre Prokoudine, Aurimas Černius, Balázs Meskó,
  Luna Jernberg, Ngọc Quân Trần, Tim Sabsch and dimspingos.
* 4 people helped with in-code documentation: Jehan, Niels De Graef,
  Alexandre Prokoudine and Daniel Novomeský.
* 2 people contributed to icons: Aryeom and Jehan.
* 9 people contributed to build-related updates: Jehan, Lukas Oberhuber,
  Øyvind Kolås, Asalle, Daniel Novomeský, Yoshinori Yamakawa, Aryeom
  Han, Markus Volk and bartoszek.
* One new splash screen by Aryeom Han.

These are the stats on babl, GEGL and ctx side:

* 4 contributors to `babl` 0.1.90:
    - 1 contributor with more than 70 commits (Øyvind Kolås).
    - 4 contributors with 1 to 4 commits: Mingye Wang, Jehan, Tomasz
      Golinski and Andrzej Hunt.
* 6 contributors to `GEGL` 0.4.36:
    - 1 contributor with more than 20 commits (Øyvind Kolås).
    - 5 contributors with 1 to 2 commits: Anders Jonsson, Jehan, Alan
      Mortensen, Caleb Xu and zamfofex.
* `ctx` doesn't have releases per-se as it is project-embedded code. So
  let's count commits in the time frame between GIMP 2.99.8 and 2.99.10:
    - 1 contributor with more than 600 commits (Øyvind Kolås)
    - 1 contributor with 1 commit (Jehan)

We should also not forget the documentation repository which gets more
activity these days, thanks to the new maintainership by Jacob. In the
same time frame as GIMP 2.99.10, it got 5 contributors:

* 2 contributors on the documentation itself: Jacob Boerema and Anders
  Jonsson.
* 4 contributors worked on translations: Rodrigo Lledó, Anders Jonsson,
  Jordi Mas and Piotr Drąg.

Finally the work on the macOS repository should not be forgotten, thanks
to our new packager, Lukas:

* 1 contributor with more than 60 commits (Lukas Oberhuber).
* 2 contributors with 1 to 2 commits: Jehan and Andreas Scherer.

## Downloading GIMP 2.99.10

As usual, GIMP 2.99.10 is available on [GIMP official website
(gimp.org)](https://www.gimp.org/downloads/devel/) in 3 package formats:

* Linux development flatpak
* Windows installer
* macOS DMG package

Other packages made by third-party are obviously expected to follow
(Linux or *BSD distributions' packages, etc.).

ℹ️ Fun fact: we learned that GIMP was successfully [built on Haiku
OS](https://twitter.com/haikunauts/status/1489019411637972997) (yet
another operating system, not a mainstream one) and is now provided
through their packaging system. Even though we know it well, it always
amazes us how cross-platform GIMP is as it can be found on nearly every
operating system (yes GIMP is on GNU/Hurd too from what we are told!)
and micro-architecture there is. 😊

## What's next

This release is quite heavy on API-side updates, which is one of the big
blocker for GIMP 3.0 release (still some work-in-progress on this side).
The "link" concept was also an old concept which needed to disappear for
GIMP 3.0, so this is one less blocker as this change is now completely
finished.

The GTK port is another big blocker; important work in this direction
has been started by some contributors regarding a move to the
`GApplication` and `GtkApplication` frameworks and the `GAction` port.
These patches are still heavily under work and need review thus it has
not been merged in any form in GIMP 2.99.10. We will most likely have
the opportunity to talk more about it on the next release.

Meanwhile work related to Wayland and macOS support are still taking
quite a bit of developer time as you can see.

Don't forget you can [donate to the project and personally fund several
GIMP developers](https://www.gimp.org/donating/), as a way to give back
and accelerate the development of GIMP. As you know, the [maintainers of
GEGL and GIMP are crowdfunding to be able to work full-time on free
software](https://www.gimp.org/news/2021/07/27/support-gimp-developers-sustainable-development/). 🥳
