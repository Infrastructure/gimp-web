Title: GIMP 2.10.32 is on the Microsoft Store!
Date: 2022-06-18
Category: News
Authors: Wilber
Slug: gimp-2-10-32-on-microsoft-store
Summary: GIMP is officially distributed on the Microsoft Store
Image: /downloads/downloadsplash-aryeom.jpg

Until now, GIMP was only officially distributed **for Windows** as an
installer provided on our [download page](https://www.gimp.org/downloads/).

In particular, the GIMP team never distributed it via the *Microsoft
Store*.
[This changes
today!](https://apps.microsoft.com/store/detail/gimp/XPDM27W10192Q0) 🥳

## What if I see several GIMP on the Microsoft Store?

You might see other "GIMP" on the Microsoft Store, so you must be
careful. For the official package, make sure you use the link we
provide:

<p class='buttons'>
  <span class='download-button download-store'>
    <a href="ms-windows-store://pdp/?productid=XPDM27W10192Q0">
      GIMP on Microsoft Store
      (Windows-only link)
    </a>
  </span>
</p>

While others distributing GIMP is not necessary a problem (we even have
a [page explaining why selling GIMP is not illegal](https://www.gimp.org/about/selling.html)),
we have no way to know what is actually in a third-party package, hence
we cannot vouch for them.

Problems could be malware 😱 participating in botnets, hack your files or
other computers or perhaps just steal CPU resources. Some are charging
for their packages while using deceptive wording — seeming to be the
official GIMP team — and at the same time introducing subtle bugs or
significant problems like uploading opened images behind the users back.
The official GIMP packages do not contain malware nor do they upload
your files. We have a strict [Privacy
Policy](https://www.gimp.org/about/privacy.html).
Our packages are also [**free of charge**](#funding-gimp).

These are not all cases, and other third-party packagers may be doing a
great job adding value (user support, additional documentation, more
filters…) so you are welcome to continue using these as long as you are
aware they are not related to us.
Anyway we saw enough problematic cases that we are happy to announce
GIMP being now officially distributed on the Microsoft Store so that
everyone can get GIMP from its source.

## Limitations of store's GIMP

We worked closely with a developer relations team at Microsoft for this
new distribution channel. GIMP is proposed as what they call a
[traditional desktop
app](https://developer.microsoft.com/en-us/microsoft-store/desktop-apps/)
which basically means that even if you install GIMP from the store, it
actually uses the exact same installer (as found on our [download
page](https://www.gimp.org/downloads/) then run by the store in silent
mode).

The good side is that it's easier for us to maintain and user experience
is exactly the same. The bad side is that it doesn't get additional
niceties made available by centralized repositories, such as
auto-update. You will still have to update yourself, the old way.

This is why working on a MSIX or UWP package remains a possibility if
anyone wants to continue the [work started](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/588).
Contributors welcome! 🤗

## Funding GIMP

As all other official GIMP packages, **GIMP is free of charge on the
Windows Store**. 💌

If GIMP is useful to you and **you wish to fund development**, you are
very welcome to [**donate** to the project and fund core GIMP
developers](https://www.gimp.org/donating/), as a way to give back
and accelerate the development of GIMP.

<p class='buttons'>
  <a href="https://www.gimp.org/donating/">
    <span class='donate-button'>
    Support us by<br/> <strong>Donating</strong>
    </span>
  </a>
</p>
