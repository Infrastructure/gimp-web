Title: 2022 annual report
Date: 2023-01-29
Category: News
Authors: Jehan
Slug: 2022-annual-report
Summary: What happened in 2022?
Image: GIMP-2023-january-report.jpg

Pursuing the newfound tradition started [a year
ago](https://www.gimp.org/news/2021/12/31/gimp-2021-annual-report/), here is my
report for past year 2022.

<figure>
<img src="{attach}GIMP-2023-january-report.jpg" alt="Go 2023 - Wilber and co. comics strip by Aryeom"/>
<figcaption>
<em>"Go 2023" by <a href="https://film.zemarmot.net">Aryeom</a>, Creative Commons by-sa 4.0 - GIMP's 2022 annual report</em>
</figcaption>
</figure>

[TOC]

# Statistics

In 2022, we had:

- **1 stable releases** ([GIMP
  2.10.32](https://www.gimp.org/news/2022/06/14/gimp-2-10-32-released/))
- **3 development releases** ([GIMP
  2.99.10](https://www.gimp.org/news/2022/02/25/gimp-2-99-10-released/),
  [2.99.12](https://www.gimp.org/news/2022/08/27/gimp-2-99-12-released/) and
  [2.99.14](https://www.gimp.org/news/2022/11/18/gimp-2-99-14-released/)).
- **1461 commits** on the unstable development branch (2.99.x, future
  3.0) and **276 commits** on the stable development branch (2.10.x) of
  the main repository.
- **87 contributors** on the main repository, including (some people belong
  to several categories):
    * 35 developers
    * 47 translators
    * 26 contributors to resources (icons, themes, in-code documentation) or
      build improvements
- **7 core developers** contributed 10 or more commits in GIMP's main
  repository:
    * Jehan: 649 commits
    * Jacob Boerema: 64 commits
    * Nikc: 50 commits
    * Daniel Novomeský: 25 commits
    * lloyd konneker: 25 commits
    * Lukas Oberhuber: 18 commits
    * Niels De Graef: 15 commits
- **115 commits** to [babl](https://gegl.org/babl/) by 10 contributors, with 3
  developers contributing 10 or more commits:
    * Øyvind Kolås: 86 commits
    * Axel Viala: 10 commits
    * Jehan: 10 commits
- **138 commits** to [GEGL](https://gegl.org/) by 32 contributors, with 5
  developers contributing 5 or more commits:
    * Øyvind Kolås: 47 commits
    * Behnam Momeni: 9 commits
    * Michael Drake: 7 commits
    * Thomas Manni: 7 commits
    * Jehan: 5 commits
- **1042 commits** to [ctx](https://ctx.graphics/) by 2 contributors (mostly
  Øyvind Kolås).
- **492 commits** in `gimp-help` (our manual) by 29 contributors, with 11
  people contributing 10 or more commits (this list mixes documenters, build
  maintenance and translators):
    * Jacob Boerema: 229 commits
    * Anders Jonsson: 47 commits
    * Rodrigo Lledó: 38 commits
    * Jehan: 28 commits
    * Jordi Mas: 25 commits
    * Tim Sabsch: 19 commits
    * Nathan Follens: 17 commits
    * Marco Ciampa: 16 commits
    * Yuri Chornoivan: 15 commits
    * Andre Klapper: 13 commits
    * Hugo Carvalho: 11 commits
- **178 commits** in `gimp-macos-build` (our macOS build) by 3 contributors
  (mostly Lukas Oberhuber).
- **33 commits** in the stable branch of our Flathub/Flatpak package and **23
  commits** on the beta branch by 6 contributors, including 4 core contributors:
  Jehan, Ondřej Míchal, Hubert Figuière and Daniel Novomeský.
- **227 commits** to GIMP's website (`gimp.org`, i.e. right here) by 10
  contributors (mostly Jehan).
- **158 commits** to our new [developers website](https://developer.gimp.org/)
  by 4 contributors:
    * Jehan:  104 commits
    * Pat David: 38 commits
    * Robin Swift: 15 commits
    * Lukas Oberhuber: 1 commit
- **178 reports fixed** and **206 merge requests integrated** in our 2022
  releases. **Hundreds** more reports handled, triaged, answered to, worked on…
- Many patches contributed by GIMP contributors in various other projects we use
  (at least GLib, GTK, Cairo, meson, Mirrorbits…) and an uncountable number of
  issues reported by our contributors to other projects.
- And more!

Compared to last year:

- The total amount of work is quite similar, and while that tendency had already
  started a year ago, the work has clearly been shifting even more towards the
  development branch (future 3.0), which nows accounts for 84% of commits
  (against 74% last year), while the stable branch is really getting into
  maintenance-only mode.
- Less work on GEGL happened but more work on babl. The recent work on automatic
  LUT creation and SIMD optimizations explains it.
- ctx stays heavily developed.
- While Øyvind and myself still remain the 2 heavy-lifters, we get more people
  around clearly pulling their weight. It is exciting to see more contributors
  stay.
- Jacob is working more on the documentation which is really increasing in
  quality.

# Outstanding evolution in 2022
## babl and GEGL

On the side of our graphics engine, the [automatic LUT
creation](https://www.gimp.org/news/2022/02/25/gimp-2-99-10-released/#automatic-lut-creation-for-conversions-in-babl)
for color conversion in babl is clearly a big step forward, introduced in GIMP
2.99.10 (then in the stable version 2.10.32).

At the same time, all babl, GEGL and ctx got nice [SIMD
optimization](https://www.gimp.org/news/2022/02/25/gimp-2-99-10-released/#simd-builds-for-x86_64-and-arm-neon-ctx-babl-and-gegl)
which allowed nice performance boosts.

Øyvind Kolås is really doing an amazing job, as usual.

It is also interesting to note how the concept of "GEGL plug-ins" took off in 2022.
It in fact just refers to third-party GEGL operations which you simply install
in a folder and GIMP will see them at next restart, including all the fancy UI,
such as on-canvas preview with split view (and when we'll have non-destructive
layer effects, these operations will also be usable!).<br/>
Among people spearheading such community development, we should cite
[LinuxBeaver](https://github.com/LinuxBeaver?tab=repositories) and [Liam
Quin](https://gitlab.com/barefootliam/gegl-pango-markup).
For anyone interested, I suggest to read the 3-part
tutorial written by Liam (["Using GEGL
Plug-Ins"](https://barefootliam.blogspot.com/2022/10/gegl-plug-ins-for-gimp-part-one-using.html),
["GEGL
Graph"](https://barefootliam.blogspot.com/2022/12/gegl-plug-ins-for-gimp-part-two-gegl.html)
and ["Writing C
Plug-Ins"](http://barefootliam.blogspot.com/2023/01/gegl-plug-ins-for-gimp-part-three.html)).

## Checking items ✅ in GIMP roadmap

We proudly checked-off several items in the [GIMP 3.0.0
roadmap](https://developer.gimp.org/core/roadmap/#gimp-30-development-branch-roadmap).

Amoung 2022 achievements, we indeed…

- ✔ ported away from intltool to gettext only (*technical debt cleanup*);
- ✔ finished the meson build: the autotools build still exists but is now
  considered secondary;
- ✔ finished the last pieces for multi-layer selection (a move [started early
  2020](https://www.gimp.org/news/2020/11/06/gimp-2-99-2-released/#multi-layer-selection)).
  including rewriting completely the interaction in the formerly terrible [align
  and distribute tool](https://www.gimp.org/news/2022/11/18/gimp-2-99-14-released/#align-and-distribute-tool-fully-reworked-interaction).

These are 3 huge pieces in our roadmap which we happily marked as completed
(apart from probable bugs).

On the *getting closer* side:

* We nearly finished the "Less floating selection" move (some use cases remain,
  which we need to think about more).
* The Wayland support is still kinda wonky at times (even disregarding all the
  issues we cannot do anything about — such as color management not implemented
  yet in Wayland —, we have weird windowing issues), but it improved in 2022.
* The API work is really moving forward; Lloyd Konneker helped a lot on
  this.
* The GTK+3 port is nearly finished, as we are handling these days the last
  annoying warnings (though it's more a January 2023 thing!).
* Space invasion: good parts of it were done since the CMYK push made us look at
  specific pieces of code more in details. Though a lot still needs to be done
  and color science is at times a very head-scratching part of the work.

Now anyone following our [development news](https://www.gimp.org/news/) knows
that a lot more happened.
This report is not going to repeat what we already wrote about in various news
items.

One particular contributor to encourage this year is Nikc who came to us with a
few patches at first then proposed a [Google Summer of Code
project](https://www.gimp.org/news/2022/06/03/cmyk-in-gsoc-2022/), and decided
to stay around. Thanks to them, a lot happened for CMYK support in GIMP and our
"Space Invasion" project also moved forward further.
They are now a very prolific core contributor. This can only mean good stuff for
the future!

## Packaging

Clearly our **macOS** support has never been better: good continuous
integration, automatic DMG package creation, and now we even got an [Apple
Silicon package](https://www.gimp.org/news/2022/12/02/gimp-2.10.32-apple-silicon/)!
The quality of maintenance and updates for this package is outstanding.
Lukas Oberhuber is the one to thank for this. Yet the bus factor for our macOS
package remains extremely low so we always welcome more contributors.

On **Windows** side, GIMP is now officially distributed [on the Windows
Store](https://www.gimp.org/news/2022/06/18/gimp-2-10-32-on-microsoft-store/),
after getting contacted by a developer relations team at Microsoft. This is
great as too many non-trusted packages used to be distributed there and now they
seem to have mostly disappeared with the official one eclipsing them with its very
good rating.

On **Flathub** (GNU/Linux), the burden is getting lightened as we now got
automation in dependency version check, thanks to Ondřej Míchal. The flatpak
package team is also getting bigger, with 4 recurring contributors.

## Infrastructure

We also got some infrastructure changes, such as our mirroring system, now based
on *Mirrorbits*. This is something I am planning to talk again about soon, so I
won't go into details.

On community side, our mailing lists have been discontinued, together [with all
of GNOME mailing
lists](https://mail.gnome.org/archives/desktop-devel-list/2022-August/msg00004.html)
whose infrastructure we are on. We now
[recommend](https://www.gimp.org/discuss.html#forums) 2 forums for the
community:

* [forum hosted by pixls.us](https://discuss.pixls.us/gimp/), the community for Free/Open Source Photography.
* [forum hosted by the GNOME project](https://discourse.gnome.org/tag/gimp), the Free Software desktop by the GNOME Foundation.

## Websites

Our [documentation
website](https://www.gimp.org/news/2022/08/27/gimp-2-99-12-released/#documentation-website)
is getting a lot of love, thanks to Jacob Boerema, with automatic updates,
statistics showing… and of course, the contents is getting serious scrutiny to
improve documentation quality. Compared to 2021, there has been nearly double
the number of commits in 2022, which is revealing of the big step up.

Meanwhile we revived the [developers
website](https://www.gimp.org/news/2022/10/16/gimp-developer-website-rewrite/)
which was in a dire state for over 10 years.

We still have a pending project to port the main website to the *Hugo* framework
as well. Unfortunately this could not happen in 2022.

# Plans for 2023
## GIMP 3.0.0?

I should not give dates, so don't take it as a promise. Maybe it's just a
foolish dream by a foolish man:
I am currently planning **GIMP 3.0.0 release in 2023, or at least our first
release candidates**.

Here. I said it. If it doesn't happen, remember that it was not a promise. 😜

There is still a lot to be done, so I hope I'm not making a fool of myself. But
at some point, not being able to release just gets frustrating. Of course, we
are still within acceptable development durations (GIMP 2.8 to 2.10 took 6
years; we are still in the 5th year since 2.10) but I really want to get it over
with.

Now to get this deadline to work, I have decided to delay some elements out of
our [3.0 roadmap](https://developer.gimp.org/core/roadmap/#gimp-30). In particular:

* Extensions management: project dear to me as I started it and developed what
  is already implemented, yet to get a safe online infrastructure to handle
  extension search and download, we will need time.
* Paint Select tool: very awesome tool, but its contributor, Thomas Manni, is
  not happy with the performance (it requires instant canvas feedback to be
  usable) and is currently investigating alternative algorithms.

In the same time, I have been pushing aside some nice new code contributed to us
when I realize reviewing it and making back and forth corrections will take us
weeks. For instance, some of you may have seen the nice "[vector
layers](https://www.youtube.com/watch?v=7M8R_yU7RhM)" demo by Nikc (based on
work by Hendrik Boom and Jacob Boerema) on social networks. This won't make it
to GIMP 3.0.

This is a rule which I apply to my own code. Some people might indeed remember
my own [link layer experiments](https://www.youtube.com/watch?v=N5oyqbD7zyQ) for
instance, which I stopped working on 2 years ago, already for the same reason.

These will still happen, I'm only moving these targets away into further
releases, which I'm explaining in the next section.

## Rethinking our roadmaps

This leads me to an organizational work I've been doing lately on our roadmaps
and on planification of releases. Up to this day, you must have read a lot about
our bi-version planification: **GIMP 3.0** for GTK+3 port then **3.2** for
advanced non-destructive editing.

While this second target is still definitely a big plan in our roadmap, I don't
think that making it again a huge development cycle with dozens of features and
taking several years is the wisest thing. This old development model made sense
back in the day, but less nowadays in my opinion.

My goal for GIMP is to release more often, with faster development cycles, maybe
less features at once, yet nice features at each release. This is something I
had been pushing for, ever since 2014, when I was still a newcomer (I first
evoked that we should be able to publish new features even in micro versions in
a [meeting during LGM
2014](https://developer.gimp.org/conferences/lgm/2014-leipzig_germany/lgm_2014_minutes/#release-management)).
This ultimately led to our [release policy
change](https://www.gimp.org/release-notes/gimp-2.10.html#roadmap-and-whats-next),
starting from GIMP 2.10.0. And this is what I want to continue pushing further.

So my point is that targetting for a "GIMP 3.2" somewhere in the distant future
doesn't make sense anymore. The non-destructive editing features, such as
non-destructive layer effects, will happen, but will it be GIMP 3.2.0? Or some
3.0.x version instead?  We'll see. It's all just numbers anyway. We may likely
break this down in smaller releases in the end.

With this in mind, I reviewed our after-3.0 roadmaps into smaller pieces,
per logical categories of projects we want and which will definitely happen.

* Link and vector layers are now into a new ["non-destructive layer
  types"](https://developer.gimp.org/core/roadmap/#non-destructive-layer-types)
  category. The code is so well advanced that it would be a waste and while
  these won't make it to GIMP 3.0.0, it will definitely become one of the prime
  targets immediately after release. Maybe in GIMP 3.0.2?<br/>
  By the way, this also opens the door to the long-awaited shape features:
  with vector layers, we could have non-destructive shape drawing.
  I mean, on-canvas shapes should be a vector features to make it right!
* Non-destructive [layer
  effects](https://developer.gimp.org/core/roadmap/#non-destructive-filters)
  (formerly the main target for 3.2) is obviously a project on its own.
* [Macro](https://developer.gimp.org/core/roadmap/#macros-script-recording)
  support is also something we've wanted for a long time and with GIMP 3.0, we
  have started to lay the foundations for this feature. This should hopefully
  soon become a reality.
* [Animation
  support](https://developer.gimp.org/core/roadmap/#animation-and-multi-page-support),
  which as most of you know is something I've worked on for years, will have to
  be in GIMP someday. So it's also its own category. It will also bring
  multi-page support (not just layers as pages).
* Our [extension platform](https://developer.gimp.org/core/roadmap/#extensions)
  is still very much planned!
* The [Space Invasion](https://developer.gimp.org/core/roadmap/#space-invasion)
  project will continue: for 3.0, we focus on correctness of color models we
  already support; after 3.0, we might look into going further with new color
  models backends, such as core CMYK or L\*a\*b\* support, but also spot color
  channels and whatnot…
* We have now a bunch of unfinished [tools](https://developer.gimp.org/core/roadmap/#tools)
  in our playground area and it would be good if we took the time to finish
  them. Of course, we also have ideas for nice new tools. And finally there are
  tools which we really want to improve, such as our Text tool which deserves
  more love.
* Finally we have started to enhance the concept of
  "[canvas](https://developer.gimp.org/core/roadmap/#canvas)", with the "Show
  all" feature since [GIMP
  2.10.14](https://www.gimp.org/news/2019/10/31/gimp-2-10-14-released/#out-of-canvas-viewing-and-editing).
  We always wanted to go further, and also to rework the concept of layer
  dimension (e.g. with auto-growing layers, or even infinite layer abilities).
* …

And this is how I completely rewrote our roadmap page. Hopefully some people
will enjoy reading the new page and will find it exciting. Note that contents
didn't change that much, except that it has been reorganized to put more
emphasis on the bigger strokes for GIMP evolution after GIMP 3.0 release, making
it more obvious (hopefully) which direction current contributors are pushing
GIMP to go.

# Conclusion

This is where we are at. I'm expecting 2023 to be an eventful year. 2022 has
been quite awesome too, but also tiring to the point that there were weeks when
I couldn't work on anything, especially soon after coding bursts for releases. I
also focused a bit more on getting healthier work habits, such as working with a
height-adjustable desk (for sitting and standing work sessions) and doing
regular walks.

This is also why I work on procedures to get faster releases, better
infrastructure and better documentation for onboarding new contributors. I am
aiming for a more organized path while keeping the slightly 🌪️ chaotic ❤️‍🔥 core
which really makes working in our team so enjoyable. ☺️

As I was saying in last year's report, GIMP is not only a **Free Software**, it
is also a **Community Software**: random human beings doing something nice
together and sharing it with everyone. Why? Because we can, because we want. And
that's why I love our small community, with just the right amount of chaos and
insanity, sparkled with just the right amount of organization!

Finally don't forget you can [💌 donate to the project and personally fund several
GIMP developers](https://www.gimp.org/donating/), as a way to give back
and accelerate the development of GIMP. As you know, **myself as
maintainer of GIMP (through "ZeMarmot" project) and Øyvind as maintainer of GEGL
are crowdfunding the work this report is about.**
Any support is appreciated to help us succeed in such endeavour.

I wish you all a happy, funny 🥳 and healthy year 2023 and/or year of the rabbit 🐇!
