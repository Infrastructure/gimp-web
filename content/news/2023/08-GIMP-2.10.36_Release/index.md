Title: GIMP 2.10.36 Released
Date: 2023-11-07
Category: News
Authors: Jehan
Slug: gimp-2-10-36-released
Summary: Release news for version GIMP 2.10.36
Image: /downloads/downloadsplash-aryeom.jpg

This stable release of GIMP comes with a few security fixes, so we advise you to
update even if you feel like your current version works fine for you. Apart from
the many bug fixes and security updates, it also provides new support for
palette formats and a new generated gradient.

[TOC]

*This news lists the most notable and visible changes. In particular, we
do not list here bug fixes or smaller improvements.
To get a more complete list of changes, you should refer to the
[NEWS](https://gitlab.gnome.org/GNOME/gimp/-/blob/70ad9853928ed7887df3aaaa3b607d6b646c51ed/NEWS#L11)
file or look at the [commit
history](https://gitlab.gnome.org/GNOME/gimp/-/commits/gimp-2-10).*

## New features and improvements
### ASE and ACB palettes support

In addition to already supported palette formats, GIMP can now load palettes in
the following formats:

* **Adobe Swatch Exchange (ASE)**
* **Adobe Color Book (ACB)**

This will make it easier to exchange palettes coming from other software.

### New Gradient: FG to Transparent (Hardedge)

Everywhere a gradient option is available, the gradient list will now feature
the additional "*FG to Transparent (Hardedge)*" option. It generates a gradient
from the foreground color to transparency, with hard-edge transitions between
the 2 colors.

In the Gradient Tool in particular, you can generate patterns very quickly with
the "Repeat" option, alternating repetitive colored shapes with full transparency
over a given background.

<figure>
<iframe title="New &quot;FG to Transparent (Hardedge)&quot; gradient - GIMP
2.10.36" width="640" height="438"
src="https://peer.tube/videos/embed/92f104c1-51c3-436c-bf82-4c9673edc357"
frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts
allow-popups"></iframe>
<figcaption>
<em>New FG to Transparent (Hardedge) gradient - GIMP 2.10.36</em>
</figcaption>
</figure>

### GIF: non-square ratio support

GIMP now loads GIF images containing the `PixelAspectRatio` header metadata by
setting different resolutions per dimension, hence rendering the image correctly
(instead of looking squashed on the screen).

Of course the option "*Dot for Dot*" in the `View` menu must be unchecked to see
the image at its expected ratio.

### More enhancements

A few more improvements were sprinkled across this update, such as:

* Text tool: improved formatting behavior when selecting and changing text on
  canvas.
* Theme: better feedback when hovering lock buttons (with a white frame) as well
  as when activating a lock (a small padlock shows up in the corner).
* Help: The `Help > User Manual` submenu now features a "*[Table of Contents]*"
  link.

## Security and bug fixes
### Fixed Vulnerabilities

Four vulnerabilities were reported by the Zero Day Initiative in code for the
following formats and fixed immediately:

* DDS: ZDI-CAN-22093
* PSD: ZDI-CAN-22094
* PSP: ZDI-CAN-22096 and ZDI-CAN-22097

Additionally dependencies have been updated in our binary packages, and with
them, some vulnerabilities recently reported in these libraries were fixed.

In any case, we recommend to update GIMP with the latest packages.

### Broken Graphics Tablets with recent linuxwacom driver

We don't usually mention bug fixes prominently but an ugly one happened
recently after a change in the `xf86-input-wacom` (linuxwacom) driver, which
provoked crashes of GIMP when using a graphic tablet on Linux.

Various distributions already downgraded the driver, or backported the fix,
since a patch to the driver has been quickly pushed as well. Nevertheless if you
are in the unlucky situation of using the non-patched driver, this version of
GIMP also contains a workaround to the bug.

## Release stats

Since GIMP 2.10.34:

* 26 reports were closed as FIXED in 2.10.36.
* 10 merge requests were merged.
* 155 commits were pushed.
* 20 translations were updated: Belarusian, Catalan, Chinese (China), Danish,
  Dutch, Georgian, German, Greek, Hungarian, Icelandic, Italian, Lithuanian,
  Polish, Portuguese, Romanian, Slovenian, Spanish, Swedish, Turkish, Ukrainian.

29 people contributed changes or fixes to GIMP 2.10.36 codebase (order is
determined by number of commits):

* 7 developers: Alx Sa, Jehan, Stanislav Grinkov, Jacob Boerema, Daniel
  Novomeský, Andras Timar and Gabriel Scherer.
* 22 translators: Marco Ciampa, Sabri Ünal, Luming Zh, Anders Jonsson, Yuri
  Chornoivan, Martin, Rodrigo Lledó, Balázs Úr, Hugo Carvalho, Jürgen Benvenuti,
  Nathan Follens, Piotr Drąg, Alan Mortensen, Cristian Secară, Ekaterine Papava,
  Jordi Mas, Vasil Pupkin, Aurimas Černius, Danial Behzadi, Petr Kovář, Sveinn í
  Felli and dimspingos.
* 3 resource creators (icons, themes, cursors, splash screen, metadata…):
  Stanislav Grinkov, Jehan, Daniel Novomeský.
* One documentation contributor: Jehan.
* 3 build or CI contributors: Jernej Simončič, Jehan and Stanislav Grinkov.

Contributions on other repositories in the GIMPverse (order is determined by
number of commits):

* babl, GEGL and ctx are actively developed, but no releases have accompanied
  this version of GIMP for once. So we will provide relevant statistics at next
  release.
* The `gimp-2-10` branch of `gimp-macos-build` (macOS build scripts) had 45
  commits since 2.10.34 release by 1 contributor: Lukas Oberhuber.
* The stable flatpak branch had 28 commits since 2.10.34, by 3 contributors (and
  a bot): Jehan, Daniel Novomeský and Hubert Figuière.
* Our main website (what you are reading right now) had 165 commits since
  2.99.16 release by 6 contributors: Sabri Ünal, Jehan, Bruno Lopes, lillolollo,
  Alx Sa and Robin Swift.
* Our [developer website](https://developer.gimp.org/) had 17 commits since
  2.99.16 release by 5 contributors: Jehan, Bruno Lopes, Aryeom, Jacob Boerema
  and Robin Swift.
* Our [2.10 documentation](https://docs.gimp.org/) had 138 commits since 2.10.34
  release by 16 contributors: Andre Klapper, Jacob Boerema, Marco Ciampa, Anders
  Jonsson, Boyuan Yang, dimspingos, Yuri Chornoivan, Jordi Mas, Rodrigo Lledó,
  Martin, Alexander Shopov, Alx Sa, Balázs Úr, Piotr Drąg, Sabri Ünal and Tim
  Sabsch.

Let's not forget to thank all the people who help us triaging in Gitlab, report
bugs and discuss possible improvements with us.
Our community is deeply thankful as well to the internet warriors who manage our
various [discussion channels](https://www.gimp.org/discuss.html) or social
network accounts such as Ville Pätsi, Liam Quin, Michael Schumacher and Sevenix!

*Note: considering the number of parts in GIMP and around, and how we
get statistics through `git` scripting, errors may slip inside these
stats. Feel free to tell us if we missed or mis-categorized some
contributors or contributions.*

## Team news and release process

Access rights to the `git` repository were recently given to Lukas Oberhuber
(our maintainer for the macOS packages).

During the duration of
[GSoC](https://www.gimp.org/news/2023/03/20/gimp-in-gsoc-2023/), "reporter"
rights on our Gitlab project were given to Idriss and Shubham, 2 of the GSoC
contributors (the third one already had git access).

Robin Swift, who already helped with [GIMP's developer
website](https://www.gimp.org/news/2022/10/16/gimp-developer-website-rewrite/)
has started working on a port of the main website (which you are reading right
now) from Pelican to Hugo, a project which was long planned yet had stalled so
far.

Finally we remind that we are [actively looking for people helping us
test](https://www.gimp.org/news/2023/07/09/gimp-2-99-16-released/#team-news-and-release-process)
packages before releases (especially for GIMP 3.0 and forward). This will help
make GIMP releases much more robust. Since the last release, Anders Jonsson and
Mark Sweeney were added as Flatpak testers. We also have several testers of the
Windows packages, yet we still have no testers for macOS.
Whatever your OS and the architecture you test on, we welcome your feedback to
detect issues early! Together, the community is stronger! 💪

## Around GIMP
### Mirror news

Since our latest news, 4 new
[mirrors](https://www.gimp.org/donating/sponsors.html#official-mirrors) were
contributed to GIMP by:

* *Silicon Hill*, student club of the Czech Technical University in Prague,
  Czech Republic;
* *Lancaster-Lebanon IU13*, an organization comprised of more than 20 public
  school districts and several non-public, parochial, and charter schools in
  Lancaster, Pennsylvania, USA;
* the *Moroccan Academic and Research Wide Area Network* (MARWAN) in Rabat,
  Morocco;
* Jing Luo, in Tokyo, Japan.

This brings us to a total of 45 mirrors so far, from all over the world.

Mirrors are important as they help the project by sharing the load for dozens of
thousands of daily downloads. Moreover by having mirrors spread across the
globe, we ensure that everyone can have fast download access to GIMP.

### Book news

Sabri Ünal continued their 📚 bibliographic research, adding so many published
books that we decided to completely reorganize the books as a structured file
database, allowing us to easily process the information or change the page
styling separately from the data.

This also triggered us to split the books page into 2:

* [Recent Books About GIMP](/books/)
* [Ancient Books About GIMP](/books/older.html)

As book descriptions don't always clearly state the version of GIMP they pertain
to, we used the release date of GIMP 2.10.0 (April 27, 2018) as the split date.

Last but not least, this new structure allows us to easily generate statistics,
which we now show at the bottom of the books pages. At least 44 books were
published after GIMP 2.10.0 release, and 305 were published before it.
Therefore we are currently listing a grand total of 349 books about GIMP in 17
languages!

We remind everyone that we welcome book additions. If you know (or even are the
author) of a not-listed-yet book about GIMP, please [report the same information
as other books in the
list](https://gitlab.gnome.org/Infrastructure/gimp-web/-/issues/new).
Thanks!

## Downloading GIMP 2.10.36

You will find all our official builds on [GIMP official website
(gimp.org)](https://www.gimp.org/downloads/):

* Linux flatpaks for x86 and ARM (64-bit)
* Universal Windows installer for x86 (32 and 64-bit) and for ARM (64-bit)
* macOS DMG packages for Intel hardware
* macOS DMG packages for Apple Silicon hardware

<del>Note: *macOS packages are a bit late but will come shortly.*</del>

Other packages made by third-parties are obviously expected to follow
(Linux or \*BSD distributions' packages, etc.).

## What's next

I believe it might be the next to last release in the 2.10 branch, though of
course, this is still to be confirmed. What may happen in real life does not
always align with plans.

In the meantime, we are working harder than ever to release GIMP 3.0. You will
hear shortly about this in our next development release.

Don't forget you can [donate and personally fund GIMP
developers](https://www.gimp.org/donating/), as a way to give back and
**accelerate the development of GIMP**.
Community commitment helps the project to grow stronger! 💪🥳
