Title: GIMP in GSoC 2023
Date: 2023-03-20
Category: News
Authors: Jehan
Slug: gimp-in-gsoc-2023
Summary: GIMP is again a Google Summer of Code mentor organization in 2023
Image: /news/2020/10/07/gimp-2-10-22-released/202010-wilber-and-co.jpg

This year [again](https://www.gimp.org/news/2022/03/08/gimp-in-gsoc-2022/), GIMP
project got selected as [mentor
organization](https://summerofcode.withgoogle.com/programs/2023/organizations/gnu-image-manipulation-program)
in the *Google Summer of Code*.

[**Applications by contributors are opening today (Monday, March 20, 2023) and
will close on Tuesday, April 4, 2023.**](https://summerofcode.withgoogle.com/)

## Project ideas

On our new developer website, we [listed a few
ideas](https://developer.gimp.org/core/internship/ideas/) which might be
suitable for a GSoC. These range from core color science projects to UX
improvements, build system updates or even making a website for our future
extension platform.

Obviously this list of ideas is far from exhaustive and we definitely welcome
your propositions. Even better, if you have great ideas of your own, it may
play in your favor, as long as they are realistic projects which can be finished
within GSoC timeframe, or at least broken down in usable parts.

## Requirements

As already explained last year, and again in our [internship
page](https://developer.gimp.org/core/internship/), if you want to participate,
some of the most important requirements are:

* **Get familiarized with our code by fixing a few patches beforehand.** You
  don't have to work on extra-complicated bugs or features at first (reports
  labelled
  "[Newcomers](https://gitlab.gnome.org/GNOME/gimp/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=4.%20Newcomers&first_page_size=20)"
  are probably good first-patch targets), nor does it absolutely need to be
  related to the topic of your planned project. We mostly need to interact with
  you on a technical topic as a first approach.
* **Communicate!** Don't just drop your project out of the blue on the GSoC
  interface (several people did this last year). Come and discuss your project
  ideas with us [on IRC](https://www.gimp.org/discuss.html#irc-matrix). You may
  also open a [report on our issue
  tracker](https://gitlab.gnome.org/GNOME/gimp/-/issues) detailing your
  proposition.
