Title: GIMP Help Manual 2.10.34 Released
Date: 2023-03-05
Category: News
Authors: Jacob Boerema
Slug: gimp-help-manual-2-10-34-released
Summary: Release news for GIMP Help Manual version 2.10.34
Image: /downloads/downloadsplash-aryeom.jpg

GIMP Help Manual 2.10.34 is finally out, with many documentation improvements
and more supported languages.

[TOC]

*This news lists the most notable and visible changes. In particular, we
do not list here the bug fixes.
To get a more complete list of changes, you should refer to the
[NEWS](https://gitlab.gnome.org/GNOME/gimp-help/-/blob/c60ec22595fcdeb1093b53032af2e55c8cd06c53/NEWS)
file or look at the [commit
history](https://gitlab.gnome.org/GNOME/gimp-help/-/commits/gimp-help-2-10).*


## Introduction

The release of a GIMP Help manual has been long overdue. We published a 2.10.0
test release, but that was never meant to be an official release.
Due to the lack of volunteers and the amount of documentation needing updates,
this test release eventually became the de facto first 2.10 release, although
it was outdated in a lot of places and missing documentation for newer
features.

There still is more work to be done, but after a long period of hard work, the
manual is finally in a state where we can present you a new official release.


## Online Manual

Not that we want to discourage you from using this release version of the
manual, but the online manual is being updated daily. It should generally be
your first choice, unless you have limited internet bandwidth or other reasons
to prefer the offline version.

We modernized our website to be more in line with the main GIMP website and
improved the information about our manuals. New automated builds are published
to our website once a day. Even better, our website now shows the completion
status for each language.
A lot of languages still need considerable work. If you would like to help
improve that, please visit
[https://docs.gimp.org/help.html](https://docs.gimp.org/help.html)
for more information.


## Translations

Several new translations are being worked on: Czech (restored), Hungarian,
Portuguese, Ukrainian. In addition to these, installers are now also
available for Slovenian and Swedish, which were missing from the 2.10.0 test
release.
Persian was added too, but since no actual translations for the 2.10.34 manual
were made yet, we do not supply an installer for that.
Almost all other languages were updated to some degree.


## Documentation Updates

A large part of the manual has seen updates. In some cases only small updates,
but many pages have seen considerable changes.

Some highlights are:

* All GEGL filters, some of which were not documented at all, are now covered.
* The new layer modes introduced in GIMP 2.10.0 are finally documented,
  including examples of each mode.
* The getting stuck section was updated and extended to cover more problematic
  situations.
* Missing preferences were added, others that are no longer there were removed.
* The Script-Fu tutorial got revised.
* Context sensitive help ids in GIMP were synchronized with the manual. This
  means that it is now a lot less likely you will encounter a missing help page.

Most parts of GIMP's interface should now be documented. If you see anything
that is still missing or that could be improved, don't hesitate to
[open a documentation issue](https://gitlab.gnome.org/GNOME/gimp-help/-/issues).


## Downloading GIMP Help Manual 2.10.34

GIMP Help Manual 2.10.34 is available on [GIMP's documentation website
(docs.gimp.org)](https://docs.gimp.org/download.html) in two formats:

* A source distribution
* Windows installers for each available language


## What's next

Since GIMP is getting closer to a 3.0 release, we need to update our
documentation to add all changes that have been made compared to GIMP 2.10,
and of course we will also keep updating the 2.10 manual.
To make this effort more manageable, we would really welcome more helping
hands.

