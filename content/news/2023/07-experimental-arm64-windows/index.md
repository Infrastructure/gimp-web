Title: GIMP now on Windows for ARM (experimental)
Date: 2023-08-13
Category: News
Authors: Jehan
Slug: experimental-windows-arm-installer
Summary: GIMP's Windows installer now supports ARM 64-bit architecture.
Image: /downloads/downloadsplash-aryeom.jpg

As architecture platform usage widens, Windows on ARM (64-bit) is now a thing.
So we decided to support experimentally GIMP for Windows on ARM!

With the newly published revision 2, our universal installer of GIMP 2.10.34 for
Windows (as found on our [downloads page](https://www.gimp.org/downloads/))
will auto-detect the running platform and install the ARM build when relevant.

Thanks in particular to our Windows packager, Jernej Simončič, for his
continuous work!

## Future work

The "**experimental**" qualificative for this new support is for the following
reasons:

1. It is not as widely tested. We are aware of some issues already and hope that
   releasing this experimental build will help us get more feedback.
2. Only Jernej has a machine with Windows on ARM so far. In particular none of
   the developers have such hardware, as far as we know. So we don't expect to
   be able to fix issues for Windows/ARM as fast as for other supported
   platforms.
3. Last, but not least, this additional build is not set up yet in our
   continuous integration platform, which means we cannot discover appearing
   issues as thoroughly and quickly as for other architectures, nor can we
   automatize builds as transparently as we wish.

## How you can help

Aside from [reports](https://gitlab.gnome.org/GNOME/gimp/-/issues) and
[patches](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests), we really need
to set up a Windows/ARM machine in our continuous integration platform.
Indeed this is considered a blocker and may be cause for abandoning the
experimentation when we release GIMP 3 since we don't want to backtrack and get
back to manual builds done by a single contributor on their personal machine for
the 3.0 series.

This means that we are looking for anyone willing to help us set up a machine
with Windows on ARM and configure it as a runner on [our Gitlab
project](https://gitlab.gnome.org/GNOME/gimp/).

Because of obvious security requirements, such a volunteer would need to have
sysadmin experience, willing to commit themselves in the long run (let's not
leave a Windows machine with holes on the internet) and have had some experience
in FLOSS contributions.

It might also be interesting to coordinate with other cross-platform Free
Software projects to share the administration burden of a CI runner which we can
use together to build for Windows/ARM.

If you are interested, please get in touch on
[IRC](https://www.gimp.org/discuss.html#irc-matrix) or in the [dedicated
report](https://gitlab.gnome.org/GNOME/gimp/-/issues/9170).
