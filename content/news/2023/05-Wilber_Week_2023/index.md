Title: Wilber Week 2023: report
Date: 2023-06-29
Category: News
Authors: Jehan
Slug: wilber-week-2023
Summary: The meeting of GIMP developers is back!
Image: WW23-GIMP-hacking-sailing-boat.jpg

With the unfortunate health situation of past years, GIMP team had not been
able to meet since 2019. This affected the software evolution (commit numbers
have been divided by about half!) because for many of us, GIMP is more than a
software: it's people, it's a community. So motivation shrank by lack of social
encounter.

Therefore we are glad to announce the return of *Wilber Week*: our week-long
meeting of GIMP contributors (started [back in
2017](https://www.gimp.org/news/2017/01/18/wilberweek-2017-announced/) as a
companion to the [Libre Graphics Meeting](https://libregraphicsmeeting.org/)).

A month ago, we had our second Wilber Week in Amsterdam!

<figure>
<img src="{attach}WW23-GIMP-team.jpg"
     alt="Wilber Week 2023: GIMP team"/>
<figcaption>
<em>Wilber Week 2023: GIMP/Inkscape contributors (from left to right: Niels, Mitch, Simon, Liam, Ville, Aryeom, Jehan, Øyvind, Chris and Schumaml) — photo by Niels, CC by-sa</em>
</figcaption>
</figure>

[TOC]

# Setting up

This year, 10 GIMP contributors showed up, by alphabetical order:

* [Aryeom](https://film.zemarmot.net/): *ZeMarmot*'s film director, 11-year
  contributor, graphics contributor, UI design, alpha-tester…
* [Carlos Garnacho](https://blogs.gnome.org/carlosg/): long-time contributor and
  advisor for anything input device, GTK or GNOME related, contributor and
  maintainer for various important bricks in GNOME.
* [Jehan](https://film.zemarmot.net/): myself, 11-year contributor, GIMP
  co-maintainer, *ZeMarmot* technical side…
* [Liam Quin (demib0y)](https://www.fromoldbooks.org/): 15+-year contributor, community
  helper, working to keep GIMP community friendly and welcoming…
* [Michael Natterer (mitch)](https://www.gimp.org/news/2017/03/01/an-interview-with-michael-natterer-gimp-maintainer/):
  26+-year contributor, my genius co-maintainer…
* [Michael Schumacher (schumaml)](https://www.gimp.org/news/2017/05/15/an-interview-with-michael-schumacher-gimp-administrator/):
  20+-year contributor, administrator, brilliant triager, community helper
  and more…
* [Niels de
  Graef](https://nielsdg.pages.gitlab.gnome.org/development-blog/about/):
  4+-years contributor, big contributor as well in GNOME, GTK and more…
* [Øyvind Kolås (pippin)](https://pippin.gimp.org/): 20+-year contributor, GEGL
  maintainer, digital media toolsmith…
* [Simon Budig (nomis)](https://www.home.unix-ag.org/simon/):
  25+-year contributor, cares about keeping GIMP a nice community forever…
* [Ville Pätsi (drc)](https://shadowdrama.net/): 22+-year contributor, photographer,
  graphics contributor…

Additionally we invited 2 Inkscape contributors. What started as a simple
[toot](https://fosstodon.org/@CmykStudent/110251080501454428) on Mastodon
transformed into a private discussion with Martin Owens from Inkscape who was
hoping to discuss color management with us. So we invited them to enjoy our
hacking retreat and discuss further!

In the end, [Marc Jeanmougin](https://inkscape.org/~MarcJeanmougin/), Inkscape
developer, and [Chris Rogers](https://inkscape.org/~C.Rogers/), graphics
contributor, spent the week with us!

We were all lodged in a fricking century-old sailing boat. No joke! That was an
insanely cool place where we could start hacking from day one!

<figure>
<img src="{attach}WW23-GIMP-hacking-sailing-boat.jpg"
     alt="Wilber Week 2023: hacking in a sailing boat"/>
<figcaption>
<em>Wilber Week 2023: hacking in a sailing boat (from left to right:
Schumaml, Mitch, Jehan, Carlos, Marc) — photo by Niels, CC by-sa</em>
</figcaption>
</figure>

About the city itself, let me state for the record that, as a vegan and
pro-soft transportation, Amsterdam seems like a very nice place to live
in!

# Blender Foundation headquarters

The Blender Foundation gracefully lent workshop and meeting rooms to our team.

<figure>
<img src="{attach}WW23-gimp-team-blender-hq.jpg"
     alt="Wilber Week 2023: GIMP contributors entering Blender headquarters"/>
<figcaption>
<em>Wilber Week 2023: GIMP contributors entering Blender headquarters — photo by Schumaml, CC by-sa</em>
</figcaption>
</figure>

Of course, having "desks" was not our real reason to choose this office. It was
very cool to meet Blender teams. We were also able to have various interesting
discussions. Quite notably, [Nathan
Vegdahl](https://projects.blender.org/nathanvegdahl) from Blender was extremely
welcoming and showed us a lot of very cool stuff!

As was expected, we discussed about color management, in particular in Wayland
as Sebastian Wick, major contributor for color management in Wayland was pulled
in a few times (thanks to Niels!) through remote video calls. This was very
constructive!

<figure>
<img src="{attach}WW23-gimp-inkscape-blender-wayland-color-management-meeting.jpg"
     alt="Wilber Week 2023: meeting with GIMP, Inkscape, Blender and Wayland contributors in Blender headquarters"/>
<figcaption>
<em>Wilber Week 2023: meeting with GIMP, Inkscape and Wayland contributors in
Blender headquarters (left to right: Liam, Øyvind, Nathan, Marc, Jehan, Mitch,
Simon, Niels; and Sebastian on screen) — photo by Aryeom, CC by-sa</em>
</figcaption>
</figure>

Bottom line: the interactions with Blender folks made the trip quite worthwhile!

In the same time, there were more things I was hoping to discuss, such as better
file exchange and interactions between our programs (think *Libre Graphics
Suite*, a major gripe we have at *ZeMarmot* project as we work with all these
software and it's not always easy!).
There was already so much going on that this didn't happen. Hopefully the
opportunity will come again!

# Immediate consequences
## Bug fixing! *Mitch is back!*

This was a very packed week for hacking on GIMP, fixing bugs, improving long
overdue code and so on.
A huge part of this was thanks to the fact that we got our co-maintainer back,
[Michael Natterer, a.k.a.
mitch](https://www.gimp.org/news/2017/03/01/an-interview-with-michael-natterer-gimp-maintainer/)!

We missed him dearly and it's so good to have him looking over our code once
more, as well as hacking frenziedly until late at night, like the old times!

<figure>
<img src="{attach}WW23-mitch-asleep-table.jpg"
     alt="Wilber Week 2023: the maintainer sleeps"/>
<figcaption>
<em>Wilber Week 2023: where has been mitch for 2 years? Turns out he was just sleeping… — photo by Jehan, CC by-sa</em>
</figcaption>
</figure>

Of course, a lot of other old-timers came back to code for the occasion, so
let's not forget them all!

## Improvements

Among the many things which happened during this very eventful week (or as
direct consequences), let's mention:

* Simon Budig should be commended for fixing warnings, cleaning code and
  updating code away from deprecated API!
* Niels de Graef and Carlos Garnacho helped with various GTK- and
  Wayland-related fixes. This also resulted in patches in GTK or other
  dependencies, not only in GIMP.
* The plug-in API got seriously worked on, adding support of `GBytes` as plug-in
  arguments, improving the new `GimpResource` class and subclasses allowing
  plug-ins to easily manipulate various data (brushes, dynamics, patterns…) and
  more.
* Autotools is finally gone from our main repository! (though it is still
  present in the stable branch)
* Our Continuous Integration now shows JUnit reports from meson unit tests.
* Ville is getting used to improving our themes: he did the 2.10 ones, now again
  he helped on the Default 3.0 theme, improving work started by other
  contributors.
* As a direct result of *Wilber Week*, Carlos implemented, soon after, pad
  customization ability to GIMP (with a [very nice write-up on this
  work](https://blogs.gnome.org/carlosg/2023/06/16/getting-the-best-of-tablet-pads/)).
  As review will take some time, it won't be in 2.99.16 though will definitely
  end up in GIMP 3.0!
* Aryeom worked on an updated logo, with the help of various GIMP contributors
  (in particular Ville, Øyvind and Simon) as well as Chris from Inkscape. This
  is still work-in-progress.
* Some improved GEGL integration discussion and work happened during the week,
  then continued after, allowing to easily add [third-party GEGL operations in
  GIMP's menu](https://floss.social/@GIMP/110193618328736185) and search for
  them in the [action search](https://floss.social/@GIMP/110163688457663102)
  (note: implementation changed since these toots; not all operations end up in
  menus now, only when a specific metadata is present in the operation).
* Aryeom updated the splash screen for the next development version (to be
  continued…).
* While they couldn't be present unfortunately, we shouldn't forget Jacob
  Boerema, Alx Sa and others who continued to improve GIMP remotely in the same
  time!
* Since we had 3 projects selected in [GSoC
  2023](https://www.gimp.org/news/2023/03/20/gimp-in-gsoc-2023/) with Liam and
  myself as mentors, we had GSoC meetings as remote calls with the students.

## Dropping bitcoin donation method

We have had a bitcoin address on the website. Some people have asked for more
crypto-currency options. With a rise in scams, high energy use and differing
national tax implications, we have decided — after discussion and a vote during
Wilber Week — to no longer feature a bitcoin donation link.

The donations in bitcoin have been received, some of them used, but we are still
working on how to properly channel these funds towards our expenses.

# Making plans
## A foundation?

It turns out that we have been interviewed by Pablo Vazquez while in Blender's,
so the cat is out of the bag in a quite public way now: we have been trying to
set up our own entity. But first, since I teased you, here was the interview:

<figure>
<iframe title="GIMP's Wilber Week 2023 at Blender HQ"
src="https://video.blender.org/videos/embed/13d47d70-ffd4-4846-b00e-029f99a69aa6"
allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"
width="560" height="315" frameborder="0"></iframe>
<figcaption>
<em>Wilber Week 2023: GIMP's Wilber Week 2023 at Blender HQ, by Pablo Vazquez (featuring Simon, Jehan and Mitch)</em>
</figcaption>
</figure>

In case you wonder, the slides can be found
[here](https://download.gimp.org/users/jehan/WW23-eof.pdf), they were taken from
a end-of-event presentation I gave to Blender folks.

<figure>
<img src="{attach}WW23-Jehan-eof-talk.jpg"
     alt="Wilber Week 2023: the EOF talk"/>
<figcaption>
<em>Wilber Week 2023: the <abbr title="End of File">EOF</abbr> talk —
photo by Aryeom, CC by-sa</em>
</figcaption>
</figure>

Making a proper entity for GIMP is something which has been on my mind for many
years and which I started to discuss with others of the team, and with friends
from other non-profits to help me find the best way, since 2019! After some
hiatus on this project, I revived my work on it late 2022, and we are actually
quite advanced, though I will refrain on giving too much details now in order
not to jinx it.

Let's see how it pans out!

Now something to be clear about: GIMP has always been a bit of a messy and
friendly community project. And that's part of what I like about it: this bit of
anarchy. Whatever we build to support the project, I will always fight for this
spirit to live on. This was in fact one of the difficult part of setting up an
organization and why it took so long: doing so without the organization taking
over the project, but instead as a support to the community.

## GIMP 3 and onward! ⛵

Clearly this Wilber Week made me trust that my initial plan (outlined in the
[2022 report](https://www.gimp.org/news/2023/01/29/2022-annual-report/#gimp-300)
as hoping to have GIMP 3.0 release candidates this year) should be possible. If
we can keep the community as lively, there is high chance to see this happen.

We are clearly sailing in exciting times, right now, toward a very cool future! 😄

# What's next

For anyone interested, the [meeting page on the developer
website](https://developer.gimp.org/conferences/wilberweek/2023-amsterdam/)
gives a bit more details on what happened, what was actually discussed, meeting
notes, etc.

Right now, we are deep into preparing the release of the next development
version of GIMP (GIMP 2.99.16). And while it's not even out, we are already
quite excited about the next one (which might even be a release candidate in the
best case!).

In the meantime, do not forget you can [donate and personally fund GIMP
developers](https://www.gimp.org/donating/), as a way to
give back and **accelerate the development of GIMP**.
Community commitment helps the project to grow stronger! 💪🥳
