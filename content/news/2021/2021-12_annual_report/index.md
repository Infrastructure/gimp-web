Title: 2021 annual report
Date: 2021-12-31
Category: News
Authors: Jehan
Slug: gimp-2021-annual-report
Summary: What happened in 2021?
Image: 2021-12-wilber-and-co-year-end-party.jpg

While we are ending 2021, let's sum up the year. For my first year as
co-maintainer, I thought it would be a good idea to write this report in
my name so that I can give personal impressions of how it is to work on
GIMP and what it means to me.

<figure>
<img src="{attach}2021-12-wilber-and-co-year-end-party.jpg" alt="Hello 2022 - Wilber and co. comics strip by Aryeom"/>
<figcaption>
<em>"Hello 2022" by <a href="https://film.zemarmot.net">Aryeom</a>, Creative Commons by-sa 4.0 - GIMP 2021 annual report</em>
</figcaption>
</figure>

[TOC]

# Statistics

In 2021, we had:

- **4 stable releases** (GIMP 2.10.24, 2.10.26, 2.10.28 and 2.10.30)
- **2 development releases** (GIMP 2.99.6 and 2.99.8).
- **1179 commits** on the unstable development branch (2.99.x, future
  3.0) and **407 commits** on the stable development branch (2.10.x) of
  the main repository.
- **91 contributors** on the main repository, including (some people belong
  to several categories):
    * 41 developers
    * 42 translators
    * 24 contributors to resources (icons, themes, in-code
      documentation) or build improvements.
- 22 people contributed more than 10 commits in the main repository,
  among which 2 contributors did more than 100 commits (Jacob Boerema
  and myself), among which only one (myself) did more than 500.
- **247 commits** to GIMP's website (`gimp.org`, i.e. right here) by 14
  contributors.
- **31 commits** to [babl](https://gegl.org/babl/) by 6 contributors.
- **229 commits** to [GEGL](https://gegl.org/) by 33 contributors.
- **1179 commits** to [ctx](https://ctx.graphics/) by 3 contributors
  (mostly Øyvind Kolås).
- **255 commits** in `gimp-help` (our manual), whose main contributor is
  Jacob Boerema who is doing an awesome work reviving it.
- **53 commits** in `gimp-macos-build` (our repository for the macOS build)
  by 4 contributors (mostly by Lukas Oberhuber who took over
  maintainance of the macOS package).
- **185 reports fixed** in our 2021 releases and **hundreds** more
  handled, triaged, answered to, worked on…
- Many patches contributed by GIMP contributors in various other
  projects we use (GLib, GTK, Cairo, GExiv2 and others… We don't keep
  track) and an uncountable number of issues reported by our
  contributors to other projects.
- **Helping** (or **getting helped** by) other Free Software when we
  can, e.g. the very nice [Siril](https://siril.org/) project for
  astronomical image processing and other software, because unlike what
  some think, we are not in a market or a competition! We all work
  together to make a better graphics work environment.
- And more!

In the end, that's quite a lot of work proudly brought to you by GIMP.
As you may notice, we have quite some contributions, yet the core work
is still actually done by a handful of people as most contributions
are one-off (out of the 91 contributors, 69 contributed less than 10
commits, and among these 51 contributed a single commit).

I want to commend [Jacob Boerema](https://www.jacobboerema.nl/en/) in
particular who is the biggest contributor this year on the stable
branch, while I must admit I mostly focus on the development branch and
sometimes tend to neglect a bit the stable branch 😒! Thanks Jacob! 🤗

And we should never forget `babl`, `GEGL` and the new project `ctx` by
Øyvind Kolås as these constitute the core of GIMP imaging engine and are
considered as much a part of GIMP project as the interface itself.

# Building a lovely team

You might have noticed a regular section for the last few news titled
"*Team news*" where we list changes in the team, in particular new
contributors who are given more access to the tracker or the source
repository. I have been trying to be more and more proactive into
integrating people into the core team.

Indeed as you saw in the statistics, Jacob Boerema is the only other
contributor who did more than 100 commits in 2021, while I did a bit
over 500. So I want to improve this ratio and increase the *bus factor*.

GIMP team has always been very welcoming, at least ever since I started
contributing, back in 2012 and this is even why I stayed back then. I
want to perpetuate this tradition. My goal is to identify faster the
people to give more responsibility to (note that technical skills are
important but social skills — in other words being a good person and
nice to others — is my priority checkbox). Well it's definitely an evil
trick 🦹 to lessen my own burden, but I also expect this to make it way
more fun 🎡 contributing to the project (based on personal experience)!

Therefore let me give special props to Jacob Boerema for his tireless
work on file format support and more, Niels de Graef for his invaluable
help and good expertise with GTK, Luca Bacci for his very nice work on
input device support, helping on Windows and his GTK expertise, Daniel
Novomesky for making HEIF/AVIF and JPEG-XL first-class formats…

Let's not forget recurring contributors such as Massimo Valentini, Lloyd
Konneker… (what would we do without these people never giving up on GIMP,
years after years?!) and promising newcomers like Stanislav Grinkov.

Now let's applaud our packagers: Jernej Simončič has been around in GIMP
for as long as I could remember, flawlessly making Windows installers
like a solid rock to rely on; macOS history is bumpier yet Lukas
Oberhuber has been doing an outstanding work lately so I sure hope he'll
stay around; on Flatpak side, Hubert Figuière helps quite a lot too (and
I <del>secretly</del> 🤫 hope he will end up taking over me maintaining our
stable, beta and nightly flatpak-s!).

At the end of the day, GIMP is much bigger than just developers, it's a
community. What would we do without people helping for the website, bug
triaging, infrastructure and more? And let's not forget the translators,
so many of them! I just love all of you! Sorry that I cannot just name
everyone (in case I forgot you, don't take it the wrong way, there are
just so many awesome people around!).

What I like to tell everyone is that GIMP is both a community software
and Free Software, or simply a **Community, Free Software**. This double
concept is extremely important to me and this is why I love GIMP and why
both Aryeom and I (from [ZeMarmot projet](https://film.zemarmot.net/),
from which our heavy-lifting contributions really started) stuck with
it. This is about humans meeting each others and trying to do something
nice together (even though each's personal end goal might be different).
Everything works wonderfully as long as we remember to be good to each
other. 🤗

Therefore **to all contributors (of any specialties) who helped GIMP so
far, I want to say a huge thank you! GIMP is what it is thanks to you!**
🙏

# GIMP 3.0 approaching

With 4 development versions released already, you know that we are
working very hard on the future: GIMP 3.0.

Some features took a lot of time, mostly when we changed core logics. I
am thinking in particular about the code for multi-selection of layers.
It's not that selecting multiple items in a list is hard to implement,
it's that any feature in the whole application has been forever
expecting just one layer or one channel selected. So what happens when
there are 2, 3 or any number of items selected? Every feature, every
tool, every plug-in and filter has to be rethought for this new use
case. This is a huge work and it has been 2 years I have been on and off
on this one in between porting or developing other code and reviewing
contributors' code. Fortunately this change is nearing the end lately
(not completely finished though).
So that's a great progress.

By the way, a part of this work has been to get rid of the "link" (chain
⛓ icon in the `Layers` dockable) concept in favor of multi-selection
(and layer search and storage as a replacement concept for the ability
to save layer links). This part is also done now. I'll talk more about
this in the GIMP 2.99.10 release news.

<figure>
<img src="{attach}2021-annual-report-multi-layer-replace-link.gif" alt="Linked layer concept replaced by layer search"/>
<figcaption>
<em>Link concept replaced by multi-layer search, lock icons moved - GIMP dev branch</em>
</figcaption>
</figure>

Among other blockers which I [listed a year
ago](https://www.patreon.com/posts/what-remains-to-40087754), we are
steadily progressing on our GTK3 port and Wayland support as well as
stabilizing the plug-in API. I do hope these will be considered in a
good enough state soon enough so that we can consider having a release
candidate.

On the other hand, the [space
invasion](https://www.gimp.org/news/2020/11/06/gimp-2-99-2-released/#space-invasion)
has been a bit on the slow pace in 2021 so this is definitely one topic
we need to get back full-on very soon. Same for the [extension
platform](https://www.gimp.org/news/2020/11/06/gimp-2-99-2-released/#extensions).

# GEGL, babl and ctx

The core 🫀⚙️  of modern GIMP is [GEGL](https://gegl.org/), a library project
nearly as old as GIMP itself, by the same core of people, even though
the first [tentative integrations only happened in GIMP
2.6](https://www.gimp.org/release-notes/gimp-2.6.html#gegl), and since
then slowly making its way to be the main engine behind most pixel
manipulation in the software.

GEGL development has been slowing down a bit since 2019, but mostly
because it is becoming stabler by the day, which is really when things
are getting good, solid and interesting.
Now it would still be unfair to forget talking about the recent support
of the `CMYK` color model in GEGL, which means we are a step closer to
get some support in GIMP itself.

Another exciting thing is the new project Øyvind Kolås has been working
on these days: [ctx](https://ctx.graphics/), a vector graphics library.

Now I know it may not sound as useful when you develop a raster graphics
application, but there are still a lot of intersecting topics. One of
these topics is the graphics interface itself which is usually rendered
out of vector primitives. In GTK case, the rendering is going through
Cairo. Øyvind has been working a lot to both render [nicer and
faster](https://www.patreon.com/posts/gegl-vectors-ctx-53188476) than
Cairo, or similarly, on many cases. `ctx` also has color-management
thought into the framework from the ground up as a first-class citizen.

Of course `ctx` is still heavily under work as can be seen by the
intense commit rate so we'll have to see where it goes, but this is for
sure exciting since Øyvind is a well-proven excellent R&D developer.

There are other areas where `ctx` is useful, such as the few GEGL
operations with vector components which have already been ported to this
new library (e.g. `gegl:fill-path`) and text itself is usually rendered
through vector shapes (so who knows what might happen when we improve
text support?). GIMP is not going to refocus on vector graphics anytime
soon, but we may definitely get more vector-related features as we go
(anyone who follow a bit *ZeMarmot*'s work knows that we are really
looking into improved ways for SVG integration for instance, such
as in my early, not merged yet, [link layer
experiment](https://www.patreon.com/posts/gimp-dev-report-39631718)).
When we do more vector work, `ctx` will definitely be a top contender.

I can already hear Øyvind telling me that `ctx` is actually much more
than these few areas I summed up here. So let me be sorry in advance,
Øyvind! This is why this report is in my name, taking into account my
own limitations in understanding your bigger plans, and looking forward
to be pleasantly surprised and amazed in the future! 🤯

# Infrastructure, Documentation

In all software projects, there is a constant which is mostly invisible,
yet extremely important: infrastructure.

You might have noticed we did speak a bit more than usual about this in
2021 because it has really been something which used to bother me in my
early years contributing to GIMP. I always thought we needed more
robust, automated and transparent builds (which is getting there for
[Windows](https://www.gimp.org/news/2021/10/20/gimp-2-99-8-released/#automated-release-installers),
[macOS](https://www.gimp.org/news/2021/12/06/gimp-2-99-8-for-macos/)
and Linux with Flatpak), better [download mirrors
handling](https://www.gimp.org/news/2021/10/06/official-mirror-policy/),
better [continuous
integration](https://www.gimp.org/news/2021/10/20/gimp-2-99-8-released/#continuous-integration-changes)
in general, better end-user documentation (Jacob is on it! And we have
plans to get a more automated release and online testing policy of GIMP
manual, which should happen in 2022)…

We also had some work done in 2021 around developer documentation:
[Akkana Peck](https://shallowsky.com/) (well known for having written
books on GIMP) and Lloyd Konneker helped set up some initial
documentation to port plug-ins and script from 2.10 to 3.0. Akkana also
jump-started generating API references for the Python and Javascript
bindings (with `g-ir-doc-tool`). Then very recently Niels De Graef
migrated our generic API documentation generation from `gtk-doc` to
`gi-docgen`, producing much more modern web documentation for plug-in
developers. None of these are online yet, only built within the source
repository for the time being. Getting an online update procedure for
these is also on the `TODO` list.

<figure>
<img src="{attach}2021-annual-report-API-reference.png" alt="New API documentation - GIMP dev branch"/>
<figcaption>
<em>New API documentation for plug-in developers - GIMP dev branch</em>
</figcaption>
</figure>

All these topics take a lot of time and are also necessary to get a much
nicer experience using GIMP. So I am already quite proud of what
we did in 2021 and really excited about our 2022 plans.

# Plans for 2022

You might wonder now: when will GIMP 3.0 be released?

Nope sorry, as always, we don't answer such question. 😛
What we can say is that we are hard at work for this to happen and for
sure I'd like it to be earlier rather than later.

As said above, apart from the code itself, I also want us to get our
new online manual infrastructure improved before the release, but also
our extension framework ready as well as a brand new developer website
with documentation and more.
So the plans are actually quite extensive and it's not only within GIMP
code itself (though code definitely needs more work too).
We'll see how things go from here!

Don't forget you can [donate to the project and personally fund several
GIMP developers](https://www.gimp.org/donating/), as a way to give back
and accelerate the development of GIMP. As you know, myself as
[maintainer of GIMP (through *ZeMarmot* project) and Øyvind as
maintainer of GEGL are crowdfunding to be able to work full-time on free
software](https://www.gimp.org/news/2021/07/27/support-gimp-developers-sustainable-development/).
Any funding is appreciated to help us succeed in such endeavour.

I wish you all a nice new year eve and a wonderful year 2022. 🥳
