Title: GIMP 2.10.30 Released
Date: 2021-12-21
Category: News
Authors: Wilber
Slug: gimp-2-10-30-released
Summary: A release of bug fixes and backend evolutions.
Image: /news/2018/11/08/gimp-2-10-8-released/wilber-co-Aryeom_stability.png

GIMP 2.10.30 is once again mostly a bugfix release, with many fixes and
incremental improvements, not so much on the "new feature" side.

# Improved file formats support

Several file format supports were updated with fixes or improvements:
AVIF, HEIF, PSD, DDS, RGBE and PBM.

Let's highlight in particular:

* **PSD** support received various types of improvements allowing it
  to load more sub-cases of PSD: layer masks tagged with invalid
  dimensions, CMYK without alpha, CMYK without layers, merged image of a
  16 bit per channel RGBA which had opaque alpha channel etc. In case
  of CMYK PSD files, for now, GIMP will convert to sRGB to allow to view
  the contents of a file rather than display a message that the file
  is not supported.
* **AVIF** export now favors AOM encoder.

# OS evolutions

Some backends got reworked to follow OS evolutions:

* Backport of selection outline drawing reimplementation from GIMP
  2.99.8 for **macOS Big Sur** and over ([already discussed
  patch](https://www.gimp.org/news/2021/12/06/gimp-2-99-8-for-macos/#fixed-regressions-in-recent-macos-versions-selection-outlines-and-slowness),
  itself exceptionnaly backported to the 2.10.28 DMG package so that
  macOS users enjoyed visible selections earlier).
* On Windows, we moved from `GetICMProfile()` to the
  `WcsGetDefaultColorProfile()` API because the former is broken in
  **Windows 11**. Thus we were failing to get monitor profiles.
* On Linux and other OSes which might use Freedesktop portals, we
  implemented color picking on display (`Colors` dockable) with the
  Freedesktop API when available, keeping old implementations as
  fallbacks.
  Screenshot plug-in now also uses in priority the Freedesktop API
  rather than specific KDE or GNOME API (which are getting restricted
  for security reasons since **KDE Plasma 5.20** and **GNOME Shell
  41**).

# Other improvements

Various improvements were made on metadata support, be it on core code
as well as in the metadata plug-ins (viewer and editor).

A noteworthy fix is that the text tool won't follow anymore the subpixel
font rendering choice from system settings. Subpixel rendering is for
GUI on a screen of a specific type and pixel order and is not suitable
for image contents which can be zoomed in or out, showed on various
screens or even printed. This change also depends on a patch we
contributed to Cairo which will be available in their next release (we
include the patched version in our flatpak).

# In brief

To get a more complete list of changes, you should refer to the
[NEWS](https://gitlab.gnome.org/GNOME/gimp/-/blob/05227c6889481e2ea973f81ccfa172150e057850/NEWS#L11-71)
file or look at the [commit
history](https://gitlab.gnome.org/GNOME/gimp/-/commits/gimp-2-10).

*Core code contributors*:
: Adam Fontenot, Jacob Boerema, Jehan, Jordi Mas, Niels De Graef, Øyvind
Kolås and Yoshinori Yamakawa.

*Plugin code contributors*:
: Daniel Novomesky, Jacob Boerema, Jehan and Niels De Graef.

*Build contributors*:
: Daniel Novomesky, Jehan, Jernej Simončič, Øyvind Kolås, Niels De
Graef and Yoshinori Yamakawa.

# Team news

Luca Bacci is now a core developer with git access, acknowledging his
very nice contributions so far.

Lukas Oberhuber, our new macOS packager, was given "*reporter*" access
on the main source repository which allows him to help triaging on the
bugtracker: labelling, closing, reopening, and moving reports…

# Translators

Among the 82 languages for which GIMP is available, 14 translations were
updated: Brazilian Portuguese, British English, Catalan, Croatian,
Finnish, Italian, Latvian, Lithuanian, Polish, Portuguese, Slovenian,
Spanish, Swedish and Ukrainian.

The Windows installer now contains Portuguese localization (from
Portugal, it already had Brazilian Portuguese), making it available in
35 languages.
Also some previously announced translations were missing in the
installer. These should be fixed now.

*Translators on GIMP 2.10.30*:
: Anders Jonsson, Aurimas Černius, Bruce Cowan, Bruno Lopes da Silva,
Daniel Mustieles, Hugo Carvalho, Jiri Grönroos, Jordi Mas, Marco Ciampa,
Matej Urbančič, Milo Ivir, Piotr Drąg, Rodrigo Lledó, Rūdolfs Mazurs and
Yuri Chornoivan.

# GEGL

As usual, this release is supplemented with the release of
[GEGL](https://gegl.org/) 0.4.34 the same day as GIMP 2.10.30.

There have been some build related improvements, such as moving the
implementation of [ctx](https://ctx.graphics/) from the main GEGL
library to one of the runtime loadable operation bundles. In operations
the robustness of `gegl:ripple` and the `gegl:magick-load` powered
fallback have been improved.

*Contributors to the GEGL release*:
: Anders Jonsson, Asier Sarasua Garmendia, Boyuan Yang, dimspingos,
Gavin Cui, Hugo Carvalho, Jehan, Jordi Mas, krzygorz, Lukas Oberhuber,
Marco Ciampa, Matej Urbančič, Øyvind Kolås, Rodrigo Lledó, Rūdolfs
Mazurs and Simon McVittie.

# Downloading GIMP 2.10.30

As usual GIMP 2.10.30 is available on [GIMP official website
(gimp.org)](https://www.gimp.org/downloads/).

The Linux flatpak, Windows installer and macOS DMG package are already
available and nearly all mirrors fully synced up, in less than a day
from source release to binary release. This may be our new record of a
perfectly timed release!

# What's next

While the development branch is getting most activity, we don't forget
the stable branch. This release contains several fixes which we really
wanted to get out there, so we recommend everyone to update.

We are not sure we will be able to do a development release before the
end of the year, so we wish you already a wonderful holiday season!
🎄🎉

Don't forget you can [donate to the project and personally fund several
GIMP developers](https://www.gimp.org/donating/), as a way to give back
and accelerate the development of GIMP. As you know, the [maintainers of
GEGL and GIMP are crowdfunding to be able to work full-time on free
software](https://www.gimp.org/news/2021/07/27/support-gimp-developers-sustainable-development/). 🥳
