Title: GIMP 2.99.8 macOS package now available
Date: 2021-12-06
Category: News
Authors: Wilber
Slug: gimp-2-99-8-for-macos
Summary: The current development version of GIMP is finally available as a macOS package!
Image: /downloads/downloadsplash-aryeom.jpg

The current development version of GIMP is finally available as a macOS
package! 🥳

» **Download it on [gimp.org "Development Downloads"
page](https://www.gimp.org/downloads/devel/)** «

So what's happening with GIMP on macOS?

## Fixed regressions in recent macOS versions (selection outlines and slowness)

Ever since the Big Sur release a year ago, macOS users have been
[experiencing regressions](https://www.gimp.org/news/2020/12/25/gimp-2-10-22-released-macos/#whats-going-on-with-big-sur) such
as invisible selection outlines (a.k.a. "marching ants" 🐜) and general lack
of responsiveness. These would happen for everyone using GIMP on macOS Big Sur
or newer, whatever GIMP version (some people reported these issues even for old
GIMP 2.8 packages). This was caused by changes in the operating system itself.

The slowness issue had been mostly worked around by Des McGuinness, our
previously very active macOS package maintainer, though GIMP's responsiveness
may still not be on par with other platforms.

We recently fixed the [selection outline visibility issue](https://www.gimp.org/news/2021/10/20/gimp-2-99-8-released/#selection-cue-fixed-on-wayland-and-macos)
as well and then **backported the fix to GIMP 2.10.28 DMG package**! Therefore
we encourage everyone still having this issue to use the latest package, either stable 2.10.28 or the unstable 2.99.8.

## Team news: new package maintainer

Our new macOS package contributor, Lukas Oberhuber, has been working
very hard to improve the packaging scripts, clarifying the process by
merging two repositories in one, getting rid of various useless files and
build rules and directly using modules of the upstream `gtk-osx` project
(when possible) rather than duplicating rules.

He simplified our [gimp-macos-build](https://gitlab.gnome.org/Infrastructure/gimp-macos-build)
repository a lot, which should hopefully lower the entry barrier.

Finally, he fixed several bugs and environment code directly in the GIMP
codebase to bring our development code up-to-date with macOS support.

As a consequence, Lukas is now officially a new co-maintainer of the
macOS package. Welcome and congratulations, Lukas! 🥂

## New contributors are still very very welcome 🤗

There are still serious macOS-specific bugs in both the stable and the
development series. They are being worked on by Lukas who is supported by the
core team, yet there are limits to what a single person is able to achieve.

For at least the past 10 years, GIMP has not been able to have two active
macOS developers at once. In fact, this is the second time this year that we
publish a [macOS-only](https://www.gimp.org/news/2020/12/25/gimp-2-10-22-released-macos/)
news post, which is not a good sign at all. It means being able to release
improvements for this operating system is exceptional enough that we feel like
we should inform people.

So we re-iterate our call for contributors. Do you like GIMP? Do you use
macOS and have technical knowledge and skills for packaging and/or
development? Then please consider joining us!

We've detailed the building process in the `README.md` file of the
[gimp-macos-build](https://gitlab.gnome.org/Infrastructure/gimp-macos-build) repository. This is the first thing potential contributors ask us about.

Once you make sense of our build scripts and are able to compile your
own local copy of GIMP, please talk to  `Jehan` or `lukaso` on [IRC (#gimp
channel on GIMPnet - irc.gimp.org)](https://www.gimp.org/discuss.html#irc),
though others may be able to help too of course. If you already have specific
solutions to existing problems, we can push your patch to a branch and generate
a new DMG build for testing.

## What's next

As usual, we hope that macOS support will improve and that we won't feel
the need to make dedicated news again. In the end, it depends on macOS
users, since GIMP is a full community Free Software. It is what we all
make of it, together. The community has many Linux and Windows
developers. We also get regular \*BSD and even Hurd patches (seriously).
How come we barely get any macOS developers?

Having M1 support is also a topic we have been discussing obviously. It
will likely require more stable contributions for this to happen. So come in
and contribute, everyone, it might be cold ❄ outside, but it's warm 🔥
in here.

Finally, please don't forget you can [donate to the project and personally fund
several GIMP developers](https://www.gimp.org/donating/) as a way to
give back and accelerate the development of GIMP.
