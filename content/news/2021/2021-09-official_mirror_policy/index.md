Title: GIMP's official mirrors and mirror policy
Date: 2021-10-06
Category: News
Authors: Wilber
Slug: official-mirror-policy
Summary: "Support GIMP with a mirror"
Image: /images/2021-wilber_and_co-mirror.jpg

As far as we could remember, organizations from all over the world have
supported the GNU Image Manipulation Program by mirroring 🪞 our file
downloads. This is important as we may have to sustain dozens of
thousands downloads a day.

<figure>
<img src="/images/2021-wilber_and_co-mirror.jpg" alt="Mirrored Wilber - Wilber and co. comics strip"/>
<figcaption>
<em>"Mirrored Wilber" by <a href="https://film.zemarmot.net">Aryeom</a>, Creative Commons by-sa 4.0</em>
</figcaption>
</figure>

# Official mirrors (to publication day)

The current list of official mirrors by alphabetical order:

- Aalborg University
- Academic Computer Club, Umeå University
- Artfiles New Media GmbH
- Astra ISP
- CSC - IT Center for Science / FUNET
- Cu.be Solutions
- Dmitry Shishkin
- dogado GmbH
- eScience Center, Nanjing University
- Friedrich-Alexander-Universität Erlangen-Nürnberg (FAU)
- Göttingen University and Max Planck Society
- Interdisciplinary Centre for Mathematical and Computational Modelling UW, University of Warsaw
- IP-Connect LLC
- Korea FreeBSD Users Group
- Jaleco
- Kumi Systems
- Lysator ACS
- nbshare.io
- Studenten Net Twente (SNT), the University of Twente
- UKFast
- University of Crete
- University of Kent UK Mirror Service
- University of Maryland, College Park
- XMission

*One more mirror request is being processed at the moment.*

The always-updated list is on a dedicated [sponsor
page](/donating/sponsors.html). The image above poetically
representating the mirrors, by Aryeom of [*ZeMarmot*](https://film.zemarmot.net/)
project, will also illustrate this sponsor page.

💌 **Thanks to all these organizations!**

# What are official mirrors?

We recently cleaned out our mirror list per these rules:

- We now list exactly the mirrors verified by the infrastructure team,
  no more no less. Being verified gives you rsync credentials allowing
  faster and safer updates.
  **See [below](#how-to-be-an-official-mirror) if you want in.**
- Mirrors are not listed anymore in the [Downloads](/downloads/) page
  since there is an automatic mirror rotation operating when clicking
  the download buttons. Instead we have the dedicated
  [Sponsors](/donating/sponsors.html) page, which means better
  visibility to sponsoring mirrors, all the while simplifying the
  download page which used to be over-crowded with information.
- We display names of mirroring organization, for instance "*University
  of XYZ*" with a link to the organization website, rather than just
  URLs.

*Note: we had more mirrors either listed on our former download page or
who made reports of interest to be official mirrors across the
years (back then our workflow was much fuzzier). We sent messages to
all of them but are not sure it reached the people in charge. If you
wish to be part of the official list, help shoulder the download burden
and be featured on our sponsors page, please read the procedure below.*

# How to be an official mirror

This is the same
[procedure](https://wiki.gnome.org/Infrastructure/Mirrors) as GNOME
mirrors, except you must explicitly ask to be a GIMP mirror:

1. Create a mirror request [on the Infrastructure
   tracker](https://gitlab.gnome.org/Infrastructure/Infrastructure/issues/new?issuable_template=new-mirror)
2. Write in title and introduction that you want to be a mirror for GIMP
   in particular
3. Add `/cc @Jehan` in the request description or in a comment
4. Fill the relevant fields from the `new-mirror` template
5. If you want a specific name and URL to be shown in the mirror [sponsor
   list](/donating/sponsors.html), please write it explicitly in the
   report (otherwise we will choose whatever organization name and URL
   seems the most relevant from what you wrote down)
6. Once we are done verifying your organization, rsync credentials will
   be exchanged securely, allowing you to sync your mirror with the
   source server
7. There is nothing to do in particular to appear on the
   [Sponsors](/donating/sponsors.html) page which will be updated
   regularly through scripts. Yet it may take a few days or even weeks at
   times. So don't worry if your organization name does not appear
   immediately!

🛈 We programmatically check at random intervals that mirrors are updated
quickly enough and that the data match for obvious security reasons.

# Technicalities

We used to list mirrors on
[gimp.org/downloads/](https://www.gimp.org/downloads/) on a declarative
basis. People wishing to download were encouraged to pick a mirror
rather than the main download server.

Years ago, we moved to an automatic mirror rotation through web
redirections. In other words, any URL to `download.gimp.org/mirror/`
automatically redirects to any of the configured mirrors (our own
download server included), hence lowering the burden on all servers
without having people to manually pick a mirror in a boring list.

Yet followup checks were not thorough enough, there were no clear
procedure to give mirror administrators and there were discrepancies
between mirrors listed on the website and the ones actually taking part
in download rotation. These are the changing points.

Listing only verified mirrors has the following advantages:

- The infrastructure team makes the background check to verify an
  organization is **trustworthy**.
- Mirrors get rsync credentials, enabling **faster** updates (instead of
  through third-parties which may themselves sync from other third
  parties, hence avoiding several-day delays).
- Our team has an accurate list of mirrors for systematic **security**
  checks of file presence and authenticity. We will react quickly in
  case of problem.
- Only verified mirrors are added in the download rotation settings,
  thus **contributing** with the bigger chunk of downloads. It is only
  normal for them to be featured prominently.

More discussions are going on to improve the procedure and mirror
technology even further by using the *Mirrorbits* software. More on this
later (and after more extensive testing).

**Additionally to mirrors, we would like to 🙏 thank the GNOME
infrastructure team for their invaluable help!**

# Contributors

The work on mirrors was a long run background job which was improved by
various people, notably [Michael
Schumacher](https://testing.gimp.org/news/2017/05/15/an-interview-with-michael-schumacher-gimp-administrator/)
and [Pat David](https://patdavid.net/).

These recent improvements to the mirror procedure, making it faster and
safer for people downloading GIMP and easier for contributors as well as
for mirror administrators, were done by GIMP co-maintainer, Jehan of
*ZeMarmot* project ([Liberapay](https://liberapay.com/ZeMarmot/) and
[Patreon](https://www.patreon.com/zemarmot) fundings). The goal is to
simplify and automatize as much as possible the process to make our list
of mirrors always up-to-date, more reliable downloads, but also to
verify the mirrors are safe by running automated data integrity
verification scripts.
