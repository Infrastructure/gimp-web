Title: GIMP 2.10.24 Released
Date: 2021-03-29
Category: News
Authors: Wilber
Slug: gimp-2-10-24-released
Summary: "The geographer release, stability and consolidation"


GIMP 2.10.24 is mostly a bug fix release, which once again comes mostly
with file format support improvements.

Release highlights:

- Off-canvas point snapping
- GeoTIFF metadata support (georeferencing information embedded within a
  TIFF file used by map makers)
- Many improvements in the metadata viewer and editor
- Many file format supports improved: HEIF, PSP, TIFF, JPEG, PNG, PDF,
  DDS, BMP, PSD
- New "*Negative Darkroom*" operation to simulate enlargement prints
  from scans of photographic negatives.
- The RAW image import now handles darktable 3.6 and over
- New Kabyle translation

<figure>
<img src="{attach}202103-wilber-and-co.jpg" alt="Wilber the geographer - Wilber and co. comics strip"/>
<figcaption>
<em>"Wilber the geographer" by <a href="https://film.zemarmot.net">Aryeom</a>, Creative Commons by-sa 4.0 - GIMP 2.10.24 gets GeoTIFF support</em>
</figcaption>
</figure>

# Core improvements
## Off-canvas point snapping

Though there are various fixes in core code, this change is probably the
most worth mentionning. Ever since GIMP 2.10.14, we are now able to see
the [out-of-canvas
area](https://www.gimp.org/news/2019/10/31/gimp-2-10-14-released/#out-of-canvas-viewing-and-editing).
Consequently, many features can now work outside the canvas, yet not all
features yet. This change is the continuation of this work, allowing you
to snap various tools to guides, grids or vectors, even outside the
canvas.

<figure>
<img src="{attach}gimp-2.10.24-snap-off-canvas.gif" alt="Snapping to guide off-canvas - GIMP 2.10.24"/>
<figcaption>
<em>Snapping to guide/grid/vectors off-canvas made possible - GIMP 2.10.24</em>
</figcaption>
</figure>

# Metadata support

A lot of work has been going on in the metadata area, mostly
consolidating our support and fixing many issues.

The metadata viewer and editor also received a lot of love, making them
more robust to various edge cases, such as duplicate tags, but also
mapping equivalency between similar IPTC and XMP tags, better encoding
handling, and so on.

The GPS data is also handled a bit better with more precision, tooltips
and better formatting.

There are still a lot of improvements to be made in the metadata area
though we are probably on the right path. This part of the development
is not as visible as other, yet this is very time-consuming and
thankless groundwork while being useful to anyone who needs good
metadata support so we'd like to thank [Jacob
Boerema](https://www.jacobboerema.nl/en/) who has worked tirelessly on
this for months.

# File formats
## GeoTIFF

A fun story which started with a [conference by Adam Cox of Louisiana
State University](https://www.youtube.com/watch?v=N1G0qyETCow) about
using GIMP for enhancing historic maps, with the issue that GeoTIFF
metadata tags were lost and made the workflow a bit more cumbersome.

It prompted a bug report then later a patch by the passing contributor
Ruthra Kumar and a review by the core team. All this within 2 months.

And now GIMP is able to import and export back the GeoTIFF tags. Note
that no semantic logics is implemented, i.e. that GIMP can only export
what it imported (the checkbox will only be sensitive on export if there
was GeoTIFF metadata on import). It will not tweak the metadata contents
for you. In particular since it contains georeferencing data, some type
of image transform could make the data meaningless. This is up to you to
know what the data references and how to keep its meaning.

<figure>
<img src="{attach}gimp-2.10.24-save-geotiff.png" alt="Save GeoTIFF metadata as imported - GIMP 2.10.24"/>
<figcaption>
<em>Save GeoTIFF metadata as imported - GIMP 2.10.24</em>
</figcaption>
</figure>

This nice little story shows once again a power of Free Software, which
is before all a software made by yourself. Anyone who contributes is
part of the GIMP team! 🤗

*Note: the sharpest mind may have realized the feature was available in
the development release 2.99.4. Yet we add the description for 2.10.24
because this is the first stable release featuring GeoTIFF support.*

## Improved support for many image formats

Similarly to our [previous stable
release](https://www.gimp.org/news/2020/10/07/gimp-2-10-22-released/),
our file format plug-ins received a lot of love.

* **TIFF** got various improvements when handling multi-page files, but also
  many edge cases, such as 2 or 4-bit TIFF images, opening some types of
  non-conformant TIFF files and so on.
* **HEIF** got some visually lossless export support when libheif 1.10 or
  later is used. We also detect separately HEIC and AVIF support at
  runtime, allowing to build the plug-in with only support of one
  encoding.
* **PNG** now ignores useless layer offset of 0, a metadata which some
  third-party software are always storing, hence getting rid of
  unecessary dialog prompts.
* **JPEG** will better warn the user when some metadata saving failed.
* **BMP** in more bit depth can now be loaded, in particular 24bpp BMP
  images; moreover GIMP is now able to rescue some non-conformant BMP
  with wrong compression noted in header.
* **PDF** import now proposes an option to reverse order of layers (same as
  we already had on export) and now support fractional DPI import.
* **DDS** in BC5 format benefited from some fixes. Moreover as we are
  able to detect some images with errors previously created by GIMP, the
  software will also automatically fix these errors upon loading them.
* **Raw** image formats are still forwarded through [featureful raw
  developers](https://www.gimp.org/release-notes/gimp-2.10.html#digital-photography-improvements)
  such as [darktable](https://www.darktable.org/) or
  [RawTherapee](https://www.rawtherapee.com/). The former is undergoing
  some API changes, and while darktable 3.6 is not even out yet, GIMP
  already has support for this upcoming version. Therefore GIMP 2.10.24
  will work with future darktable.

# New translation

GIMP is now available in one more language: **Kabyle**. This is still an
early translation as only [18% of the stable branch is
translated](https://l10n.gnome.org/languages/kab/gnome-gimp/ui/) so far
(and 32% of the development branch!) yet we can already thank these new
translators to bring GIMP to even more people.

This makes GIMP available to 82 languages other than default English!

Translators are also contributors doing an incredible work, even though
their work doesn't always get the visibility and acknowledgement they
deserve. Thanks to all of them!

# GEGL and babl

As usual, this release is supplemented with the releases of
[babl](https://gegl.org/babl/) 0.1.86 and [GEGL](https://gegl.org/)
0.4.30.

## Changes in short

Our pixel encoding and color space conversion engine, babl 0.1.86, now
supports creating babl space from input-class ICC profiles and improves
thread safety.

On its side, GEGL 0.4.30 made improvements to its test suite and to
the following operations: `jpg-load`, `png-load`, `tiff-load`,
`rgbe-load`, `color-reduction`, `fattal02` and `paint-select`. This
later operation in particular was introduced for the new [Paint Select
tool](https://www.gimp.org/news/2020/12/25/gimp-2-99-4-released/#new-experimental-paint-select-tool)
so we will have the opportunity to talk about the improvements on the
next development release news.

## Negative Darkroom

Additionally to the other improvements, a new very interesting operation
appears in GEGL, contributed by Jonny Robbie: `negative-darkroom`.

This operation is for artists who use hybrid workflow technique of
analog photography. After scanning a developed negative, this operation
is used to invert the scan to create a positive image by simulating the
light behaviour of darkroom enlarger and common photographic papers.

<figure>
<img src="{attach}gimp-2.10.24-negative-darkroom.jpg" alt="Negative Darkroom in GEGL 0.4.30 / GIMP 2.10.24"/>
<figcaption>
<em>Negative Darkroom operation in GEGL 0.4.30 / GIMP 2.10.24</em>
</figcaption>
</figure>

As all GEGL operations are automatically detected and made available by
GIMP, this new operation can be used in GIMP 2.10.24 through the generic
GEGL tool (menu `Tools > GEGL Operation…` then select "*Negative
Darkroom (negative-darkroom)*" in the dropdown list).

Creating a custom dedicated dialog for this operation has been raised
and may happen in an further version of GIMP to even more improve the
usage and experience.

## babl minimum requirement in GEGL and GIMP

Meanwhile babl minimum requirement in GEGL has been downgraded to 0.1.78
(same as in GIMP) because newer versions require too recent `meson`
build tool, which is unfortunately still not available on some
distributions. In order not to prevent people from benefiting from a
newer version of GEGL and GIMP, we refrain on purpose to bump the
minimum requirement for a bit even though we highly encourage every
packager to use the last version of babl when possible. Many fixes and
improvements were also made available in recent versions.

# Downloading GIMP 2.10.24

As usual GIMP 2.10.24 is available on [GIMP official website
(gimp.org)](https://www.gimp.org/downloads/):

* The Linux flatpak has already been published so that anyone who
  installed it previously should have an update proposed by their
  software manager (or from terminal: `flatpak update org.gimp.GIMP`).

  *Note: our flatpak now supports only `x86-64` and `AArch64` (i.e. the
  64-bit variants of the x86 and ARM architectures). In particular `i386`
  (32-bit x86) had been dropped quite some time ago by the Freedesktop
  runtime we depend on. This is now the `ARM` (32-bit) support which has
  been dropped (even though 32-bit hardware is still being released or
  often 64-bit board computers are sold with a 32-bit OS). We tried to
  hold back a bit, for more than 6 months even, but now that the older
  runtime we used is unsupported, updating is the only sane choice.*

  *For the record, our i386 flatpak is therefore stuck at GIMP 2.10.14
  and our ARM flatpak is stuck at GIMP 2.10.22 with a few thousands
  downloads for this last version of GIMP i386 and a bit more than 400
  for the last version of GIMP ARM.*

* The Windows installer is already available. Most mirrors have picked
  it up, but some still haven't. So if the download fails, just try to
  click the `Download` button again.

* The macOS DMG package will be published in the next few days once our
  packager can make the time.

# What's next

The development continues very strong on the development branch and we
can clearly see the shift towards more work on GIMP 3 as 2.10.x release
become more about robustness and less about new features (though we
still continue to backport features when it can be done without too much
additional work).
We will give more details on this side of development when we will
release the upcoming 2.99.6 development version.

Finally, please don't forget you can [donate to the project and personally
fund several GIMP developers](https://www.gimp.org/donating/), as a way to
give back and accelerate the development of GIMP.
