Title: Funding GIMP developers for sustainable development
Date: 2021-07-27
Category: News
Authors: Wilber
Slug: support-gimp-developers-sustainable-development
Summary: GIMP is developed by its community - support the humans behind

GIMP has been developed as a community effort since very early on, after
its original authors left the project.
This begs the question of sustainability when contributors wish to stay
longer while not being able to afford being penniless volunteers
forever.

We have seen skilled developers come and go for years, the latter
becoming a growing concern. Contributing takes a crazy amount of time
and people have family, work and other responsibilities to take care of.
Thus when core team contributors are willing to be paid for making Free
Software, we have decided that GIMP as a project should encourage such
endeavours by putting more emphasis on their funding.

There are currently 2 such crowdfunding projects. **You can consider
these crowdfundings as "official" as can be and completely endorsed by
the GIMP project**:

* [Øyvind Kolås](https://www.patreon.com/pippin): **GEGL maintainer**
  (GIMP's Image Processing framework).
  [¹](#yvind-kolas-gegl-maintainer)
* [ZeMarmot project](https://www.patreon.com/zemarmot): Jehan (**GIMP
  maintainer**) and Aryeom (**artist, designer** and more…).
  [²](#zemarmot-gimp-maintainer-and-libre-art)

For more information, please read below.

# Øyvind Kolås: GEGL maintainer

Øyvind has been contributing to GIMP since 2004. He soon became
[GEGL](https://gegl.org/) maintainer, for what was meant to become one
day GIMP's image engine. GEGL and babl (its companion pixel format
encoding library) already support color management, higher bit depths
and CMYK; some of the non-destructive capabilities are already exposed
as part of GIMP's on-canvas preview of image filters.
The integration of GEGL started in [GIMP
2.8](https://www.gimp.org/release-notes/gimp-2.8.html#gegl) with a wider
port towards color management happening in
[2.10](https://www.gimp.org/release-notes/gimp-2.10.html). It is still a
work in progress, with plans for deeper integration of existing and
future capabilities of GEGL in future versions of GIMP as they continue
to get refined and tested.

Øyvind is known not only for his work on GEGL itself, but also for his
various experiments around images and colors. One of his visual
experiments with [color grids on grayscale
images](https://www.redsharknews.com/post-vfx/item/6540-this-image-is-black-and-white-no-really)
even got viral back in 2019 and spread all over the web on too many news
outlets to name them all.

<figure>
<img src="{attach}sc-low-saturation.webp" alt="Color assimilation grid illusion, by Øyvind Kolås"/>
<figcaption>
<em>Color assimilation grid illusion, by Øyvind Kolås</em>
</figcaption>
</figure>

One of his later endeavours is [ctx](https://ctx.graphics/), a vector
graphics stack, which is probably hard to describe as some parts of this
project are a bit mind blowing, so let's give you the official
description:

> ctx is a media-rich terminal and a vector graphics stack, built around
> a textual serialization protocol. The textual serialization has a
> corresponding binary representation that is used by the core
> rasterizer as well as sharing data among threads. The ctx protocol is
> available as an extension to ANSI escape sequences in the ctx terminal
> - the terminal launched when running the ctx binary with no arguments.
> This extension allows scripts in any language to output vector
> graphics in addition to or instead of monospace text in each
> tab/window, and also provides network transparent rendering.

All of this development background is most likely why Øyvind describes
himself as:

> I am a digital media toolsmith, creating tools and infrastructure to
> aid creation of my own and others artistic and visual digital media
> experiments.

GIMP wouldn't be here without sharp minds like his. Can you imagine, 17
years or more of dedication to free software and researching images? So
yes, [GIMP project fully endorses Øyvind's Patreon campaign and we
encourage everyone to donate!](https://www.patreon.com/pippin)

<p class='buttons'>
  <span class='donate-button'>
    <a href="https://www.patreon.com/pippin">
      Fund Øyvind Kolås<br/>
      GEGL development
    </a>
  </span>
</p>

*Note*: Øyvind is also present on [Liberapay](https://liberapay.com/pippin/).

# ZeMarmot: GIMP maintainer and Libre Art

[ZeMarmot](https://film.zemarmot.net/) is a Libre Art project born as
an idea in 2014, launched in 2015 with production starting in 2016. In
particular, it is an Open Animation short film ([Creative Commons
BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) license
promoting sharing and reuse) led by the film director, Aryeom, and GIMP
co-maintainer, Jehan.

<figure>
<img src="{attach}ZeMarmot_header.png" alt="ZeMarmot project logo"/>
<figcaption>
<em>ZeMarmot project logo</em>
</figcaption>
</figure>

Now you might wonder why an animation film would help GIMP, yet this is
the whole reason why Jehan has been heavily developing the software, to
the point that he became the biggest contributor since 2020 and is now
co-maintainer. Working on other fun projects with Aryeom is even the
whole reason why Jehan started contributing to GIMP at all, ever since
2012.

*ZeMarmot* is a traditional (yet digital) 2D animation film, meaning a
lot of illustration work (each frame hand-drawn), hence requiring heavy
usage of brush tools, selections, filters, transformation tools,
scripting for automatization and all sort of raster edit needs.

This is why the ensuing development is not only about illustration and
animation, as some might believe. They are not aiming at changing the
scope of the software. GIMP is a generic graphics manipulation tool and
what we have realized over the years is that most features are so
cross-disciplined that it makes total sense to have a generic tool.
Moreover Aryeom and Jehan also work from time to time on printing jobs,
logos, photos, pin buttons, board games… they do all kinds of things
with GIMP!

A lot of care is taken to fix bugs, improve stability, efficiency,
packaging and add generic tools and features useful to photographers,
designers, illustrators, scientists, animators…
Trying to list all the improvements brought by their work would be a
challenge because there are too many.

Nowadays, *ZeMarmot* is as much an **artisanal animation film project**
(Aryeom single-handedly working on nearly every production role) as it
is a **software development project** (both with Jehan on development
and Aryeom on testing and designing features). Aryeom and Jehan believe
that taking ownership of your working tool matters and this why their
collaboration works so well, by bringing together artist and developer.

The nice final touch is that their big dream is to be able one day to
hire more Free Software developers and artists. This project is under
[LILA](https://libreart.info/)'s (*Libre comme L'Art* — *Free as
Art* in French) umbrella, a France-registered non-profit dedicated to
Libre Art and creative media Free Software.
Imagine in a not-so distant future if we had a non-profit studio
producing artworks in Libre Art licenses, for everyone to view, share
and even modify (source files are provided, e.g. [the
ones](https://gitlab.gnome.org/Jehan/what-is-peertube/) for this video
they made for [Framasoft about
Peertube](https://framatube.org/videos/watch/9c9de5e8-0a1e-484a-b099-e80766180a6d))
with many artists (hence producing films at much better pace than they
are able right now) and developers fixing production issues.
We can truly consider LILA as GIMP's sister non-profit Open Animation
studio! And yes, an Open Animation studio producing Libre Art will help
get better tools for photography editing or design as well.

This is why [GIMP project also fully endorses LILA and *ZeMarmot*
project and encourages the whole community to give to this project in
order to improve GIMP](https://www.patreon.com/zemarmot). You can
consider it a fostered project of GIMP.

<p class='buttons'>
  <span class='donate-button'>
    <a href="https://www.patreon.com/zemarmot">
      Fund ZeMarmot (Aryeom and Jehan)<br/>
      GIMP development
    </a>
  </span>
</p>

*Note*: *ZeMarmot* is also present on
[Liberapay](https://liberapay.com/ZeMarmot/) and
[Tipeee](https://en.tipeee.com/zemarmot) platforms.

# What about donations to the GNOME Foundation?

For years, we have received [donations](https://www.gimp.org/donating/)
through the umbrella of the GNOME Foundation. It is still possible to do
so of course. Yet these donations are so far only usable for community
needs. In particular, it helps us when we need hardware (not so often),
and to gather contributors for
[conferences](https://www.gimp.org/news/2018/01/09/libre-graphics-meeting-scale-2018/)
(regularly we also help funding the conferences itself, because we
believe that a sane free software graphics ecosystem should not be only
about GIMP so we love to support other graphics software too) and
[contributor
meetings](https://www.gimp.org/news/2017/01/18/wilberweek-2017-announced/).

Note that it is also possible to fund several contributors through [GIMP
Liberapay account](https://liberapay.com/GIMP/) as an interesting
alternative.

What these donations through GNOME still cannot do is **funding paid
development, so if that's what you want, please fund the developers
directly as explained above.** GIMP project obviously welcomes the 2
types of donation, for community needs through GNOME and for paid
development through the 2 crowdfundings listed.

**Thanks to everyone for supporting GIMP in whatever way suits you best!
🤗**
