Title: Draft Files 
Date: 2015-11-18T16:32:24-05:00
Category: News
Authors: Pat David
Status: draft
template: draft-index
slug: index

This file is only a placeholder, so we can have an index of current draft articles being written.
If you're on testing.gimp.org or www.gimp.org you should see any drafts here.

Files with the metadata attribute *Status* set to *draft* (`Status: draft`) should show here.
