Title: GIMP 2.10.38 Released
Date: 2024-05-05
Category: News
Authors: GIMP Team
Slug: gimp-2-10-38-released
Summary: Release news for version GIMP 2.10.38
Image: /downloads/downloadsplash-aryeom.jpg

This (possibly last) GIMP 2 stable release brings much-requested backports
from GTK3, including improved support for tablets on Windows. A number of
bug fixes and minor improvements are also included in this release.

[TOC]

*This news lists the most notable and visible changes. In particular, we
do not list every single bug fix or smaller improvement.
To get a more complete list of changes, you should refer to the
[NEWS](https://gitlab.gnome.org/GNOME/gimp/-/blob/7d4a7604a8f038eca45045dfa246b363874c44b2/NEWS#L11)
file or look at the [commit
history](https://gitlab.gnome.org/GNOME/gimp/-/commits/gimp-2-10).*

## New features and improvements
### Improved support for tablets on Windows

Before this release, GIMP only supported connecting tablets on Windows through WinTab drivers
rather than the newer Windows Ink drivers. Because of this, we received a number of reports
about tablets having issues with unresponsive buttons, incorrect pressure sensitivity,
lagging brush movement, and mid-stroke position changes.  

These problems were due to a limitation of GTK2, as 
[support for Windows Ink](https://gitlab.gnome.org/GNOME/gtk/-/merge_requests/1563)
was implemented in GTK3 by long-time contributor Luca Bacci. For this release, Luca was
gracious enough to backport this support to GTK2. You can now switch between WinTab and
Windows Ink drivers (if supported by your computer) in the Preferences dialog under the
Input Device settings.

<figure>
<img src="{attach}gimp-2.10.38-windows-ink-api.png"
     alt="Windows Pointer Input API option in GIMP 2.10.38"/>
<figcaption>
<em>Windows Pointer Input API can now be changed - GIMP 2.10.38</em>
</figcaption>
</figure>

### Backports of other GTK3 features
Luca also contributed a [number of other features](https://gitlab.gnome.org/GNOME/gimp/-/issues/7498)
from GTK3 to GTK2. Some of the backported improvements include updating the size of the Print Dialog so buttons
are not cut off, fixing issues with pop-up dialogs appearing behind the previous ones, and several fixes to
keyboard input.

These improvements are primarily for Windows and are already included in the 2.99 development release.
However, we are very happy that these quality of life improvements are now available in this stable release
of GIMP 2.10!

## Bugfixes
### Recent crashes

Two commonly reported crashes have now been corrected.
A change in GLib 2.80 exposed a bug in our closing process and caused a
crash on exit. Luca Bacci once again devised a fix for both 2.10.38 and
the upcoming 3.0 release candidate. Another crash that some users 
encountered when making very small selections was also fixed.

### Other fixes

A number of other small bugs were fixed in this release. Among them:

- Indexed PNGs with transparency are now exported with the correct colors
- Anders Jonsson fixed the input ranges for several filters such as Waves and Distort
- The titlebar customization field now supports UTF-8 characters 
- Existing image comments no longer "leak" into newly created images

## Release stats

Since GIMP 2.10.36:

* 16 reports were closed as FIXED in 2.10.38
* 9 merge requests were merged
* 81 commits were pushed
* 1 new translation was added: Kabyle
* 16 translations were updated: Belarusian, Brazilian Portuguese, British English,
Danish, Georgian, German, Greek, Hungarian, Icelandic, Italian, Norwegian Nynorsk,
Slovenian, Spanish, Swedish, Turkish, Spanish

25 people contributed changes or fixes to GIMP 2.10.36 codebase (order is
determined by number of commits):

* 7 developers: Alx Sa, Jehan, Luca Bacci, Jacob Boerema, Lukas Oberhuber,
lillolollo, Øyvind Kolås
* 19 translators: Kolbjørn Stuestøl, Sabri Ünal, Bruce Cowan, Yuri Chornoivan,
Vasil Pupkin, Anders Jonsson, Rodrigo Lledó, Jürgen Benvenuti, Sveinn í Felli,
Andi Chandler, Juliano de Souza Camargo, Ekaterine Papava, Balázs Úr, Martin,
Philipp Kiemle, Alan Mortensen, Dimitris Spingos, Marco Ciampa, Yacine Bouklif

Contributions on other repositories in the GIMPverse (order is determined by
number of commits):

* The `gimp-2-10` branch of `gimp-macos-build` (macOS build scripts) had 30
  commits since the 2.10.36 release by 2 contributors: Lukas Oberhuber, Bruno Lopes.
* The flatpak release is made of 11 commits by 3 contributors: Jehan,
  Hubert Figuière and Bruno Lopes.
* Our main website (what you are reading right now) had 42 commits since
  2.99.18 release by 4 contributors: Jehan, Alx Sa, Andre Klapper and
  Lukas Oberhuber.
* Our [developer website](https://developer.gimp.org/) had 34 commits since
  2.99.18 release by 6 contributors: Bruno Lopes, Jehan, Alx Sa,
  bootchk, Alpesh Jamgade and Robin Swift.
* Our [2.10 documentation](https://docs.gimp.org/) had 35 commits since
  2.10.36 release by 8 contributors: Alan Mortensen, Anders Jonsson,
  Rodrigo Lledó, Jacob Boerema, Kolbjørn Stuestøl, Marco Ciampa, Andi
  Chandler and Víttor Paulo Vieira da Costa.

Let's not forget to thank all the people who help us triaging in Gitlab, report
bugs and discuss possible improvements with us.
Our community is deeply thankful as well to the internet warriors who manage our
various [discussion channels](https://www.gimp.org/discuss.html) or social
network accounts such as Ville Pätsi, Liam Quin, Michael Schumacher and Sevenix!

*Note: considering the number of parts in GIMP and around, and how we
get statistics through `git` scripting, errors may slip inside these
stats. Feel free to tell us if we missed or mis-categorized some
contributors or contributions.*

## Team news and release process

Idriss, 2023 GSoC contributor, has been recently granted "developer" access on
the main source repository, for the awesome continued job since then.

Ville Pätsi, very long term contributor (more than 20 years!), on various
topics (design, theming and more) got the "reporter" access to Gitlab to help
with triaging and organizing directly in the tracker.

## Around GIMP
### Mirror News

Since our [last
news](https://www.gimp.org/news/2024/02/21/gimp-2-99-18-released/), 3 new
[mirrors](https://www.gimp.org/donating/sponsors.html#official-mirrors) have been
contributed to GIMP by:

* *Clarkson Open Source Institute*, USA
* *FCIX*, Switzerland
* *Tomás Leite de Castro*, Portugal

This brings us to a total of 49 mirrors all over the world.

Mirrors are important as they help the project by sharing the load for dozens of
thousands of daily downloads. Moreover by having mirrors spread across the
globe, we ensure that everyone can have fast download access to GIMP.

### Infrastructure and Hardware Sponsors

We enhanced the sponsor page with 2 sections:

* "[Infrastructure Sponsors](https://www.gimp.org/donating/sponsors.html#infrastructure-sponsors)"
  lists the sponsors who help GIMP with infrastructure:

    * *CircleCI* and *MacStadium* make our macOS continuous integration platform
      possible.
    * *Arm Ltd.* sponsors and administers several `Aarch64` runners on
      Windows for our ARM 64-bit build for Windows; and *Microsoft* had
      given away the one-time fee for their Microsoft Store.

* "[Hardware Sponsors](https://www.gimp.org/donating/sponsors.html#hardware-sponsors)"
  lists sponsors which donated some hardware to contributors to help
  with development:

    * *Arm Ltd.* recently donated a Windows Dev Kit 2023 to support our
      recent [Aarch64/Windows support](https://www.gimp.org/news/2023/08/13/experimental-windows-arm-installer/).
    * *Purism* donated a Librem Mini in 2021.

## Downloading GIMP 2.10.38

You will find all our official builds on [GIMP official website
(gimp.org)](https://www.gimp.org/downloads/):

* Linux flatpaks for x86 and ARM (64-bit)
* Universal Windows installer for x86 (32 and 64-bit) and for ARM (64-bit)
* macOS DMG packages for Intel hardware
* macOS DMG packages for Apple Silicon hardware

Other packages made by third-parties are obviously expected to follow
(Linux or \*BSD distributions' packages, etc.).

## What's next

Clearly one of the smallest releases ever in the 2.10 series, and it might be our
last. We'll see, though we also know some people get stuck longer than others on
older series (especially when using <abbr="Long Term Support">LTS</abbr>
distributions of Free Software operating systems), so we might do (if we feel
like it's needed) a 2.10.40 release with bug fixes only just before or just after
GIMP 3.0.0 release, as a wrap up.

In any case, we are now stopping backporting features in the 2.10 series. These
graphics tablet support improvements for Windows are huge enough that they had
to get in; yet from now on, we want to focus solely on releasing GIMP 3.0.0.

Now you might wonder when that is? Very soon! We are on the last sprint towards
the release candidate. This includes a lot of bug fixes, but also still some API
changes going on. We will keep you updated!

Don't forget you can [donate and personally fund GIMP
developers](https://www.gimp.org/donating/), as a way to give back and
**accelerate the development of GIMP**.
Community commitment helps the project to grow stronger! 💪🥳
