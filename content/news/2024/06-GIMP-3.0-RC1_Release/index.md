Title: GIMP 3.0 RC1 Released
Date: 2024-11-06
Category: News
Authors: GIMP Team
Slug: gimp-3-0-RC1-released
Summary: Release news for version GIMP 3.0 RC1
Image: gimp-3.0.RC1-splash.jpg

We are very excited to share the first release candidate for the long-awaited GIMP 3.0! We've been hard at work since [our last development update](https://www.gimp.org/news/2024/10/05/development-update/) to get this ready, and we're looking forward to everyone finally being able to see the results.

<figure>
<img src="{attach}gimp-3.0.RC1-splash.jpg"
     alt="GIMP 3.0 RC1: splash screen"/>
<figcaption>
<em>New release candidate splash screen, by Sevenix (CC by-sa 4.0) - GIMP 3.0 RC1</em>
</figcaption>
</figure>

So, what exactly is a "release candidate" (RC)? A release candidate is something that *might* be ready to be GIMP 3.0, but we want the larger community to test it first and [report any problems they find](https://gitlab.gnome.org/GNOME/gimp/-/issues). If user feedback reveals only small and easy to fix bugs, we will solve those problems and issue the result as GIMP 3.0. However, we hope and expect a much larger audience to try out 3.0 RC1 - including many people who have only been using 2.10 up until now. If larger bugs and regressions are uncovered that require more substantial code changes, we may need to publish a second release candidate for further testing.

[TOC]

# New Graphics

## Wilber Icons

The current Wilber logo was created by **Jakub Steiner** for GIMP 2.6 in 2008! While it is still a fantastic logo, design trends have changed a bit in the last sixteen years and Wilber's more detailed appearance sticks out on modern desktops.

Therefore in [collaboration with other contributors](https://developer.gimp.org/conferences/wilberweek/2023-amsterdam/#logo-update), **Aryeom** developed our new logo for GIMP 3.0!

<figure>
<img src="{attach}gimp-logo.png"
     alt="New Wilber Icon"/>
<figcaption>
<em>New Wilber Icon, by <a href="https://film.zemarmot.net">Aryeom</a> (CC by-sa 4.0)</em>
</figcaption>
</figure>

If you're interested in learning more about the design choices, usage, and design variants, please check out our [logo guide](https://gitlab.gnome.org/GNOME/gimp-data/-/blob/main/images/logo/README.md). We also documented [the history of the Wilber logo](https://gitlab.gnome.org/GNOME/gimp-data/-/blob/main/images/logo-log.md).

## Splash Screen

Our wonderful new splash screen (shown at the top of this news post) was created by longtime contributor and artist **Sevenix**! You can see more of their work on their [personal art page](https://www.deviantart.com/sevenics).

Going forward, we plan to change splash screens much more frequently to show off all the many kinds of art made with GIMP (photography, illustration, design…).
Related to this, we have created an updated [splash screen archive](https://gitlab.gnome.org/GNOME/gimp-data/-/blob/main/images/splash-log.md) to highlight the work of current and previous splash screen artists.

## Legacy Icon Theme Improvements

One of the major improvements from the GTK3 port is that the vector UI icons now scale more cleanly based on your preference settings. Our Legacy icon theme was mainly raster PNGs however, so it could not take advantage of the GTK3 scaling system. Contributor **Denis Rangelov** took on the extensive challenge of recreating the Legacy tool icons as SVGs. Now both of GIMP's icon themes look great on HiDPI screens!

<figure>
<img src="{attach}gimp-legacy-icon-theme.png"
     alt="Vectorized Legacy Icon theme"/>
<figcaption>
<em>Scaled Legacy Icon Theme Tool Icons by Denis Rangelov (CC by-sa 4.0)</em>
</figcaption>
</figure>

The work is not finished, as many icons are still non-scalable and some icons are still missing. Denis has expressed interest in continuing to improve the Legacy icon theme, so we hope to rename it as *Classic* when this project is achieved, to show it is now well-maintained.

# Color Space Invasion

One of the [key changes in 2.99.18](https://www.gimp.org/news/2024/02/21/gimp-2-99-18-released/#color-space-invasion) was massive improvements to color management in GIMP. As this work was not fully finished in 2.99.18, it was a major blocker of the 3.0 RC1 release.

Since that release, we have found and fixed a number of bugs and missed areas that needed to be color space aware. We have also reviewed reports by color expert **Elle Stone** to make sure that the color values shown by GIMP are as accurate as possible. At the same time, it's very important to ensure that XCF project files created in GIMP 2.10 and before will render the same when opened in 3.0. For instance, one of the [first Google logos was created in GIMP](https://en.wikipedia.org/wiki/Google_logo#History) - and if you open the [original XCF project file](https://www.google.com/logos/google5.xcf) in GIMP 3.0 RC1, it still appears the same as it did when it was created in 1998!
Therefore, we have thoroughly reviewed the various layer modes to ensure that commitment to compatibility is retained for this release.

Color space invasion is a long-running project, which will continue after GIMP 3.0 is released.

# Public API Finalization

Another task that had to be finished before the 3.0 release was finalizing the public <abbr title="Application Programming Interface">API</abbr>. Since [our last news post](https://www.gimp.org/news/2024/10/05/development-update/#api), we finished the remaining major changes - replacing all instances of our custom `GimpRGB` color structures with the better color-managed `GeglColor`, and improving our array format so the number of elements does not have to be specified separately. This work was a long process by **Jehan** and **Lloyd Konneker**, with a great deal of bugtesting and feedback from **Anders Jonsson**.

In addition, a number of functions have been added, renamed, or removed from the public API compared to 2.10. For instance, an older patch by **Massimo Valentini** adds `gimp-context-get-emulate-brush-dynamics` and `gimp-context-set-emulate-brush-dynamics`, which allows script and plug-in developers to use the Emulate Paint Brush Dynamics setting when painting. On the other hand, the various `gauss` functions were all consolidated into a single function, `plug-in-gauss`. While this change will require some updates in existing scripts, developers now have more direct control over the Gaussian Blur effect rather than relying on hidden preset values.

Since the API is now stable, plug-in and script developers can begin porting their 2.10 scripts based on this release. You can find initial API documentation on [our developer site](https://developer.gimp.org/resource/). We intend to add more tutorials and porting guides here during the release candidate phase. We also encourage you to check out the [Script-fu](https://gitlab.gnome.org/GNOME/gimp/-/tree/master/plug-ins/script-fu/scripts) and [Python](https://gitlab.gnome.org/GNOME/gimp/-/tree/master/plug-ins/python) plug-ins in our repository to see working examples of the new API.

# Non-Destructive Editing Updates

[Since our last update](https://www.gimp.org/news/2024/10/05/development-update/#non-destructive-editing-updates), we have continued to make improvements and bug fixes to our non-destructive filter code. Many of these issues were reported by **Sam Lester** during the developing and testing of his third-party GEGL filters.

While non-destructive filters have been a very popular addition to GIMP 3.0, some early adopters have requested that we provide a way to return to the original destructive workflow. Therefore, we have added an optional "Merge Filters" checkbox at the bottom of NDE filters. If enabled, the filter will be immediately merged down after it is committed. Note that filters can not be applied destructively on layer groups – in those cases, the option to merge filters is not available.

<figure>
<img src="{attach}gimp-merge-filter.png"
     alt="Example of Merge Filter checkbox"/>
<figcaption>
<em>Example of Filter with "Merge Filter" checkbox - GIMP 3.0 RC1</em>
</figcaption>
</figure>

On a related note, **Jehan** also implemented storing version of filters in GIMP's XCF project files. This will allow us to update filters in the future without impacting how older project files look when they're opened. Additional work will be needed in GEGL to fully implement this feature, but that can be done after 3.0 without impacting existing project files.

# User Interface

GIMP 3.0 RC1 contains several updates to the user interface. For example, more aspects of the <abbr title="Graphical User Interface">GUI</abbr> are now able to take advantage of the multi-select features implemented by **Jehan** in earlier versions of 2.99.

We also restored the ability to use the mouse scrollwheel to flip through the different dockable dialogue tabs. This feature was built into GTK2 but removed in GTK3. Per [user request](https://gitlab.gnome.org/GNOME/gimp/-/issues/6970), we reimplemented this feature in GIMP itself based on a [similar implementation in geany](https://github.com/geany/geany/pull/3134).

During development, we received a report that the [scrolling credits in our About Dialog](https://gitlab.gnome.org/GNOME/gimp/-/issues/10406) could cause discomfort due to its motion. As a result we've added code to check your operating system's "Reduced Animation" setting and turn off those animations in GIMP per your preference settings.

# Plug-ins

As we have been in a feature freeze since the last release of 2.99, most of the changes to plug-ins have been API updates and bug fixes (some of them for issues that were [quite old](https://gitlab.gnome.org/GNOME/gimp/-/issues/215)). However, a few smaller enhancements have been implemented.

## BMP

The BMP format [now supports 64 bits per pixel images](https://learn.microsoft.com/en-us/windows/win32/gdiplus/-gdiplus-types-of-bitmaps-about?redirectedfrom=MSDN#graphics-file-formats). New contributor **[Rupert Weber](https://rupertwh.github.io/bmplib/)** assisted us with adding support for importing this BMP format correctly. They have also submitted patches with more fixes to our BMP plug-in and testing pipeline.

## TIFF

Since GIMP 2.99.16, we've been able to import TIFFs [with Photoshop format layers](https://www.gimp.org/news/2023/07/09/gimp-2-99-16-released/#psd-and-a-bit-of-tiff-and-jpeg). However, the Alias/Autodesk Sketchbook program created their own standard to save layers which was not compatible. Since this was marked as a bug in our issue tracker, we added support for loading layers from TIFFs saved in Sketchbook format as well.

# GEGL and babl

Both GEGL and babl have seen a number of updates since their last releases in February.

**GEGL 0.4.50** introduces a number of new filters created by **Sam Lester**.

- *Inner Glow*

- *Bevel*

- *GEGL Styles*

<figure>
<iframe title="&quot;GEGL Styles&quot; effect in GIMP 3.0 RC1" width="560" height="315" src="https://peer.tube/videos/embed/031e13ca-29e6-499a-9d7c-126370cce793" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>
<figcaption>
<em>*GEGL Styles* effect - GIMP 3.0 RC1</em>
</figcaption>
</figure>

These can all be accessed via the GEGL Operations tool, or by searching for them with the `/` search action shortcut.

**Øyvind Kolås** made a number of bug fixes and improvements to the stability of GEGL. Several changes were also made related to the color space invasion in GIMP, such as adding convenience methods for getting and setting `GeglColor`s in HSV(A) and HSL(A) color models, implemented by **Alx Sa**. **Jacob Boerema** and his <abbr title="Google Summer of Code">GSoC</abbr> student **Varun Samaga B L** merged a number of improvements to the OpenCL version of filters. While GIMP still does not enable OpenCL by default, their work brings us much closer to being able to so. We will discuss these improvements in a future news post.

**babl 0.1.110** also received some contributions during this cycle. **Jehan** implemented new conversion processes between RGB and HSL color models, which improves the performance of a number of filters compared to GIMP 2.99.18. He also fixed certain parts of the code that behaved differently depending on whether your processor supported SSE2. **Øyvind Kolås** improved the accuracy of several sections of code when converting from floating point to integer values. Additionally, **Lukas Oberhuber** found and fixed a memory leak and **Jacob Boerema** fixed an issue where images with <abbr title="Not a Number">NaN</abbr> could cause a crash.

# Release Stats

Since GIMP 2.99.18, in the main GIMP repository:

* 384 reports were closed as FIXED.
* 442 merge requests were merged.
* 1892 commits were pushed.
* 31 translations were updated: Basque, Belarusian, Brazilian
  Portuguese, British English, Bulgarian, Catalan, Chinese (China),
  Chinese (Taiwan), Danish, Dutch, Galician, Georgian, German, Greek,
  Hungarian, Icelandic, Italian, Korean, Latvian, Norwegian Nynorsk,
  Polish, Portuguese, Russian, Serbian, Serbian (Latin), Slovenian,
  Spanish, Swedish, Turkish, Ukrainian, Vietnamese.

72 people contributed changes or fixes to GIMP 3.0.0 RC1 codebase (order
is determined by number of commits; some people are in several groups):

* 27 developers to core code: Jehan, Alx Sa, Jacob Boerema, bootchk,
  Anders Jonsson, Øyvind Kolås, Cheesequake, cheesequake, Niels De
  Graef, Idriss Fekir, Simon Budig, lillolollo, lloyd konneker, Andre
  Klapper, Andrzej Hunt, Bruno, Joachim Priesner, Nils Philippsen,
  Alfred Wingate, Bruno Lopes, Elle Stone, Kamil Burda, Luca Bacci, Mark
  Sweeney, Massimo Valentini, Oleg Kapitonov, Stanislav Grinkov,
  megakite.
* 15 developers to plug-ins or modules: Alx Sa, Jehan, lloyd konneker,
  bootchk, Jacob Boerema, Anders Jonsson, Nils Philippsen, Andrzej Hunt,
  Andre Klapper, Rupert, Bruno Lopes, Daniel Novomeský, Mark Sweeney,
  Stanislav Grinkov, lillolollo.
* 42 translators: Martin, Yuri Chornoivan, Luming Zh, Rodrigo Lledó,
  Kolbjørn Stuestøl, Ekaterine Papava, Cheng-Chia Tseng, Sabri Ünal,
  Marco Ciampa, Tim Sabsch, Jordi Mas, Alexander Shopov, Anders Jonsson,
  Alan Mortensen, Asier Sarasua Garmendia, Sveinn í Felli, Andi
  Chandler, Balázs Úr, dimspingos, Juliano de Souza Camargo, Ngọc Quân
  Trần, Vasil Pupkin, Alexandre Prokoudine, Bruce Cowan, Jürgen
  Benvenuti, Nathan Follens, Милош Поповић, Balázs Meskó, Christian
  Kirbach, Daniel, Emin Tufan Çetin, Fran Dieguez, Guntupalli Karunakar,
  Hugo Carvalho, Jehan, Philipp Kiemle, Piotr Drąg, Robin Mehdee,
  Rūdolfs Mazurs, Seong-ho Cho, Víttor Paulo Vieira da Costa, ayesha
  akhtar.
* 7 resource creators (icons, themes, cursors, splash screen,
  metadata… though a good part of there were moved to `gimp-data`
  repository): Alx Sa, Jehan, Bruno Lopes, Anders Jonsson, Jacob
  Boerema, bootchk, nb1.
* 10 documentation contributors: Jehan, Bruno, Lloyd Konneker, Alx Sa,
  Bruno Lopes, Anders Jonsson, bootchk, Lukas Oberhuber, Andre Klapper,
  Jacob Boerema.
* 11 build, packaging or CI contributors: Bruno Lopes, Jehan, bootchk,
  Alx Sa, lloyd konneker, Jacob Boerema, Niels De Graef, Alfred Wingate,
  Lukas Oberhuber, Michael Schumacher, Anders Jonsson.

Contributions on other repositories in the GIMPverse (order is determined by
number of commits):

* babl 0.1.110 is made of 22 commits by 7 contributors: Øyvind Kolås,
  Jehan, Bruno Lopes, Anders Jonsson, Biswapriyo Nath, Jacob Boerema,
  Lukas Oberhuber.
* GEGL 0.4.50 is made of 204 commits by 33 contributors: Øyvind Kolås,
  Sam Lester, Martin, Varun Samaga B L, Yuri Chornoivan, Luming Zh,
  Rodrigo Lledó, Jehan, Jordi Mas, Anders Jonsson, Kolbjørn Stuestøl,
  Marco Ciampa, Sabri Ünal, Bruno Lopes, Alan Mortensen, Asier Sarasua
  Garmendia, Ekaterine Papava, Bruce Cowan, Lukas Oberhuber, Tim Sabsch,
  psykose, Alexandre Prokoudine, Alx Sa, Andi Chandler, Andre Klapper,
  ArtSin, Daniel Șerbănescu, Jacob Boerema, Joe Locash, Morgane Glidic,
  Niels De Graef, dimspingos, lillolollo.
* [ctx](https://ctx.graphics/) had 616 commits since 2.99.18 release by 2
  contributor: Øyvind Kolås, Ian Geiser.
* `gimp-data` (new repository holding images, splashes, icons and other
  binary data for the software) had 76 commits by 7 contributors: Jehan,
  Aryeom, Bruno, Alx Sa, Denis Rangelov, Anders Jonsson, Bruno Lopes.
* The `gimp-macos-build` (macOS packaging scripts) release had 41
  commits by 3 contributors: Lukas Oberhuber, Bruno Lopes, Jehan.
* The flatpak release had 38 commits by 4 contributors: Bruno Lopes,
  Jehan, Hubert Figuière, Will Thompson.
* Our main website (what you are reading right now) had 60 commits since
  2.10.38 release by 5 contributors: Jehan, Alx Sa, Andre Klapper,
  Bruno Lopes and Denis Rangelov.
* Our [developer website](https://developer.gimp.org/) had 33 commits
  since 2.10.38 release by 5 contributors: Bruno Lopes, Jehan, Lloyd
  Konneker, Alx Sa, Lukas Oberhuber.
* Our [3.0 documentation](https://testing.docs.gimp.org/) had 928
  commits since 2.99.18 release by 14 contributors: Andre Klapper,
  Kolbjørn Stuestøl, Jacob Boerema, Alan Mortensen, Yuri Chornoivan,
  Jordi Mas, Marco Ciampa, Anders Jonsson, Sabri Ünal, dimspingos, Alx
  Sa, Andi Chandler, Daniel, Nathan Follens.

Let's not forget to thank all the people who help us triaging in Gitlab, report
bugs and discuss possible improvements with us.
Our community is deeply thankful as well to the internet warriors who manage our
various [discussion channels](https://www.gimp.org/discuss.html) or social
network accounts such as Ville Pätsi, Liam Quin, Michael Schumacher and Sevenix!

*Note: considering the number of parts in GIMP and around, and how we
get statistics through `git` scripting, errors may slip inside these
stats. Feel free to tell us if we missed or mis-categorized some
contributors or contributions.*

# Future changes to release process

We are well aware that the path to GIMP 3.0 has been a long one, and GIMP 2.10 users have not had access to all of the great new features we've been working on over the years. Going forward, we are restructuring our development process to decrease time between releases. As briefly mentioned in [our post 3.0 roadmap](https://developer.gimp.org/core/roadmap/#post-gimp-300), we want to focus on smaller, feature-focused releases. This means that we are aiming for GIMP 3.2 to come out within a year after the final release of 3.0, rather than in 2050 as is often joked! Micro releases with bug fixes may happen in-between.

Smaller releases with few "big" features will also allow us to more thoroughly test each change, further improving the stability of each release. During the 3.0 development process, developers like **Jacob Boerema**, **Lloyd Konneker**, **Bruno Lopes**, and **Jehan** have been creating and improving our automated testing processes to further catch and identify bugs early. We will talk more about these improvements in future news posts.

# Around GIMP

## Download Mirrors

Since [our last news](https://www.gimp.org/news/2024/05/05/gimp-2-10-38-released/#mirror-news), 8 new [mirrors](https://www.gimp.org/donating/sponsors.html#official-mirrors) have been contributed to GIMP by:

- *Sahil Dhiman*, India
- *FCIX*, in the Dominican Republic, Australia and 2 in the USA.
- *Taiwan Digital Streaming Co.*, Taiwan
- *OSSPlanet*, Taiwan
- *Shrirang Kahale*, India

This brings us to a total of 56 mirrors from all over the world!

<figure>
<img src="{attach}gimp-mirrorbit-mirrors.jpeg"
     alt="World Map of GIMP Mirror locations"/>
<figcaption>
<em>Map of GIMP Mirrors worldwide, generated from MirrorBits</em>
</figcaption>
</figure>

Mirrors are important as they help the project by sharing the load for dozens of thousands of daily downloads. Moreover by having mirrors spread across the globe, we ensure that everyone can have fast download access to GIMP.

## Platform Changes
**Bruno Lopes** has truly taken the lead to improve our build and packaging process on multiple platforms.

Over the summer, he created an experimental AppImage build ([as detailed in a prior news post](https://www.gimp.org/news/2024/05/28/experiments-appimage/)). If you are interested in improving it further and hopefully making it available as a standard download, please [get in touch](https://www.gimp.org/discuss.html)! Bruno has also created [flatpak build scripts](https://gitlab.gnome.org/GNOME/gimp/-/tree/master/build/linux/flatpak?ref_type=heads) to make the process of creating your own GIMP flatpak much easier.

A lot of work was done to improve our presence on the Microsoft Store for 3.0. Our GIMP 2.10 app was not fully integrated into the store platform due to certain limitations - it is really just a wrapper for our existing GIMP installer. Therefore it did not automatically update for users and it was not possible to automate installations with tools like Microsoft Intune. Thanks to a lot of effort on Bruno's part, we will have a new GIMP app in the Microsoft Store which resolves these issues ([and many others](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/1154)) for the final GIMP 3.0 release. From now, we also have a separate GIMP (Preview) which allows you to install development versions in a similar manner to the Beta flatpak on Linux. You can try it out at [this Microsoft Store link](https://www.microsoft.com/store/apps/9NZVDVP54JMR).

(For technical and maintenance reasons [described here](https://gitlab.gnome.org/GNOME/gimp/-/commit/184e2704a42f5bc75ef5d3c2d23da4cb5e9ba7c0), 32-bit binaries will not be available in the new MSIX packages of GIMP, which unfortunately removes support for the legacy TWAIN plug-in in x64 and arm64 packages used for quick scanning. If you depend on these, the [.exe installer](https://www.gimp.org/downloads/devel/) still supports 32-bit processors. However, the support for this architecture is planned to be [dropped in the future](https://gitlab.gnome.org/GNOME/gimp/-/issues/10922))

Additionally, the standard Windows installer has been updated to a more modern design. It also lets you install individual language packages and lets you start up GIMP immediately after the installer is finished. For the more technically inclined, the [Windows build scripts](https://gitlab.gnome.org/GNOME/gimp/-/tree/master/build/windows) have also been ported to use PowerShell, and the cross build scripts can now run locally.

Due to changes and updates in our software building infrastructure, we've had to raise the minimum OS requirement for MacOS to Big Sur (MacOS 11).

## GNOME Foundation fiscal host agreement

Earlier this year, the [GNOME Foundation](https://foundation.gnome.org/) announced a [fiscal sponsorship agreement](https://foundation.gnome.org/2024/07/12/gnome-foundation-announces-transition-of-executive-director/) with GIMP. This is all thanks to a lot of hard work by **Jehan** over many, many months. Our goals with this agreement are to support stable funding for developers interested in working on GIMP for a longer term through grants, and to provide easier ways for people to contribute to GIMP's development. This is still a work-in-progress, so we will make a more detailed announcement once everything has stabilized.

## Translations

Thanks to volunteer translators, we now have a Bengali language translation of GIMP! If you are interested in translating GIMP into your own language or assisting with an existing translation, you can [find out how here](https://l10n.gnome.org/releases/gnome-gimp/).


# Downloading GIMP 3.0 RC1

You will find all our official builds on [GIMP official website (gimp.org)](https://www.gimp.org/downloads/devel/):

- Linux flatpaks for x86 and ARM (64-bit)
- Universal Windows installer for x86 (32 and 64-bit) and for ARM (64-bit)
- MSIX package (GIMP Preview) for x86 (64-bit only) and ARM (64-bit)
- macOS DMG packages for Intel hardware
- macOS DMG packages for Apple Silicon hardware

Other packages made by third-parties are obviously expected to follow (Linux or \*BSD distributions' packages, etc.).

# What's Next

We are now entering the last stage of this major release: candidates for
the final version! Though one can always hope to get a <abbr
title="Release Candidate">RC</abbr> right the first time, experience
tells us that this RC1 — which is the result of more than 6 years of
work — will likely have problems, bugs, probably nasty crashes. This is
where we need you all! We rely on everyone to find and
[report issues](https://gitlab.gnome.org/GNOME/gimp/-/issues) so that
the actual 3.0.0 release can really be considered *stable*. 🤗

Some small bugs may be considered secondary (though we still welcome
reports for all bugs, even smaller ones!), because perfection barely
exists in software. There are other things in particular we really want
to catch, such as:

* any inconsistency or problem in the API (it will stay stable for the
  whole v3 series, so if there are problems to find, it's now; we want a
  robust plug-in framework);
* bugs when reading or rendering existing XCF made by former stable
  versions of GIMP;
* crashes;
* regressions;
* proper migration of configuration from previous versions.

We are not giving out a date estimate for the actual 3.0.0 release,
firstly because we can't know for sure, secondly because each time we
do, news outlets seem to just skim every warning out of our text and
transform our words into unbreakable promises. Just know that we also
want it to happen as soon as possible, i.e. when we can consider our
software to feel stable enough.

Don't forget you can [donate and personally fund GIMP
developers](https://www.gimp.org/donating/), as a way to give back and
**accelerate the development of GIMP**.
Community commitment helps the project to grow stronger! 💪🥳
