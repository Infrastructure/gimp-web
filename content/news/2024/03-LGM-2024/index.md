Title: GIMP at LGM 2024 (Rennes, France)
Date: 2024-05-06
Category: News
Authors: GIMP Team
Slug: gimp-at-lgm-2024
Summary: GIMP team will be at LGM 2024, in Rennes, France
Image: logo_LGM2024.png

The **Libre Graphics Meeting** (LGM) is the biggest international
gathering of Free Software for graphics creation. Born in
[2006](https://libregraphicsmeeting.org/2006/) as an evolution of our
[*GIMPCon*](https://developer.gimp.org/conferences/), the event went on
every year since then, thanks to the support of various major projects,
such as [Blender](https://www.blender.org/), [Inkscape](https://inkscape.org/),
[Scribus](https://www.scribus.net/)… until 2019, because of a pandemic
which everybody knows about!

After 2 years where the event went online, then 2 years without LGM at
all, it is finally back, this time in France, Rennes, from Thursday, May
9 (French holiday) to Saturday, May 11, 2024!

<figure>
<img src="{attach}logo_LGM2024.png"
     alt="Libre Graphics Meeting 2024 logo"/>
<figcaption>
<em>Logo of Libre Graphics Meeting 2024</em>
</figcaption>
</figure>

As every year, the GIMP team will be present. Three talks are presented
by members of the team, one of them being prolonged through a
standards-making workshop:


* **Friday, May 10 at 2:30PM**: [OpenType and the
  desktop](https://libregraphicsmeeting.org/2024/wk-liamquin-opentypedesktop.html)
  by Liam Quin, one of our long term contributors:

    > Proposing a cross-desktop font service (DBUS-based?) to support user
    > interfaces for people to instantiate variable fonts, to edit colour
    > font palettes, choose alternate glyphs, install/uninstall fonts, and
    > that can return paths, or glyph lists, or font names, or rendered
    > text, to any application.

* **Friday, May 10 from 4PM**: workshop part of [OpenType and the
  desktop](https://libregraphicsmeeting.org/2024/wk-liamquin-opentypedesktop.html)
  talk by Liam Quin: hoping that the presentation will move on to a
  discussion so that Free Software projects can work together to propose
  a standard for font listing, selection, usage and more.
* **Saturday, May 11 at 2PM**: [GIMP 3.0 and
  beyond](https://libregraphicsmeeting.org/2024/gimpteam-gimp.html) by
  the whole team:

    > GIMP team will present the long-awaited new major version, GIMP
    > 3.<br/>
    > On the menu : non-destructive editing, multi-layer selection,
    > color management improvements, brand new plug-in API, port to GTK3
    > (HiPPI, Wayland support, CSS themes, better tablet support, etc.)
    > and more.<br/>
    > We will also expose our plans for the future.

* **Saturday, May 11, at 3PM**: [Early screening with live music (cinema-concert): « ZeMarmot »](https://libregraphicsmeeting.org/2024/Aryeomehan-zemarmot.html)

    Jehan (GIMP maintainer) and Aryeom (artist in residence,
    illustrator, designer…) will present their short animation film,
    [*ZeMarmot*](https://film.zemarmot.net/>), produced by the
    non-profit film production [LILA (*Libre comme l'Art* / *Free as
    Art*)](https://libreart.info/).<br/>
    The movie in its current state (color and background unfinished)
    will be screened with live music by 3 musicians (ORL, Pelin, Adrien)
    from our friend music collective, [AMMD](https://ammd.net/), which
    work with us on this movie and only produces Libre Music.

    The showing will take about 10 minutes, followed by a talk and
    questions with Aryeom (film director), ORL (film score composer, and
    musician) and Jehan (technical, development, organization, backend…).

We will await you!

-------------------------

## Summed-Up Information

* [Event page](https://libregraphicsmeeting.org/2024/index.html)
* Location:
  > *Activdesign*<br/>
  > 4A rue du Bignon<br/>
  > 35000 Rennes<br/>
  > FRANCE
* **Libre Graphics Meeting**: from May 9 to 11, 2024, doors opening at 9AM, then all day long!
* **Main GIMP talk**: Saturday, May 11, 2024 from 2PM to 3PM (by the GIMP team)
* **OpenType talk**: Friday, May 10, 2024 from 2:30PM to 3:30PM (by Liam Quin)
* **OpenType workshop**: Friday, May 10, 2024 from 4PM to 6PM (by Liam Quin)
* **ZeMarmot musical showing** and talk: Saturday, May 11, 2024 from 3PM to 4PM (by Aryeom, film director, ORL, composer, Jehan, developer, and Pelin and Adrien, musicians)
* [Full program](https://libregraphicsmeeting.org/2024/program.html)
* [Online map](https://www.openstreetmap.org/?mlat=48.09567&mlon=-1.63642#map=19/48.09567/-1.63642)
* [More info on how to reach the location](https://libregraphicsmeeting.org/2024/location.html)
* Event is recorded: *yes*

Don't forget you can [donate and personally fund GIMP
developers](https://www.gimp.org/donating/), as a way to give back and
**accelerate the development of GIMP**.
Community commitment helps the project to grow stronger! 💪🥳
