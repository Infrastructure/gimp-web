Title: GIMP 3.0 RC2 Released 🎁
Date: 2024-12-27
Category: News
Authors: GIMP Team
Slug: gimp-3-0-RC2-released
Summary: Release news for version GIMP 3.0 RC2
Image: gimp-3.0.RC2-splash.jpg

After the first round of community feedback, we're happy to share the second release candidate for GIMP 3.0!
People gave us really helpful feedback from the [first release candidate](https://www.gimp.org/news/2024/11/06/gimp-3-0-RC1-released/) and we were able to fix a lot of bugs.

It is our little under-the-tree 🎄 present for you all!

<figure>
<img src="{attach}gimp-3.0.RC2-splash.jpg"
     alt="GIMP 3.0 RC2: splash screen"/>
<figcaption>
<em>New release candidate splash screen, by Sevenix (CC by-sa 4.0) - GIMP 3.0 RC2</em>
</figcaption>
</figure>

[TOC]

# Important Bug Fixes

There have been a number of fixes since RC1. We want to highlight the most impactful
bugs so that users are aware and can provide additional testing. For details on other
bug fixes, please check our
[NEWS](https://gitlab.gnome.org/GNOME/gimp/-/blob/cdac2cd1feb82a06fd00015717eed7bfa727ad26/NEWS#L9)
page in GitLab.

## 2.10 Settings Migration

During community testing, we discovered that user's 2.10 settings were not being migrated to
GIMP 3.0 due to some incorrect assumptions in the import code. Since most of the developers
have been using GIMP 3.0 exclusively for some time, we did not notice this issue. The bug
should now be fixed, so we ask for bug reports if any 2.10 preferences are not being imported
correctly in RC2. Note that if you already used 3.0 RC1, you'll need to delete those configurations
first, as otherwise RC2 won't try to import the 2.10 preferences (make sure you back up your settings
of course!)

## Windows Console

In the 2.99 development releases, the Windows versions automatically launched a console display
in addition to GIMP itself. This is very useful for Windows developers to see debug messages, but
the console was not intended to be shown during stable releases. Since we changed our build process
to use Meson instead of Autotools, we learned we needed to make additional changes to stop the
console from being displayed. This should be fixed now thanks to **Jehan** - if you still see the 
console on Windows, please file a new bug report!

## Missing GUI Font issues on macOS

There has been a long-standing issue where some macOS users only saw "missing Unicode" symbols instead
of menu text in GIMP (both in 2.10 and in 3.0). This was due to a bug in [Pango](https://www.pango.org/),
the library we use for text layouts. This was fixed with the recent **Pango 1.55.0 release**, so we encourage
all third-party macOS packagers to update to this version as they build GIMP for distribution.

<figure>
<img src="{attach}gimp-3.0.0-rc2-broken-font-macos.jpg"
     alt="GIMP 3.0.0 RC2: official macOS package now has Pango with no broken fonts"/>
<figcaption>
<em>If you had this issue of broken fonts on macOS (left), it is now fixed (right) - screenshots by reporters - GIMP 3.0.0 RC2</em>
</figcaption>
</figure>

## darktable Integration

After the 3.0 RC1 release, we received reports from some users that they still could not import and export
images between GIMP and darktable. We worked with the darktable developers to iron out the remaining bugs,
so integration between [darktable 5.0.0](https://www.darktable.org/install/) and GIMP 3.0 RC2 should be working
for everyone now. However, please file a new bug report if you continue to have trouble connecting the two!

# Enhancements

While the main focus of 3.0 RC2's development was bugfixes and polish, some new features were also implemented.

## GEGL Filter API

Many of the older API wrappers for GEGL operations were removed in RC1. While this reduced technical debt,
it also caused issues for many third-party plug-in and script developers who wanted to port their plug-ins
to 3.0. While our initial plan was to implement the new public filter API after the 3.0 release, the feedback
from the community convinced us to add it in for 3.0 RC2.

<figure>
<iframe title="Applying filters through libgimp 3.0.0 API (Script-fu et al.) - GIMP 3.0.0 RC2" width="560" height="315" src="https://peer.tube/videos/embed/73cfd99d-1429-4c54-93d1-f125438a0acd?title=0" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>
<figcaption>
<em>Applying filters through libgimp 3.0.0 API (Script-fu et al.) - GIMP 3.0.0 RC2</em>
</figcaption>
</figure>

**Jehan**'s work allows developers to apply filter effects either immediately or as a non-destructive effect.
You can see examples of how to do this in C, Python, and Script-Fu in the 
[merge request](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/2008), or by looking up `gimp-drawable-filter`
in the Procedure Browser inside GIMP. We've also begun using the filter
API in our Python scripts to automatically create [blurred background
effects](https://gitlab.gnome.org/GNOME/gimp-data/-/blob/cd59b23be0aa686493d6083d3ec678387e7eda78/images/splash2installer.py#L45)
for the Windows installer, and with this same API in C, **Alx Sa** added support
for importing
Photoshop's legacy [Color Overlay layer
style](https://gitlab.gnome.org/GNOME/gimp/-/blob/61abcd7412504deb1c1f50eae12f9e734dd5c95b/plug-ins/file-psd/psd-load.c#L2775).

We ask for feedback and bug reports from plug-in and script authors who utilize the new filter API in their work! We have
more updates planned for it in GIMP 3.0 as well.

## Layer blend spaces and compositing in XCFs

Discussions between color science experts **Elle Stone** and **Øyvind Kolås** revealed another area that needed
improvement as part of our Color Space Invasion project. Specifically, images with color profiles that have non-perceptual 
<abbr title="tone reproduction curve">TRC</abbr>s might not be rendered correctly when set to certain layer modes.

Øyvind has implemented a correction for this problem by adding proper
perceptual space as default to specific layer modes. While we believe
this enhancement should not impact older XCF files, we of course want to
hear from you if there are any backwards compatibility issues with 3.0
RC2!

## Packages
### AppImage

Thanks to the continued efforts of **Bruno Lopes** and with assistance from **Samueru** and the AppImage community, our experimental
AppImage now works on most Linux distros. We want to encourage more testing of it, in hopes that we can offer it as
another Linux release in addition to our Flatpak. You can see instructions to install experimental AppImage packages from our
[development download page](https://www.gimp.org/downloads/devel/#appimage).

### Flatpak

Our nightly flatpak has now a dedicated App-ID `org.gimp.GIMP.Nightly`.
This mostly means that it can be installed side by side with the stable
flatpak while both are visible in your menus (no need to select which
version is to be shown with `flatpak make-current` anymore).

Yet it also means that anyone who had the nightly flatpak until now
won't see any update coming anytime soon. In order to continue using the
nightly flatpak, uninstall the existing one and install the new one with
these commands:

```sh
flatpak uninstall org.gimp.GIMP//master
flatpak install https://nightly.gnome.org/repo/appstream/org.gimp.GIMP.Nightly.flatpakref
```

⚠️  *Reminder: the nightly flatpak is current development code as it
happens in source repository. At times, it may even be very broken or
render your project files invalid. We do not recommend it for
production! Use this version to help us debugging by reporting issues or
if you really like risks to test latest features.*

## BMP Plug-in Improvements

New contributor **[Rupert Weber](https://rupertwh.github.io/bmplib/)** has been busy 
[since the last update](https://www.gimp.org/news/2024/11/06/gimp-3-0-RC1-released/#bmp)
with more updates to our BMP plug-in. A few highlights of their work: 

- BMPs are now imported losslessly in their original precision, rather than being converted
to 8 bit integer precision.
- The plug-in now supports loading BMPs with RLE24 and Huffman compression.
- We now load BMPs in chunks rather than trying to load the entire image at once. Related work
also allows us to load much larger BMPs.
- Rupert has also done a lot of code clean-up and maintenance, to make the plug-in easier to
work on in the future.

## Assorted Updates

- **Jehan** made some quality of life improvements to the Python console. You can now use
<kbd>Ctrl+R</kbd> and <kbd>Ctrl+S</kbd> shortcuts to search through your
command history, and <kbd>Page Up</kbd> and <kbd>Page Down</kbd> now let
you scroll history in addition to the <kbd>Up</kbd> and <kbd>Down</kbd> arrow keys.

<figure>
<iframe title="History search in Python Console - GIMP 3.0.0 RC2" width="560" height="315" src="https://peer.tube/videos/embed/a7e9d659-8152-4678-86e5-6b1d15d3b9dc?title=0" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups allow-forms"></iframe>
<figcaption>
<em>History search in Python Console with <kbd>Ctrl-R</kbd> - GIMP 3.0 RC2</em>
</figcaption>
</figure>

- **Alx Sa** implemented loading CMYK PAM files in the PNM plug-in.

- On Windows, we've also added the ability to open images through
  Windows Shortcuts (<code>.lnk</code> files) directly from the file chooser dialog.
  This is also work by **Alx Sa**.

- More tweaks and improvements have been made to the theme. In particular, the slider styling has
been substantially improved after feedback and work from **Denis Rangelov**. Denis also made new
icons for the Navigation Dockable Dialogue, replacing duplicates with distinct symbols.
**Anders Jonsson** has also been reviewing the theme and removing some workarounds which were required
in GIMP 2.10, but no longer needed with our new 3.0 themes.

- **Idriss Fekir** has made improvements to our XCF font loading code, to improve compatibility
when importing older XCF files.

# Overview of Changes since 2.10

For those who haven't followed GIMP's development as closely, these news posts only cover
incremental changes since the last release. They do not list every change or improvements
made for GIMP 3.0 - that would be a very long article indeed!

While we'll have full release notes for the final 3.0, we thought it'd be helpful
to summarize a few of the major changes made during the 2.99 development process:

- The initial work to port GIMP to GTK3 occurred in [2.99.2](https://www.gimp.org/news/2020/11/06/gimp-2-99-2-released/).
This release also introduced multi-layer selections, along with initial API changes and
color space management improvements.
- More API updates were made in [2.99.4](https://www.gimp.org/news/2020/12/25/gimp-2-99-4-released/), including the ability to auto-generate plug-in UIs
based on user input. Various usability improvements were also made, along with the introduction of the experimental Paint Select tool.
- [2.99.6](https://www.gimp.org/news/2021/05/08/gimp-2-99-6-released/) brought more API updates and internal work.
More user-facing features included the ability to place guides outside the canvas, better touchpad support, and more support for
different PNG color metadata.
- The development pipeline was improved significantly in [2.99.8](https://www.gimp.org/news/2021/10/20/gimp-2-99-8-released/),
allowing for faster build times and automation. Support for new file formats like PSB and JPEGXL were added in this release,
along with Windows Ink tablet support.
- [2.99.10](https://www.gimp.org/news/2022/02/25/gimp-2-99-10-released/) introduced "layer sets", replacing the older concept
of linked layers. Painting dynamics were streamlined in this release, along with the first version of the Welcome dialog.
- Early-binding CMYK support was implemented in [2.99.12](https://www.gimp.org/news/2022/08/27/gimp-2-99-12-released/).
The CSS GUI themes also received a major overhaul in this release, along with more file format supports and major
improvements to Script-Fu.
- [2.99.14](https://www.gimp.org/news/2022/11/18/gimp-2-99-14-released/) saw the introduction of non-destructive outlines
for the text tool. The alignment tool was also revised, theme and icon scaling were improved, and floating selections were 
largely replaced in the workflow.
- The GTK3 port was finally completed in [2.99.16](https://www.gimp.org/news/2023/07/09/gimp-2-99-16-released/). The `/` search
pop-up was updated to show the menu path for all entries, as well as making individual filters searchable.
- Non-destructive filters were first introduced in [2.99.18](https://www.gimp.org/news/2024/02/21/gimp-2-99-18-released/). 
Major color management improvements were also made, and new auto-expanding layer boundary and snapping options were also
implemented.

# GEGL

Similar to GIMP, there's a number of bugfixes for the GEGL 0.4.52 release. **Øyvind Kolås** has fixed some generic "Aux input"
labels to be more meaningful - this will be visible in GIMP's filters as well. He also improved the accuracy of some filter's
color processing. Longtime contributor **Thomas Manni** also fixed crashes when some filters were run on very small layers.

# Release Stats

Since GIMP 3.0 RC1, in the main GIMP repository:

* 73 reports were closed as FIXED.
* 71 merge requests were merged.
* 277 commits were pushed.
* 18 translations were updated: Bulgarian, Catalan, Chinese (China),
  Chinese (Taiwan), Danish, Finnish, Georgian, Icelandic, Italian,
  Latvian, Lithuanian, Norwegian Nynorsk, Persian, Portuguese, Russian,
  Slovenian, Swedish, Ukrainian.

35 people contributed changes or fixes to GIMP 3.0.0 RC2 codebase (order
is determined by number of commits; some people are in several groups):

* 12 developers to core code: Jehan, Alx Sa, Michael Schumacher, Anders
  Jonsson, Lloyd Konneker, Øyvind Kolås, Idriss Fekir, Andre Klapper,
  Jacob Boerema, Michael Natterer, Rupert Weber, Thomas Manni.
* 11 developers to plug-ins or modules: Jehan, Lloyd Konneker, Alx Sa,
  Rupert Weber, Daniel Novomeský, Jacob Boerema, Aki, Bruno, Ryan Sims,
  Simon Munton.
* 19 translators: Alan Mortensen, Cheng-Chia Tseng, Kolbjørn Stuestøl,
  Rūdolfs Mazurs, Jiri Grönroos, Sveinn í Felli, Alexander Shopov,
  Aurimas Černius, Marco Ciampa, Danial Behzadi, Hugo Carvalho, Jordi
  Mas, Anders Jonsson, Ekaterine Papava, Julia Dronova, Luming Zh,
  Martin, Michael Schumacher, Yuri Chornoivan.
* 2 Theme designers: Alx Sa, Anders Jonnson.
* 2 documentation contributors: Jehan, Bruno.
* 5 build, packaging or CI contributors: Bruno, Jehan, lloyd konneker,
  Alx Sa, Jacob Boerema, Rupert Weber.

Contributions on other repositories in the GIMPverse (order is determined by
number of commits):

* GEGL 0.4.52 is made of 31 commits by 16 contributors: Øyvind Kolås,
  Sam L, Thomas Manni, lillolollo, Alan Mortensen, Anders Jonsson,
  Ekaterine Papava, Hugo Carvalho, Jordi Mas, Juliano de Souza Camargo,
  Kolbjørn Stuestøl, Lukas Oberhuber, Luming Zh, Marco Ciampa, Martin,
  Yuri Chornoivan.
* [ctx](https://ctx.graphics/) had 48 commits since RC1 release by 1
  contributor: Øyvind Kolås.
* `gimp-data` had 6 commits by 5 contributors: Anders Jonsson, Jehan,
  Sevenix, Alx Sa and Denis Rangelov.
* `gimp-test-images` (new repository for image support testing) had 2
  commits by 1 contributor: Rupert.
* The `gimp-macos-build` (macOS packaging scripts) release had 5
  commits by 1 contributor: Lukas Oberhuber.
* The flatpak release had 4 commits by 2 contributors: Bruno Lopes,
  Jehan.
* Our main website (what you are reading right now) had 29 commits by 3
  contributors: Jehan, Alx Sa, Andrea Veri.
* Our [developer website](https://developer.gimp.org/) had 16 commits by
  2 contributors: Jehan, Bruno Lopes.
* Our [3.0 documentation](https://testing.docs.gimp.org/) had 157
  commits by 10 contributors: Andre Klapper, Kolbjørn Stuestøl, Jacob
  Boerema, Alan Mortensen, Anders Jonsson, Marco Ciampa, Jordi Mas, Yuri
  Chornoivan, Alx Sa, Jiri Grönroos.

Let's not forget to thank all the people who help us triaging in Gitlab, report
bugs and discuss possible improvements with us.
Our community is deeply thankful as well to the internet warriors who manage our
various [discussion channels](https://www.gimp.org/discuss.html) or social
network accounts such as Ville Pätsi, Liam Quin, Michael Schumacher and Sevenix!

*Note: considering the number of parts in GIMP and around, and how we
get statistics through `git` scripting, errors may slip inside these
stats. Feel free to tell us if we missed or mis-categorized some
contributors or contributions.*

# Around GIMP

## Download Mirrors

GNOME has [moved away from using mirrors](https://discourse.gnome.org/t/gnome-mirroring-system-updates/25031)
during their latest infrastructure update. As our download mirrors are hosted by them, we were asked if
we wanted to move as well. As a community project, we value everyone who contributes a mirror to make GIMP
more accessible in their area. Therefore, we have decided to stay with using mirrors to distribute GIMP.

If you are interested in contributing a mirror for your region, here is the new procedure:

### How to be an official mirror (procedure update)

1. Create a mirror request [on the gimp-web
   tracker](https://gitlab.gnome.org/Infrastructure/gimp-web/-/issues/new)
2. Tell us about why you want to mirror GIMP, which other Free Software
   you already mirror, what is your setup, the server's location…
3. Tell us about you: are you an organization or an individual? Give us
   the specific name and URL to be shown in the mirror [sponsor
   list](/donating/sponsors.html).
4. Once we are done verifying your organization, rsync credentials will
   be exchanged securely, allowing you to sync your mirror with the
   source server
5. There is nothing to do in particular to appear on the
   Sponsors page which will be updated regularly through scripts. Yet it
   may take a few days or even weeks at times. So don't worry if your
   organization name does not appear immediately!

🛈 We programmatically check at random intervals that mirrors are updated
quickly enough and that the data match for obvious security reasons.

### Mirror changes

Also, since the [3.0RC1 news post](https://www.gimp.org/news/2024/11/06/gimp-3-0-RC1-released/),
a new mirror has been contributed:

- *Sahil Dhiman*, Mumbai, India

Mirrors are important as they help the project by sharing the load for dozens of thousands of daily downloads.
Moreover by having mirrors spread across the globe, we ensure that everyone can have fast download access to GIMP.

## Sponsoring GitLab Runner

GIMP's [code repository](https://gitlab.gnome.org/GNOME/gimp) is also hosted on GNOME's GitLab platform. 
**Andrea Veri** asked if we would be able to sponsor a runner on the platform, which allows any project
on the platform to test building their software before, during, and after code changes are made.
After a [vote by GIMP's committee](https://gitlab.gnome.org/Teams/GIMP/Committee/decisions/-/issues/9), we
agreed and are now the sponsors of an x86 <abbr title="Continuous Integration/Continuous Deployment">CI/CD</abbr>
runner!

# Downloading GIMP 3.0 RC2

You will find all our official builds on [GIMP official website (gimp.org)](https://www.gimp.org/downloads/devel/):

- Linux flatpaks for x86 and ARM (64-bit)
- Universal Windows installer for x86 (32 and 64-bit) and for ARM (64-bit)
- MSIX package (GIMP Preview) for x86 and ARM (64-bit)
- macOS DMG packages for Intel hardware
- macOS DMG packages for Apple Silicon hardware

Other packages made by third-parties are obviously expected to follow (Linux or \*BSD distributions' packages, etc).

🛈 *Notes*:

* *The 2 macOS DMG packages will likely be late as we are waiting for
  Apple update's validation by the GNOME Foundation before being able to
  sign our packages.*
* *The MSIX package takes usually a few business days of validation by
  Microsoft*.

# What's Next

Thanks to the huge amount of reports we got for our first release
candidate, we are able to present you this second version which is all
the more robust. As you saw, a few additional surprises 🎁 came together
with bugfixes, in particular the new filter API, which triggered support
of PSD legacy Color Overlay import, improved blend modes and compositing,
and more. We thought that it was worth breaking the feature freeze for
these changes and that this will make all the difference!

With this second release candidate, we are closer than ever to actual
GIMP 3.0.0. As usual, we are looking forward to any new community [issue
reports](https://gitlab.gnome.org/GNOME/gimp/-/issues) allowing us to
finalize the 3.0.0 release! 🤗

Don't forget you can [donate and personally fund GIMP
developers](https://www.gimp.org/donating/), as a way to give back and
**accelerate the development of GIMP**.
Community commitment helps the project to grow stronger! 💪🥳

🎅🎄🎉 Oh and of course, the whole team wishes you all a happy holiday season! 🥳🥂🎆
