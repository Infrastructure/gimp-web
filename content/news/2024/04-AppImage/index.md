Title: Experiments with AppImage
Date: 2024-05-28
Category: News
Authors: Bruno
Slug: experiments-appimage
Summary: Dev report about AppImage bundling
Image: /downloads/downloadsplash-aryeom.jpg

Earlier in April and May, we were working behind the scenes on improving our
<abbr title="Continuous Integration">CI</abbr> and build-related code. In this
context, one thing that came up was: how easy it is to test a merge request on
Linux? For example, on Windows, we have `.zip` files for each commit; on macOS,
we have `.app` (inside `.dmg`). For Linux... well we had none (we have weekly
flatpak builds but they are time consuming for testing purposes). So, after a
brief consideration we decided to go with AppImages.

AppImage is a application package format, basically a bundle, that's great for
the development and testing workflow described above. To be clear,
⚠️ **we're not distributing AppImage as official packages yet** ⚠️
(more about this later in this post). About the experiments...

## Picking the "right" tool

AppImage doesn't have a mandatory <abbr title="Software Development
Kit">SDK</abbr>. The creation process can be done using freely available tools,
such as `linuxdeployqt`, `appimage-builder` and `AppImageKit`. But we decided to
go with `go-appimage`, which is multi purpose and ours is to do a quick test
build.

In our case, the tool is responsible for bundling almost every dependency and
for squashing everything with proper <abbr title="Executable and Linkable
Format">ELF</abbr> data to be executable in one click. But the tool, naturally,
can't guess the particularities of the different software (e.g. use of script
interpreters), so we need to copy and set some things
[manually](https://gitlab.gnome.org/GNOME/gimp/-/raw/master/build/linux/appimage/bundle-gimp-appimage.sh).
By the way, we opened issues in the `go-appimage` repo in the hope of improving
some things, one little example of [FOSS collaboration](https://github.com/probonopd/go-appimage/issues/282).

## Learning from past appimages

Of course, we didn't start from scratch! We learned from other unofficial GIMP
AppImage builds (a list of which can be found [here](https://gitlab.gnome.org/GNOME/gimp/-/raw/master/build/linux/appimage/AppRun)).
Maybe there are others, but we could only find these four.

We also contacted the developers of these unofficial builds for testing and
feedback about a potential official appimage. Huge thanks to them! Also, other
people contributed too (this info can be found in the [merge request](https://gitlab.gnome.org/GNOME/gimp/-/merge_requests/1440)).
Thanks to all people involved! 😄

## Patching Wilber's wisdom into appimage

We couldn't simply take these unofficial appimages code and put it in our repo
because this isn't how software works. Our code, even the packaging code,
preexists the new packaging (in this case, bundling) so the former needs to be
considered in order to the proper adaptations be made into the later.

Considering our past packaging code and experiences, we defined some principles
to be checked before approving a new package format. In short, the format needs to:

1. _Have its scripts inside GIMP repo and using GIMP/GNOME runners for better transparency_
(macOS is the exception right now for historical reasons, which should ideally be fixed in the future);
2. _Have its scripts building/packaging over official GIMP git source/binaries for better security;_
3. _Have its scripts simplified and human-readable as much as possible for better maintenance._

The last point assumes that some person is maintaining the package, and this is
the main reason our appimage (bundle) is not ready for distribution yet. No person
volunteered to tackle this responsibility by following these principles. So, the
best that we could do, staying compliant with the principles, was a testing bundle.

## Actual use and future

Our appimage can be used, and it's indeed being used [right now](https://gitlab.gnome.org/search?group_id=8&project_id=1848&repository_ref=master&scope=notes&search=%22CI+appimage%22)
to triage issues and test merge requests on the Debian version [supported](https://testing.developer.gimp.org/core/setup/build/linux/#supported-distributions)
by the respective branch (on master branch it is Debian 12 currently), and that
has been very handy. Let us explain:

Suppose that you don't have a very powerful machine, which is very common.
Normally, you will only build GIMP natively and contribute to this specific
platform, since building inside a VM is quite clumbersome. But thanks to our
testing AppImage:

- a Windows user can just log into the VM, download the Debian artifact from
the MR and test it. We have contributors that use Windows VM (and they can
download the cross .zip artifact), now the inverse is possible;
- And this is useful for issues too: triaging them recommends the latest master
so constant local rebuilding. Fortunately, this isn't needed since our CI auto
generates an `.appimage` for every new commit.

Of course, this is a limited use case and makes our appimage unsuitable even
for being linked in the dev version download page. Not every contributor uses
Debian (12) nor does every Windows or Mac contributor have a Debian (12) VM.
To be fair, if the appimage displays problems that we can't fix, it can even
be dropped at any time.

So, **we welcome contributions to improve compatibility with other distros**
(at least the oldest supported Ubuntu and newest Fedora) in order to raise it to a
package level. If you are interested, [talk to us](/discuss.html). 
